package jp.co.rilarc.instagram_robot.thread;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jp.co.rilarc.instagram_robot.model.Action;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.thread.ActionRotation.ActionInterval;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class ActionRotationTest {
	
	Configuration config;
	RobotSetting robotSetting;

	@Before
	public void setUp() throws Exception {
		config = new Configuration();
		robotSetting = new RobotSetting();
		
		robotSetting.setDisableLike(false);
		robotSetting.setDisableFollow(false);
		
	}

	@Test
	public void testNoSetting_001() {
		robotSetting.setUnfollowsNotFollowingBackAutoUnfollow(false);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(3, listActionInterval.size());
	}
	
	@Test
	public void testNoSetting_002() {
		robotSetting.setDisableLike(true);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(3, listActionInterval.size());
	}
	
	@Test
	public void testSettingOnlyLike() {
		robotSetting.setLikesHashtagAutoLike(true);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(4, listActionInterval.size());
	}
	
	@Test
	public void testSettingLikeAndFollow() {
		robotSetting.setLikesHashtagAutoLike(true);
		robotSetting.setFollowsHashtagAutoFollow(true);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(5, listActionInterval.size());
	}
	
	@Test
	public void testSettingLikeAndFollowAndUnfollow() {
		robotSetting.setLikesHashtagAutoLike(true);
		robotSetting.setFollowsHashtagAutoFollow(true);
		robotSetting.setUnfollowsNotFollowingBackAutoUnfollow(true);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(6, listActionInterval.size());
	}
	
	@Test
	public void testSettingLikeFullAndFollowAndUnfollow() {
		robotSetting.setLikesHashtagAutoLike(true);
		robotSetting.setLikesLocationAutoLike(true);
		robotSetting.setLikesFollowingAutoLikeFeed(true);
		robotSetting.setFollowsHashtagAutoFollow(true);
		robotSetting.setUnfollowsNotFollowingBackAutoUnfollow(true);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(6, listActionInterval.size());
	}
	
	@Test
	public void testResetInterval() {
		robotSetting.setLikesHashtagAutoLike(true);
		robotSetting.setLikesLocationAutoLike(true);
		robotSetting.setLikesFollowingAutoLikeFeed(true);
		robotSetting.setFollowsHashtagAutoFollow(true);
		robotSetting.setUnfollowsNotFollowingBackAutoUnfollow(true);
		
		ActionRotation obj = new ActionRotation(robotSetting, config);
		
		List<ActionInterval> listActionInterval = obj.getListActionInterval();
		System.out.println(listActionInterval);
		assertEquals(6, listActionInterval.size());
		
		obj.resetActionInterval();
		System.out.println(listActionInterval);
		
		obj.resetActionInterval();
		System.out.println(listActionInterval);
		
		obj.removeAction(Action.LIKE);
		obj.resetActionInterval();
		System.out.println(listActionInterval);
		
		obj.resetActionInterval();
		System.out.println(listActionInterval);
		
		obj.resetActionInterval();
		System.out.println(listActionInterval);
		
		obj.removeAction(Action.FOLLOW);
		obj.resetActionInterval();
		System.out.println(listActionInterval);
		obj.removeAction(Action.FOLLOW);
	}

}
