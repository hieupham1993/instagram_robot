package jp.co.rilarc.instagram_robot.helpers;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class DatabaseHelperTest {
	
	IAPIHelper dbHelper;

	@Before
	public void setUp() throws Exception {
		Configuration config = new Configuration();
		dbHelper = new DatabaseHelper(config);
	}

	@Test
	public void testGetDisableSendMessageFlg() {
		int userId = 496;	// 330emi 	
		boolean result = dbHelper.getDisableSendMessageFlg();
		assertFalse(result);
	}
	
	@Test
	public void testLoginToSystem() {
		String email = "nghi306@gmail.com";
		String pass = null;
		
		User user = dbHelper.loginToSystem(email, pass);
		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getInstagramAccount());
		Assert.assertNotNull(user.getInstagramAccount().getInstagramAvatar());
	}

}
