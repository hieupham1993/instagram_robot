package jp.co.rilarc.instagram_robot.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.BadHastag;
import jp.co.rilarc.instagram_robot.model.FollowingUser;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.LikeHashtag;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;

public class UtilsTest {

	final private IAPIHelper dbHelper = new DatabaseHelper(new Configuration());

	@Test
	public void testParseHashtag()
	{
		String str="sdfsdf #first_tag#sơn #san";
		Pattern MY_PATTERN = Pattern.compile("#([^\\s#]+)");
		Matcher mat = MY_PATTERN.matcher(str);
		JSONArray hashtags = new JSONArray();
		while (mat.find()) {
			hashtags.put(mat.group(1));
		}

		System.out.println(hashtags.toString());
	}

	@Test
	public void testMarkBadHashtag()
	{
		String hashtag = "test4";
		BadHastag badHastag = dbHelper.getBadHashtag(hashtag);

		if (badHastag == null) //insert new
			dbHelper.insertBadHashtag(hashtag);

		badHastag = dbHelper.getBadHashtag(hashtag);

//		System.out.println(badHastag.getMarkedTime());

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long currentTimestamp = timestamp.getTime()/1000;

		System.out.println(currentTimestamp);
	}

	@Test
	public void getExpireFollowing()
	{
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Tokyo"));

		int userId = 31;
		int date = 1;

		ArrayList<FollowingUser> listFollowingUsers = dbHelper.getListFollowingUser(date);

		for (FollowingUser followingUser: listFollowingUsers) {
			System.out.println(followingUser);
		}
	}

	@Test
	public void testThread()
	{
		System.out.println("start main");

		for (int i=1; i<=10; i++) {
			TestThread testThread = new TestThread(String.valueOf(i));
			testThread.start();
		}

		System.out.println("stop main");
	}

	class TestThread extends Thread {
		private String mName;
		private int mCount;

		public TestThread(String name) {
			mName = name;
			mCount = 1;
		}

		@Override
		public void run() {
			System.out.println("Thread " + mName + " start!");

			try {
				Thread.sleep(5 * 1000);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}

			while (mCount < 10) {
				System.out.println("Thread " + mName + " count: " + mCount++);


				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println(e);
				}
			}

			System.out.println("Thread " + mName + " stop!");
		}
	}

	@Test
	public void testCheckExpired()
	{
		String sentence = "Check this answer and you can find the keyword with this code";
		String search  = "keyword";

		if ( sentence.toLowerCase().contains(search.toLowerCase())) {
			System.out.println("I found the keyword");
		} else {
			System.out.println("not found");
		}
	}

	@Test
	public void testInstagram() throws InstagramCheckpointRequiredException, InstagramUserInvalidException
	{
		String username = "loposloq";
		String password = "1234567890123456";

		InstagramAccount instagramAccount = new InstagramAccount();

		instagramAccount.setUsername(username);
		instagramAccount.setPassword(password);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(instagramAccount, robotSetting);

		try {
//			instagram.login(true);
			HttpResponse response = instagram.getMediaInfo("1584070432158523124_2127617808");
			String responseAsString = EntityUtils.toString(response.getEntity());
			System.out.println(responseAsString);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testFollow() throws InstagramCheckpointRequiredException, InstagramUserInvalidException
	{
		String username = "loposloq";
		String password = "6TQzJ2C4+78cGHScKBvm9A==";

		InstagramAccount instagramAccount = new InstagramAccount();

		instagramAccount.setUsername(username);
		instagramAccount.setPassword(password);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(instagramAccount, robotSetting);

		try {
//			instagram.login(true);
			HttpResponse response = instagram.follow("4629901568");
			String responseAsString = EntityUtils.toString(response.getEntity());
			System.out.println(responseAsString);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testHashcode() {
		String data = "10 20000 3 1509170933809";
		String key = "iN4$aGr0m";

		String result = Signatures.sha256Base64String(data, key);
		System.out.println(result);
		System.out.println(Base64.encodeBase64String(data.getBytes()));

	}

	@Test
	public void testGetCommentSchedule() {
		Configuration mConfig = new Configuration();
		IAPIHelper dbHelper = new DatabaseHelper(mConfig);

		ScheduleComment schedulePost = dbHelper.getCommentSchedule();
		System.out.println(schedulePost.getId());

	}

//	@Test
//	public void testPhantomJs() {
//		WebDriver d = new PhantomJSDriver();
//		PhantomJSDriver phantom = (PhantomJSDriver)d;
//
//		phantom.get("https://www.instagram.com/accounts/login/");
//
//		WebElement usernameField = phantom.findElement(By.name("username"));
//		WebElement passwordField = phantom.findElement(By.name("password"));
//		WebElement loginButton = phantom.findElement(By.tagName("button"));
//
//		usernameField.sendKeys("sonnt0411");
//		passwordField.sendKeys("759426183");
//		loginButton.click();
//
//		try {
//			Thread.sleep(10000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		File scrFile = phantom.getScreenshotAs(OutputType.FILE);
//		try {
//			FileUtils.copyFile(scrFile, new File("/Users/nguyenthanhson/Desktop/test_screen_shot.png"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//
//		File file = new File("cookies/cookies.txt");
//		try
//		{
//			// Delete old file if exists
//
//			file.delete();
//			file.createNewFile();
//			FileWriter fileWrite = new FileWriter(file);
//			BufferedWriter Bwrite = new BufferedWriter(fileWrite);
//			// loop for getting the cookie information
//			for(Cookie ck : phantom.manage().getCookies())
//			{
//				Bwrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
//				Bwrite.newLine();
//			}
//			Bwrite.flush();
//			Bwrite.close();
//			fileWrite.close();
//		}catch(Exception ex)
//		{
//			ex.printStackTrace();
//		}
////
////
////		// like media
////		phantom.get("https://www.instagram.com/p/Ba_olXtDp5b/");
////		phantom.manage().getCookies();
////
////		try {
////			Thread.sleep(5000);
////		} catch (InterruptedException e) {
////			e.printStackTrace();
////		}
////		scrFile = phantom.getScreenshotAs(OutputType.FILE);
////		try {
////			FileUtils.copyFile(scrFile, new File("/Users/nguyenthanhson/Desktop/before_like.png"));
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
////
////		Set<Cookie> cookies = phantom.manage().getCookies();
////
////		WebElement likeButton = phantom.findElement(By.className("coreSpriteHeartOpen"));
////		likeButton.click();
////		try {
////			Thread.sleep(5000);
////		} catch (InterruptedException e) {
////			e.printStackTrace();
////		}
////		scrFile = phantom.getScreenshotAs(OutputType.FILE);
////		try {
////			FileUtils.copyFile(scrFile, new File("/Users/nguyenthanhson/Desktop/after_like.png"));
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
//	}
//
//	@Test
//	public void phantomJsUsingCookieFile() {
//		WebDriver d = new PhantomJSDriver();
//		PhantomJSDriver phantom = (PhantomJSDriver)d;
//		phantom.get("https://www.instagram.com/");
//
//		try{
//
//			File file = new File("cookies/cookies.txt");
//			FileReader fileReader = new FileReader(file);
//			BufferedReader Buffreader = new BufferedReader(fileReader);
//			String strline;
//			while((strline=Buffreader.readLine())!=null){
//				StringTokenizer token = new StringTokenizer(strline,";");
//				while(token.hasMoreTokens()){
//					String name = token.nextToken();
//					String value = token.nextToken();
//					String domain = token.nextToken();
//					String path = token.nextToken();
//					Date expiry = null;
//					System.out.println(domain);
//
//					String val;
//					if(!(val=token.nextToken()).equals("null"))
//					{
//						expiry = new Date(val);
//					}
//					Boolean isSecure = Boolean.valueOf(token.nextToken());
//					Cookie ck = new Cookie(name,value,domain,path,expiry,isSecure);
//					phantom.manage().addCookie(ck); // This will add the stored cookie to your current session
//				}
//			}
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}
//
//		phantom.get("https://www.instagram.com/accounts/edit/");
//
//		try {
//			Thread.sleep(10000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		File scrFile = phantom.getScreenshotAs(OutputType.FILE);
//		try {
//			FileUtils.copyFile(scrFile, new File("edit_profile.png"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}
	
	@Test
	public void testExtractUrl01() {
		String text = "https://www.theverge.com/2017/5/25/15691746/instagram-direct-links-update-portrait-landscape";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, text);
	}
	
	@Test
	public void testExtractUrl02() {
		String text = "tim kiem http://google.com toi muon an ngon";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, "http://google.com");
	}
	
	@Test
	public void testExtractUrl03() {
		String text = "tim kiem http://google.com";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, "http://google.com");
	}
	
	@Test
	public void testExtractUrl04() {
		String text = "https://barcode.tec-it.com/en/Code128?data=JPAR000150 toi muon an thi cho https://barcode.tec-it.com/en/Code128?data=JPAR000150 toi muon an ngon";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, "https://barcode.tec-it.com/en/Code128?data=JPAR000150");
	}
	
	@Test
	public void testExtractUrl05() {
		String text = "tim kiem https://google.com and https://facebook.com aaaa";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, "https://google.com");
	}
	
	@Test
	public void testExtractUrl06() {
		String text = "no link";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, null);
	}
	
	@Test
	public void testExtractUrl07() {
		String text = "https://www.yahoo.co.jp/";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals(actualUrl, "https://www.yahoo.co.jp/");
	}
	
	@Test
	public void testExtractUrl08() {
		String text = "こんにちは。\nhttp://www.gundam.info/";
		
		String actualUrl = Utils.extractUrl(text);
		Assert.assertEquals("http://www.gundam.info/", actualUrl);
	}

	@Test
	public void checkNewDay() {
		Date date1 = Utils.getCurrentZeroTimeDate();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Date date2 = Utils.getCurrentZeroTimeDate();

		System.out.println("date1: " + date1);
		System.out.println("date2: " + date2);

		if (date1.before(date2)) {
			System.out.println("new day");
		} else {
			System.out.println("same day");
		}
	}

	@Test
	public void json() {
		JSONObject result = new JSONObject("{\"success\":true,\"data\":[{\"id\":51,\"user_id\":68,\"hashtag\":\"\\u6771\\u4eac\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u30e9\\u30f3\\u30c9\",\"like_count\":10},{\"id\":54,\"user_id\":68,\"hashtag\":\"\\u6771\\u4eac\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u30b7\\u30fc\",\"like_count\":0},{\"id\":55,\"user_id\":68,\"hashtag\":\"\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u30e9\\u30f3\\u30c9\",\"like_count\":1},{\"id\":56,\"user_id\":68,\"hashtag\":\"\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u30b7\\u30fc\",\"like_count\":0},{\"id\":57,\"user_id\":68,\"hashtag\":\"\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\",\"like_count\":0},{\"id\":58,\"user_id\":68,\"hashtag\":\"tdl\",\"like_count\":0},{\"id\":59,\"user_id\":68,\"hashtag\":\"tds\",\"like_count\":0},{\"id\":60,\"user_id\":68,\"hashtag\":\"disney\",\"like_count\":0},{\"id\":61,\"user_id\":68,\"hashtag\":\"instadisney\",\"like_count\":0},{\"id\":62,\"user_id\":68,\"hashtag\":\"\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u5199\\u771f\\u90e8\",\"like_count\":0},{\"id\":63,\"user_id\":68,\"hashtag\":\"tdr\",\"like_count\":1},{\"id\":68,\"user_id\":68,\"hashtag\":\"\\u30cd\\u30c3\\u30c8\\u30d3\\u30b8\\u30cd\\u30b9\",\"like_count\":0},{\"id\":81,\"user_id\":68,\"hashtag\":\"\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u30ab\\u30e1\\u30e9\\u968a\",\"like_count\":0},{\"id\":28464,\"user_id\":68,\"hashtag\":\"\\u30d5\\u30a9\\u30c8\\u30b8\\u30a7\\u30cb\\u30c3\\u30af\",\"like_count\":0},{\"id\":43339,\"user_id\":68,\"hashtag\":\"\\u30c7\\u30a3\\u30ba\\u30cb\\u30fc\\u597d\\u304d\\u306a\\u4eba\\u3068\\u7e4b\\u304c\\u308a\\u305f\\u3044\",\"like_count\":13}],\"message\":\"\"}");
		ArrayList<LikeHashtag> likeHashtags = new ArrayList<>();


		JSONArray likeHashtagsArr = result.getJSONArray("data");

		likeHashtagsArr.forEach(item -> {
			JSONObject likeHashtagJO = (JSONObject) item;

			LikeHashtag likeHashtag = new LikeHashtag();

			likeHashtag.setId(likeHashtagJO.getInt("id"));
			likeHashtag.setUserId(likeHashtagJO.getInt("user_id"));
			likeHashtag.setHashtag(likeHashtagJO.getString("hashtag"));

			likeHashtags.add(likeHashtag);
		});

		System.out.print(likeHashtags);
	}
}
