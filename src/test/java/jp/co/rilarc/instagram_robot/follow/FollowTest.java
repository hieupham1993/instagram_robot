package jp.co.rilarc.instagram_robot.follow;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class FollowTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testFollow() throws InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException
	{
		String username = "yousaykawakubo";
		String password = "MerNS82uhF7kFDq4nkzJIA==";

		InstagramAccount instagramAccount = new InstagramAccount();

		instagramAccount.setUsername(username);
		instagramAccount.setPassword(password);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(instagramAccount, robotSetting);

		try {
			instagram.login(true, false);
			HttpResponse response = instagram.follow("5824604939");
			String responseAsString = EntityUtils.toString(response.getEntity());
			System.out.println(responseAsString);
		} catch (IOException | URISyntaxException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFollowUser() throws InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException, ActionBlockException
	{
		int userId = 105;
		String username = "kano_st";
		String password = "f3YSh8/r5fK+OI6FOowaTg==";
		
		Configuration mConfig = new Configuration();
		IAPIHelper dbHelper = new DatabaseHelper(mConfig);

		InstagramAccount instagramAccount = new InstagramAccount();

		instagramAccount.setUsername(username);
		instagramAccount.setPassword(password);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(instagramAccount, robotSetting);
		
		FollowByFollower followByFollower = new FollowByFollower(instagram, null, userId, dbHelper, null, new Statistic(),100);

		try {
			instagram.login(true, false);
			boolean result = followByFollower.followUser("195222115", "");
			System.out.println(result);
		} catch (IOException | URISyntaxException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}
	}

}
