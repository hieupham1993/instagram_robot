package jp.co.rilarc.instagram_robot.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class ScheduledAutoThreadTest {

	@Test
	public void testAdjustSendTimeForEachUser() {
		 List<ScheduleSendMessage> list = new ArrayList<>();
		 
		 System.out.println(new Timestamp(new Date().getTime())); 
		 
		 for(int i = 0; i < 5; i++) {
			 ScheduleSendMessage s = new ScheduleSendMessage();
			 s.setId(i);
			 s.setUserId(1);
			 s.setTimeToAction(new Timestamp(new Date().getTime() - (5-i)*3*60*1000));
			 s.setDelay(300);
			 
			 list.add(s);
		 }
		 
//		 for(int i = 0; i < 5; i++) {
//			 ScheduleSendMessage s = new ScheduleSendMessage();
//			 s.setId(i);
//			 s.setUserId(3);
//			 s.setTimeToAction(new Timestamp(new Date().getTime() - 4*60*1000));
//			 s.setDelay(400);
//			 
//			 list.add(s);
//		 }
		 
		 System.out.println(list);
		 
		 Configuration config = new Configuration();
		 IAPIHelper IAPIHelper = new DatabaseHelper(config);
		 ScheduledAutoThread auto = new ScheduledAutoThread(IAPIHelper, config);
		 
//		 List<ScheduleSendMessage> list2 = 
		 auto.adjustSendTimeForEachUser(list);
		 
		 System.out.println(list);
	}
	
	@Test
	public void testadjustSendTime() {
		 List<ScheduleSendMessage> list = new ArrayList<>();
		 
		 System.out.println(new Timestamp(new Date().getTime())); 
		 
		 for(int i = 0; i < 5; i++) {
			 ScheduleSendMessage s = new ScheduleSendMessage();
			 s.setId(i);
			 s.setUserId(1);
			 s.setTimeToAction(new Timestamp(new Date().getTime() - (5-i)*3*60*1000));
			 s.setDelay(300);
			 
			 list.add(s);
		 }
		 
		 for(int i = 0; i < 5; i++) {
			 ScheduleSendMessage s = new ScheduleSendMessage();
			 s.setId(i);
			 s.setUserId(3);
			 s.setTimeToAction(new Timestamp(new Date().getTime() - (5-i)*4*60*1000));
			 s.setDelay(600);
			 
			 list.add(s);
		 }
		 
		 System.out.println(list);
		 
		 Configuration config = new Configuration();
		 IAPIHelper IAPIHelper = new DatabaseHelper(config);
		 ScheduledAutoThread auto = new ScheduledAutoThread(IAPIHelper, config);
		 
		 List<ScheduleSendMessage> list2 = auto.adjustSendTime(list);
		 
		 System.out.println(list2);
	}

}
