package jp.co.rilarc.instagram_robot.like;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;

public class LikeByHashtagTest {

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Cach de lay media ID
     * https://api.instagram.com/oembed/?url=http://instagram.com/p/BZNzWrbnqZF/
     *
     * @throws InstagramCheckpointRequiredException
     * @throws InstagramInvalidCredentialsException
     */
    @Test
    public void testLikeByHashTag() throws InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException {
        String username = "nghilt";
        String password = "U6hIzF8YsDPpS8iN8Y0mJA==";

        InstagramAccount instagramAccount = new InstagramAccount();

        instagramAccount.setUsername(username);
        instagramAccount.setPassword(password);


        RobotSetting robotSetting = new RobotSetting();
        Instagram instagram = new Instagram(instagramAccount, robotSetting);

        LikeByHashtag like = new LikeByHashtag(instagram, null, null, null, null, new Statistic(), 0);

        try {
            instagram.login(true, false);

            JSONObject result = like.likeMedia("1607166500875970117_1473827525");
            System.out.println(result);
        } catch (IOException | URISyntaxException | InstagramSentryBlockException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLikeByHashTag2() throws InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException {
        String username = "e_shuya1441";
        String password = "KxsbbMHnTl87RLHBTPxJ+A==";

        InstagramAccount instagramAccount = new InstagramAccount();

        instagramAccount.setUsername(username);
        instagramAccount.setPassword(password);

        RobotSetting robotSetting = new RobotSetting();
        Instagram instagram = new Instagram(instagramAccount, robotSetting);

        LikeByHashtag like = new LikeByHashtag(instagram, null, null, null, null, new Statistic(), 0);

        try {
            instagram.login(true, false);
            JSONObject result = like.likeMedia("1606997402827748366_5745608016");
            System.out.println(result);
        } catch (IOException | URISyntaxException | InstagramSentryBlockException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLikeByHashTag3() throws InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException {
        String username = "330emi";
        String password = "igawa03301985";

        InstagramAccount instagramAccount = new InstagramAccount();

        instagramAccount.setUsername(username);
        instagramAccount.setPassword(password);

        RobotSetting robotSetting = new RobotSetting();
        Instagram instagram = new Instagram(instagramAccount, robotSetting);
        Proxy proxy = new Proxy("150.95.147.50", 3128, "wmuser", "VN201501");
        instagram.setProxy(proxy);

        LikeByHashtag like = new LikeByHashtag(instagram, null, null, null, null, new Statistic(), 0);

        try {
            instagram.login(true, false);
            JSONObject result = like.likeMedia("1646441230493510074_2320413375");
            System.out.println(result);
        } catch (IOException | URISyntaxException | InstagramSentryBlockException e) {
            e.printStackTrace();
        }
    }

}
