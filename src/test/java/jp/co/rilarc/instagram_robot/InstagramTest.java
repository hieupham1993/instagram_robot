package jp.co.rilarc.instagram_robot;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Security;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.SchedulePost;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.thread.InstagramAutoThread;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;

public class InstagramTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testLogin() throws IOException, URISyntaxException, InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException, InstagramSentryBlockException {
		InstagramAccount account = new InstagramAccount();
//		account.setUsername("sonnt0411");
//		account.setPassword("icJYAYDjIdoIjoV9i4nTxQ==");
		account.setUsername("stitchlovelove_201");
		account.setPassword("inst456789");

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);

		boolean loggedon = instagram.login(true, false);
		System.out.println(loggedon);
		Assert.assertTrue(loggedon);
	}
	
	@Test
	public void testLogin2() throws IOException, URISyntaxException, InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException, InstagramSentryBlockException {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("flamingodance_12");
		account.setPassword("Czq3gDK7wK/3S0VM8imYsw==");

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);

		boolean loggedon = instagram.login(true, false);
	}
	
	@Test
	public void testLogin3() throws IOException, URISyntaxException, InstagramCheckpointRequiredException, InstagramUserInvalidException, InstagramInvalidCredentialsException, InstagramSentryBlockException {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("sonnt0411");
		account.setPassword("icJYAYDjIdoIjoV9i4nTxQ==");

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);

		boolean loggedon = instagram.login(true, false);
	}

	@Test
	public void testProxy2() throws IOException {
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(
				new AuthScope("23.106.28.58", 29842),
				new UsernamePasswordCredentials("mshjyk", "gK3pF9HG"));
		CloseableHttpClient httpclient = HttpClients.custom()
				.setDefaultCredentialsProvider(credsProvider).build();
		try {
			String endpoint = "https://requestb.in/107rw591";
			HttpHost proxy = new HttpHost("23.106.28.58", 29842);

			RequestConfig config = RequestConfig.custom()
					.setProxy(proxy)
					.build();
			HttpGet httpget = new HttpGet(endpoint);
			httpget.setConfig(config);

			System.out.println("Executing request " + httpget.getRequestLine() + " to " + endpoint + " via " + proxy);

			CloseableHttpResponse response = httpclient.execute(httpget);
			try {
				System.out.println("----------------------------------------");
				System.out.println(response.getStatusLine());
				System.out.println(EntityUtils.toString(response.getEntity()));
			} finally {
				response.close();
			}
		} finally {
			httpclient.close();
		}
	}

	@Test
	public void testUploadImage() throws InstagramInvalidCredentialsException {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("sonnt0411");
		account.setPassword("icJYAYDjIdoIjoV9i4nTxQ==");
		account.setUserId(1);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);

		try {
			boolean loggedon = instagram.login(true, false);

			if (loggedon) {
				JSONObject externalData = new JSONObject();
				externalData.put("caption", "test api from local with location 1");
				externalData.put("location", new JSONObject("{\"name\":\"La Rambla, Catalunya, Barcelona\",\"external_id_source\":\"facebook_places\",\"external_source\":null,\"address\":\"128  Carrer La Rambla, Barcelona\",\"lat\":41.38427,\"lng\":2.17115,\"external_id\":\"661740270691759\",\"facebook_places_id\":null,\"city\":null,\"pk\":null,\"status\":null,\"message\":null,\"fullResponse\":null}"));
				HttpResponse response = instagram.uploadTimelinePhoto("/Users/nguyenthanhson/Downloads/1661122_572236116206234_189422701_n.jpg", externalData);
				String responseAsString = EntityUtils.toString(response.getEntity());
				System.out.println(responseAsString);
			} else {
				System.out.println("login failed");
			}
		} catch (IOException | URISyntaxException | InstagramCheckpointRequiredException | InstagramUserInvalidException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getPostSchedule() {
		Configuration mConfig = new Configuration();
		IAPIHelper dbHelper = new DatabaseHelper(mConfig);

		SchedulePost schedulePost = dbHelper.getPostSchedule();
		System.out.println(schedulePost.getId());
	}

	@Test
	public void testSendComment() throws InstagramInvalidCredentialsException {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("sonnt0411");
		account.setPassword("icJYAYDjIdoIjoV9i4nTxQ==");
		account.setUserId(1);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);

		try {
			boolean loggedon = instagram.login(true, false);

			if (loggedon) {
				HttpResponse response = instagram.comment("1607166500875970117_1473827525", "nice hair");
				String responseAsString = EntityUtils.toString(response.getEntity());
				System.out.println(responseAsString);
			} else {
				System.out.println("login failed");
			}
		} catch (IOException | URISyntaxException | InstagramCheckpointRequiredException | InstagramUserInvalidException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}
	}

	/**
	 * nghilt: 241385981
	 * unknow: 1438049127
	 * vnladies: 5953330777
	 * 佐藤太郎 @finalsatotaro   6298132564
	 * @throws InstagramInvalidCredentialsException 
	 */
	@Test
	public void testSendMessage() throws InstagramInvalidCredentialsException {
		InstagramAccount account = new InstagramAccount();
//		account.setUsername("sonnt0411");
//		account.setPassword("759426183");
		account.setUsername("nghilt");
		account.setPassword("Potetotabetaina!");
		account.setUserId(1);

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);
		
//		String msg = "こんにちは。\nhttp://www.gundam.info/";		// NG
		String msg = "こんにちは。\nhttp://www.ashmart.com/web/";	// OK
//		String msg = "こんにちは。\nhttp://master-system.biz/";	// OK
//		String msg = "こんにちは。\nhttps://www.yahoo.co.jp\nはリンクをクリックできますが、\nこんにちは。\nhttp://www.gundam.info/\nはリンクをクリックできない"; // OK

		try {
			boolean loggedon = instagram.login(true, false);

			if (loggedon) {
				HttpResponse response = instagram.sendMessage(new String[]{"5953330777"} , msg);
				String responseAsString = EntityUtils.toString(response.getEntity());
				System.out.println(responseAsString);
			} else {
				System.out.println("login failed");
			}
		} catch (IOException | URISyntaxException | InstagramCheckpointRequiredException | InstagramUserInvalidException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUser() {
		Configuration config = new Configuration();
		IAPIHelper dbHelper = new DatabaseHelper(config);

		User user = dbHelper.getUser(1972);
		InstagramAutoThread autoThread = new InstagramAutoThread(user, dbHelper);
		autoThread.run();
	}

	@Test
	public void getFriendship() {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("sonnt0411");
		account.setPassword("759426183");

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);
		try {
			instagram.login(true, false);
		} catch (IOException | URISyntaxException | InstagramUserInvalidException | InstagramCheckpointRequiredException | InstagramInvalidCredentialsException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}

		HttpResponse response;
		try {
			response = instagram.getSelfUserFollowers(null);
			//get response body
			String responseAsString = EntityUtils.toString(response.getEntity());

			System.out.println(responseAsString);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}

		// result: {"users": [{"pk": 5789354790, "username": "cuongtiti_ku1234", "full_name": "Cuongtiti_ku", "is_private": false, "profile_pic_url": "https://instagram.fhan2-1.fna.fbcdn.net/t51.2885-19/s150x150/20634967_1784749315150368_4688889713430364160_a.jpg", "profile_pic_id": "1574520908893915318_5789354790", "is_verified": false, "has_anonymous_profile_picture": false}, {"pk": 1438049127, "username": "lun.512", "full_name": "Khanh Huyen", "is_private": true, "profile_pic_url": "https://instagram.fhan2-1.fna.fbcdn.net/t51.2885-19/s150x150/22857896_724492527735766_6010627394392555520_n.jpg", "profile_pic_id": "1636456348546751607_1438049127", "is_verified": false, "has_anonymous_profile_picture": false}], "big_list": false, "page_size": 200, "status": "ok"}
	}

	@Test
	public void getTimelineFeed() {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("ilmimimli");
		account.setPassword(Security.decrypt("UswdyqS9stpeRC/xnompBQ==", Const.SECURITY_KEY));

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);
		try {
			instagram.login(true, false);
		} catch (IOException | URISyntaxException | InstagramUserInvalidException | InstagramCheckpointRequiredException | InstagramInvalidCredentialsException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}

		HttpResponse response;
		try {
			response = instagram.getTimelineFeed(null);
			//get response body
			String responseAsString = EntityUtils.toString(response.getEntity());

			System.out.println(responseAsString);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getPopularFeed() {
		InstagramAccount account = new InstagramAccount();
		account.setUsername("wmsonnt1");
		account.setPassword("7594261830");

		RobotSetting robotSetting = new RobotSetting();
		Instagram instagram = new Instagram(account, robotSetting);
		try {
			instagram.login(true, false);
		} catch (IOException | URISyntaxException | InstagramUserInvalidException | InstagramCheckpointRequiredException | InstagramInvalidCredentialsException | InstagramSentryBlockException e) {
			e.printStackTrace();
		}

		HttpResponse response;
		try {
			response = instagram.getPopularFeed();
			//get response body
			String responseAsString = EntityUtils.toString(response.getEntity());

			System.out.println(responseAsString);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}
}
