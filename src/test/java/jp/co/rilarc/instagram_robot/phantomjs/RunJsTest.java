package jp.co.rilarc.instagram_robot.phantomjs;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jp.co.rilarc.instagram_robot.instagram.Security;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.utils.Const;

public class RunJsTest {
	
	InstagramAccount instagramAccount;
	Proxy proxy = null;
	List<String> listProxy = new ArrayList<>();

	@Before
	public void setUp() throws Exception {
//		String username = "sonnt0411";
//		String password = "759426183";
		
//		String username = "330emi";
//		String password = "igawa03301985";
		
		String username = "siohasu";
		String password = "toku1124";

		instagramAccount = new InstagramAccount();

		instagramAccount.setUsername(username);
		instagramAccount.setPassword(password);
		
		proxy = new Proxy("150.95.156.156", 3128, "wmuser", "VN201501");
		
//		proxy = new Proxy("160.16.136.76", 3128, "wmuser", "VN201501");
//		proxy = new Proxy("150.95.147.50", 3128, "wmuser", "VN201501"); // OK
//		proxy = new Proxy("jp.proxymesh.com", 31280, "ap-promote", "x745knd9");
//		proxy = new Proxy("us.proxymesh.com", 31280, "ap-promote", "x745knd9");
//		proxy = new Proxy("133.130.71.248", 3128);
		
//		listProxy.add("150.95.180.26");	// OK
//		listProxy.add("150.95.184.152"); // OK
//		listProxy.add("163.44.171.23");	// OK
//		listProxy.add("133.130.101.99");	// OK
		listProxy.add("150.95.128.221");
		listProxy.add("150.95.184.149");
		listProxy.add("163.44.175.124");
		listProxy.add("163.44.164.100");
		listProxy.add("150.95.154.54");
		listProxy.add("150.95.147.130");
		listProxy.add("150.95.129.149");
		listProxy.add("133.130.112.202");
		listProxy.add("150.95.128.128");
		listProxy.add("150.95.149.150");
		listProxy.add("133.130.123.172");
		listProxy.add("150.95.156.159");
		listProxy.add("150.95.152.180");
		listProxy.add("133.130.101.244");
		listProxy.add("150.95.143.90");
		listProxy.add("150.95.177.239");
		listProxy.add("150.95.139.68");
		listProxy.add("150.95.143.212");
		listProxy.add("133.130.88.205");
		listProxy.add("150.95.175.222");
		listProxy.add("150.95.149.17");
		listProxy.add("150.95.141.72");
		listProxy.add("133.130.108.86");
		listProxy.add("133.130.123.101");
		listProxy.add("150.95.131.247");
		listProxy.add("150.95.140.146");
		listProxy.add("150.95.150.133");
		listProxy.add("150.95.142.244");
		listProxy.add("150.95.177.184");
		listProxy.add("150.95.151.2");
		listProxy.add("150.95.182.127");
		listProxy.add("150.95.134.67");
		listProxy.add("150.95.151.106");
		listProxy.add("150.95.173.221");
		listProxy.add("150.95.155.107");
		listProxy.add("133.130.125.85");
		listProxy.add("133.130.127.160");
		listProxy.add("150.95.153.48");
		listProxy.add("133.130.121.150");
		listProxy.add("150.95.145.241");
		listProxy.add("150.95.156.156");
		listProxy.add("133.130.120.53");
		listProxy.add("133.130.91.121");
		listProxy.add("150.95.138.240");
		listProxy.add("133.130.100.125");
		listProxy.add("150.95.139.47");
		listProxy.add("150.95.149.71");
		listProxy.add("150.95.176.204");
		listProxy.add("150.95.156.106");
		listProxy.add("150.95.156.163");
		listProxy.add("150.95.128.29");
		listProxy.add("133.130.116.238");
		listProxy.add("133.130.125.191");
		listProxy.add("133.130.103.245");
		listProxy.add("150.95.173.4");
		listProxy.add("163.44.175.208");
	}

	@Test
	public void testLogin() {
		RunJs.login(instagramAccount, proxy);
	}

	@Test
	public void testLike() {
		long start = System.currentTimeMillis();

		boolean result = RunJs.like(instagramAccount, "BbJuUcDhqAl", proxy);
		System.out.println(result);

		long end = System.currentTimeMillis();
		System.out.println("Run in " + (end - start) / 1000 + " seconds");
	}

	@Test
	public void testFollow() {
		long start = System.currentTimeMillis();
		boolean result = RunJs.follow(instagramAccount, "saxton_long", proxy);
		long end = System.currentTimeMillis();
		System.out.println("Run in " + (end - start) / 1000 + " seconds");
		System.out.println("final result: " + result);
	}

	@Test
	public void testLikeList() {
		long start = System.currentTimeMillis();
		int i = 0;
		boolean result = false;
		while(!result && i < listProxy.size()) {
			proxy = new Proxy(listProxy.get(i), 3128, "wmuser", "VN201501");
			result = RunJs.like(instagramAccount, "BbJuUcDhqAl", proxy);
			i++;
			System.out.println(result);
		}
		long end = System.currentTimeMillis();
		System.out.println("Run in " + (end - start) / 1000 + " seconds");
	}
	
	@Test
	public void decryptPass() {
		String encryptedPass = "G2hZFfkKSn68pRF7nHURBg==";
		String pass = Security.decrypt(encryptedPass, Const.SECURITY_KEY);
		System.out.println(pass);
	}
	
	@Test
	public void encnryptPass() {
		String rawPass = "ngan12345";
		String pass = Security.encrypt(rawPass, Const.SECURITY_KEY);
		System.out.println(pass);
	}
}
