package jp.co.rilarc.instagram_robot.encryption;

import jp.co.rilarc.instagram_robot.instagram.Security;
import jp.co.rilarc.instagram_robot.utils.Const;
import org.junit.Before;
import org.junit.Test;

public class EncryptionTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void encryption()
    {
        try
        {
            String data = "7594261830"; // xQDvryaiMNNLfVpP976HVg==
//            System.out.println(Security.decrypt("6TQzJ2C4+78cGHScKBvm9A==", Const.SECURITY_KEY));
            System.out.println(Security.encrypt(data, Const.SECURITY_KEY));
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
