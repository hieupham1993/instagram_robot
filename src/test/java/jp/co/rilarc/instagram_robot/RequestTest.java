package jp.co.rilarc.instagram_robot;

import jp.co.rilarc.instagram_robot.device.GoodDevices;
import jp.co.rilarc.instagram_robot.instagram.Request;
import jp.co.rilarc.instagram_robot.utils.Const;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by nguyenthanhson on 10/25/17.
 */
public class RequestTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testMultipart() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", GoodDevices.getRandomGoodDevice());
        headers.put("Connection", "keep-alive");
        headers.put("Accept", "*/*");
        headers.put("Accept-Encoding", "gzip, deflate, sdch");
        headers.put("X-IG-Capabilities", "3ToAAA==");
        headers.put("X-IG-Connection-Type", "WIFI");
        int connectionSpeed = ((int) (Math.random() * (3700 - 1000))) + 1000;
        headers.put("X-IG-Connection-Speed", String.valueOf(connectionSpeed) + "kbps");
        headers.put("X-FB-HTTP-Engine", "Liger");
        headers.put("Content-Type", "multipart/form-data; boundary=1234567890");
        headers.put("Accept-Language", Const.ACCEPT_LANGUAGE);

        Request request = new Request("https://requestb.in/1jlpss61", headers, null);
        request.setBoundary("1234567890");
        request.addFile("photo", "/Applications/MAMP/htdocs/master_system_2/public/img/robot.png");

        try {
            request.getResponse();
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void datetime() {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Tokyo"));

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String dateInString = "2018-01-22 08:32:24";

        try {

            Date date = formatter.parse(dateInString);
            System.out.println(date);
            System.out.println(formatter.format(date));

            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            System.out.println("sqlDate:" + sqlDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void json() {
        JSONObject result = new JSONObject("{\"success\":true,\"data\":null,\"message\":\"\"}");

        System.out.print(result.get("data").getClass());
    }
}
