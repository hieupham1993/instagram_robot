package jp.co.rilarc.instagram_robot;

/**
 * Created by HieuPT on 11/10/2017.
 */
public final class BuildConfig {

    public static final boolean DEBUG = false;

    public static final String APP_NAME = "#strike";

    public static final String VERSION_NAME_DEV = "1.1_Dev";

    public static final String VERSION_NAME_RELEASE = "1.1";

    public static final String SERVER_URL_DEV = "http://150.95.150.220";

    public static final String SERVER_URL_RELEASE = "https://hashstrike.jp";

    public static final String VERSION_NAME = DEBUG ? VERSION_NAME_DEV : VERSION_NAME_RELEASE;

    public static final String SERVER_URL = DEBUG ? SERVER_URL_DEV : SERVER_URL_RELEASE;
}
