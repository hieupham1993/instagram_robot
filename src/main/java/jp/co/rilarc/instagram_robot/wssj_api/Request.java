package jp.co.rilarc.instagram_robot.wssj_api;

import jp.co.rilarc.instagram_robot.utils.Const;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Request {
    private String _url;
    private Map<String, String> _params = new HashMap<>();
    private Map<String, String> _posts = new HashMap<>();
    private Map<String, String> _headers;



    public Request(String url, Map<String, String> headers) {
        _url = url;
        _headers = headers;
    }

    public Request addParam(String key, String value) {
        _params.put(key, value);
        return this;
    }

    public Request addPost(String key, String value) {
        _posts.put(key, value);
        return this;
    }

    public Request addHeader(String key, String value) {
        _headers.put(key, value);
        return this;
    }

    public HttpResponse getResponse() throws URISyntaxException, IOException {

        String endpoint = this.buildEndpoint(_url);

        CloseableHttpClient client;
        RequestConfig config;

        client = HttpClients.createDefault();
        config = RequestConfig.DEFAULT;


        HttpClientContext context = HttpClientContext.create();
        HttpResponse response;

        if (!_posts.isEmpty()) //post method
        {
            HttpPost request = new HttpPost(endpoint);

            for (Map.Entry<String, String> entry : _headers.entrySet())
                request.addHeader(entry.getKey(), entry.getValue());

            HttpEntity entity = this.buildBodyEntity();
            request.setEntity(entity);

            request.setConfig(config);
            response = client.execute(request, context);
        }
        else //get method
        {
            HttpGet request = new HttpGet(endpoint);

            for (Map.Entry<String, String> entry : _headers.entrySet())
                request.setHeader(entry.getKey(), entry.getValue());

            request.setConfig(config);
            response = client.execute(request, context);
        }

        return response;
    }

    private String buildEndpoint(String url) throws URISyntaxException {
        if (url.contains("http"))
            return url;

        String endpoint = Const.BASE_WSSJ_URL + "/api/v1/" + _url;

        URIBuilder builder = new URIBuilder(endpoint);

        // create query string
        if (!_params.isEmpty()) {
            List<NameValuePair> queryParams = new ArrayList<>();
            for (Map.Entry<String, String> entry : _params.entrySet()) {
                queryParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            builder.setParameters(queryParams);
        }

        URI uri = builder.build();
        endpoint = uri.toString();

        return endpoint;
    }

    private HttpEntity buildBodyEntity() throws UnsupportedEncodingException {
        HttpEntity result;
        List<NameValuePair> postBody = new ArrayList<>();
        for (Map.Entry<String, String> entry : _posts.entrySet())
            postBody.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));

        result = new UrlEncodedFormEntity(postBody, "UTF-8");

        return result;
    }
}
