package jp.co.rilarc.instagram_robot.wssj_api;

import org.apache.http.HttpResponse;
import org.apache.http.util.TextUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class WssjService {
    private Request request(String url, String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        if (!TextUtils.isEmpty(token))
            headers.put("Authorization", token);

        return new Request(url, headers);
    }

    public HttpResponse getUser(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-user", token);

        return request.getResponse();
    }

    public HttpResponse refreshToken(String token) throws IOException, URISyntaxException {
        Request request = this.request("refresh", token);

        return request.getResponse();
    }

    public HttpResponse updateRobotStatus(String token, boolean status) throws IOException, URISyntaxException {
        Request request = this.request("update-robot-status", token)
                .addPost("status", (status) ? "1" : "0");

        return request.getResponse();
    }

    public HttpResponse getRobotSetting(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-robot-setting", token);

        return request.getResponse();
    }

    public HttpResponse getInstagramAccount(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-instagram-account", token);

        return request.getResponse();
    }

    public HttpResponse disableUser(String token, String notes) throws IOException, URISyntaxException {
        Request request = this.request("disable-user", token)
                .addPost("notes", notes);

        return request.getResponse();
    }

    public HttpResponse updateInstagramConnectStatus(String token, boolean connectStatus, String notes) throws IOException, URISyntaxException {
        Request request = this.request("update-instagram-connect-status", token)
                .addPost("connect_status", (connectStatus) ? "1" : "0");

        if (!TextUtils.isEmpty(notes))
            request.addPost("notes", notes);

        return request.getResponse();
    }

    public HttpResponse getOrCreateStatistic(String token, String instagramUsername, String date) throws IOException, URISyntaxException {
        Request request = this.request("get-or-create-statistic", token)
                .addParam("instagram_username", instagramUsername)
                .addParam("date", date);

        return request.getResponse();
    }

    public HttpResponse getAdvanceSetting(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-advance-setting", token);

        return request.getResponse();
    }

    public HttpResponse increaseNumLikesStatistic(String token, int statisticId, int numLikes) throws IOException, URISyntaxException {
        Request request = this.request("increase-num-likes-statistic", token)
                .addPost("statistic_id", String.valueOf(statisticId))
                .addPost("num_likes", String.valueOf(numLikes));

        return request.getResponse();
    }

    public HttpResponse getBadHashtag(String token, String hashtag) throws IOException, URISyntaxException {
        Request request = this.request("get-bad-hashtag", token)
                .addParam("hashtag", hashtag);

        return request.getResponse();
    }

    public HttpResponse markBadHashtag(String token, String hashtag) throws IOException, URISyntaxException {
        Request request = this.request("mark-bad-hashtag", token)
                .addPost("hashtag", hashtag);

        return request.getResponse();
    }

    public HttpResponse getInstagramUser(String token, String userId) throws IOException, URISyntaxException {
        Request request = this.request("get-instagram-user", token)
                .addPost("instagram_user_id", userId);

        return request.getResponse();
    }

    public HttpResponse getLikeHashtags(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-like-hashtags", token);

        return request.getResponse();
    }

    public HttpResponse deleteBadHashtag(String token, int badHashtagId) throws IOException, URISyntaxException {
        Request request = this.request("delete-bad-hashtag", token)
                .addPost("bad_hashtag_id", String.valueOf(badHashtagId));

        return request.getResponse();
    }

    public HttpResponse increaseLikeHashtagCount(String token, int hashtagId, int numLikes) throws IOException, URISyntaxException {
        Request request = this.request("increase-likes-hashtag-count", token)
                .addPost("hashtag_id", String.valueOf(hashtagId))
                .addPost("num_likes", String.valueOf(numLikes));

        return request.getResponse();
    }

    public HttpResponse getSpamHashtags(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-spam-hashtags", token);

        return request.getResponse();
    }

    public HttpResponse insertActivity(String token, int type, String instagramNameApp, String instagramUsername,
                                       String instagramAvatar, String hashtag, String locationId, String mediaPost,
                                       String imagePost) throws IOException, URISyntaxException {
        Request request = this.request("insert-activity", token)
                .addPost("type", String.valueOf(type))
                .addPost("instagram_name_app", instagramNameApp)
                .addPost("instagram_username", instagramUsername)
                .addPost("instagram_avatar", instagramAvatar)
                .addPost("hashtag", hashtag)
                .addPost("location_id", locationId)
                .addPost("media_post", mediaPost)
                .addPost("image_post", imagePost);

        return request.getResponse();
    }

    public HttpResponse getLikeLocations(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-like-locations", token);

        return request.getResponse();
    }

    public HttpResponse increaseLikeLocationCount(String token, int locationId, int numLikes) throws IOException, URISyntaxException {
        Request request = this.request("increase-likes-location-count", token)
                .addPost("location_id", String.valueOf(locationId))
                .addPost("num_likes", String.valueOf(numLikes));

        return request.getResponse();
    }

    public HttpResponse disableAction(String token, String action, String notes) throws IOException, URISyntaxException {
        Request request = this.request("disable-action", token)
                .addPost("action", action)
                .addPost("notes", notes);

        return request.getResponse();
    }

    public HttpResponse increaseNumFollowsStatistic(String token, int statisticId, int numFollows) throws IOException, URISyntaxException {
        Request request = this.request("increase-num-follows-statistic", token)
                .addPost("statistic_id", String.valueOf(statisticId))
                .addPost("num_follows", String.valueOf(numFollows));

        return request.getResponse();
    }

    public HttpResponse increaseNumUnfollowsStatistic(String token, int statisticId) throws IOException, URISyntaxException {
        Request request = this.request("increase-num-unfollows-statistic", token)
                .addPost("statistic_id", String.valueOf(statisticId));

        return request.getResponse();
    }

    public HttpResponse getRelatedAccount(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-related-account", token);

        return request.getResponse();
    }

    public HttpResponse getPreviouslyUnfollowUsers(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-previously-unfollow-users", token);

        return request.getResponse();
    }

    public HttpResponse saveFollowingUser(String token, String followingUserId, String followingUsername, String followingAvatarUrl) throws IOException, URISyntaxException {
        Request request = this.request("save-following-user", token)
                .addPost("following_user_id", followingUserId)
                .addPost("following_username", followingUsername)
                .addPost("following_avatar_url", followingAvatarUrl);

        return request.getResponse();
    }

    public HttpResponse getFollowHashtags(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-follow-hashtags", token);

        return request.getResponse();
    }

    public HttpResponse increaseFollowHashtagCount(String token, int hashtagId, int numFollows) throws IOException, URISyntaxException {
        Request request = this.request("increase-follows-hashtag-count", token)
                .addPost("hashtag_id", String.valueOf(hashtagId))
                .addPost("num_follows", String.valueOf(numFollows));

        return request.getResponse();
    }

    public HttpResponse getFollowLocations(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-follow-locations", token);

        return request.getResponse();
    }

    public HttpResponse increaseFollowLocationCount(String token, int locationId, int numFollows) throws IOException, URISyntaxException {
        Request request = this.request("increase-follows-location-count", token)
                .addPost("location_id", String.valueOf(locationId))
                .addPost("num_follows", String.valueOf(numFollows));

        return request.getResponse();
    }

    public HttpResponse updateRelatedAccountStatus(String token, int relatedAccountId, boolean status) throws IOException, URISyntaxException {
        Request request = this.request("update-related-account-status", token)
                .addPost("related_account_id", String.valueOf(relatedAccountId))
                .addPost("status", (status) ? "1" : "0");

        return request.getResponse();
    }

    public HttpResponse getPreviouslyUnfollowUser(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-previously-unfollow-user", token);

        return request.getResponse();
    }

    public HttpResponse updateUnfollowedUsers(String token, String unfollowedUsers) throws IOException, URISyntaxException {
        Request request = this.request("update-unfollowed-users", token)
                .addPost("unfollowed_users", unfollowedUsers);

        return request.getResponse();
    }

    public HttpResponse getListFollowingUser(String token, Integer daysToCheckUnfollow) throws IOException, URISyntaxException {
        Request request = this.request("get-list-following-user", token);

        if (daysToCheckUnfollow != null)
            request.addParam("days_to_check_unfollow", daysToCheckUnfollow.toString());

        return request.getResponse();
    }

    public HttpResponse removeFollowingUser(String token, int followingUserId) throws IOException, URISyntaxException {
        Request request = this.request("remove-following-user", token)
                .addPost("following_user_id", String.valueOf(followingUserId));

        return request.getResponse();
    }

    public HttpResponse updateUnfollowBoostStatus(String token, boolean status) throws IOException, URISyntaxException {
        Request request = this.request("update-unfollow-boost-status", token)
                .addPost("status", (status) ? "1" : "0");

        return request.getResponse();
    }

    public HttpResponse reduceBoostAmount(String token) throws IOException, URISyntaxException {
        Request request = this.request("reduce-boost-amount", token);

        return request.getResponse();
    }

    public HttpResponse updateCheckedFollowingUser(String token, int followingUserId) throws IOException, URISyntaxException {
        Request request = this.request("update-checked-following-user", token)
                .addPost("following_user_id", String.valueOf(followingUserId));

        return request.getResponse();
    }

    public HttpResponse getCommentSchedule(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-comment-schedule", token);

        return request.getResponse();
    }

    public HttpResponse getPostSchedule(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-post-schedule", token);

        return request.getResponse();
    }

    public HttpResponse getSendMessageSchedule(String token) throws IOException, URISyntaxException {
        Request request = this.request("get-send-message-schedule", token);

        return request.getResponse();
    }

    public HttpResponse cancelScheduleSendMessage(String token) throws IOException, URISyntaxException {
        Request request = this.request("cancel-schedule-send-message", token);

        return request.getResponse();
    }

    public HttpResponse updateSendMessageScheduleStatus(String token, int scheduleId, int status) throws IOException, URISyntaxException {
        Request request = this.request("update-send-message-schedule-status", token)
                .addPost("schedule_id", String.valueOf(scheduleId))
                .addPost("status", String.valueOf(status));

        return request.getResponse();
    }

    public HttpResponse deletePostSchedule(String token, int scheduleId) throws IOException, URISyntaxException {
        Request request = this.request("delete-post-schedule", token)
                .addPost("schedule_id", String.valueOf(scheduleId));

        return request.getResponse();
    }

    public HttpResponse updatePostSchedule(String token, int scheduleId, Integer status, Long lastestPost, String linkAfterPost, Long nextTimeUpload, Long timeToAction) throws IOException, URISyntaxException {
        Request request = this.request("update-post-schedule", token)
                .addPost("schedule_id", String.valueOf(scheduleId))
                .addPost("status", String.valueOf(status));

        if (lastestPost != null)
            request.addPost("lastest_post", String.valueOf(lastestPost));

        if (linkAfterPost != null)
            request.addPost("link_after_post", linkAfterPost);

        if (nextTimeUpload != null)
            request.addPost("next_time_upload", String.valueOf(nextTimeUpload));

        if (nextTimeUpload != null)
            request.addPost("time_to_action", String.valueOf(timeToAction));

        return request.getResponse();
    }

    public HttpResponse updateCommentScheduleStatus(String token, int scheduleId, int status) throws IOException, URISyntaxException {
        Request request = this.request("update-comment-schedule-status", token)
                .addPost("schedule_id", String.valueOf(scheduleId))
                .addPost("status", String.valueOf(status));

        return request.getResponse();
    }

}
