package jp.co.rilarc.instagram_robot.action;

import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.follow.Follow;
import jp.co.rilarc.instagram_robot.follow.FollowByFollower;
import jp.co.rilarc.instagram_robot.follow.FollowByHashtag;
import jp.co.rilarc.instagram_robot.follow.FollowByLocation;
import jp.co.rilarc.instagram_robot.follow.FollowByRelatedAccount;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RelatedAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FollowAction extends BaseAction {
	final private static Logger logger = LogManager.getLogger(FollowAction.class);

    final private RobotSetting mRobotSetting;
    final private Configuration config;
    private AdvanceSetting mAdvanceSetting;

    public FollowAction(Instagram instagram, RobotSetting robotSetting, AdvanceSetting advanceSetting,
						IAPIHelper IAPIHelper, Configuration config, Statistic statistic) {
    	super(instagram, IAPIHelper, statistic);
        mRobotSetting = robotSetting;
        mAdvanceSetting = advanceSetting;

        this.config = config;
    }

//    public void run() {
//        long startTime = System.currentTimeMillis();
//        try {
//        	runThread(startTime);
//        }catch(Exception e) {
//        	logger.error("Unknown Error: ", e);
//        }
//    }
//    
//    public void runThread(long startTime) {
//    	String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//        
//        Statistic statistic = dbHelper.getOrCreateStatistic(mUserId, mInstagram.getInstagramAccount().getUsername(), date);
//
//        while (System.currentTimeMillis() - startTime < duration * 60 * 1000) {
//            //check like per day
//            int maxFollows = config.getFollowSpeed(mRobotSetting.getFollowsAutoFollowSpeed());
//            if (statistic.getNumFollowings() >= maxFollows) {
//                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": reach to max follow (" + maxFollows + ") of this day");
//                break;
//            }
//
//            int remainingFollow = maxFollows - statistic.getNumFollowings();
//            // like 10-16 medias in a round
//            int numberUsersToFollow = Signatures.randomInt((remainingFollow < 10 ? remainingFollow : 10), (remainingFollow < 16 ? remainingFollow : 16));
//
//            logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": remaining follow " + remainingFollow);
//
//
//            Follow follow;
//            String followMethod;
//            // check follow by related account
//            RelatedAccount relatedAccount = dbHelper.getRelatedAccount(mUserId);
//            if (mAdvanceSetting != null && mAdvanceSetting.isActive() && relatedAccount != null) {
//                follow = new FollowByRelatedAccount(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, relatedAccount, numberUsersToFollow);
//                followMethod = "related_account";
//            } else {
//                //select follow method
//                String lastFollowMethod = statistic.getLastFollowMethod();
//                followMethod = this.getFollowMethod(lastFollowMethod);
//
//                if (Objects.equals(followMethod, Statistic.FOLLOW_METHOD_HASHTAG))
//                    follow = new FollowByHashtag(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, numberUsersToFollow);
//                else if (Objects.equals(followMethod, Statistic.FOLLOW_METHOD_LOCATION))
//                    follow = new FollowByLocation(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, numberUsersToFollow);
//                else
//                    follow = new FollowByFollower(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, numberUsersToFollow);
//            }
//
//            logger.debug("account " + mInstagram.getInstagramAccount().getUsername() + ": follow by " + followMethod);
//
//            int numMediasLiked = follow.autoFollow();
//            dbHelper.increaseNumFollowsStatistic(statistic.getId(), numMediasLiked);
//            statistic.setNumFollowings(statistic.getNumFollowings() + numMediasLiked);
//
//
//            // update last method
//            dbHelper.updateLastFollowMethod(statistic.getId(), followMethod);
//            statistic.setLastFollowMethod(followMethod);
//
//            try {
//                Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
    
    @Override
	public List<String> getActionMethod() {
		List<String> listFollowMethod = new ArrayList<>();

		RelatedAccount relatedAccount = dbHelper.getRelatedAccount();
		if (relatedAccount != null) {
			listFollowMethod.add(Statistic.FOLLOW_METHOD_RELATED_ACCOUNT);
		}else {
			if (mRobotSetting.isFollowsHashtagAutoFollow()) {
				listFollowMethod.add(Statistic.FOLLOW_METHOD_HASHTAG);
			}

			if (mRobotSetting.isFollowsLocationAutoFollow()) {
				listFollowMethod.add(Statistic.FOLLOW_METHOD_LOCATION);
			}

			if (mRobotSetting.isFollowsFollowersAutoFollowBack()) {
				listFollowMethod.add(Statistic.FOLLOW_METHOD_FOLLOWER);
			}
		}

		// Random like action
		Collections.shuffle(listFollowMethod);

		return listFollowMethod;
	}

	@Override
	public void updateStatisticInfo(Statistic statistic, int numUserFollowed) {
//		dbHelper.increaseNumFollowsStatistic(statistic.getId(), numUserFollowed);
//        statistic.setNumFollowings(statistic.getNumFollowings() + numUserFollowed);


        // update last method
//        dbHelper.updateLastFollowMethod(statistic.getId(), followMethod);
//        statistic.setLastFollowMethod(followMethod);
        
//        statistic.setNumLikes(statistic.getNumFollowers() + numUserFollowed);
	}

	@Override
	public int runMethod(String followMethod, int numberUsersToFollow) throws ActionBlockException, InstagramCannotAccessException {
		Follow follow = null;
        // check follow by related account
        if(Objects.equals(followMethod, Statistic.FOLLOW_METHOD_RELATED_ACCOUNT)) {
			RelatedAccount relatedAccount = dbHelper.getRelatedAccount();
			if (relatedAccount != null) {
				follow = new FollowByRelatedAccount(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, relatedAccount, statistic, numberUsersToFollow);
			}else {
				logger.warn("Cannot found related account");
				return 0;
			}
        } else {
            //select follow method
            if (Objects.equals(followMethod, Statistic.FOLLOW_METHOD_HASHTAG)) {
                follow = new FollowByHashtag(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToFollow);
            		logger.debug("Run action follow by hashtag");
            }
            else if (Objects.equals(followMethod, Statistic.FOLLOW_METHOD_LOCATION)) {
                follow = new FollowByLocation(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToFollow);
                logger.debug("Run action follow by location");
            }
            else {
                follow = new FollowByFollower(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToFollow);
                logger.debug("Run action follow by follower");
            }
        }

//        logger.debug("account " + mInstagram.getInstagramAccount().getUsername() + ": follow by " + followMethod);

        return follow.autoFollow();
	}

	@Override
	public String getMyAction() {
		return "Follow";
	}
	
	@Override
	public int getActionSpeed() {
		return config.getFollowSpeed(mRobotSetting.getFollowsAutoFollowSpeed());
	}

	@Override
	protected int getRemainAction() {
		return getActionSpeed() - statistic.getNumFollowings();
	}

//    private String getFollowMethod(String lastFollowMethod) {
//        String followMethod;
//
//        if (lastFollowMethod == null)
//            lastFollowMethod = "";
//
//        switch (lastFollowMethod) {
//            case Statistic.FOLLOW_METHOD_HASHTAG:
//                if (mRobotSetting.isFollowsLocationAutoFollow()) {
//                    followMethod = Statistic.FOLLOW_METHOD_LOCATION;
//                } else if (mRobotSetting.isFollowsFollowersAutoFollowBack()) {
//                    followMethod = Statistic.FOLLOW_METHOD_FOLLOWER;
//                } else {
//                    followMethod = Statistic.FOLLOW_METHOD_HASHTAG;
//                }
//                break;
//            case Statistic.FOLLOW_METHOD_LOCATION:
//                if (mRobotSetting.isFollowsFollowersAutoFollowBack()) {
//                    followMethod = Statistic.FOLLOW_METHOD_FOLLOWER;
//                } else if (mRobotSetting.isFollowsHashtagAutoFollow()) {
//                    followMethod = Statistic.FOLLOW_METHOD_HASHTAG;
//                } else {
//                    followMethod = Statistic.FOLLOW_METHOD_LOCATION;
//                }
//                break;
//            case Statistic.FOLLOW_METHOD_FOLLOWER:
//            default:
//                if (mRobotSetting.isFollowsHashtagAutoFollow()) {
//                    followMethod = Statistic.FOLLOW_METHOD_HASHTAG;
//                } else if (mRobotSetting.isFollowsLocationAutoFollow()) {
//                    followMethod = Statistic.FOLLOW_METHOD_LOCATION;
//                } else {
//                    followMethod = Statistic.FOLLOW_METHOD_FOLLOWER;
//                }
//                break;
//        }
//
//        return followMethod;
//    }
}
