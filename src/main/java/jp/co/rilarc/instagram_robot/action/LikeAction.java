package jp.co.rilarc.instagram_robot.action;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.like.Like;
import jp.co.rilarc.instagram_robot.like.LikeByFollowing;
import jp.co.rilarc.instagram_robot.like.LikeByHashtag;
import jp.co.rilarc.instagram_robot.like.LikeByLocation;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class LikeAction extends BaseAction {

    final private static Logger logger = LogManager.getLogger(LikeAction.class);

    public static final int MAX_TIME_FOR_ONE_ACTION = 2;  // hours

    final private RobotSetting mRobotSetting;

    final private Configuration config;

    final private AdvanceSetting mAdvanceSetting;

    public LikeAction(Instagram instagram, RobotSetting robotSetting, AdvanceSetting advanceSetting,
                      IAPIHelper IAPIHelper, Configuration config, Statistic statistic) {
        super(instagram, IAPIHelper, statistic);

        mRobotSetting = robotSetting;
        mAdvanceSetting = advanceSetting;

        this.config = config;
    }

    @Override
    public List<String> getActionMethod() {
        List<String> listLikeMethod = new ArrayList<>();
        if (mRobotSetting.isLikesHashtagAutoLike()) {
            listLikeMethod.add(Statistic.LIKE_METHOD_HASHTAG);
        }

        if (mRobotSetting.isLikesLocationAutoLike()) {
            listLikeMethod.add(Statistic.LIKE_METHOD_LOCATION);
        }

        if (mRobotSetting.isLikesFollowingAutoLikeFeed()) {
            listLikeMethod.add(Statistic.LIKE_METHOD_FOLLOWING);
        }

        // Random like action
        Collections.shuffle(listLikeMethod);

        return listLikeMethod;
    }

    @Override
    public void updateStatisticInfo(Statistic statistic, int numMediasActionSuccess) {
//        dbHelper.increaseNumLikesStatistic(statistic.getId(), numMediasActionSuccess);

        // update last method
//		dbHelper.updateLastLikeMethod(statistic.getId(), likeMethod);

//		statistic.setNumLikes(statistic.getNumLikes() + numMediasActionSuccess);
    }

    @Override
    public int runMethod(String methodName, int maxNumMediaToAction) throws ActionBlockException, InstagramCannotAccessException {
        Like like;
        //
        if (Objects.equals(methodName, Statistic.LIKE_METHOD_HASHTAG)) {
            logger.debug("Run thread like by hashtag");
            like = new LikeByHashtag(mInstagram, mRobotSetting, dbHelper, config, mAdvanceSetting,
                    statistic, maxNumMediaToAction);
        } else if (Objects.equals(methodName, Statistic.LIKE_METHOD_LOCATION)) {
            logger.debug("Run thread like by location");
            like = new LikeByLocation(mInstagram, mRobotSetting, dbHelper, config, mAdvanceSetting,
                    statistic, maxNumMediaToAction);
        } else {
            logger.debug("Run thread like by following");
            like = new LikeByFollowing(mInstagram, mRobotSetting, dbHelper, config, mAdvanceSetting,
                    statistic, maxNumMediaToAction);
        }

//		logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": like by " + methodName);

        return like.autoLike();
    }

    @Override
    public String getMyAction() {
        return "Like";
    }

    @Override
    public int getActionSpeed() {
        return config.getLikeSpeed(mRobotSetting.getLikesAutoLikeSpeed());
    }

    @Override
    protected int getRemainAction() {
        return getActionSpeed() - statistic.getNumLikes();
    }
}
