/**
 *
 */
package jp.co.rilarc.instagram_robot.action;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.ConfigureImageHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.InstagramRequester;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;
import jp.co.rilarc.instagram_robot.model.SchedulePost;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;

/**
 * @author nghilt
 */
public class ScheduledAction extends Thread {

    final private static Logger logger = LogManager.getLogger(ScheduledAction.class);

    public static boolean sendMessage(ScheduleSendMessage scheduleSendMessage, IAPIHelper dbHelper, Configuration config) {
        if (scheduleSendMessage == null) {
            logger.error("cannot send message schedule");
            return false;
        }

        int scheduleId = scheduleSendMessage.getId();
        int userId = scheduleSendMessage.getUserId();
        Instagram instagram;
        try {
            instagram = UserPool.getInstagram(userId, dbHelper, config);
        } catch (InstagramCannotAccessException e1) {
            logger.error("[" + userId + "][SendMessage stopped]" + e1.getMessage());
            return false;
        }

        String instagramAccountName = instagram.getInstagramAccount().getUsername();

        // Get Disable Send message status
        boolean isSendMessageDisabled = dbHelper.getDisableSendMessageFlg();
        if (isSendMessageDisabled) {
//        		dbHelper.updateSendMessageScheduleStatus(scheduleId, ScheduleComment.STATUS_CANCEL);
            dbHelper.cancelScheduleSendMessage();
            logger.warn("user [" + instagramAccountName + "] was blocked send message action");

            return false;
        }

        logger.info("[" + instagramAccountName + "][send message] schedule id " + scheduleId);

        JSONObject sendMessageResponse = null;
        try {
            sendMessageResponse = new InstagramRequester(instagram).sendMessage(scheduleSendMessage.getInstagramUserId(), scheduleSendMessage.getMessage());
        } catch (ActionBlockException e) {
            dbHelper.disableAction(BaseAction.ACTION_SEND_MESSAGE, e.getMessage());
            dbHelper.cancelScheduleSendMessage();
            logger.error("[" + instagramAccountName + "][send message] disable send message)", e);
            VLogger.append("インスタグラムからメッセージ送信行動に対してスパム警告が出ています");
        }

        if (sendMessageResponse == null) {
            dbHelper.updateSendMessageScheduleStatus(scheduleSendMessage.getId(), ScheduleComment.STATUS_CANCEL);
            logger.warn("[" + instagramAccountName + "][send message][failed] to user " + scheduleSendMessage.getInstagramName());
            VLogger.append("メッセージ送信失敗. https://www.instagram.com/" + scheduleSendMessage.getInstagramName());
            return false;
        }

        dbHelper.updateSendMessageScheduleStatus(scheduleSendMessage.getId(), ScheduleComment.STATUS_FINISH);
        logger.info("[" + instagramAccountName + "][send message][successful] to user https://www.instagram.com/" + scheduleSendMessage.getInstagramName());
        VLogger.append("メッセージ送信成功. https://www.instagram.com/" + scheduleSendMessage.getInstagramName());

        return true;
    }

    public static boolean post(SchedulePost schedulePost, IAPIHelper dbHelper, Configuration config) {

        if (schedulePost == null) {
            logger.debug("Cannot post");
            return false;
        }

        int scheduleId = schedulePost.getId();
        int userId = schedulePost.getUserId();
        Instagram instagram;
        try {
            instagram = UserPool.getInstagram(userId, dbHelper, config);
        } catch (InstagramCannotAccessException e1) {
            logger.error("[" + userId + "][Post stopped]" + e1.getMessage());
            return false;
        }

        logger.info("[" + instagram.getInstagramAccount().getUsername() + "][post] schedule id " + scheduleId);


        JSONObject externalMetadata = new JSONObject();
        externalMetadata.put("caption", schedulePost.getCaption());
        if (schedulePost.getLocation() != null) {
            JSONObject location = new JSONObject(schedulePost.getLocation());
            if (location.has("latitude") && !location.isNull("latitude")
                    && location.has("longitude") && !location.isNull("longitude")) {
                InstagramRequester instagramRequester = new InstagramRequester(instagram);

                JSONObject locationDetail = instagramRequester.searchLocation(location.getString("latitude"), location.getString("longitude"), location.getString("name"));

                if (locationDetail.has("name"))
                    externalMetadata.put("location", locationDetail);
            }
        }

        boolean uploadResult = false;
        JSONObject uploadPhotoResponse = null;
        try {
            uploadPhotoResponse = new InstagramRequester(instagram).uploadPhoto(schedulePost.getImagePath(), externalMetadata);
            if (uploadPhotoResponse != null)
                uploadResult = true;
        } catch (InstagramCheckpointRequiredException e) {
            e.printStackTrace();
        }

        ConfigureImageHandler configureImageHandler = new ConfigureImageHandler(uploadPhotoResponse);

        if (uploadResult && Objects.equals(configureImageHandler.getStatus(), "ok")) { // upload successful

            if (schedulePost.isDeleteSchedule()) {
                dbHelper.deletePostSchedule(schedulePost.getId());
                return false;
            }

            // update last time upload photo
            schedulePost.setLastestPost(new Timestamp(System.currentTimeMillis()));
            schedulePost.setLinkAfterPost("https://www.instagram.com/p/" + configureImageHandler.getMediaCode());

            if (schedulePost.isPostLoop()) {
                // update next time upload photo
                long nextTimeUpload = schedulePost.getTimeToAction().getTime() + schedulePost.getRepeatTime() * Const.MILISECOND_PER_HOUR;
                schedulePost.setNextTimeUpload(new Timestamp(nextTimeUpload));

                // if this is the last time upload photo
                if (schedulePost.getNextTimeUpload().after(schedulePost.getDayEndSchedule())) {
                    schedulePost.setStatus(SchedulePost.STATUS_FINISH);
                } else {
                    schedulePost.setStatus(SchedulePost.STATUS_RUNNING);
                    schedulePost.setTimeToAction(new Timestamp(nextTimeUpload));
                }
            } else {
                schedulePost.setStatus(SchedulePost.STATUS_FINISH);
            }

            dbHelper.updatePostSchedule(schedulePost);

            logger.info("[" + instagram.getInstagramAccount().getUsername() + "][upload photo] successful");
            VLogger.append("設定した投稿内容をインスタグラムに投稿しました。 https://www.instagram.com/p/" + configureImageHandler.getMediaCode());
        } else { // upload failed
            // canceling schedule
            dbHelper.updatePostScheduleStatus(schedulePost.getId(), SchedulePost.STATUS_CANCEL);
            logger.warn("[" + instagram.getInstagramAccount().getUsername() + "][upload photo] failed");
            return false;
        }

        return true;
    }

    public static boolean comment(ScheduleComment scheduleComment, IAPIHelper dbHelper, Configuration config) {
        if (scheduleComment == null) {
            logger.error("Cannot commentsetting");
            return false;
        }

        int scheduleId = scheduleComment.getId();
        int userId = scheduleComment.getUserId();
        Instagram instagram;
        try {
            instagram = UserPool.getInstagram(userId, dbHelper, config);
        } catch (InstagramCannotAccessException e1) {
            logger.error("[" + userId + "][Comment stopped]" + e1.getMessage());
            return false;
        }

        logger.info("[" + instagram.getInstagramAccount().getUsername() + "][commentsetting] schedule id " + scheduleId);

        JSONObject uploadPhotoResponse = null;
        try {
            uploadPhotoResponse = new InstagramRequester(instagram).sendComment(scheduleComment.getMediaId(), scheduleComment.getCommentContent());
        } catch (InstagramCheckpointRequiredException e) {
            logger.error("error", e);
        }

        if (uploadPhotoResponse == null) {
            dbHelper.updateCommentScheduleStatus(scheduleComment.getId(), ScheduleComment.STATUS_CANCEL);
            logger.warn("[" + instagram.getInstagramAccount().getUsername() + "][commentsetting] failed");
            return false;
        }

        dbHelper.updateCommentScheduleStatus(scheduleComment.getId(), ScheduleComment.STATUS_FINISH);
        logger.info("[" + instagram.getInstagramAccount().getUsername() + "][commentsetting] successful");
        VLogger.append("投稿にコメントを投げました。" + InstagramWebController.INSTAGRAM_BASE_POST_URL + scheduleComment.getMediaCode());

        return true;
    }
}
