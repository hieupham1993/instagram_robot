/**
 * 
 */
package jp.co.rilarc.instagram_robot.action;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.ScheduleBase;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;
import jp.co.rilarc.instagram_robot.model.SchedulePost;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;

/**
 * @author nghilt
 *
 */
public class ScheduledAutoThread extends Thread {
	
	final private static Logger logger = LogManager.getLogger(ScheduledAutoThread.class);

	private final IAPIHelper dbHelper;
	private final Configuration config;
	private final List<ScheduleBase> listSchedule;

	public ScheduledAutoThread(IAPIHelper IAPIHelper, Configuration config) {
		this.dbHelper = IAPIHelper;
		this.config = config;
		listSchedule = new ArrayList<>();
	}

	@Override
	public void run() {
		logger.info("Start scan schedule and do");
		while(true) {
			getSchedules();
			
			// Lan luot gui message
			boolean success = false;
			try {
				for (ScheduleBase schedule : listSchedule) {
					
					Timestamp sendTime = schedule.getTimeToAction();
					int duration = (int) (sendTime.getTime() - new Date().getTime());
					if(duration <= 0) {
						duration = 0;
					}
					
					Utils.waitForMiliSecond(duration, duration);
					
					if(schedule instanceof ScheduleSendMessage) {
						success = ScheduledAction.sendMessage((ScheduleSendMessage) schedule, dbHelper, config);
					}else if(schedule instanceof SchedulePost) {
						success = ScheduledAction.post((SchedulePost) schedule, dbHelper, config);
					}else if(schedule instanceof ScheduleComment) {
						success = ScheduledAction.comment((ScheduleComment) schedule, dbHelper, config);
					}
					
					if(!success) {
						break;
					}
					
//					Utils.waitFor(5, 10); // wait for 5~10s before next action
				}
			}catch(Exception e) {
				logger.error("error", e);
			}
			
			// Wait for 1 minute before start new user
			Utils.waitForMinutes(1, 1);
		}
	}
	
	private void getSchedules() {
		listSchedule.clear();	// Xoa toan bo schedule da gui
		
		// Get Uptime ScheduleSendMessage
		List<ScheduleSendMessage> listScheduleSendMessage = getListScheduleSendMessage();
		List<SchedulePost> listSchedulePost = getListSchedulePost();
		List<ScheduleComment> listScheduleComment = getListScheduleComment();
		
		// schedule post va commentsetting
		listSchedule.addAll(listScheduleSendMessage);
		listSchedule.addAll(listSchedulePost);
		listSchedule.addAll(listScheduleComment);
		
		if(listSchedule.isEmpty()) {
			logger.debug("There are no schedule registered");
			return;
		}
		
		// sort
		Collections.sort(listSchedule, new Comparator<ScheduleBase>() {

			@Override
			public int compare(ScheduleBase o1, ScheduleBase o2) {
				return o1.getTimeToAction().before(o2.getTimeToAction()) ? -1 : 1;
			}
		});
		
		logger.debug("List schedule " + listSchedule);
		logger.info("Process " + listSchedule.size() + " schedules");
	}

	private List<ScheduleComment> getListScheduleComment() {
		return dbHelper.getListScheduleComment();
	}

	private List<SchedulePost> getListSchedulePost() {
		return dbHelper.getListSchedulePost();
	}

	protected List<ScheduleSendMessage> getListScheduleSendMessage() {
		List<ScheduleSendMessage> list = dbHelper.getListScheduleSendMessage();
		
		return adjustSendTime(list);
	}
	
	protected List<ScheduleSendMessage> adjustSendTime(final List<ScheduleSendMessage> list) {
		// Tap hop schedule theo tung user
		Map<Integer, List<ScheduleSendMessage>> maps = new HashMap<>();
		for(ScheduleSendMessage scheduleSendMessage : list) {
			List<ScheduleSendMessage> listMessage = maps.get(scheduleSendMessage.getUserId());
			if(listMessage == null) {
				listMessage = new ArrayList<>();
				maps.put(scheduleSendMessage.getUserId(), listMessage);
			}
			listMessage.add(scheduleSendMessage);
		}
		
		List<ScheduleSendMessage> adjustedList = new ArrayList<>();
		
		// Dieu chinh thoi gian gui
		Set<Integer> userIds = maps.keySet();
		for(Integer userId : userIds) {
			List<ScheduleSendMessage> listMessageOfUser = maps.get(userId);
			adjustSendTimeForEachUser(listMessageOfUser);
			adjustedList.addAll(listMessageOfUser);
		}
		
		return adjustedList;
	}
	
	/**
	 * Dieu chinh thoi gian
	 * @param list
	 */
	protected void adjustSendTimeForEachUser(final List<ScheduleSendMessage> list) {
		
		Timestamp sendTime = null;
		for(ScheduleSendMessage scheduleSendMessage : list) {
			if(sendTime == null) {
				sendTime = new Timestamp(new Date().getTime()); 
			}else {
				sendTime.setTime(sendTime.getTime() + scheduleSendMessage.getDelay() * 1000);
			}
			scheduleSendMessage.setTimeToAction(new Timestamp(sendTime.getTime()));
		}
	}
}
