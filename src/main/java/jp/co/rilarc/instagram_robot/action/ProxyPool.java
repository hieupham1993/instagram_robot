/**
 * 
 */
package jp.co.rilarc.instagram_robot.action;

import java.util.HashMap;
import java.util.Map;

import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.Proxy;

/**
 * @author nghilt
 *
 */
public class ProxyPool {
	
	private final static Map<Integer, Proxy> proxyPool = new HashMap<>();
	
	public static Proxy getProxy(int userId, IAPIHelper dbHelper) {
		Proxy proxy = proxyPool.getOrDefault(userId, null);
		if(proxy == null) {
			proxy = dbHelper.getProxyByUserId(userId);
			
			// Add vao pool
			addToPool(userId, proxy);
		}
		return proxy;
	}
	
	public static void addToPool(int userId, Proxy proxy) {
		proxyPool.put(userId, proxy);
	}
}
