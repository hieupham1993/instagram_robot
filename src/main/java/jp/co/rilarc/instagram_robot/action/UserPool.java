/**
 *
 */
package jp.co.rilarc.instagram_robot.action;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 */
public class UserPool {

    final private static Logger logger = LogManager.getLogger(UserPool.class);

    private final static Map<Integer, Instagram> userPool = new HashMap<>();

    public static Instagram getInstagram(int userId, IAPIHelper dbHelper, Configuration config) throws InstagramCannotAccessException {
        Proxy proxy = ProxyPool.getProxy(userId, dbHelper);
        return getInstagram(userId, proxy, dbHelper, config);
    }

    public static Instagram getInstagram(int userId, Proxy proxy, IAPIHelper dbHelper, Configuration config) throws InstagramCannotAccessException {
        Instagram instagram = userPool.getOrDefault(userId, null);
        InstagramAccount instagramAccount;
        if (instagram == null) {
            instagramAccount = dbHelper.getInstagramAccountByToken();
            if (instagramAccount == null) {
                logger.error("Cannot found InstagramAccount of user " + userId);
                return null;
            }

            instagram = new Instagram(instagramAccount);
            instagram.setProxy(proxy);

            // Login vao instagram
            try {
                instagram.login(true, false);
            } catch (IOException | URISyntaxException e) {
                logger.error("error", e);
                throw new InstagramCannotAccessException(e.getMessage() + proxy);
            } catch (InstagramInvalidCredentialsException | InstagramUserInvalidException e) {
                logger.error("error", e);
                dbHelper.disableUser(e.getMessage());
                throw new InstagramCannotAccessException(e.getMessage() + proxy);
            } catch (InstagramCheckpointRequiredException e) {
                logger.error("error", e);
                dbHelper.updateInstagramConnectStatus(false, e.getMessage());
                throw new InstagramCannotAccessException(e.getMessage() + proxy);
            }

            // Add vao pool
            addToPool(instagram);
        }
        return instagram;
    }

    public static Instagram getInstagram(int userId, String username, String password, IAPIHelper dbHelper) throws InstagramCannotAccessException {
        Instagram instagram;
        InstagramAccount instagramAccount;
        instagramAccount = new InstagramAccount();
        instagramAccount.setUserId(userId);
        instagramAccount.setUsername(username);
        instagramAccount.setPassword(password);
        instagram = new Instagram(instagramAccount);

        // Login vao instagram
        try {
            boolean isSuccess = instagram.login(true, false);
            if (isSuccess) {
                // Add vao pool
                addToPool(instagram);
                return instagram;
            } else {
                return null;
            }
        } catch (IOException | URISyntaxException | InstagramSentryBlockException e) {
            logger.error("error", e);
            throw new InstagramCannotAccessException(e.getMessage());
        } catch (InstagramInvalidCredentialsException | InstagramUserInvalidException e) {
            logger.error("error", e);
            dbHelper.disableUser(e.getMessage());
            throw new InstagramCannotAccessException(e.getMessage());
        } catch (InstagramCheckpointRequiredException e) {
            logger.error("error", e);
            dbHelper.updateInstagramConnectStatus(false, e.getMessage());
            throw new InstagramCannotAccessException(e.getMessage());
        }


    }

    public static Instagram getCachedInstagram(int userId) {
        return userPool.getOrDefault(userId, null);
    }

    private static void addToPool(Instagram instagram) {
        int userId = instagram.getInstagramAccount().getUserId();
        userPool.put(userId, instagram);
    }
}
