/**
 *
 */
package jp.co.rilarc.instagram_robot.action;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.Action;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;

/**
 * @author nghilt
 */
public class ProxyAutoThread extends Thread {

    final private static Logger logger = LogManager.getLogger(ProxyAutoThread.class);

    private final Proxy proxy;

    private final IAPIHelper dbHelper;

    private final Configuration config;

    private Statistic statistic;

    public ProxyAutoThread(Proxy proxy, IAPIHelper IAPIHelper, Configuration config) {
        logger.info("Start thread for proxy [" + proxy.getId() + "][" + proxy.getProxyAddress() + "]");
        this.proxy = proxy;
        this.dbHelper = IAPIHelper;
        this.config = config;
    }

    @Override
    public void run() {

        while (true) {
            // Get All User of this thread
            List<User> unActiveUsers = getUnactiveRobotUsers();

            // Lan luot chay tung user
            for (User user : unActiveUsers) {
                runUser(user);

                // Wait for 3 ~ 7 minute before start new user
                Utils.waitForMinutes(3, 7);
            }
            Utils.waitForMinutes(3, 7);
        }
    }

    private void runUser(User user) {
        int userId = user.getId();

        ProxyPool.addToPool(userId, proxy);  // update proxy to cache

        dbHelper.updateRobotStatus(true);

        RobotSetting robotSetting = dbHelper.getRobotSettingByToken();

        // check sleep mode
        if (robotSetting.isSleepTime()) {
            return;
        }

        List<Action> actionList = getListAction(robotSetting);

        // User ko setting action nao thi stop ngay
        if (actionList.isEmpty()) {
            logger.info("[" + user.getId() + "][" + user.getEmail() + "] not setting any actions. So Stopped!");
            return;
        }

        Instagram instagram;
        try {
            instagram = UserPool.getInstagram(userId, proxy, dbHelper, config);
        } catch (InstagramCannotAccessException e) {
            logger.error("[" + user.getId() + "][" + user.getEmail() + "] cannot access to instagram. So Stopped!");
            return;
        }

        String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        this.statistic = dbHelper.getOrCreateStatistic(instagram.getInstagramAccount().getUsername(), today);

        AdvanceSetting advanceSetting = dbHelper.getAdvanceSetting();
        for (Action action : actionList) {
            BaseAction nextAction = null;
            switch (action) {
                case LIKE:
                    nextAction = new LikeAction(instagram, robotSetting, advanceSetting, dbHelper, config, statistic);
                    break;
                case FOLLOW:
                    nextAction = new FollowAction(instagram, robotSetting, advanceSetting, dbHelper, config, statistic);
                    break;
                case UNFOLLOW:
                    nextAction = new UnfollowAction(instagram, robotSetting, advanceSetting, dbHelper, config, statistic);
                    break;
                default:
                    break;
            }

            if (nextAction != null) {
                nextAction.runAction();

                // Wait for 1 ~ 3 minute before start new action
                Utils.waitForMinutes(1, 3);
            }
        }
    }

    private List<User> getUnactiveRobotUsers() {
        List<User> list = dbHelper.getUnactiveRobotUsersBelongProxy(proxy);
        Collections.shuffle(list);
        return list;
    }

    private List<Action> getListAction(RobotSetting robotSetting) {
        List<Action> listAction = new ArrayList<>();

        int likeActionNumber = 0;
        int followActionNumber = 0;
        int unfollowActionNumber = 0;

        if (!robotSetting.isDisableLike()) {
            if (robotSetting.isLikesHashtagAutoLike()) {
                likeActionNumber++;
            }

            if (robotSetting.isLikesLocationAutoLike()) {
                likeActionNumber++;
            }

            if (robotSetting.isLikesFollowingAutoLikeFeed()) {
                likeActionNumber++;
            }
        }

        if (!robotSetting.isDisableFollow() && !robotSetting.isUnfollowsBoostUnfollow()) {
            if (robotSetting.isFollowsHashtagAutoFollow()) {
                followActionNumber++;
            }

            if (robotSetting.isFollowsLocationAutoFollow()) {
                followActionNumber++;
            }

            if (robotSetting.isFollowsFollowersAutoFollowBack()) {
                followActionNumber++;
            }
        }

        if (robotSetting.isUnfollowsNotFollowingBackAutoUnfollow()) {
            unfollowActionNumber++;
        }

        if (likeActionNumber > 0)
            listAction.add(Action.LIKE);
        if (followActionNumber > 0)
            listAction.add(Action.FOLLOW);
        if (unfollowActionNumber > 0)
            listAction.add(Action.UNFOLLOW);

        Collections.shuffle(listAction); // Random action
        return listAction;
    }
}
