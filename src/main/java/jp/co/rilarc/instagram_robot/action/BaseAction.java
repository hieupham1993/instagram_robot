/**
 * 
 */
package jp.co.rilarc.instagram_robot.action;

import java.util.List;


import jp.co.rilarc.instagram_robot.exception.*;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Utils;

/**
 * @author nghilt
 *
 */
public abstract class BaseAction {
	
	final private static Logger logger = LogManager.getLogger(BaseAction.class);
	
	public static final int MAX_TIME_FOR_ONE_ACTION = 2;  // hours

	final protected Instagram mInstagram;
	final protected IAPIHelper dbHelper;
	final protected Statistic statistic;

	public static final String ACTION_LIKE = "like";
	public static final String ACTION_FOLLOW = "follow";
	public static final String ACTION_SEND_MESSAGE = "send_message";

	private final String myAction;
	protected final int mUserId;
	protected final String instagramUsername;
	private final Proxy proxy;

	
	public BaseAction(Instagram instagram, IAPIHelper dbHelper, Statistic statistic) {
		this.mInstagram = instagram;
		this.dbHelper = dbHelper;
		this.statistic = statistic;

		this.mUserId = instagram.getInstagramAccount().getUserId();
		this.instagramUsername = instagram.getInstagramAccount().getUsername();
		this.proxy = instagram.getProxy();
		myAction = getMyAction();
	}
	
	public void runAction() {
		
		List<String> actionMethods = getActionMethod();
		
		if(actionMethods.isEmpty()) {
			logger.info("No action method is setting");
			return;		// No method to like
		}
		
//		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//		Statistic statistic = dbHelper.getOrCreateStatistic(mUserId, instagramUsername, date);
		
		int remainingAction = getRemainAction();
//		int numberMediaToAction = remainingAction / actionMethods.size();	// Da check empty o tren roi nen ko can check zero devide nua
		
		/*
		 * Moi action chi chay tu 5 ~ 11 action, sau do dung lai chuyen sang action khac.
		 * Sau khi chay het 1 vong thi lai bat dau lai 
		 */
		int numberMediaToAction = Math.min(remainingAction, Signatures.randomInt(5, 11));

		long startTime = System.currentTimeMillis();
		for (String actionMethod : actionMethods) {
			
			if(isOverTimeOfAction(startTime)) {
				break;
			}
			
			// Check xem da dat den max like cua 1 ngay chua
			if (remainingAction <= 0) {
				logger.info(buildLog().append(": reach to max (").append(getActionSpeed()).append(") of this day "));
				String msg = "";
				if (this instanceof LikeAction) {
					msg = "本日の設定した分（" + getActionSpeed() + "回）のいいねを完了しました。";
					VLogger.append(msg);
					logger.info(msg);
				} else if (this instanceof FollowAction) {
					msg = "本日の設定した分（" + getActionSpeed() + "回）のフォローを完了しました。";
					VLogger.append(msg);
					logger.info(msg);
				} else if (this instanceof UnfollowAction) {
					msg = "本日の設定した分（" + getActionSpeed() + "回）のアンフォローを完了しました。";
					VLogger.append(msg);
					logger.info(msg);
				}

				break;
			}
			
			try {
				int numMediasRequestSuccess = runMethod(actionMethod, numberMediaToAction);
				if(numMediasRequestSuccess > 0) {
					updateStatisticInfo(statistic, numMediasRequestSuccess);
					remainingAction -= numMediasRequestSuccess;
				}else {
					logger.info("Action " + myAction + " by " + actionMethod + " khong chay duoc lan nao");
				}
			} catch (ActionBlockException e) {
				logger.error(buildLog(), e);
				if(e instanceof LikeBlockedException) {
					//dbHelper.disableAction(ACTION_LIKE, e.getMessage());
					VLogger.append("インスタグラムからいいね行動に対してスパム警告が出ています");
				}else if(e instanceof FollowBlockedException) {
					//dbHelper.disableAction(ACTION_FOLLOW, e.getMessage());
					VLogger.append("インスタグラムからフォロー行動に対してスパム警告が出ています");
				}else if(e instanceof UnfollowBlockedException) {
					// TODO Hien tai chua co chuc nang block unfollow
				}
				logger.info(buildLog().append(" Stop this action by spam. ").append(proxy));
				break; // Stop action
			} catch (InstagramCannotAccessException e) {
				logger.info(e.getMessage());
				dbHelper.updateInstagramConnectStatus(false, e.getMessage());
				logger.info(buildLog().append(" Stop this action by conectivity to instagram. ").append(proxy));
				if(e instanceof InstagramCheckpointRequiredException) {
					VLogger.append("ログインできませんでした。スマホにてインスタグラムアプリを開き、 「This was me」（白いボタン）をクリックしてから、もう一度、ボタンをクリックして、再度接続してください。");
				}
				break; // Stop action
			}

			Utils.waitFor(40, 2 * 60); 	// Wait for 40s ~ 2m
		}
	}
	
	protected abstract List<String> getActionMethod();
	protected abstract int getActionSpeed();
	protected abstract int getRemainAction();
	protected abstract void updateStatisticInfo(Statistic statistic, int numMediasActionSuccess);
	protected abstract int runMethod(String methodName, int maxNumMediaToAction) throws ActionBlockException, InstagramCannotAccessException;
	public abstract String getMyAction();
	
	public boolean isOverTimeOfAction(long startTime) {
		long nowTime = System.currentTimeMillis();
		return nowTime - startTime > MAX_TIME_FOR_ONE_ACTION * 60 * 1000;
	}
	
	public StringBuilder buildLog() {
		return new StringBuilder("[" + mUserId + "][" + instagramUsername + "][" + myAction + "]");
	}
}
