package jp.co.rilarc.instagram_robot.action;

import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.unfollow.Unfollow;
import jp.co.rilarc.instagram_robot.unfollow.UnfollowBoost;
import jp.co.rilarc.instagram_robot.unfollow.UnfollowNotFollowingBack;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class UnfollowAction extends BaseAction {
	final private static Logger logger = LogManager.getLogger(UnfollowAction.class);
	
	private Instagram mInstagram;
    final private RobotSetting mRobotSetting;
    final private Configuration config;
    private AdvanceSetting mAdvanceSetting;

    public UnfollowAction(Instagram instagram, RobotSetting robotSetting, AdvanceSetting advanceSetting,
						  IAPIHelper IAPIHelper, Configuration config, Statistic statistic) {
    		super(instagram, IAPIHelper, statistic);
    		
        mInstagram = instagram;
        mRobotSetting = robotSetting;
        mAdvanceSetting = advanceSetting;

        this.config = config;
    }

	@Override
	public List<String> getActionMethod() {
		List<String> listUnfollowMethod = new ArrayList<>();
		if (mRobotSetting.isUnfollowsBoostUnfollow()) {
			listUnfollowMethod.add(Statistic.UNFOLLOW_METHOD_UNFOLLOW_BOOST);
        } else {
			listUnfollowMethod.add(Statistic.UNFOLLOW_METHOD_NOT_FOLLOWING_BACK);
        }
		return listUnfollowMethod;
	}

	@Override
	public int getActionSpeed() {
		return config.getUnfollowSpeed(mRobotSetting.getUnfollowsAutoUnfollowSpeed());
	}

	@Override
	public void updateStatisticInfo(Statistic statistic, int numMediasActionSuccess) {
		dbHelper.increaseNumUnfollowsStatistic(statistic.getId());
		
//		statistic.setNumLikes(statistic.getNumUnfollows() + numMediasActionSuccess);
	}

	@Override
	public int runMethod(String methodName, int numberUsersToUnFollow) throws ActionBlockException, InstagramCannotAccessException {
		Unfollow unfollow;

        if (mRobotSetting.isUnfollowsBoostUnfollow()) {
            logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": unfollow by boost");

            unfollow = new UnfollowBoost(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToUnFollow);
        } else {
            logger.debug("account " + mInstagram.getInstagramAccount().getUsername() + ": unfollow by check day");

            unfollow = new UnfollowNotFollowingBack(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToUnFollow);
        }

        return unfollow.autoUnfollow();
	}

	@Override
	public String getMyAction() {
		return "Unfollow";
	}

	@Override
	protected int getRemainAction() {
		int remain = getActionSpeed() - statistic.getNumUnfollows();
		logger.info("Number unfollowed = " + statistic.getNumUnfollows());
		logger.info("Remain unfollow action = " + remain);
		return remain;
	}
	

//    public void runThread(long startTime) {
//    	String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//        Statistic statistic = dbHelper.getOrCreateStatistic(mUserId, mInstagram.getInstagramAccount().getUsername(), date);
//
//        while (System.currentTimeMillis() - startTime < duration * 60 * 1000) {
//            //check like per day
//            int maxUnfollows = config.getUnfollowSpeed(mRobotSetting.getUnfollowsAutoUnfollowSpeed());
//            if (statistic.getNumUnfollows() >= maxUnfollows) {
//                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": reach to max unfollow (" + maxUnfollows + ") of this day");
//                return;
//            }
//            Unfollow unfollow;
//
//            if (mRobotSetting.isUnfollowsBoostUnfollow()) {
//                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": unfollow by boost");
//
//                unfollow = new UnfollowBoost(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting);
//            } else {
//                logger.debug("account " + mInstagram.getInstagramAccount().getUsername() + ": unfollow by check day");
//
//                unfollow = new UnfollowNotFollowingBack(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting);
//            }
//
//            boolean likeResult = unfollow.autoUnfollow();
//
//            if (likeResult) {
//                //update unfollow statistic
//                dbHelper.increaseNumUnfollowsStatistic(statistic.getId());
//            }
//
//            try {
//                Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
}
