/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;

/**
 * @author nghilt
 *
 */
public class TestUser {
	private int userId;
	private String email;
	private String instagramUsername;
	private String instagramPassword;
	private String instagramId;
	
	public TestUser(int userId, String email, String instagramUsername, String instagramPassword, String instagramId) {
		super();
		this.userId = userId;
		this.email = email;
		this.instagramUsername = instagramUsername;
		this.instagramPassword = instagramPassword;
		this.instagramId = instagramId;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInstagramUsername() {
		return instagramUsername;
	}
	public void setInstagramUsername(String instagramUsername) {
		this.instagramUsername = instagramUsername;
	}
	public String getInstagramPassword() {
		return instagramPassword;
	}
	public void setInstagramPassword(String instagramPassword) {
		this.instagramPassword = instagramPassword;
	}
	public String getInstagramId() {
		return instagramId;
	}
	public void setInstagramId(String instagramId) {
		this.instagramId = instagramId;
	}
	@Override
	public String toString() {
		return "TestUser [userId=" + userId + ", email=" + email + ", instagramUsername=" + instagramUsername
				+ ", instagramPassword=" + instagramPassword + ", instagramId=" + instagramId + "]";
	}
	
	
}
