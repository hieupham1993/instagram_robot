package jp.co.rilarc.instagram_robot.tool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.helpers.ConnectionPool;
import jp.co.rilarc.instagram_robot.instagram.Security;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.tool.AssignNewProxy.NewProxy;
import jp.co.rilarc.instagram_robot.tool.model.ProxyResult;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;

public class ToolDatabaseHelper {
    final private static Logger logger = LogManager.getLogger(ToolDatabaseHelper.class);

	private static final boolean INVALID_USER = false;

	private static final boolean INSTAGRAM_NOT_CONNECTED = false;

    private ConnectionPool connectionPool = null;

    public ToolDatabaseHelper(Configuration config) {
        connectionPool = new ConnectionPool(config);
    }

	public boolean updateBadProxy(Proxy proxy) {
		Connection connection = null;
		boolean result = false;

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "UPDATE proxies SET is_good = ? where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setBoolean(1, false);
            preparedStatement.setInt(2, proxy.getId());
            preparedStatement.executeUpdate();
            connection.commit();
            result = true;
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }
        return result;
	}

	/**
	 * Update proxy moi va enable like
	 * @param userId
	 * @param proxyId
	 * @return
	 */
	public boolean updateNewProxy(int userId, int proxyId) {
		Connection connection = null;
		boolean result = false;

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "UPDATE users SET proxy_id = ? where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, proxyId);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();
            
            String sqlEnableLike = "UPDATE robot_settings set disable_like = ? where user_id = ?";
            PreparedStatement preparedStatement2 = connection.prepareStatement(sqlEnableLike);
            preparedStatement2.setBoolean(1, false);
            preparedStatement2.setInt(2, userId);
            preparedStatement2.executeUpdate();
            
            connection.commit();
            result = true;
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }
        return result;
	}
	
	public void disableUser(int userId, String notes) {
        Connection connection = null;

        logger.debug("Disable user due to invalid " + userId);

        try {
        	BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "UPDATE users SET active = ?, proxy_id = NULL, notes = ?, updated_at = NOW() " +
                    "WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setBoolean(1, INVALID_USER);
            preparedStatement.setString(2, notes);
            preparedStatement.setInt(3, userId);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	
	public void disableInstagramConnectStatus(int userId, String notes) {
        Connection connection = null;

        logger.debug("Update robot status user " + userId);

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "UPDATE users SET is_instagram_connected = ?, proxy_id = NULL, notes = ?, updated_at = NOW() " +
                    "WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setBoolean(1, INSTAGRAM_NOT_CONNECTED);
            preparedStatement.setString(2, notes);
            preparedStatement.setInt(3, userId);
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

	public InstagramAccount getInstagramAccountByUserId(int userId) {
		Connection connection = null;
        InstagramAccount instagramAccount = null;

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "SELECT *, UNIX_TIMESTAMP(`last_time_login`) as last_time_login_timestamp" +
                    " FROM instagram_accounts" +
                    " WHERE user_id = ? ";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                if (instagramAccount == null)
                    instagramAccount = new InstagramAccount();

                instagramAccount.setId(rs.getInt("id"));
                instagramAccount.setUsername(rs.getString("username"));
                instagramAccount.setPassword(Security.decrypt(rs.getString("password"), Const.SECURITY_KEY));
                instagramAccount.setAccountId(rs.getString("instagram_userid"));
                instagramAccount.setDeviceId(rs.getString("device_id"));
                instagramAccount.setPhoneId(rs.getString("phone_id"));
                instagramAccount.setCsrfToken(rs.getString("csrf_token"));
                instagramAccount.setUuid(rs.getString("uuid"));
                instagramAccount.setUserAgent(rs.getString("user_agent"));
                instagramAccount.setDeviceString(rs.getString("device_string"));
                instagramAccount.setLastTimeLogin(rs.getLong("last_time_login_timestamp"));
                instagramAccount.setRankToken(rs.getString("rank_token"));
                instagramAccount.setUserId(rs.getInt("user_id"));
            }
            connection.commit();
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }

        return instagramAccount;
	}

	public List<User> getListDisableLikeUsers() {
		Connection connection = null;
        List<User> users = new ArrayList<>();

        logger.debug("Start get list user!");

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

//            String SQL = "SELECT users.id, email, proxy_id FROM users "
//            		+ " LEFT JOIN robot_settings ON user_id = users.id "
//            		+ " WHERE disable_like = ? and active = ? and is_robot_working = ? and is_instagram_connected = ?";
            String SQL = "SELECT users.id, email, proxy_id FROM users "
            		+ " LEFT JOIN robot_settings ON user_id = users.id "
            		+ " WHERE disable_like = ? and active = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setBoolean(1, true);
            preparedStatement.setBoolean(2, true);
//            preparedStatement.setBoolean(3, true);
//            preparedStatement.setBoolean(4, true);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setProxyId(rs.getInt("proxy_id"));
                users.add(user);
            }
        } catch (SQLException ex) {
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }
        
        return users;
	}

	public Map<Proxy, Integer> getGoodProxies() {
		Connection connection = null;
		Map<Proxy, Integer> goodProxies = new HashMap<>();

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();
            
            String SQL = "SELECT proxy_id, proxy_address, proxy_port, proxy_username, proxy_password, count(*) as usednum "
            		+ " FROM `users` left join proxies on proxy_id =  proxies.id "
            		+ " WHERE is_good = ? group by proxy_id order by usednum, proxy_id";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setBoolean(1, true);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
            	int usednum = rs.getInt("usednum");
            	if(usednum < 30) {
	            	Proxy proxy = new Proxy();
	                proxy.setId(rs.getInt("proxy_id"));
	                proxy.setProxyAddress(rs.getString("proxy_address"));
	                proxy.setProxyPort(rs.getInt("proxy_port"));
	                proxy.setProxyUsername(rs.getString("proxy_username"));
	                proxy.setProxyPassword(rs.getString("proxy_password"));
	                goodProxies.put(proxy, usednum);
            	}
            }
        } catch (SQLException ex) {
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }

        return goodProxies;
	}
	
	public Map<Proxy, Integer> getGoodProxies(String proxyAddress) {
		Connection connection = null;
		Map<Proxy, Integer> goodProxies = new HashMap<>();

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();
            
            String SQL = "SELECT proxies.id as proxy_id, proxy_address, proxy_port, proxy_username, proxy_password, count(*) as usednum "
            		+ " FROM proxies left join `users` on proxy_id = proxies.id "
            		+ " WHERE proxy_address = ? GROUP BY proxies.id order by usednum, proxy_id ";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, proxyAddress);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
            	int usednum = rs.getInt("usednum");
            	if(usednum < 30) {
	            	Proxy proxy = new Proxy();
	                proxy.setId(rs.getInt("proxy_id"));
	                proxy.setProxyAddress(rs.getString("proxy_address"));
	                proxy.setProxyPort(rs.getInt("proxy_port"));
	                proxy.setProxyUsername(rs.getString("proxy_username"));
	                proxy.setProxyPassword(rs.getString("proxy_password"));
	                goodProxies.put(proxy, usednum);
            	}
            }
        } catch (SQLException ex) {
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }

        return goodProxies;
	}

	public boolean updateProxy(String email, int proxyId) {
        Connection connection = null;

        boolean result = false;
        try {
        	BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "UPDATE users SET active = 1, is_instagram_connected = 1, proxy_id = ?, notes = ?, updated_at = NOW() " +
                    "WHERE email = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, proxyId);
            preparedStatement.setString(2, "Assigned new proxy");
            preparedStatement.setString(3, email);
            preparedStatement.executeUpdate();
            connection.commit();
            result = true;
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}

	public jp.co.rilarc.instagram_robot.tool.model.User getUserInfo(String email) {
		Connection connection = null;

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "Select distinct u.id as user_id, email, u.active, u.is_robot_working, u.is_active_robot, u.is_instagram_connected, r.disable_like, r.disable_follow, r.disable_send_message, u.proxy_id, p.proxy_address from users as u " + 
            		" left join robot_settings as r on r.user_id = u.id " + 
            		" left join proxies as p on p.id = u.proxy_id " + 
            		" where u.email = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, email);
            ResultSet rs = preparedStatement.executeQuery();
            jp.co.rilarc.instagram_robot.tool.model.User user = null;
            if (rs.next()) {
            		user = new jp.co.rilarc.instagram_robot.tool.model.User();
            		user.setUserid(rs.getInt("user_id"));
    				user.setEmail(rs.getString("email"));
    				user.setActive(rs.getBoolean("active"));
    				user.setIs_robot_working(rs.getBoolean("is_robot_working"));
    				user.setIs_active_robot(rs.getBoolean("is_active_robot"));
    				user.setIs_instagram_connected(rs.getBoolean("is_instagram_connected"));
    				user.setDisable_like(rs.getBoolean("disable_like"));
    				user.setDisable_follow(rs.getBoolean("disable_follow"));
    				user.setDisable_sendmessage(rs.getBoolean("disable_send_message"));
    				user.setProxy_id(rs.getInt("proxy_id"));
    				user.setProxy_address(rs.getString("proxy_address"));
            }
            return user;
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
	}
	
	public List<jp.co.rilarc.instagram_robot.tool.model.User> getListUser() {
		
		List<jp.co.rilarc.instagram_robot.tool.model.User> listUser = new ArrayList<>();
		
		Connection connection = null;

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "Select distinct u.id as user_id, email, u.active, u.is_robot_working, u.is_active_robot, u.is_instagram_connected, r.disable_like, r.disable_follow, r.disable_send_message, u.proxy_id, p.proxy_address from users as u " + 
            		" left join robot_settings as r on r.user_id = u.id " + 
            		" left join proxies as p on p.id = u.proxy_id order by u.id asc";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            ResultSet rs = preparedStatement.executeQuery();
            jp.co.rilarc.instagram_robot.tool.model.User user = null;
            while (rs.next()) {
            		user = new jp.co.rilarc.instagram_robot.tool.model.User();
            		user.setUserid(rs.getInt("user_id"));
    				user.setEmail(rs.getString("email"));
    				user.setActive(rs.getBoolean("active"));
    				user.setIs_robot_working(rs.getBoolean("is_robot_working"));
    				user.setIs_active_robot(rs.getBoolean("is_active_robot"));
    				user.setIs_instagram_connected(rs.getBoolean("is_instagram_connected"));
    				user.setDisable_like(rs.getBoolean("disable_like"));
    				user.setDisable_follow(rs.getBoolean("disable_follow"));
    				user.setDisable_sendmessage(rs.getBoolean("disable_send_message"));
    				user.setProxy_id(rs.getInt("proxy_id"));
    				user.setProxy_address(rs.getString("proxy_address"));
    				listUser.add(user);
            }
        } catch (SQLException ex) {
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return listUser;
	}

	public List<NewProxy> getNewProxies() {
		String sql = "select p.id as proxy_id, number_user, count(u.id) as assigned_users "
				+ " from proxies as p left join users as u on u.proxy_id = p.id where p.id > ? group by p.id";
		Connection connection = null;
		
		
		List<NewProxy> listProxies = new ArrayList<>();
        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, 305);
            ResultSet rs = preparedStatement.executeQuery();
            NewProxy proxy;
            while (rs.next()) {
            		proxy = new NewProxy(
            				rs.getInt("proxy_id"),
            				rs.getInt("assigned_users"),
            				rs.getInt("number_user")
            				);
            		listProxies.add(proxy);
            }
        } catch (SQLException ex) {
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return listProxies;
	}

	public int activeUser() {
		Connection connection = null;

		int result = 0;
        try {
        	BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "UPDATE users SET active = 1, notes = ?, updated_at = NOW() " +
                    "WHERE active = 0 and is_instagram_connected = 1";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, "Reactive user");
            result = preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            logger.error("ERROR: ", ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
	}
	
	public List<Proxy> getAllProxies() {
		Connection connection = null;
        List<Proxy> proxies = new ArrayList<>();

        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "SELECT *" +
                    " FROM proxies" +
                    " order by id asc";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            ResultSet rs = preparedStatement.executeQuery();
            Proxy proxy;
            while (rs.next()) {
                proxy = new Proxy();
                proxy.setId(rs.getInt("id"));
                proxy.setProxyAddress(rs.getString("proxy_address"));
                proxy.setProxyPort(rs.getInt("proxy_port"));
                proxy.setProxyUsername(rs.getString("proxy_username"));
                proxy.setProxyPassword(rs.getString("proxy_password"));
                
                proxies.add(proxy);
            }
            connection.commit();
        } catch (SQLException ex) {
            // handle any errors
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException e) {
            	logger.error("ERROR: ", ex);
            }
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }

        return proxies;
	}
	
	public ProxyResult getProxyResult(Proxy proxy) {
		Connection connection = null;
		int proxyId = proxy.getId();
        try {
            BasicDataSource dataSource = connectionPool.getDataSource();
            connection = dataSource.getConnection();

            String SQL = "Select \n" + 
            		"(SELECT count(*) FROM `users` where proxy_id = ?) as user_num,\n" + 
            		"(SELECT count(*) FROM `users` as u left join robot_settings as r on r.user_id = u.id where r.disable_like = 1 and proxy_id = ?) as disable_like_num,\n" + 
            		"(SELECT count(*) FROM `users` as u left join robot_settings as r on r.user_id = u.id where r.disable_follow = 1 and proxy_id = ?) as disable_follow_num,\n" + 
            		"(SELECT count(*) FROM `users` as u left join robot_settings as r on r.user_id = u.id where r.disable_send_message = 1 and proxy_id = ?) as disable_send_message_num;";
            
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, proxyId);
            preparedStatement.setInt(2, proxyId);
            preparedStatement.setInt(3, proxyId);
            preparedStatement.setInt(4, proxyId);
            ResultSet rs = preparedStatement.executeQuery();
            
            ProxyResult proxyResult;
            if (rs.next()) {
	            	proxyResult = new ProxyResult();
	            	proxyResult.setId(proxyId);
	            	proxyResult.setAddress(proxy.getProxyAddress());
	            	proxyResult.setUser_num(rs.getInt("user_num"));
	            	proxyResult.setDisable_like_num(rs.getInt("disable_like_num"));
	            	proxyResult.setDisable_follow_num(rs.getInt("disable_follow_num"));
	            	proxyResult.setDisable_send_message_num(rs.getInt("disable_send_message_num"));
                
                return proxyResult;
            }
        } catch (SQLException ex) {
            logger.error("ERROR: ", ex);
        } finally {
            try {
                if (connection != null) connection.close();
            } catch (SQLException e) {
                logger.error("ERROR: ", e);
            }
        }

        return null;
	}
}
