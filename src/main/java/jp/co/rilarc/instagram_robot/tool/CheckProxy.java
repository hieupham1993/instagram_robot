/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;


import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.tool.model.ProxyResult;
import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 *
 */
public class CheckProxy {
	
	final protected static Logger logger = LogManager.getLogger(CheckProxy.class);
	
	final private ToolDatabaseHelper dbHelper;
	
	public CheckProxy() {
		Configuration config = new Configuration();
		dbHelper = new ToolDatabaseHelper(config);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("------------------------------ START ---------------------------");
		CheckProxy program = new CheckProxy();
		program.run();
		logger.info("Completed");
	}
	
	int run() {
		List<Proxy> listProxy = dbHelper.getAllProxies();
		
//		List<ProxyResult> listProxyResult = new ArrayList<>();
		for(Proxy proxy : listProxy) {
			ProxyResult proxyResult = dbHelper.getProxyResult(proxy);
			if(proxyResult != null) {
				logger.info(proxyResult);
			}
		}
		return 0;
	}
}
