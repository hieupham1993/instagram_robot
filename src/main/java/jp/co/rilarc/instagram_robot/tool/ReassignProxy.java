/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.phantomjs.RunJs;
import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 *
 */
public class ReassignProxy {
	
	final protected static Logger logger = LogManager.getLogger(ReassignProxy.class);
	
	static final int CHECK_NOT_PASS_TIME = 5; 
	
	final Map<Proxy, Integer> proxies;
	final Map<Proxy, Integer> badProxies;
	final Set<Proxy> passedProxies;
	
	final List<String> testMedias;
	final Configuration config;
	
    final private ToolDatabaseHelper dbHelper;
    
    private static int checkingUserNumber = 0;
    
    final List<User> listActiveUser;
    
	public ReassignProxy() {
		config = new Configuration();
		dbHelper = new ToolDatabaseHelper(config);
		
		badProxies = new HashMap<>();
		passedProxies = new HashSet<>();
		
		proxies = getGoodProxies();
		testMedias = new ArrayList<>();
		createTestMedia();
		
		listActiveUser = getActiveUser();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("Start ReassignProxy");
		try{
			ReassignProxy instance = new ReassignProxy();
			instance.doMain();
		}catch(Exception e) {
			logger.error("error", e);
		}
		logger.info("End ReassignProxy");
	}
	
	public void doMain2() {
		for(Proxy proxy : proxies.keySet()) {
			checkProxy(proxy);
		}
	}
	
	private void checkProxy(Proxy proxy) {
		for(User user : listActiveUser) {
			InstagramAccount instagramAccount = dbHelper.getInstagramAccountByUserId(user.getId());
			boolean loggedin = tryToLogin(instagramAccount);
			if(!loggedin) {
				continue;
			}
			
			boolean validProxy = tryToLike(instagramAccount, proxy);
		}
		
	}

	public void doMain() {
		List<User> listActiveUser = getActiveUser();
		int totalUser = listActiveUser.size();
		logger.info("Reassign Proxy for " + totalUser + " users");
		for(User user : listActiveUser) {
			logger.info(String.format("Checking for %d/%d user(%s)", ++checkingUserNumber, totalUser, user.getEmail()));
			checkForUser(user);
			removeBadProxy();
		}
	}

	private void checkForUser(User user) {
		InstagramAccount instagramAccount = dbHelper.getInstagramAccountByUserId(user.getId());
		boolean loggedin = tryToLogin(instagramAccount);
		if(!loggedin) {
			return;
		}
		
		boolean validProxy = false;
		
		// Uu tien check proxy da assign
		Proxy currentProxy = getProxy(user.getProxyId());
		if(currentProxy != null) {
			validProxy = checkForUser(instagramAccount, currentProxy);
		}
		
		if(validProxy) {
			return;
		}
		
		// Sau do check nhung proxy da pass
		validProxy = checkUserWithListProxies(instagramAccount, passedProxies);
		
		if(validProxy) {
			return;
		}
		
		// Cuoi cung moi check nhung proxy khac
		validProxy = checkUserWithListProxies(instagramAccount, proxies.keySet());
		if(!validProxy) {
			logger.warn(String.format("[Failed] cannot reassign proxy for user %s (%s)", 
					user.getId(), user.getEmail())); 
		}
	}
	
	private boolean checkUserWithListProxies(InstagramAccount instagramAccount, Set<Proxy> listProxy) {
		boolean validProxy = false;
//		int retry = 0;
		for(Proxy proxy : listProxy) {
			validProxy = checkForUser(instagramAccount, proxy);
			if(validProxy) {
				break;
			}
//			retry++;
//			if(retry > 1) { // Tam thoi ko retry
//				break;
//			}
			logger.info("Switch proxy");
		}
		return validProxy;
	}

	private boolean checkForUser(InstagramAccount instagramAccount, Proxy proxy) {
		boolean validProxy = false;
		
		if(validProxy) {
			validProxy = tryToLike(instagramAccount, proxy);
		}
		
		if(validProxy) {
			updateProxy(instagramAccount, proxy);
		}else {
			int badTimes = badProxies.getOrDefault(proxy, 0);
			badProxies.put(proxy, ++badTimes);
			logger.info("Proxy " + proxy.getProxyAddress() + " not passed. Try another.");
		}
		
		return validProxy;
	}

	private void removeBadProxy() {
		List<Proxy> removeProxies = new ArrayList<>();
		for(Proxy proxy : badProxies.keySet()) {
			int badTimes = badProxies.get(proxy);
			if(badTimes > CHECK_NOT_PASS_TIME) {
				// TODO Update notes (tuong lai)
				dbHelper.updateBadProxy(proxy);
				removeProxies.add(proxy);
			}
		}
		
		for(Proxy proxy : removeProxies) {
			proxies.remove(proxy);
			badProxies.remove(proxy);
			logger.warn(String.format("[Failed] Proxy %s is blocked", proxy.getProxyAddress()));
		}
	}
	
	private void updateProxy(InstagramAccount instagramAccount, Proxy proxy) {
		// Update proxy moi va enable like
		dbHelper.updateNewProxy(instagramAccount.getUserId(), proxy.getId());
		logger.info(String.format("[Success] Set proxy %d (%s) for user %d (%s)", 
				proxy.getId(), proxy.getProxyAddress(), instagramAccount.getUserId(), instagramAccount.getUsername()));
		passedProxies.add(proxy);
		int deviceNum = proxies.getOrDefault(proxy, 0);
		proxies.put(proxy, ++deviceNum);
		if(deviceNum > 29) {
			//TODO update device to table proxies
			
			// Proxy nay da check du so luong device nen khong check tiep
			proxies.remove(proxy);
			passedProxies.remove(proxy);
		}
	}

	private boolean tryToLike(InstagramAccount instagramAccount, Proxy proxy) {
		String mediaId = getRandomMediaId();
		boolean likeSuccess = RunJs.like(instagramAccount, mediaId, proxy);
//		int retry = 0;
//        while(!likeSuccess && retry < 2) { 	// Thu login 2 lan
//        	mediaId = getRandomMediaId();
//        	likeSuccess = RunJs.like(instagramAccount, mediaId, proxy);
//        	retry++;
//        	logger.info(String.format("Retry checking like function for user have instagram_id = %s with proxy %s", 
//    				instagramAccount.getUsername(), proxy.getProxyAddress())); 
//        	Utils.waitFor(3);	// wait 3 seconds
//        }
		return likeSuccess;
	}

//	private boolean tryToLogin(InstagramAccount instagramAccount, Proxy proxy) {
//		// login by phantomjs
//        boolean loginResult = RunJs.login(instagramAccount, proxy);
//        if(!loginResult) {
//        	// Login by API
//        	loginResult = tryToLogin(instagramAccount);
//        }
////        int retry = 0;
////        while(!loginPhantomResult && retry < 2) { 	// Thu login 2 lan
////        	loginPhantomResult = RunJs.login(instagramAccount, proxy);
////        	retry++;
////        	Utils.waitFor(3);	// wait 3 seconds
////        }
//        
//        logger.info("User " + instagramAccount.getUsername() + " logged in " + loginResult);
//        
//        return loginResult;
//	}
	
	private boolean tryToLogin(InstagramAccount instagramAccount) {
		// Login by API
    	Instagram instagram = new Instagram(instagramAccount, null);
		try {
			return instagram.login(true, config.isEnablePhantomjs());
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (URISyntaxException e) {
			logger.error(e.getMessage());
		} catch (InstagramInvalidCredentialsException | InstagramUserInvalidException e) {
			// Disable user
			dbHelper.disableUser(instagramAccount.getUserId(), e.getMessage());
			logger.error(e.getMessage());
		} catch (InstagramCheckpointRequiredException e) {
			dbHelper.disableInstagramConnectStatus(instagramAccount.getUserId(), e.getMessage());
			logger.error(e.getMessage());
		} catch (InstagramSentryBlockException e) {
			logger.error(e.getMessage());
		}
		return false;
	}

	private List<User> getActiveUser() {
		List<User> listUser = dbHelper.getListDisableLikeUsers();
		Collections.shuffle(listUser);
		return listUser;
	}

	private Map<Proxy, Integer> getGoodProxies() {
		return dbHelper.getGoodProxies();
	}
	
	private Proxy getProxy(int id) {
		if(id == 0) {
			return null;
		}
		
		for(Proxy proxy : proxies.keySet()) {
			if(proxy.getId() == id) {
				return proxy;
			}
		}
		return null;
	}

	private String getRandomMediaId() {
		int size = testMedias.size();
		int idx = Signatures.randomInt(0, size - 1);
		return testMedias.get(idx);
	}
	
	private void createTestMedia() {
		testMedias.add("BbPrweSlSYR");
		testMedias.add("BbQo_swjASI");
		testMedias.add("BbRMCeXnRmH");
		testMedias.add("BbRKruZF1o3");
		testMedias.add("BbRITLzD1Lk");
		testMedias.add("BbRIBJKgTcK");
		testMedias.add("BbRFzcGHse2");
		testMedias.add("BbRE-3PFZC2");
		testMedias.add("BbRELrZg6cM");
		testMedias.add("BbRDSqAhNZc");
		testMedias.add("BbQ_ldcgJ4K");
		testMedias.add("BbQ-7e0lVkA");
		testMedias.add("BbQ69PIF_F5");
		testMedias.add("BbQ5VTaHKx3");
		testMedias.add("BbQ5T2fhzZw");
		testMedias.add("BbRRBkRDupv");
		testMedias.add("BbRRBUtlpIc");
		testMedias.add("BbRRBMyFxD5");
		testMedias.add("BbRRBATlqAq");
		testMedias.add("BbRRA9Bly1V");
		testMedias.add("BbRRA63nBgM");
		testMedias.add("BbRRA6fHS_X");
		testMedias.add("BbRRA49g9zn");
		testMedias.add("BbRRAsuAv2W");
		testMedias.add("BbRRAq_HT0Z");
		testMedias.add("BbRRAoqFkxb");
		testMedias.add("BbRVp7wgWaF");
		testMedias.add("BbRVnT3FKGJ");
		testMedias.add("BbRVltOjb0I");
		testMedias.add("BbRVksFHREl");
		testMedias.add("BbRVXmkFMpO");
		testMedias.add("BbRVXPnjbpu");
		testMedias.add("BbRVRLdhZXL");
		testMedias.add("BbRVQ7OgGZK");
		testMedias.add("BbRVMlMlIXU");
		testMedias.add("BbRVHXWBBPQ");
		testMedias.add("BbRVGmaAcQv");
		testMedias.add("BbRVE7-B1Ra");
		testMedias.add("BbRVCnkDWyc");
		testMedias.add("BbRaAaxhwrB");
		testMedias.add("BbRZsQrh4x9");
		testMedias.add("BbQO4lKngR9");
		testMedias.add("BbQKeaBjirp");
		testMedias.add("BbPa5Meg-yt");
		testMedias.add("BbPHOgChM4s");
		testMedias.add("BbO2omCFwYV");
		testMedias.add("BbOqUt1FwIi");
		testMedias.add("BbOlXKeBrYe");
		testMedias.add("BbOd-qmlITb");
		testMedias.add("BbOZlxUFOhp");
		testMedias.add("BbOTOm1DSIp");
		testMedias.add("BbOGy3aBsXt");
		testMedias.add("BbRigmZleXq");
		testMedias.add("BbRiMHwAaaI");
		testMedias.add("BbRhn-RlWWW");
		testMedias.add("BbRhZbfHV5H");
		testMedias.add("BbRgr6vAOap");
		testMedias.add("BbRgoUShj_q");
		testMedias.add("BbRfz0vh2HS");
		testMedias.add("BbRfhT9BS7e");
		testMedias.add("BbRfa2cBIxl");
		testMedias.add("BbRfPAEF596");
		testMedias.add("BbRhUk5hGbD");
		testMedias.add("BbRhPNgBOcv");
		testMedias.add("BbRhJ4thNRE");
		testMedias.add("BbRhFm0B7hr");
		testMedias.add("BbRhA8pBY_u");
		testMedias.add("BbRg6TEhtHp");
		testMedias.add("BbRgyrPhJQs");
		testMedias.add("BbQVWC5D5BB");
	}
}
