package jp.co.rilarc.instagram_robot.tool.model;

public class ProxyResult {
	private int id; 
	private String address;
	private int user_num;
	private int disable_like_num;
	private int disable_follow_num;
	private int disable_send_message_num;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getUser_num() {
		return user_num;
	}
	public void setUser_num(int user_num) {
		this.user_num = user_num;
	}
	public int getDisable_like_num() {
		return disable_like_num;
	}
	public void setDisable_like_num(int disable_like_num) {
		this.disable_like_num = disable_like_num;
	}
	public int getDisable_follow_num() {
		return disable_follow_num;
	}
	public void setDisable_follow_num(int disable_follow_num) {
		this.disable_follow_num = disable_follow_num;
	}
	public int getDisable_send_message_num() {
		return disable_send_message_num;
	}
	public void setDisable_send_message_num(int disable_send_message_num) {
		this.disable_send_message_num = disable_send_message_num;
	}
	
	public static String createCSVHeader() {
		return "ProxyId,ProxyAddress,現在動かしてる人の数,いいねブロック,フォローブロック,メッセージ送信ブロック";
	}
	@Override
	public String toString() {
		return String.format(
				"%s,%s,%s,%s,%s,%s",
				id, address, user_num, disable_like_num, disable_follow_num, disable_send_message_num);
	}
	
}
