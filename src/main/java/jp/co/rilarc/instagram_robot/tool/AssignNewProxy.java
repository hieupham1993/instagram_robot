/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 *
 */
public class AssignNewProxy {
	
	final protected static Logger logger = LogManager.getLogger(AssignNewProxy.class);
	
	final private ToolDatabaseHelper dbHelper;

	public AssignNewProxy() {
		Configuration config = new Configuration();
		dbHelper = new ToolDatabaseHelper(config);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AssignNewProxy program = new AssignNewProxy();
		program.run();
	}
	
	void run() {
		List<String> listEmails = getEmails();
		List<NewProxy> proxies = getProxies();
		
//		int minIdx = listEmails.size();
//		if(minIdx > proxies.size()) {
//			minIdx = proxies.size();
//		}
		
		Iterator<NewProxy> itr = proxies.iterator();
		NewProxy proxy = itr.next();
		for(String email : listEmails) {
			boolean updateSuccess = updateProxy(email, proxy);
			if(!updateSuccess) {
				while(!updateSuccess && itr.hasNext()) {
					proxy = itr.next();
					updateSuccess = updateProxy(email, proxy);
				}
			}
		}
	}
	
	boolean updateProxy(String email, NewProxy proxy) {
		if(proxy.isEnough()) {
			return false;
		}
		
		boolean updateSuccess = dbHelper.updateProxy(email, proxy.id);
		if(updateSuccess) {
			proxy.incrementCurrentNumUser();
		}
		logger.info("Update success proxy " + proxy.id + " for user with email " + email);
		return updateSuccess;
	}
	
	private static List<String> getEmails() {
		List<String> emails = new ArrayList<>();
		
//		emails.add("aldebarank9254@gmail.com");
//		emails.add("yuzuyuzuhoncho@gmail.com");
//		emails.add("zincanmake117@gmail.com");
//		emails.add("nexttazzy@gmail.com");
//		emails.add("shiawase7yume@gmail.com");
//		emails.add("ikzwyj01w8@gmail.com");
//		emails.add("otawa710@gmail.com");
//		emails.add("oumeoku1@gmail.com");
//		emails.add("kakutougi.no1@i.softbank.jp");
//		emails.add("accomplish.4652@gmail.com");
//		emails.add("7summer21@gmail.com");
//		emails.add("cod26ipmm@gmail.com");
//		emails.add("cosumowind@gmail.com");
//		emails.add("jky11322@gmail.com");
//		emails.add("8226desiree@gmail.com");
//		emails.add("hide.your-face.1964-1998@ezweb.ne.jp");
//		emails.add("fkmhkato@gmail.com");
//		emails.add("kakenozo@gmail.com");
//		emails.add("kcglobal.b@gmail.com");
//		emails.add("masako.moody9988@gmail.com");
//		emails.add("aaaux108@yahoo.co.jp");
//		emails.add("infouaoei@gmail.com");
//		emails.add("nakata823k@gmail.com");
//		emails.add("mirhsi6685@yahoo.co.jp");
//		emails.add("ht_0302@yahoo.co.jp");
//		emails.add("anfuu0114@gmail.com");
//		emails.add("tk.1031.fishing@i.softbank.jp");
//		emails.add("efuemuai38@gmail.com");
//		emails.add("rummybusters1221@gmail.com");
//		emails.add("bliss-world@i.softbank.jp");
//		emails.add("jiiji.insta@gmail.com");
//		emails.add("coma.0469@gmail.com");
//		emails.add("comunica0612@gmail.com");
//		emails.add("kms.mcd-s-style0326@softbank.ne.jp");
//		emails.add("dethnote77777@yahoo.co.jp");
//		emails.add("tuya3790@gmail.com");
//		emails.add("ty1510002@gmail.com");
//		emails.add("fuyasou.tomodati@gmail.com");
//		emails.add("fumi.8yagi@gmail.com");
//		emails.add("kikotan@gmail.com");
//		emails.add("tazuko.1955.12.11@gmail.com");
//		emails.add("aayalove46496@gmail.com");
//		emails.add("yasuhikomotoki1017@gmail.com");
//		emails.add("mukumitorunurse@gmail.com");
//		emails.add("ple07@icloud.com");
//		emails.add("arai.iphone@gmail.com");
//		emails.add("inukkonaonao1@gmail.com");
//		emails.add("teamway2000@gmail.com");
//		emails.add("oityandasukedo+100@gmail.com");
//		emails.add("kofuku.kenkou.kokoroheian@ezweb.ne.jp");
//		emails.add("pxp00416@nifty.com");
//		emails.add("04130486to@gmail.com");
//		emails.add("tnetmq@gmail.com");
//		emails.add("hvkoh500@yahoo.co.jp");
//		emails.add("fu9ro1038@goo.jp");
//		emails.add("satoai13579@ezweb.ne.jp");
//		emails.add("e2kyoto@yahoo.co.jp");
//		emails.add("zururu893@gmail.com");
//		emails.add("ekomu.hitosi.t1@gmail.com");
//		emails.add("zettyan69@gmail.com");
//		emails.add("mnshima41@gmail.com");
//		emails.add("chieko.matsu5108@ezweb.ne.jp");
//		emails.add("pukapuka0928@gmail.com");
//		emails.add("kousei4247.001@gmail.com");
//		emails.add("kenjiman0811@gmail.com");
//		emails.add("akira.s@a-net.biz");
//		emails.add("hidehidematsu@yahoo.co.jp");
//		emails.add("bssuc1610@ybb.ne.jp");
//		emails.add("souka@yacht.ocn.ne.jp");
//		emails.add("intimate0420@gmail.com");
//		emails.add("snowwhite8409@yahoo.co.jp");
//		emails.add("w12k20.4s6.10a22w@i.softbank.jp");
//		emails.add("on5kazun@gmail.com");
//		emails.add("yoko.m.9701@gmail.com");
//		emails.add("inamura8@camel.plala.or.jp");
//		emails.add("liebe.lif.mk@gmail.com");
//		emails.add("w.yama.1209864@gmail.com");
//		emails.add("changye-s@earth.ocn.ne.jp");
//		emails.add("h.sima13@me.com");
//		emails.add("gangan1732@yahoo.co.jp");
//		emails.add("m.create18@gmail.com");
//		emails.add("20010220yugi@gmail.com");
//		emails.add("bombom1105@via.tokyo.jp");
//		emails.add("daisen.11@gmail.com");
//		emails.add("nobuhito0416@nobuno-ie.com");
//		emails.add("ohsaki60528@gmail.com");
//		emails.add("jibunwoikiru12345@gmail.com");
//		emails.add("abcdemag111@yahoo.co.jp");
//		emails.add("kizuna.natukage@gmail.com");
//		emails.add("kumasan903@gmail.com");
//		emails.add("jstnetjob@gmail.com");
//		emails.add("heaventosevenstar.010@gmail.com");
//		emails.add("nobu.great0121@gmail.com");
//		emails.add("heart@japan.email.ne.jp");
//		emails.add("pu_chiro@ybb.ne.jp");
//		emails.add("kazu-19580907-33na@i.softbank.jp");
//		emails.add("star_1118_1018@yahoo.co.jp");
//		emails.add("pan99761@yahoo.co.jp");
//		emails.add("oes_tomi@ybb.ne.jp");
//		emails.add("yhanada7@gmail.com");
//		emails.add("qymkh439@ybb.ne.jp");
//		emails.add("nobuom0904@gmail.com");
//		emails.add("creators.hompo@gmail.com");
//		emails.add("machiruda0604@gmail.com");
//		emails.add("wara.family777+1@gmail.com");
//		emails.add("kattiny85@gmail.com");
//		emails.add("karaage30523@gmail.com");
//		emails.add("minamikenji0124@gmail.com");
//		emails.add("st.ricanan@gmail.com");
//		emails.add("jknbox1217@gmail.com");
//		emails.add("aromatherapy-herberist7@ezweb.ne.jp");
//		emails.add("benchanhouse@gmail.com");
//		emails.add("romane19conte55@Gmail.com");
//		emails.add("tars-1500@i.softbank.jp");
//		emails.add("rjjrp14p@yahoo.co.jp");
//		emails.add("jr6bth@yahoo.co.jp");
//		emails.add("rie39happy@gmail.com");
//		emails.add("surume3mai+ms1@gmail.com");
//		emails.add("pipinosuke_1@yahoo.co.jp");
//		emails.add("hirorin5557@gmail.com");
//		emails.add("yadori8416@gmail.com");
//		emails.add("okurio7@gmail.com");
//		emails.add("rumirin0517@gmail.com");
//		emails.add("naru12.asc@gmail.com");
//		emails.add("monitr.nao@gmail.com");
//		emails.add("fainal0088@gmail.com");
//		emails.add("kinu170316@outlook.com");
//		emails.add("arook@yahoo.co.jp");
//		emails.add("ku5080ohshan@gmail.com");
//		emails.add("umeboku@gmail.com");
//		emails.add("punyan25punyan@gmail.com");
//		emails.add("thesouthnorthdock3@yahoo.co.jp");
//		emails.add("miyakawa6500@coast.ocn.ne.jp");
//		emails.add("naturalland2016@gmail.com");
//		emails.add("yc3041960@yahoo.co.jp");
//		emails.add("n8a10k12.50@gmail.com");
//		emails.add("toyama_kigyo@yahoo.co.jp");
//		emails.add("tama-40203-tomo@ezweb.ne.jp");
//		emails.add("nao91032@yahoo.co.jp");
//		emails.add("yasuhira706303@gmail.com");
//		emails.add("yoppy.asagiri@gmail.com");
//		emails.add("noda15man@yahoo.co.jp");
//		emails.add("rocky88jp@yahoo.co.jp");
//		emails.add("tanulin2000@yahoo.co.jp");
//		emails.add("take-hiro8@ezweb.ne.jp");
//		emails.add("haengja0630@gmail.com");
//		emails.add("tamm97852@zeus.eonet.ne.jp");
//		emails.add("tomonao.y60718@gmail.com");
//		emails.add("kabu013495@gmail.com");
//		emails.add("haba.n@outlook.jp");
//		emails.add("npckk447@yahoo.co.jp");
//		emails.add("hacci5963@gmail.com");
//		emails.add("oobayasi@cloud-line.net");
//		emails.add("kpbyt075@gmail.com");
//		emails.add("yoshie7o23@gmail.com");
//		emails.add("hinode.com@docomo.ne.jp");
//		emails.add("ohayousan0309io@gmail.com");
//		emails.add("tks05112@gmail.com");
//		emails.add("kirinsou0448@gmail.com");
//		emails.add("iolear@gmail.com");
//		emails.add("asutarisuku70@gmail.com");
//		emails.add("yoshinori6221@gmail.com");
//		emails.add("maytune11@gmail.com");
//		emails.add("tokokicyan@yahoo.co.jp");
//		emails.add("yamauchinousannn@gmail.com");
//		emails.add("fx123win@yahoo.co.jp");
//		emails.add("info_box@space-lab.net");
//		emails.add("onigiri224@icloud.com");
//		emails.add("rsa.uehara@gmail.com");
//		emails.add("reliance10000@gmail.com");
//		emails.add("shallow_river_ra@yahoo.co.jp");
//		emails.add("ysak5655@gmail.com");
//		emails.add("htatu03200806@yahoo.co.jp");
//		emails.add("mickytiger0722@gmail.com");
//		emails.add("joygreen0358@gmail.com");
//		emails.add("heart-reef2003@ezweb.ne.jp");
//		emails.add("roseyukie1108@gmail.com");
//		emails.add("mumeisakka@orion.ocn.ne.jp");
//		emails.add("t_kuni@mac.com");
//		emails.add("ostakarakongu@yahoo.co.jp");
//		emails.add("koma_819@yahoo.co.jp");
//		emails.add("westvillage2460.com@gmail.com");
//		emails.add("fuzoku627@gmail.com");
//		emails.add("keiko.satou.info@gmail.com");
//		emails.add("yjo0908@icloud.com");
//		emails.add("eightball5416@gmail.com");
//		emails.add("yaek.315@gmail.com");
//		emails.add("honto.nannan@docomo.ne.jp");
//		emails.add("tarsuke.stts@gmail.com");
//		emails.add("kazukazu.491012@gmail.com");
//		emails.add("ageinpapiko@yahoo.co.jp");
//		emails.add("sumiyo426001@gmail.com");
		emails.add("etervalu2016@gmail.com");
		emails.add("myra.watanabe@gmail.com");
		emails.add("yokog0820@gmail.com");
		emails.add("reoreox225@gmail.com");
		emails.add("lupin13golgo31@gmail.com");
		emails.add("365egaode@gmail.com");
		emails.add("komurohhn@gmail.com");
		emails.add("ganbarebaii@gmail.com");
		emails.add("dreamsun1208@gmail.com");
		emails.add("kobuichi6334@gmail.com");
		emails.add("mamano12129@gmail.com");
		emails.add("a11-051.2-a.sekitani.igitan@docomo.ne.jp");
		emails.add("michikof013@gmail.com");
		emails.add("maron.pepe7@gmail.com");
		emails.add("nakazimada@leto.eonet.ne.jp");
		
		return emails;
	}
	
	private List<NewProxy> getProxies() {
		return dbHelper.getNewProxies();
	}
	
//	private static List<NewProxy> getProxies() {
//		List<NewProxy> proxies = new ArrayList<>();
//		
//		proxies.add(new NewProxy(305, 0, 3));
//		proxies.add(new NewProxy(306, 0, 3));
//		proxies.add(new NewProxy(307, 1, 3));
//		proxies.add(new NewProxy(308, 0, 3));
//		proxies.add(new NewProxy(309, 2, 4));
//		proxies.add(new NewProxy(310, 0, 3));
//		proxies.add(new NewProxy(311, 0, 1));
//		proxies.add(new NewProxy(312, 1, 3));
//		proxies.add(new NewProxy(313, 0, 3));
//		proxies.add(new NewProxy(314, 2, 3));
//		proxies.add(new NewProxy(315, 1, 3));
//		proxies.add(new NewProxy(316, 0, 3));
//		proxies.add(new NewProxy(317, 0, 3));
//		proxies.add(new NewProxy(318, 1, 3));
//		proxies.add(new NewProxy(319, 0, 3));
//		proxies.add(new NewProxy(320, 0, 3));
//		proxies.add(new NewProxy(321, 0, 2));
//		proxies.add(new NewProxy(322, 0, 2));
//		proxies.add(new NewProxy(323, 0, 2));
//		proxies.add(new NewProxy(324, 0, 2));
//		proxies.add(new NewProxy(325, 0, 2));
//		proxies.add(new NewProxy(326, 0, 2));
//		proxies.add(new NewProxy(327, 0, 2));
//		proxies.add(new NewProxy(328, 0, 2));
//		proxies.add(new NewProxy(329, 0, 2));
//		proxies.add(new NewProxy(330, 0, 2));
//		proxies.add(new NewProxy(331, 1, 1));
//		proxies.add(new NewProxy(332, 1, 1));
//		proxies.add(new NewProxy(333, 0, 1));
//		proxies.add(new NewProxy(334, 0, 1));
//		proxies.add(new NewProxy(335, 0, 1));
//		proxies.add(new NewProxy(336, 0, 1));
//		proxies.add(new NewProxy(337, 0, 1));
//		proxies.add(new NewProxy(338, 0, 1));
//		proxies.add(new NewProxy(339, 0, 1));
//		proxies.add(new NewProxy(340, 0, 1));
//		proxies.add(new NewProxy(341, 0, 4));
//		proxies.add(new NewProxy(342, 0, 4));
//		proxies.add(new NewProxy(343, 0, 4));
//		proxies.add(new NewProxy(344, 0, 4));
//		proxies.add(new NewProxy(345, 1, 3));
//		proxies.add(new NewProxy(346, 2, 3));
//		proxies.add(new NewProxy(347, 0, 3));
//		proxies.add(new NewProxy(348, 1, 3));
//		proxies.add(new NewProxy(349, 0, 3));
//		proxies.add(new NewProxy(350, 0, 3));
//		proxies.add(new NewProxy(351, 0, 3));
//		proxies.add(new NewProxy(352, 0, 3));
//		proxies.add(new NewProxy(353, 0, 3));
//		proxies.add(new NewProxy(354, 0, 3));
//		proxies.add(new NewProxy(355, 1, 3));
//		proxies.add(new NewProxy(356, 1, 3));
//		proxies.add(new NewProxy(357, 0, 3));
//		proxies.add(new NewProxy(358, 0, 3));
//		proxies.add(new NewProxy(359, 2, 3));
//		proxies.add(new NewProxy(360, 1, 3));
//		proxies.add(new NewProxy(361, 0, 3));
//		proxies.add(new NewProxy(362, 1, 3));
//		proxies.add(new NewProxy(363, 0, 3));
//		proxies.add(new NewProxy(364, 1, 3));
//		proxies.add(new NewProxy(365, 2, 3));
//		proxies.add(new NewProxy(366, 0, 3));
//		proxies.add(new NewProxy(367, 0, 3));
//		proxies.add(new NewProxy(368, 2, 3));
//		proxies.add(new NewProxy(369, 1, 3));
//		proxies.add(new NewProxy(370, 0, 2));
//		proxies.add(new NewProxy(371, 1, 2));
//		proxies.add(new NewProxy(372, 1, 2));
//		proxies.add(new NewProxy(373, 1, 2));
//		proxies.add(new NewProxy(374, 0, 2));
//		proxies.add(new NewProxy(375, 1, 2));
//		proxies.add(new NewProxy(376, 0, 2));
//		proxies.add(new NewProxy(377, 0, 2));
//		proxies.add(new NewProxy(378, 0, 1));
//		proxies.add(new NewProxy(379, 0, 3));
//		proxies.add(new NewProxy(380, 0, 3));
//		proxies.add(new NewProxy(381, 0, 3));
//		proxies.add(new NewProxy(382, 1, 3));
//		proxies.add(new NewProxy(383, 0, 3));
//		proxies.add(new NewProxy(384, 1, 3));
//		proxies.add(new NewProxy(385, 1, 3));
//		proxies.add(new NewProxy(386, 1, 3));
//		proxies.add(new NewProxy(387, 1, 3));
//		proxies.add(new NewProxy(388, 0, 3));
//		proxies.add(new NewProxy(389, 0, 3));
//		proxies.add(new NewProxy(390, 0, 3));
//		proxies.add(new NewProxy(391, 0, 3));
//		proxies.add(new NewProxy(392, 1, 3));
//		proxies.add(new NewProxy(393, 0, 3));
//		proxies.add(new NewProxy(394, 0, 3));
//		proxies.add(new NewProxy(395, 0, 3));
//		proxies.add(new NewProxy(396, 1, 3));
//		proxies.add(new NewProxy(397, 0, 3));
//		proxies.add(new NewProxy(398, 0, 3));
//		proxies.add(new NewProxy(399, 0, 3));
//		proxies.add(new NewProxy(400, 0, 3));
//		proxies.add(new NewProxy(401, 1, 3));
//		proxies.add(new NewProxy(402, 1, 3));
//		proxies.add(new NewProxy(403, 0, 3));
//		proxies.add(new NewProxy(404, 1, 3));
//		proxies.add(new NewProxy(405, 0, 3));
//		proxies.add(new NewProxy(406, 0, 3));
//		proxies.add(new NewProxy(407, 0, 3));
//		proxies.add(new NewProxy(408, 0, 3));
//		proxies.add(new NewProxy(409, 0, 3));
//		proxies.add(new NewProxy(410, 1, 3));
//		
//		return proxies;
//	}
	
	public static class NewProxy {
		int id;
		int currentNumUser;
		int maxUser;
		
		public NewProxy(int id, int currentNumUser, int maxUser) {
			this.id = id;
			this.currentNumUser = currentNumUser;
			this.maxUser = maxUser;
		}
		
		public boolean isEnough() {
			return currentNumUser >= maxUser;
		}
		
		public void incrementCurrentNumUser() {
			currentNumUser++;
		}
	}

}
