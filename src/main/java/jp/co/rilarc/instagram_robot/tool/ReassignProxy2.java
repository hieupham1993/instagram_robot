/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.phantomjs.RunJs;
import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 *
 */
public class ReassignProxy2 {
	
	final protected static Logger logger = LogManager.getLogger(ReassignProxy2.class);
	
	static final int CHECK_NOT_PASS_TIME = 5; 
	
	Map<Proxy, Integer> proxies;
	
	final List<String> testMedias;
	final Configuration config;
	
    final private ToolDatabaseHelper dbHelper;
    
    
    final List<User> listActiveUser;
    final List<User> passedUsers;
    
	public ReassignProxy2() {
		config = new Configuration();
		dbHelper = new ToolDatabaseHelper(config);
		
		
		testMedias = new ArrayList<>();
		createTestMedia();
		
		listActiveUser = getActiveUser();
		passedUsers = new ArrayList<>();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("Start ReassignProxy");
		String proxyAddress = null;
		if(args.length > 0) {
			proxyAddress = args[0];
		}
		
		try{
			ReassignProxy2 instance = new ReassignProxy2();
			instance.doMain(proxyAddress);
		}catch(Exception e) {
			logger.error(e);
		}
		logger.info("End ReassignProxy");
	}
	
	public void doMain(String proxyAddress) {
		if(proxyAddress != null) {
			proxies = getGoodProxies(proxyAddress);
		}else {
			proxies = getGoodProxies();
		}
		for(Proxy proxy : proxies.keySet()) {
			checkProxy(proxy);
			
			if(passedUsers.isEmpty()) {
				// Bad proxy
				dbHelper.updateBadProxy(proxy);
				logger.warn(String.format("[Failed] Proxy %s is blocked", proxy.getProxyAddress()));
			}
			
			// Bo nhung user da pass ra khoi danh sach can check
			for(User user : passedUsers) {
				listActiveUser.remove(user);
			}
			passedUsers.clear();
		}
	}
	
	private void checkProxy(Proxy proxy) {
		int deviceNum = proxies.get(proxy);
		for(User user : listActiveUser) {

			if(deviceNum >= 30) {
				return;
			}
			logger.info(String.format("[Check] Proxy %s, user %s", proxy.getProxyAddress(), user.getEmail()));
			InstagramAccount instagramAccount = dbHelper.getInstagramAccountByUserId(user.getId());
			boolean loggedin = tryToLogin(instagramAccount);
			if(!loggedin) {
				continue;
			}
			
			boolean likeOk = tryToLike(instagramAccount, proxy);
			if(likeOk) {
				updateProxy(instagramAccount, proxy);
				passedUsers.add(user);
				// update so luong device cua proxy
				proxies.put(proxy, ++deviceNum);
			}
		}
		
	}
	
	/**
	 * Update proxy moi cho user 
	 * Dong thoi enable like
	 * @param instagramAccount
	 * @param proxy
	 */
	private void updateProxy(InstagramAccount instagramAccount, Proxy proxy) {
		// Update proxy moi va enable like
		dbHelper.updateNewProxy(instagramAccount.getUserId(), proxy.getId());
		logger.info(String.format("[Success] Set proxy %d (%s) for user %d (%s)", 
				proxy.getId(), proxy.getProxyAddress(), instagramAccount.getUserId(), instagramAccount.getUsername()));
	}

	private boolean tryToLike(InstagramAccount instagramAccount, Proxy proxy) {
		String mediaId = getRandomMediaId();
		boolean likeSuccess = RunJs.like(instagramAccount, mediaId, proxy);
		return likeSuccess;
	}
	
	private boolean tryToLogin(InstagramAccount instagramAccount) {
		// Login by API
    	Instagram instagram = new Instagram(instagramAccount, null);
		try {
			return instagram.login(true, config.isEnablePhantomjs());
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (URISyntaxException e) {
			logger.error(e.getMessage());
		} catch (InstagramInvalidCredentialsException | InstagramUserInvalidException e) {
			// Disable user
			dbHelper.disableUser(instagramAccount.getUserId(), e.getMessage());
			logger.error(e.getMessage());
		} catch (InstagramCheckpointRequiredException e) {
			dbHelper.disableInstagramConnectStatus(instagramAccount.getUserId(), e.getMessage());
			logger.error(e.getMessage());
		} catch (InstagramSentryBlockException e) {
			logger.error(e.getMessage());
		}
		return false;
	}

	private List<User> getActiveUser() {
		List<User> listUser = dbHelper.getListDisableLikeUsers();
		Collections.shuffle(listUser);
		return listUser;
	}

	private Map<Proxy, Integer> getGoodProxies() {
		return dbHelper.getGoodProxies();
	}
	
	private Map<Proxy, Integer> getGoodProxies(String proxyAddress) {
		return dbHelper.getGoodProxies(proxyAddress);
	}
	
	private String getRandomMediaId() {
		int size = testMedias.size();
		int idx = Signatures.randomInt(0, size - 1);
		return testMedias.get(idx);
	}
	
	private void createTestMedia() {
		testMedias.add("BbPrweSlSYR");
		testMedias.add("BbQo_swjASI");
		testMedias.add("BbRMCeXnRmH");
		testMedias.add("BbRKruZF1o3");
		testMedias.add("BbRITLzD1Lk");
		testMedias.add("BbRIBJKgTcK");
		testMedias.add("BbRFzcGHse2");
		testMedias.add("BbRE-3PFZC2");
		testMedias.add("BbRELrZg6cM");
		testMedias.add("BbRDSqAhNZc");
		testMedias.add("BbQ_ldcgJ4K");
		testMedias.add("BbQ-7e0lVkA");
		testMedias.add("BbQ69PIF_F5");
		testMedias.add("BbQ5VTaHKx3");
		testMedias.add("BbQ5T2fhzZw");
		testMedias.add("BbRRBkRDupv");
		testMedias.add("BbRRBUtlpIc");
		testMedias.add("BbRRBMyFxD5");
		testMedias.add("BbRRBATlqAq");
		testMedias.add("BbRRA9Bly1V");
		testMedias.add("BbRRA63nBgM");
		testMedias.add("BbRRA6fHS_X");
		testMedias.add("BbRRA49g9zn");
		testMedias.add("BbRRAsuAv2W");
		testMedias.add("BbRRAq_HT0Z");
		testMedias.add("BbRRAoqFkxb");
		testMedias.add("BbRVp7wgWaF");
		testMedias.add("BbRVnT3FKGJ");
		testMedias.add("BbRVltOjb0I");
		testMedias.add("BbRVksFHREl");
		testMedias.add("BbRVXmkFMpO");
		testMedias.add("BbRVXPnjbpu");
		testMedias.add("BbRVRLdhZXL");
		testMedias.add("BbRVQ7OgGZK");
		testMedias.add("BbRVMlMlIXU");
		testMedias.add("BbRVHXWBBPQ");
		testMedias.add("BbRVGmaAcQv");
		testMedias.add("BbRVE7-B1Ra");
		testMedias.add("BbRVCnkDWyc");
		testMedias.add("BbRaAaxhwrB");
		testMedias.add("BbRZsQrh4x9");
		testMedias.add("BbQO4lKngR9");
		testMedias.add("BbQKeaBjirp");
		testMedias.add("BbPa5Meg-yt");
		testMedias.add("BbPHOgChM4s");
		testMedias.add("BbO2omCFwYV");
		testMedias.add("BbOqUt1FwIi");
		testMedias.add("BbOlXKeBrYe");
		testMedias.add("BbOd-qmlITb");
		testMedias.add("BbOZlxUFOhp");
		testMedias.add("BbOTOm1DSIp");
		testMedias.add("BbOGy3aBsXt");
		testMedias.add("BbRigmZleXq");
		testMedias.add("BbRiMHwAaaI");
		testMedias.add("BbRhn-RlWWW");
		testMedias.add("BbRhZbfHV5H");
		testMedias.add("BbRgr6vAOap");
		testMedias.add("BbRgoUShj_q");
		testMedias.add("BbRfz0vh2HS");
		testMedias.add("BbRfhT9BS7e");
		testMedias.add("BbRfa2cBIxl");
		testMedias.add("BbRfPAEF596");
		testMedias.add("BbRhUk5hGbD");
		testMedias.add("BbRhPNgBOcv");
		testMedias.add("BbRhJ4thNRE");
		testMedias.add("BbRhFm0B7hr");
		testMedias.add("BbRhA8pBY_u");
		testMedias.add("BbRg6TEhtHp");
		testMedias.add("BbRgyrPhJQs");
		testMedias.add("BbQVWC5D5BB");
	}
}
