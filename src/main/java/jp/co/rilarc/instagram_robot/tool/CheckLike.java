/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.phantomjs.RunJs;
import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 *
 */
public class CheckLike {
	
	final protected static Logger logger = LogManager.getLogger(CheckLike.class);
	
	static final int CHECK_NOT_PASS_TIME = 5; 
	
	final List<String> testMedias;
	List<TestUser> testUser = new ArrayList<>();
	List<Proxy> testProxies = new ArrayList<>();
	List<Proxy> badProxies = new ArrayList<>();
	
	final Configuration config;
    
	public CheckLike() {
		
		config = new Configuration();
		
		testMedias = new ArrayList<>();
		createTestMedia();
		createTestProxies();
		createTestUsers();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("Start Check like");
		try{
			CheckLike instance = new CheckLike();
			instance.doMain();
		}catch(Exception e) {
			logger.error("error", e);
		}
		logger.info("End Check like");
	}
	
	public void doMain() {
		for(TestUser user : testUser) {
//			InstagramAccount instagramAccount = dbHelper.getInstagramAccountByToken(user.getUserId());
			InstagramAccount instagramAccount = new InstagramAccount();
			instagramAccount.setUsername(user.getInstagramUsername());
			instagramAccount.setPassword(user.getInstagramPassword());
			checkUser(instagramAccount);
		}
	}

	private void checkUser(InstagramAccount instagramAccount) {
		boolean result = false;
		try {
			for(Proxy proxy : testProxies) {
				result = tryToLogin(instagramAccount, proxy);
				
				if(result) {
					result = tryToLike(instagramAccount, proxy);
				}
				
				if(!result) {
					logger.warn(String.format("Proxy %s bad", proxy.getProxyAddress()));
					badProxies.add(proxy);
				}else {
					break;
				}
			}
			
			for(Proxy badProxy : badProxies) {
				testProxies.remove(badProxy);
			}
			badProxies.clear();
			
		}catch(Exception e) {
			logger.error("Error: ", e);
		}
		
	}

	private boolean tryToLike(InstagramAccount instagramAccount, Proxy proxy) {
		String mediaId = getRandomMediaId();
		boolean likeSuccess = RunJs.like(instagramAccount, mediaId, proxy);
		return likeSuccess;
	}
	
	private boolean tryToLogin(InstagramAccount instagramAccount, Proxy proxy) {
		// Login by API
//		Instagram instagram = new Instagram(instagramAccount, null);
//		instagram.setProxy(proxy);
//		try {
//			return instagram.login(true);
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		} catch (URISyntaxException e) {
//			logger.error(e.getMessage());
//		} catch (InstagramInvalidCredentialsException | InstagramUserInvalidException e) {
//			// Disable user
//			logger.error(e.getMessage());
//		} catch (InstagramCheckpointRequiredException e) {
//			logger.error(e.getMessage());
//		}
//		return false;
		return RunJs.login(instagramAccount, proxy);
	}
	
	private String getRandomMediaId() {
		int size = testMedias.size();
		int idx = Signatures.randomInt(0, size - 1);
		return testMedias.get(idx);
	}
	
	private void createTestMedia() {
		testMedias.add("Bbyv1ZslWqv");
		testMedias.add("BbyvqD-BQC9");
	}
	
	
	private void createTestUsers() {
		//testUser.add(new TestUser(6,"twmeated@gmail.com","shizukzk","hsMEH3NiMu7HIZMpsIxAUA==","6224404903")); // 133.130.123.101 OK
//		testUser.add(new TestUser(69,"yoshidon@js8.so-net.ne.jp","yoshidon.238","K5SZGYHhioo0ICMCMrXPlw==","6228643166")); // NG
//		testUser.add(new TestUser(187,"mefistss39173@gmail.com","mefistss39173","vgU615DqklRGzBqN1j0oSA==","6228468356")); // NG
//		testUser.add(new TestUser(247,"dondoll.pkp@gmail.com","dondoll.pkp","wrOuViHi2J556U/KMKjG9A==","6227610333"));		// 150.95.131.247 OK
//		testUser.add(new TestUser(272,"kamikazesilent@gmail.com","kamikazesilen","NyrjmpxfTmb0V3ZXJApawg==","6228545185")); // 150.95.131.247 OK 
//		testUser.add(new TestUser(321,"cbt43150@yahoo.co.jp","marlowepinpon","q8ZNEhY7loBvFvs+38Nomg==","6228477272")); // 150.95.131.247 OK
//		testUser.add(new TestUser(364,"masumi.lumienne@gmail.com","masumi.lumienne","MFGD1BPtbL/06TSCKrJxgg==","521141745")); // 150.95.131.247 NG
//		testUser.add(new TestUser(366,"tokiwasinobu007@gmail.com","tokiwasinobu007","IuK4M9JmELJ8pRFXkeq7+g==","6229891955")); // 150.95.140.146 OK
//		testUser.add(new TestUser(369,"kv1119vk@yahoo.co.jp","kingkazu178","+Sw/5NVcbm/0RZGPVeBJlQ==","5633470853")); // 150.95.140.146 OK
//		testUser.add(new TestUser(397,"reiki2012.kanaoka@gmail.com","singing.ring.2017","n8i4NQl0dyBLYiux2Dci5Q==","6229143322")); // 150.95.140.146 OK
//		testUser.add(new TestUser(476,"a-matsuo@xd6.so-net.ne.jp","rumpus.thefinal","0+tNLVyWrb8Dp8YS7ttlrw==","6229600293"));  // NG
//		testUser.add(new TestUser(480,"phrebre1991@gmail.com","phrebre1991","ggGG8xFgGtZrzM8/LV7xQw==","6228745338")); // NG
//		testUser.add(new TestUser(485,"mthut1841@ceres.ocn.ne.jp","mthut1841","52L5bYSJXOnz8f5BvEN2VA==","6227789752"));
//		testUser.add(new TestUser(488,"hybrid_theory.4721@docomo.ne.jp","f3d9chy","glUzLJj58/iyjRMANYw44g==","6229233837"));
//		testUser.add(new TestUser(489,"tefinks@gmail.com","jp_ks_f","5tu9pOmugasN+LXzlPNqVA==","6229011918"));
//		testUser.add(new TestUser(534,"happy445kiyu@gmail.com","happy445kiyu","lW61H4FmeMvT8Nj487rp6g==","6228852581"));
//		testUser.add(new TestUser(570,"maami_0624_902@yahoo.co.jp","maami_0624_902","jyZlfTMftOu7QrBhghwbPA==","6229470846"));
//		testUser.add(new TestUser(574,"tsunetsune2221@gmail.com","tsunetsune2221","bj38crsM0iaz6YiGqG5hfA==","6228598096"));
//		testUser.add(new TestUser(622,"tsenba3722@gmail.com","tsenba3722","WyIzvsQzRWUR2PJ4ND1l5w==","6229197003"));
		
//		testUser.add(new TestUser(1001,"xxxx@gmail.com","thaihx8299","C/z91zkC3vAjo1snmxBt0A==","xxxx"));	// thaihx8299/Abc123456
//		testUser.add(new TestUser(1001,"xxxx@gmail.com","maruko54th1","DOzlOsuBtL9/OiPyaaD6sw==","xxxx")); // maruko54th1/ngan12345
//		testUser.add(new TestUser(1001,"xxxx@gmail.com","evamaruko7","DOzlOsuBtL9/OiPyaaD6sw==","xxxx"));  // evamaruko7/ngan12345
		
//		testUser.add(new TestUser(1001,"xxxx@gmail.com","thaihx8299","Abc123456","xxxx"));	// thaihx8299/Abc123456	    150.95.151.2
//		testUser.add(new TestUser(1001,"xxxx@gmail.com","maruko54th1","ngan12345","xxxx")); // maruko54th1/ngan12345		150.95.151.2
//		testUser.add(new TestUser(1001,"xxxx@gmail.com","evamaruko7","ngan12345","xxxx"));  // evamaruko7/ngan12345		150.95.151.2
		
		testUser.add(new TestUser(1001,"xxxx@gmail.com","nghiyahoo", "1234567890@", "xxxx"));
		testUser.add(new TestUser(1001,"xxxx@gmail.com","loposloq", "1234567890@", "xxxx"));
	}
	
	
	private void createTestProxies() {
		// testProxies.add(new Proxy( "133.130.123.101",3128, "wmuser", "VN201501")); // OK
//		testProxies.add(new Proxy( "150.95.131.247",3128, "wmuser", "VN201501"));		// OK
//		testProxies.add(new Proxy( "150.95.140.146",3128, "wmuser", "VN201501"));
//		testProxies.add(new Proxy( "150.95.150.133",3128, "wmuser", "VN201501"));		// NG
//		testProxies.add(new Proxy( "150.95.142.244",3128, "wmuser", "VN201501"));		// OK
//		testProxies.add(new Proxy( "150.95.177.184",3128, "wmuser", "VN201501"));		// OK
		testProxies.add(new Proxy( "150.95.151.2",3128, "wmuser", "VN201501"));		//
		testProxies.add(new Proxy( "150.95.182.127",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.134.67",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.151.106",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.173.221",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.155.107",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "133.130.125.85",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "133.130.127.160",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.153.48",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "133.130.121.150",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.145.241",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "150.95.156.156",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "133.130.120.53",3128, "wmuser", "VN201501"));
		testProxies.add(new Proxy( "133.130.91.121",3128, "wmuser", "VN201501"));
	}
}
