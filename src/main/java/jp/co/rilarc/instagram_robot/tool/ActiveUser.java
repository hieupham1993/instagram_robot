/**
 * 
 */
package jp.co.rilarc.instagram_robot.tool;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.utils.Configuration;

/**
 * @author nghilt
 *
 */
public class ActiveUser {
	
	final protected static Logger logger = LogManager.getLogger(ActiveUser.class);
	
	final private ToolDatabaseHelper dbHelper;
	
	public ActiveUser() {
		Configuration config = new Configuration();
		dbHelper = new ToolDatabaseHelper(config);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("------------------------------ START ---------------------------");
		ActiveUser program = new ActiveUser();
		int updatedUser = program.run();
		logger.info("Update " + updatedUser + " users successful");
	}
	
	int run() {
		return dbHelper.activeUser();
	}
}
