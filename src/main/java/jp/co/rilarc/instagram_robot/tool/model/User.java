package jp.co.rilarc.instagram_robot.tool.model;

public class User {
	private int userid; 
	private String email; 
	private boolean active; 
	private boolean is_robot_working; 
	private boolean is_active_robot; 
	private boolean is_instagram_connected; 
	private boolean disable_like; 
	private boolean disable_follow; 
	private boolean disable_sendmessage; 
	private int proxy_id;
	private String proxy_address;
	
	public static String createCsvHeader() {
		return "userid, email, active, is_robot_working, is_active_robot, is_instagram_connected, disable_like, disable_follow, disable_sendmessage, proxy_id, proxy_address";
	}

	public String toCsv() {
//		String format = "User [userid=%s, email=%s, active=%s, is_robot_working=%s, is_active_robot=%s, is_instagram_connected=%s, disable_like=%s, disable_follow=%s, disable_sendmessage=%s, proxy_id=%s, proxy_address=%s]";
		return String.format(
				"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
				userid, email, active, is_robot_working, is_active_robot, is_instagram_connected, disable_like,
				disable_follow, disable_sendmessage, proxy_id, proxy_address);
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isIs_robot_working() {
		return is_robot_working;
	}

	public void setIs_robot_working(boolean is_robot_working) {
		this.is_robot_working = is_robot_working;
	}

	public boolean isIs_active_robot() {
		return is_active_robot;
	}

	public void setIs_active_robot(boolean is_active_robot) {
		this.is_active_robot = is_active_robot;
	}

	public boolean isIs_instagram_connected() {
		return is_instagram_connected;
	}

	public void setIs_instagram_connected(boolean is_instagram_connected) {
		this.is_instagram_connected = is_instagram_connected;
	}

	public boolean isDisable_like() {
		return disable_like;
	}

	public void setDisable_like(boolean disable_like) {
		this.disable_like = disable_like;
	}

	public boolean isDisable_follow() {
		return disable_follow;
	}

	public void setDisable_follow(boolean disable_follow) {
		this.disable_follow = disable_follow;
	}

	public boolean isDisable_sendmessage() {
		return disable_sendmessage;
	}

	public void setDisable_sendmessage(boolean disable_sendmessage) {
		this.disable_sendmessage = disable_sendmessage;
	}

	public int getProxy_id() {
		return proxy_id;
	}

	public void setProxy_id(int proxy_id) {
		this.proxy_id = proxy_id;
	}

	public String getProxy_address() {
		return proxy_address;
	}

	public void setProxy_address(String proxy_address) {
		this.proxy_address = proxy_address;
	}
}
