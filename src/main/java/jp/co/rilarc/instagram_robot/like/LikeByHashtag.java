package jp.co.rilarc.instagram_robot.like;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.SearchMediaHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.BadHastag;
import jp.co.rilarc.instagram_robot.model.LikeHashtag;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public class LikeByHashtag extends Like {

    final protected static Logger logger = LogManager.getLogger(LikeByHashtag.class);

    public LikeByHashtag(Instagram instagram, RobotSetting robotSetting, IAPIHelper dbHelper, Configuration config,
                         AdvanceSetting advanceSetting, Statistic statistic, int numberMediaToLike) {
        super(instagram, robotSetting, dbHelper, config, advanceSetting, statistic, numberMediaToLike);
    }

    @Override
    public int autoLike() throws ActionBlockException, InstagramCheckpointRequiredException {
//        boolean result = false;
        int totalMediasLiked = 0;


        // get list hashtags
        ArrayList<LikeHashtag> likeHashtags = mDbHelper.getLikeHashtagsByToken();
        if (likeHashtags.size() == 0) {
            logger.debug(buildLog(": have no like_hashtags record"));
            return 0;
        }

        // get random hastag in db
        String maxId = null;
        BadHastag badHashtag = null;
        LikeHashtag likeHashtag = null;
        while (likeHashtags.size() > 0) {
            int rnd = new Random().nextInt(likeHashtags.size());
            likeHashtag = likeHashtags.get(rnd);

            //check bad hashtag
            badHashtag = mDbHelper.getBadHashtag(likeHashtag.getHashtag());
            if (badHashtag != null && badHashtag.isBlock()) {
                likeHashtags.remove(rnd);
                likeHashtag = null;
            } else
                break;
        }

        if (likeHashtag == null) {
            logger.info(buildLog(": all like hashtag exists in bad list"));
            return 0;
        }

        logger.info(buildLog(likeHashtag.getHashtag()));
        VLogger.append("「ハッシュタグ【" + likeHashtag.getHashtag() + "】で自動いいねする」タスクを実行します");

        for (int loopCount = 0; loopCount < Const.MAX_SEARCH_PAGE; loopCount++) {
            // search media
            logger.info(buildLog(": search hashtag " + likeHashtag.getHashtag() + " page " + (loopCount + 1)));

            JSONObject searchMediaResponse = null;
//			try {
            searchMediaResponse = mInstagramRequester.searchMediaByHashtag(likeHashtag.getHashtag(), maxId);
//			} catch (InstagramCheckpointRequiredException e1) {
//				logger.info(e1.getMessage());
//				mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//			}

            logger.debug(searchMediaResponse);
            if (searchMediaResponse == null)
                break;

            SearchMediaHandler searchMediaHandler = new SearchMediaHandler(searchMediaResponse);
            if (!Objects.equals(searchMediaHandler.getStatus(), "ok"))
                break;

            // get list media
            JSONArray medias = searchMediaHandler.getMedias();

            if (loopCount == 0 && medias.length() == 0) {
                // mark bad hashtag
                mDbHelper.markBadHashtag(likeHashtag.getHashtag());

                logger.info(buildLog(": mark bad hashtag " + likeHashtag.getHashtag()));

                break;
            }

            JSONObject jsonResult = this.likeMediaFromList(medias, likeHashtag.getHashtag(), numberMediaToLike);

            int numMediasLiked = jsonResult.getInt("num_medias_liked");

            if (numMediasLiked > 0 && badHashtag != null)
                mDbHelper.deleteBadHashtag(badHashtag.getId());

            mDbHelper.increaseLikeHashtagCount(likeHashtag.getId(), numMediasLiked);
            totalMediasLiked += numMediasLiked;
            if (numMediasLiked < numberMediaToLike)
                numberMediaToLike -= numMediasLiked;
            else
                break;

            //check has next page
            if (!searchMediaHandler.hasNext())
                break;

            //next page id
            maxId = searchMediaHandler.getNextPageId();

            // wait
            try {
                Thread.sleep(Signatures.randomInt(2, 3) * 1000);
            } catch (InterruptedException e) {
                logger.error("ERROR: ", e);
            }
        }

        return totalMediasLiked;
    }

    @Override
    protected boolean checkLikeFilter(MediaHandler media) {
        int likeCount = media.getLikeCount();
        JSONArray hashtags = media.getListHashtags();

        //check like count
        if ((mRobotSetting.isLikesHashtagMinLikeFilterMode() && mRobotSetting.getLikesHashtagMinLikeFilter() > likeCount) ||
                (mRobotSetting.isLikesHashtagMaxLikeFilterMode() && mRobotSetting.getLikesHashtagMaxLikeFilter() <= likeCount)) {
            String likeFilter = "";
            if (mRobotSetting.isLikesHashtagMinLikeFilterMode())
                likeFilter += "min: " + mRobotSetting.getLikesHashtagMinLikeFilter();
            if (mRobotSetting.isLikesHashtagMaxLikeFilterMode())
                likeFilter += "; max: " + mRobotSetting.getLikesHashtagMaxLikeFilter();
            likeFilter += "; current: " + likeCount;

            logger.debug(buildLog(": media " + media.getMediaId() + " invalid like count (" + likeFilter + ")"));
            return false;
        }

        //check max hashtag
        if (mRobotSetting.isLikesHashtagMaxHashtagFilterMode() && mRobotSetting.getLikesHashtagMaxHashtagFilter() < hashtags.length()) {
            logger.debug(buildLog(": media " + media.getMediaId() + " invalid hashtag count"));
            return false;
        }

        //check spam hashtag
        if (mRobotSetting.isLikesHashtagEnableSpamFilter() && hashtags.length() > 0) {
            JSONArray spamHashtags = mDbHelper.getSpamHashtagsByToken();

            if (spamHashtags != null && spamHashtags.length() > 0) {
                for (int i = 0; i < hashtags.length(); i++)
                    for (int j = 0; j < spamHashtags.length(); j++)
                        if (Objects.equals(hashtags.getString(i).toLowerCase(), spamHashtags.getString(j).toLowerCase())) {
                            logger.info(buildLog(": media " + media.getMediaId() + " invalid spam hashtag"));

                            return false;
                        }
            }
        }

        return true;
    }

    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {
        String userName = mediaHandler.getUsername();
        String avatarUrl = mediaHandler.getAvatarUrl();
        String mediaPost = mediaHandler.getMediaPost();
        String imagePost = mediaHandler.getImagePost();
        mDbHelper.insertActivity(Const.ACTIVITY_TYPE_LIKE, mInstagram.getInstagramAccount().getUsername(), userName, avatarUrl, key, null, mediaPost, imagePost);
    }

    protected StringBuilder buildLog() {
        return super.buildLog("[ByHashtag]");
    }
}
