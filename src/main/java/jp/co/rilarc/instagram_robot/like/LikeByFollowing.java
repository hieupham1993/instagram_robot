package jp.co.rilarc.instagram_robot.like;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.TimelineHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public class LikeByFollowing extends Like {

    public LikeByFollowing(Instagram instagram, RobotSetting robotSetting, IAPIHelper dbHelper, Configuration config,
                           AdvanceSetting advanceSetting, Statistic statistic, int numberMediaToLike) {
        super(instagram, robotSetting, dbHelper, config, advanceSetting, statistic, numberMediaToLike);
    }

    @Override
    public int autoLike() throws ActionBlockException, InstagramCheckpointRequiredException {
//        boolean result = false;
        int totalMediasLiked = 0;

        VLogger.append("「フォロー中のユーザーの投稿に自動でいいねする」タスクを実行します");
        String maxId = null;
        for (int loopCount = 0; loopCount< Const.MAX_SEARCH_PAGE; loopCount++) {
            // search media

            JSONObject searchMediaResponse = null;
//			try {
				searchMediaResponse = mInstagramRequester.searchMediaFromTimeline(maxId);
//			} catch (InstagramCheckpointRequiredException e1) {
//				logger.info(e1.getMessage());
//				mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//			}

            if (searchMediaResponse == null)
                break;

            TimelineHandler searchMediaHandler = new TimelineHandler(searchMediaResponse);
            if (!Objects.equals(searchMediaHandler.getStatus(), "ok"))
                break;

            // get list media
            JSONArray medias = searchMediaHandler.getMedias();

			JSONObject jsonResult = this.likeMediaFromList(medias, null, numberMediaToLike);
			
			int numMediasLiked = jsonResult.getInt("num_medias_liked");
            totalMediasLiked += numMediasLiked;
            if (numMediasLiked < numberMediaToLike)
                numberMediaToLike -= numMediasLiked;
            else
                break;


            //check has next page
            if (!searchMediaHandler.hasNext())
                break;

            //next page id
            maxId = searchMediaHandler.getNextPageId();

            try {
                Thread.sleep(Signatures.randomInt(2, 3) * 1000);
            } catch (InterruptedException e) {
                logger.error(buildLog(), e);
            }

//            result = jsonResult.getBoolean("like_result");
//            if (result)
//                break;
//            else if (jsonResult.getBoolean("spam_error")) {
//                mDbHelper.disableAction(mUserId);
//                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": disable like");
//
//                break;
//            }
        }

        return totalMediasLiked;
    }

    @Override
    protected boolean checkLikeFilter(MediaHandler media) {
        JSONArray hashtags = media.getListHashtags();

        //check spam hashtag
        if (mRobotSetting.isLikesFollowingEnableSpaFilter() && hashtags.length() > 0) {
            JSONArray spamHashtags = mDbHelper.getSpamHashtagsByToken();

            if (spamHashtags == null || spamHashtags.length() == 0)
                return true;

            for (int i=0; i<hashtags.length(); i++)
                for (int j=0; j<spamHashtags.length(); j++)
                    if (Objects.equals(hashtags.getString(i).toLowerCase(), spamHashtags.getString(j).toLowerCase()))
                        return false;
        }

        return true;
    }


    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {
        String userName = mediaHandler.getUsername();
        String avatarUrl = mediaHandler.getAvatarUrl();
        String mediaPost = mediaHandler.getMediaPost();
        String imagePost = mediaHandler.getImagePost();
        mDbHelper.insertActivity(Const.ACTIVITY_TYPE_LIKE, mInstagram.getInstagramAccount().getUsername(), userName, avatarUrl, null, null, mediaPost, imagePost);
    }

    protected StringBuilder buildLog() {
		return super.buildLog("[ByFollowing]");
	}
}
