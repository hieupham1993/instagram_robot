package jp.co.rilarc.instagram_robot.like;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.LikeBlockedException;
import jp.co.rilarc.instagram_robot.handler.InstagramUser;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.UserHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.InstagramRequester;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.Action;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public abstract class Like {

    final protected static Logger logger = LogManager.getLogger(Like.class);

    final protected Instagram mInstagram;

    final protected RobotSetting mRobotSetting;

    final protected AdvanceSetting mAdvanceSetting;

    final protected IAPIHelper mDbHelper;

    final protected Configuration mConfig;

    final protected Statistic statistic;

    protected InstagramRequester mInstagramRequester;

    protected int numberMediaToLike;

    final protected int mUserId;

    final protected String instagramUsername;

    public Like(Instagram instagram, RobotSetting robotSetting, IAPIHelper dbHelper, Configuration config,
                AdvanceSetting advanceSetting, Statistic statistic, int numberMediaToLike) {
        mInstagram = instagram;
        mUserId = instagram.getInstagramAccount().getUserId();
        instagramUsername = instagram.getInstagramAccount().getUsername();
        mRobotSetting = robotSetting;
        mAdvanceSetting = advanceSetting;
        mConfig = config;
        mDbHelper = dbHelper;
        this.statistic = statistic;
        mInstagramRequester = new InstagramRequester(instagram);
        this.numberMediaToLike = numberMediaToLike;
    }

    public abstract int autoLike() throws ActionBlockException, InstagramCheckpointRequiredException;

    protected abstract boolean checkLikeFilter(MediaHandler media);

    protected JSONObject likeMedia(String mediaId) {
        JSONObject result = new JSONObject();
        boolean likeResult = false;
        boolean spamError = false;

        try {
            HttpResponse response = mInstagram.like(mediaId);

            // check like result
            if (response.getStatusLine().getStatusCode() == 200) {
                likeResult = true;

                logger.info(buildLog("[API][successful] media " + mediaId));
            } else {
                String responseAsString = EntityUtils.toString(response.getEntity());
                logger.info(buildLog(responseAsString));

                JSONObject responseJson = new JSONObject(responseAsString);
                if (responseJson.has("spam") && responseJson.getBoolean("spam"))
                    spamError = true;

            }
        } catch (IOException | URISyntaxException e) {
            logger.error(buildLog(e.getMessage()), e);
        }

        result.put("like_result", likeResult);
        result.put("spam_error", spamError);
        return result;
    }

    protected JSONObject likeMediaFromList(JSONArray medias, String key, int numberMediaToLike)
            throws ActionBlockException, InstagramCheckpointRequiredException {
        boolean likeResult = false;
        boolean spamError = false;
        int numMediasLiked = 0;

        for (int i = 0; i < medias.length() && numMediasLiked < numberMediaToLike; i++) {
            JSONObject media = medias.getJSONObject(i);

            MediaHandler mediaHandler = new MediaHandler(media);

            // check like filter
            if (!mediaHandler.isMedia()) {
                // logger.debug("account "+ mInstagram.getInstagramAccount().getUsername() + ":
                // check media " + mediaHandler.getMediaPost() + " is not media");
                continue;
            }

            if (mediaHandler.hasLiked()) {
                logger.debug(buildLog(": check media " + mediaHandler.getMediaPost() + " has liked"));
                continue;
            }

            if (!this.checkLikeFilter(mediaHandler)) {
                logger.debug(buildLog(": check media " + mediaHandler.getMediaPost() + " invalid filter"));
                continue;
            }

            if (mAdvanceSetting != null && mAdvanceSetting.isActive()) {
                logger.debug(buildLog(": like checking user " + mediaHandler.getUserId()));

                if (this.isBlackUser(mediaHandler.getUserId())
                        || !this.checkConflictWordFilter(mediaHandler.getUserId())) {
                    try {
                        Thread.sleep(Signatures.randomInt(10, 20) * 1000);
                    } catch (InterruptedException e) {
                        logger.error("ERROR: ", e);
                    }
                    continue;
                }
            }

            // like media
            logger.debug(buildLog(": like media " + mediaHandler.getMediaPost()));

            if (mConfig.isEnablePhantomjs()) {
                likeResult = mInstagram.likeMediaByJs(mediaHandler.getMediaPost());
                logger.info("[Like] " + mediaHandler.getMediaPost() + " by js result: " + likeResult);
                if (!likeResult) {
                    logger.error(buildLog("[Phantomjs][Failed] media " + mediaHandler.getMediaPost()));
                }
            }

            // Try to like by API to get error message
            if (!likeResult) {
                JSONObject likeMediaResult = this.likeMedia(mediaHandler.getMediaId());

                likeResult = likeMediaResult.getBoolean("like_result");
                spamError = likeMediaResult.getBoolean("spam_error");
            }
            if (likeResult) {
                numMediasLiked++;
                statistic.increaseNumLikes();
                this.createActivity(mediaHandler, key);
                mDbHelper.increaseNumLikesStatistic(statistic.getId(), 1);
                logger.info(buildLog("[successful] media https://www.instagram.com/p/" + mediaHandler.getMediaPost() + "/ . Total liked for this media list is " + numMediasLiked + "/" + statistic.getNumLikes()));
                VLogger.append(statistic.getNumLikes() + "回目のいいね： https://www.instagram.com/p/" + mediaHandler.getMediaPost() + "/");
            }

            if (spamError) {
                String msg = "account " + mInstagram.getInstagramAccount().getUsername() + " was blocked action "
                        + Action.LIKE;
                throw new LikeBlockedException(msg);
            }

            Utils.waitFor(8, 237);
        }

        JSONObject result = new JSONObject();

        result.put("like_result", likeResult);
        result.put("spam_error", spamError);
        result.put("num_medias_liked", numMediasLiked);
        return result;
    }

    protected abstract void createActivity(MediaHandler mediaHandler, String key);

    private boolean checkConflictWordFilter(String userId) throws InstagramCheckpointRequiredException {
        JSONObject userInfo = null;
//		try {
        userInfo = mInstagramRequester.getUserInfo(userId);
//		} catch (InstagramCheckpointRequiredException e1) {
//			logger.info(e1.getMessage());
//			mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//		}
        if (userInfo == null || !Objects.equals(userInfo.getString("status"), "ok"))
            return false;

        UserHandler userHandler = new UserHandler(userInfo);
        String biography = userHandler.getBiography();
        String conflictWords = mAdvanceSetting.getConflict();

        if (conflictWords != null) {
            JSONArray conflictArr = new JSONArray(conflictWords);

            for (int i = 0; i < conflictArr.length(); i++) {
                String conflictWord = conflictArr.getString(i);

                if (biography.toLowerCase().contains(conflictWord.toLowerCase())) {
                    logger.info(buildLog(": user " + userId + " is conflict"));
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isBlackUser(String userId) {
        String blackList = mAdvanceSetting.getBlackList();

        if (blackList != null) {
            InstagramUser instagramUser = mDbHelper.getInstagramUser(userId);

            if (instagramUser != null) {
                JSONArray blackArr = new JSONArray(blackList);

                for (int i = 0; i < blackArr.length(); i++) {
                    if (Objects.equals(blackArr.getString(i), instagramUser.getUsername())) {
                        logger.info(buildLog(": user " + userId + " is black user"));

                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected StringBuilder buildLog(String msg) {
        return new StringBuilder("[" + mUserId + "][" + instagramUsername + "][Like]").append(msg);
    }
}
