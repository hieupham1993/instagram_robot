package jp.co.rilarc.instagram_robot.like;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.SearchMediaHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.LikeLocation;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public class LikeByLocation extends Like {

    public LikeByLocation(Instagram instagram, RobotSetting robotSetting, IAPIHelper dbHelper, Configuration config,
                          AdvanceSetting advanceSetting, Statistic statistic, int numberMediaToLike) {
        super(instagram, robotSetting, dbHelper, config, advanceSetting, statistic, numberMediaToLike);
    }

    @Override
    public int autoLike() throws ActionBlockException, InstagramCheckpointRequiredException {
//        boolean result = false;
        int totalMediasLiked = 0;

        // get list location ids
        ArrayList<LikeLocation> likeLocations = mDbHelper.getLikeLocationsByToken();
        if (likeLocations.size() == 0) {
            logger.debug(buildLog(": have no like_locations record"));
            return 0;
        }

        // get random location id in db
        String maxId = null;
        int rnd = new Random().nextInt(likeLocations.size());
        LikeLocation likeLocation = likeLocations.get(rnd);

        VLogger.append("「ロケーション【" + likeLocation.getLocationName() + "】で自動いいねする」タスクを実行します。");

        for (int loopCount = 0; loopCount < Const.MAX_SEARCH_PAGE; loopCount++) {
            // search media

//            JSONObject searchMediaResponse = null;
//			try {
            JSONObject searchMediaResponse = mInstagramRequester.searchMediaByLocation(likeLocation.getLocationId(), maxId);
//			} catch (InstagramCheckpointRequiredException e1) {
//				logger.info(e1.getMessage());
//				mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//			}

            if (searchMediaResponse == null)
                break;

            SearchMediaHandler searchMediaHandler = new SearchMediaHandler(searchMediaResponse);
            if (!Objects.equals(searchMediaHandler.getStatus(), "ok"))
                break;

            // get list media
            JSONArray medias = searchMediaHandler.getMedias();

            JSONObject jsonResult = this.likeMediaFromList(medias, likeLocation.getLocationId(), numberMediaToLike);

            int numMediasLiked = jsonResult.getInt("num_medias_liked");

            mDbHelper.increaseLikeLocationCount(likeLocation.getId(), numMediasLiked);
            totalMediasLiked += numMediasLiked;
            if (numMediasLiked < numberMediaToLike)
                numberMediaToLike -= numMediasLiked;
            else
                break;

            //check has next page
            if (!searchMediaHandler.hasNext())
                break;

            //next page id
            maxId = searchMediaHandler.getNextPageId();

            try {
                Thread.sleep(Signatures.randomInt(2, 3) * 1000);
            } catch (InterruptedException e) {
                logger.error(buildLog(), e);
            }
//            result = jsonResult.getBoolean("like_result");
//
//            if (result) {
//                mDbHelper.increaseLikeLocationCount(likeLocation.getId());
//                break;
//            }
//            else if (jsonResult.getBoolean("spam_error")) {
//                mDbHelper.disableAction(mUserId);
//                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": disable like)");
//                break;
//            }


        }

        return totalMediasLiked;
    }

    @Override
    protected boolean checkLikeFilter(MediaHandler media) {
        int likeCount = media.getLikeCount();
        JSONArray hashtags = media.getListHashtags();

        //check like count
        if ((mRobotSetting.isLikesLocationMinLikeFilterMode() && mRobotSetting.getLikesLocationMinLikeFilter() > likeCount) ||
                (mRobotSetting.isLikesLocationMaxLikeFilterMode() && mRobotSetting.getLikesLocationMaxLikeFilter() <= likeCount))
            return false;

        //check max hashtag
        if (mRobotSetting.isLikesLocationMaxHashtagFilterMode() && mRobotSetting.getLikesLocationMaxHashtagFilter() < hashtags.length())
            return false;

        //check spam hashtag
        if (mRobotSetting.isLikesLocationEnableSpamFilter() && hashtags.length() > 0) {
            JSONArray spamHashtags = mDbHelper.getSpamHashtagsByToken();

            if (spamHashtags != null && spamHashtags.length() > 0) {
                for (int i = 0; i < hashtags.length(); i++)
                    for (int j = 0; j < spamHashtags.length(); j++)
                        if (Objects.equals(hashtags.getString(i).toLowerCase(), spamHashtags.getString(j).toLowerCase()))
                            return false;
            }
        }

        return true;
    }

    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {
        String userName = mediaHandler.getUsername();
        String avatarUrl = mediaHandler.getAvatarUrl();
        String mediaPost = mediaHandler.getMediaPost();
        String imagePost = mediaHandler.getImagePost();

        mDbHelper.insertActivity(Const.ACTIVITY_TYPE_LIKE, mInstagram.getInstagramAccount().getUsername(), userName, avatarUrl, null, key, mediaPost, imagePost);
    }

    protected StringBuilder buildLog() {
        return super.buildLog("[ByLocation]");
    }
}
