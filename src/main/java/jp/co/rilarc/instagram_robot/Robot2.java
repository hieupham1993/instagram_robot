package jp.co.rilarc.instagram_robot;

import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.thread.InstagramAutoThread;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

public class Robot2 {

    final private IAPIHelper dbHelper;

    final private static Logger logger = LogManager.getLogger(Robot2.class);

    public Robot2() {
        Configuration config = new Configuration();
        dbHelper = new DatabaseHelper(config);

        System.setProperty("file.encoding", "UTF-8");
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Tokyo"));

        Robot2 robot = new Robot2();
        logger.info("Start robot");

        robot.dbHelper.resetAllUser();
        ArrayList<User> unactiveRobotUsers;
        while (true) {
        	try {
	        	unactiveRobotUsers = robot.dbHelper.getListUnactiveRobotUsers();
	
	            for (User user : unactiveRobotUsers) {
	                logger.info("Check and start auto thread for inactive user " + user.getEmail());
	                InstagramAutoThread autoThread = new InstagramAutoThread(user, robot.dbHelper);
	                autoThread.start();
	            }
        	} catch (Exception e) {
        		logger.error("Unknown Error: ", e);
        	}

            try {
                Thread.sleep(5 * 60 * 1000);        // 5m
            } catch (InterruptedException e) {
                logger.error("ERROR: ", e);
            }
        }
    }
}