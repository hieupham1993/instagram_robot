package jp.co.rilarc.instagram_robot.unfollow;

import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.handler.FriendshipHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.FollowingUser;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Utils;

import jp.co.rilarc.instagram_robot.utils.VLogger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class UnfollowNotFollowingBack extends Unfollow {
	
	final protected static Logger logger = LogManager.getLogger(UnfollowNotFollowingBack.class);

    final protected int numberUsersToUnFollow;
	
    public UnfollowNotFollowingBack(Instagram instagram, RobotSetting robotSetting, int userId,
                                    IAPIHelper dbHelper, AdvanceSetting advanceSetting,
                                    Statistic statistic, int numberUsersToUnFollow) {
        super(instagram, robotSetting, userId, dbHelper, advanceSetting, statistic);

        this.numberUsersToUnFollow = numberUsersToUnFollow;
    }

    @Override
    public int autoUnfollow() throws ActionBlockException, InstagramCannotAccessException {
        int totalUsersUnFollowed = 0;

        int daysToCheckUnfollow = 3;
        if (mRobotSetting.isUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode())
            daysToCheckUnfollow = mRobotSetting.getUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow();

        // get list following user after
        ArrayList<FollowingUser> checkingUsers = mDbHelper.getListFollowingUser(daysToCheckUnfollow);

        if (checkingUsers.size() == 0)
            return 0;

        VLogger.append("「フォローを返さないユーザーを自動でフォロー解除する」タスクを実行します");

        for (FollowingUser followingUser : checkingUsers) {
            logger.info(buildLog(" checking user " + followingUser.getFollowingUserId()));
            if(totalUsersUnFollowed >  numberUsersToUnFollow) {
                return totalUsersUnFollowed;
            }

            if (mAdvanceSetting != null && mAdvanceSetting.isActive()) {
                if (this.isWhiteUser(followingUser.getFollowingUserId())) {
                    logger.info(buildLog(" user " + followingUser.getFollowingUserId() + " is white user"));

                    continue;
                }
            }

            JSONObject friendship = this.getFriendshipStatus(followingUser.getFollowingUserId());
            if (!friendship.has("result")) {
                if (friendship.has("status_code") && friendship.getInt("status_code") == 404)
                    mDbHelper.removeFollowingUser(followingUser.getId());

                continue;
            }

            FriendshipHandler friendshipHandler = new FriendshipHandler(friendship.getJSONObject("result"));

            if (!Objects.equals(friendshipHandler.getStatus(), "ok"))
                continue;

            if (friendshipHandler.isFollowing() && !friendshipHandler.isFollower()) {
                logger.debug(buildLog(" friendship " + friendshipHandler));
                logger.info(buildLog(" user " + followingUser.getFollowingUserId()));

                // unfollow usser
                boolean result = this.unfollowUser(followingUser);
                if (result) {
                    // Tang so luong nguoi da unfollow
                    totalUsersUnFollowed++;

                    mDbHelper.removeFollowingUser(followingUser.getId());

                    // create activity
                    String instagramUsername = followingUser.getInstagramUsername();
                    String avatarUrl = followingUser.getAvatarUrl();
                    this.createActivity(instagramUsername, avatarUrl);

                    // update list unfollowed users
                    this.addToPreviouslyUnfollowUser(followingUser.getFollowingUserId());
                }
                // break;  // Ko hieu tai sao lai break o day?
            } else if (friendshipHandler.isFollower()) {
                // update has checked
//                logger.info(buildLog("[successfull] user " + followingUser.getFollowingUserId()));
                mDbHelper.updateCheckedFollowingUser(followingUser.getId());
            } else {
                // remove following user in db
                logger.info(buildLog(" user " + followingUser.getFollowingUserId()));
                mDbHelper.removeFollowingUser(followingUser.getId());
            }
            
            Utils.waitFor(10, 30);	// wait for 10 ~ 30s 
        }

        return totalUsersUnFollowed;
    }
    
    private StringBuilder buildLog(String msg) {
    		return super.buildLog().append(msg);
    }
}
