package jp.co.rilarc.instagram_robot.unfollow;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.handler.FriendshipHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.FollowingUser;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public class UnfollowBoost extends Unfollow {
	
	final protected static Logger logger = LogManager.getLogger(UnfollowBoost.class);

    final protected int numberUsersToUnFollow;

    public UnfollowBoost(Instagram instagram, RobotSetting robotSetting, int userId,
                         IAPIHelper dbHelper, AdvanceSetting advanceSetting,
                         Statistic statistic, int numberUsersToUnFollow) {
        super(instagram, robotSetting, userId, dbHelper, advanceSetting, statistic);

        this.numberUsersToUnFollow = numberUsersToUnFollow;
    }

    @Override
    public int autoUnfollow() throws ActionBlockException, InstagramCannotAccessException {

        int totalUsersUnFollowed = 0;

        // get list following user after
        ArrayList<FollowingUser> checkingUsers = mDbHelper.getListFollowingUser(null);

        if (checkingUsers.size() == 0 || mRobotSetting.getUnfollowsBoostAmount() <= 0) {
            this.disableUnfollowBoost();
            return 0;
        }

        VLogger.append("「フォローしているユーザーが全てが対象で、古いフォローユーザーから順に入力された人数のフォローを解除する。」タスクを実行します");

        for (FollowingUser followingUser : checkingUsers) {
            if(totalUsersUnFollowed >  numberUsersToUnFollow) {
                return totalUsersUnFollowed;
            }
            logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": unfollow checking user " + followingUser.getFollowingUserId());

            if (mAdvanceSetting != null) {
                if (this.isWhiteUser(followingUser.getFollowingUserId())) {
                    logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": user " + followingUser.getFollowingUserId() + " is white user");

                    continue;
                }
            }

            JSONObject friendship = this.getFriendshipStatus(followingUser.getFollowingUserId());

            if (!friendship.has("result")) {
                if (friendship.has("status_code") && friendship.getInt("status_code") == 404)
                    mDbHelper.removeFollowingUser(followingUser.getId());

                continue;
            }


            FriendshipHandler friendshipHandler = new FriendshipHandler(friendship.getJSONObject("result"));
            if (!Objects.equals(friendshipHandler.getStatus(), "ok"))
                continue;

            if (friendshipHandler.isFollowing()) {
                logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": friendship " + friendshipHandler);
                logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": unfollowing user " + followingUser.getFollowingUserId());

                // unfollow usser
                boolean result = this.unfollowUser(followingUser);
                if (result) {

                    // Tang so luong nguoi da unfollow
                    totalUsersUnFollowed++;

                    mDbHelper.removeFollowingUser(followingUser.getId());

                    // update list unfollowed users
                    this.addToPreviouslyUnfollowUser(followingUser.getFollowingUserId());

                    // create activity
                    String instagramUsername = followingUser.getInstagramUsername();
                    String avatarUrl = followingUser.getAvatarUrl();
                    this.createActivity(instagramUsername, avatarUrl);

                    mRobotSetting.setUnfollowsBoostAmount(mRobotSetting.getUnfollowsBoostAmount() - 1);
                    // reduce boost amount
                    if (mRobotSetting.getUnfollowsBoostAmount() >= 1)
                        this.reduceBoostAmount(false);
                    else {
                        this.reduceBoostAmount(true);
                        break;
                    }
                }

//                break;
                Utils.waitForMinutes(1, 3);  // wait for 1~3 minutes
            } else {
                // remove following user in db
                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + "unfollowed user " + followingUser.getFollowingUserId());
                mDbHelper.removeFollowingUser(followingUser.getId());
            }
        }

        return totalUsersUnFollowed;
    }

    private void disableUnfollowBoost() {
        mDbHelper.updateUnfollowBoostStatus(false);
    }

    private void reduceBoostAmount(boolean isDisableBoost) {
        if (isDisableBoost)
            this.disableUnfollowBoost();

        mDbHelper.reduceBoostAmount();
    }
}
