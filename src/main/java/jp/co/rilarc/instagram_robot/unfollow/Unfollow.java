package jp.co.rilarc.instagram_robot.unfollow;

import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.handler.InstagramUser;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.FollowingUser;
import jp.co.rilarc.instagram_robot.model.InstagramResponse;
import jp.co.rilarc.instagram_robot.model.PreviouslyUnfollowUser;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public abstract class Unfollow {

    final protected static Logger logger = LogManager.getLogger(Unfollow.class);

    final protected Instagram mInstagram;

    final protected RobotSetting mRobotSetting;

    final protected AdvanceSetting mAdvanceSetting;

    final protected IAPIHelper mDbHelper;

    final protected Statistic statistic;

    final protected int mUserId;

    public Unfollow(Instagram instagram, RobotSetting robotSetting, int userId,
                    IAPIHelper dbHelper, AdvanceSetting advanceSetting, Statistic statistic) {
        mInstagram = instagram;
        mRobotSetting = robotSetting;
        mAdvanceSetting = advanceSetting;
        mUserId = userId;
        mDbHelper = dbHelper;
        this.statistic = statistic;
    }

    public abstract int autoUnfollow() throws ActionBlockException, InstagramCannotAccessException;

    protected boolean unfollowUser(FollowingUser followingUser) throws ActionBlockException, InstagramCannotAccessException {
        boolean result = false;
        String username = followingUser.getInstagramUsername();
        if (!TextUtils.isEmpty(username)) {
            result = mInstagram.unfollowUserByJs(username);
            logger.info("[Unfollow] " + username + " by js result: " + result);
        }
        if (!result) {
            String userId = followingUser.getFollowingUserId();
            try {
                InstagramResponse response = mInstagram.unfollow(userId);

                //check like result
                if (response.getStatusCode() == 200) {
                    result = true;
                } else {
                    String responseAsString = response.getResponseAsString();
                    logger.info(buildLog().append(" reponse " + responseAsString));
                }
            } catch (IOException | URISyntaxException e) {
                logger.error(buildLog(), e);
            }
        }

        if (result) {
            statistic.increaseNumUnfollows();
            mDbHelper.increaseNumUnfollowsStatistic(statistic.getId());

            logger.info(buildLog().append("[successfull] user " + followingUser.getFollowingUserId()));
            VLogger.append(statistic.getNumUnfollows() + "回目のアンフォロー: https://www.instagram.com/" + followingUser.getInstagramUsername());
        } else {
            VLogger.append("【失敗】アンフォローユーザー: https://www.instagram.com/" + followingUser.getFollowingUserId());
        }
        return result;
    }

    protected void addToPreviouslyUnfollowUser(String unfollowUserId) {
        PreviouslyUnfollowUser previouslyUnfollowUser = mDbHelper.getPreviouslyUnfollowUser();

        JSONArray unfollowUsersList;
        if(previouslyUnfollowUser == null) {
            unfollowUsersList = new JSONArray();
        }else {
            String unfollowUsers = previouslyUnfollowUser.getUnfollowedUsers();

            if (unfollowUsers == null)
                unfollowUsersList = new JSONArray();
            else
                unfollowUsersList = new JSONArray(unfollowUsers);

        }

        unfollowUsersList.put(unfollowUserId);

        String listUnfollowUsers = unfollowUsersList.toString();

        mDbHelper.updateUnfollowedUsers(listUnfollowUsers);
    }

    protected JSONObject getFriendshipStatus(String userId) throws InstagramCannotAccessException {
        JSONObject returnData = new JSONObject();
        returnData.putOpt("result", null);
        returnData.put("status_code", 0);

        try {
            InstagramResponse response = mInstagram.getUserFriendship(userId);

            //get response body
            String responseAsString = response.getResponseAsString();

            returnData.remove("status_code");
            returnData.putOpt("status_code", response.getStatusCode());

            if (response.getStatusCode() == 200) {
                returnData.remove("result");
                returnData.putOpt("result", new JSONObject(responseAsString));
            } else {
                if (response.getStatusCode() == 404)
                    logger.info(buildLog().append(": get friend ship user not found"));
                else
                    logger.info(buildLog().append(" response = " + responseAsString));
            }
        } catch (IOException | URISyntaxException e) {
            logger.error(buildLog(), e);
        }

        return returnData;
    }

    protected boolean isWhiteUser(String userId) {
        String whiteList = mAdvanceSetting.getWhiteList();

        if (whiteList != null) {
            InstagramUser instagramUser = mDbHelper.getInstagramUser(userId);

            if (instagramUser != null) {
                JSONArray whiteArr = new JSONArray(whiteList);

                for (int i = 0; i < whiteArr.length(); i++) {
                    if (Objects.equals(whiteArr.getString(i), instagramUser.getUsername()))
                        return true;
                }
            }
        }

        return false;
    }

    protected void createActivity(String instagramUsername, String avatarUrl) {
        mDbHelper.insertActivity(Const.ACTIVITY_TYPE_UNFOLLOW, mInstagram.getInstagramAccount().getUsername(), instagramUsername, avatarUrl, null, null, null, null);
    }

    protected StringBuilder buildLog() {
        return new StringBuilder("[" + mUserId + "][" + mInstagram.getInstagramAccount().getUsername() + "][Unfollow]");
    }
}
