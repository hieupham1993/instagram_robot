package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONObject;

/**
 * Created by nguyenthanhson on 10/27/17.
 */
public class ConfigureImageHandler {
    private JSONObject configureImage;

    public ConfigureImageHandler(JSONObject uploadImage) {
        this.configureImage = uploadImage;
    }

    public String getMediaCode() {
        return configureImage.getJSONObject("media").getString("code");
    }

    public String getStatus() {
        return configureImage.getString("status");
    }

    @Override
    public String toString() {
        return configureImage.toString();
    }
}
