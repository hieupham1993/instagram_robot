package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MediaHandler {
    private JSONObject mMedia;
    private JSONArray hashtags = null;

    public MediaHandler (JSONObject media){
        if (media.has("media_or_ad"))
            mMedia = media.getJSONObject("media_or_ad");
        else
            mMedia = media;
    }

    public int getLikeCount()
    {
        return mMedia.getInt("like_count");
    }

    public JSONArray getListHashtags() {
        if (hashtags == null)
            hashtags = this.parseHashtags();

        return hashtags;
    }

    private JSONArray parseHashtags() {
        JSONArray hashtags = new JSONArray();

        if (mMedia.has("caption") && !mMedia.isNull("caption")) {
            String caption = mMedia.getJSONObject("caption").getString("text");

            Pattern MY_PATTERN = Pattern.compile("#([^\\s#]+)");
            Matcher mat = MY_PATTERN.matcher(caption);
            while (mat.find()) {
                hashtags.put(mat.group(1));
            }
        }

        return hashtags;
    }

    public String getMediaId()
    {
        return mMedia.getString("id");
    }

    public boolean hasLiked() {
        return mMedia.getBoolean("has_liked");
    }

    public boolean hasFollowedOrRequested() {
        return mMedia.getJSONObject("user").getJSONObject("friendship_status").getBoolean("following") ||
                mMedia.getJSONObject("user").getJSONObject("friendship_status").getBoolean("outgoing_request");
    }

    public boolean isFromPrivateUser() {
        return mMedia.getJSONObject("user").getBoolean("is_private");
    }

    public boolean isMedia() {
        return (mMedia.has("media_type") && !mMedia.isNull("media_type"));
    }

    public String getUserId() {
        BigInteger userId = mMedia.getJSONObject("user").getBigInteger("pk");
        return String.valueOf(userId);
    }

    public String getUsername() {
        return mMedia.getJSONObject("user").getString("username");
    }

    public String getAvatarUrl() {
        return mMedia.getJSONObject("user").getString("profile_pic_url");
    }

    public String getMediaPost() {
        return mMedia.getString("code");
    }

    public String getImagePost() {
        if (mMedia.has("carousel_media"))
            return mMedia.getJSONArray("carousel_media").getJSONObject(0).getJSONObject("image_versions2").getJSONArray("candidates").getJSONObject(0).getString("url");
        return mMedia.getJSONObject("image_versions2").getJSONArray("candidates").getJSONObject(0).getString("url");
    }
}
