package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListFollowHandler {
    private JSONObject mListFollow;

    public ListFollowHandler(JSONObject listFollower) {
        mListFollow = listFollower;
    }

    public String getStatus() {
        return mListFollow.getString("status");
    }

    public boolean hasNext() {
        return mListFollow.has("next_max_id");
    }

    public String getNextPageId() {
        return mListFollow.getString("next_max_id");
    }

    public ArrayList<String> getListIds() {
        ArrayList<String> listIds = new ArrayList<>();

        JSONArray listUsers = mListFollow.getJSONArray("users");

        for (int i=0; i<listUsers.length(); i++) {
            JSONObject user = listUsers.getJSONObject(i);
            listIds.add(String.valueOf(user.getBigDecimal("pk")));
        }

        return listIds;
    }

    public String searchUsername(String userId) {
        JSONArray listUsers = mListFollow.getJSONArray("users");

        for (int i=0; i<listUsers.length(); i++) {
            JSONObject user = listUsers.getJSONObject(i);
            if (String.valueOf(user.getBigDecimal("pk")).equals(userId))
                return user.getString("username");
        }

        return null;
    }
}
