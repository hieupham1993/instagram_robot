package jp.co.rilarc.instagram_robot.handler;

public class InstagramUser {
    private int mId;
    private String mInstagramUserId;
    private String mUsername;

    public InstagramUser(int mId, String mInstagramUserId, String mUsername) {
        this.mId = mId;
        this.mInstagramUserId = mInstagramUserId;
        this.mUsername = mUsername;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getInstagramUserId() {
        return mInstagramUserId;
    }

    public void setInstagramUserId(String mInstagramUserId) {
        this.mInstagramUserId = mInstagramUserId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String mUsername) {
        this.mUsername = mUsername;
    }
}
