package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONObject;

public class FriendshipHandler {
    private JSONObject mFriendship;

    public FriendshipHandler(JSONObject friendship) {
        mFriendship = friendship;
    }

    public boolean isFollowing() {
        return mFriendship.getBoolean("following");
    }

    public boolean isFollower() {
        return mFriendship.getBoolean("followed_by");
    }

    public String getStatus() {
        return mFriendship.getString("status");
    }

    @Override
    public String toString() {
        return mFriendship.toString();
    }
}
