package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONArray;
import org.json.JSONObject;

public class SearchMediaHandler {
    private JSONObject mSearchMediaResponse;

    public SearchMediaHandler(JSONObject searchMediaResponse) {
        mSearchMediaResponse = searchMediaResponse;
    }

    public JSONArray getMedias() {
        return mSearchMediaResponse.getJSONArray("items");
    }

    public String getStatus() {
        return mSearchMediaResponse.getString("status");
    }

    public boolean hasNext() {
        return mSearchMediaResponse.getBoolean("more_available");
    }

    public String getNextPageId() {
        return mSearchMediaResponse.getString("next_max_id");
    }
}
