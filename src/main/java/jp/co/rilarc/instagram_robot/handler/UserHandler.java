package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONObject;

public class UserHandler {
    private JSONObject mUser;

    public UserHandler(JSONObject user) {
        mUser = user;
    }

    public String getBiography() {
        return mUser.getJSONObject("user").getString("biography");
    }
}
