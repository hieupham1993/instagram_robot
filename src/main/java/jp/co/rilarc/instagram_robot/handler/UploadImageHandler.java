package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONObject;

import java.math.BigInteger;

/**
 * Created by nguyenthanhson on 10/26/17.
 */
public class UploadImageHandler {
    private JSONObject uploadImage;

    public UploadImageHandler(JSONObject uploadImage) {
        this.uploadImage = uploadImage;
    }

    public BigInteger getUploadId() {
        return uploadImage.getBigInteger("upload_id");
    }

    public String getStatus() {
        return uploadImage.getString("status");
    }

    @Override
    public String toString() {
        return uploadImage.toString();
    }
}
