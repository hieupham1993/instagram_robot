package jp.co.rilarc.instagram_robot.handler;

import org.json.JSONArray;
import org.json.JSONObject;

public class TimelineHandler {
    private JSONObject mGetTimelineResponse;

    public TimelineHandler(JSONObject searchMediaResponse) {
        mGetTimelineResponse = searchMediaResponse;
    }

    public JSONArray getMedias() {
        return mGetTimelineResponse.getJSONArray("feed_items");
    }

    public String getStatus() {
        return mGetTimelineResponse.getString("status");
    }

    public boolean hasNext() {
        return mGetTimelineResponse.getBoolean("more_available");
    }

    public String getNextPageId() {
        return mGetTimelineResponse.getString("next_max_id");
    }
}
