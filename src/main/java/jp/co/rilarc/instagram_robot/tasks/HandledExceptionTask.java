package jp.co.rilarc.instagram_robot.tasks;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class HandledExceptionTask extends BaseTask {

    private static final Logger LOGGER = LogManager.getLogger(HandledExceptionTask.class);

    @Override
    public final void run() {
        try {
            doWork();
        } catch (Exception e) {
            LOGGER.warn("UnhandledException", e);
            onExceptionOccur(e);
        }
    }

    protected void onExceptionOccur(Exception e) {

    }

    protected abstract void doWork() throws Exception;
}
