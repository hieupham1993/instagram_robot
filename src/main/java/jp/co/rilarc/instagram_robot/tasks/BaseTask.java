package jp.co.rilarc.instagram_robot.tasks;

import java.util.concurrent.TimeUnit;

/**
 * Created by HieuPT on 11/14/2017.
 */
abstract class BaseTask implements Runnable, IDestroyable {

    protected final void sleep(int time, TimeUnit timeUnit) {
        try {
            Thread.sleep(timeUnit.toMillis(time));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
