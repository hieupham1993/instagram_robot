package jp.co.rilarc.instagram_robot.tasks;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.api.IWssjServices;
import jp.co.rilarc.instagram_robot.api.requests.WssjRegisterInstagramAccountRequest;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.exception.WebDriverNotFoundException;
import jp.co.rilarc.instagram_robot.messenger.Message;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.RequestQueue;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import jp.co.rilarc.instagram_robot.utils.WebDriverUtils;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;
import jp.co.rilarc.instagram_robot.webcontroller.responses.IActionResponse;
import jp.co.rilarc.instagram_robot.webcontroller.responses.LoginErrorResponse;

/**
 * Created by HieuPT on 11/14/2017.
 */
public class LoginInstagramWebTask extends BroadcastTask {

    private static final Logger LOGGER = LogManager.getLogger(LoginInstagramWebTask.class);

    private WebDriver webDriver;

    private final String username;

    private final String password;

    public LoginInstagramWebTask(String username, String password) {
        this.username = username;
        this.password = password;
    }

    private void initWebDriver() throws WebDriverNotFoundException {
        webDriver = WebDriverUtils.createWebDriver();
        webDriver.manage().window().maximize();
        webDriver.get(InstagramWebController.INSTAGRAM_BASE_URL);
    }

    @Override
    protected void doWork() throws Exception {
        initWebDriver();
        InstagramWebController instagramController = new InstagramWebController(webDriver);
        instagramController.login(username, password, new IActionResponse<Void, LoginErrorResponse>() {

            @Override
            public void onSuccess(Void data) {
                LOGGER.info("Login instagram web success");
                InstagramDb instagramDb = Main.getInstance().getInstagramDb();
                instagramDb.setCookies(webDriver.manage().getCookies());
                LocalDatabaseHelper.getInstance().updateInstagram(instagramDb);
                VLogger.append("アカウント【" + username + "】でインスタグラムにログインしました!");
                Preference preference = LocalDatabaseHelper.getInstance().getPreference();
                RequestQueue.getInstance().executeRequest(IWssjServices.SERVICES.registerInstagramAccount(preference.getAccessToken(),
                        new WssjRegisterInstagramAccountRequest(instagramDb.getUsername(), instagramDb.getPassword(), instagramDb.getInstagramUserId())));
                sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_WEB_SUCCESS));
            }

            @Override
            public void onError(LoginErrorResponse errorResponse) {
                LOGGER.info("Login instagram web failure");
                sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_WEB_FAILURE));
            }
        });
        destroy();
    }

    @Override
    protected void onExceptionOccur(Exception e) {
        sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_WEB_FAILURE));
        destroy();
    }

    @Override
    public void destroy() {
        if (webDriver != null) {
            try {
                webDriver.close();
                webDriver.quit();
            } catch (WebDriverException e) {
                e.printStackTrace();
            } finally {
                webDriver = null;
            }
        }
    }
}
