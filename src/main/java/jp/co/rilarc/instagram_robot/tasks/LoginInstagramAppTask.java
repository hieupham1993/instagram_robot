package jp.co.rilarc.instagram_robot.tasks;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.action.UserPool;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.messenger.Message;
import jp.co.rilarc.instagram_robot.model.InstagramLoginResponse;
import jp.co.rilarc.instagram_robot.model.InstagramUserInfo;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 11/14/2017.
 */
public class LoginInstagramAppTask extends BroadcastTask {

    private static final Logger LOGGER = LogManager.getLogger(LoginInstagramAppTask.class);

    private static final boolean SIMULATE_LOGIN = false;

    private final Preference preference;

    private final IAPIHelper apiHelper;

    private final String username;

    private final String password;

    private final boolean forceLoginWeb;

    public LoginInstagramAppTask(String username, String password, boolean forceLoginWeb) {
        this.username = username;
        this.password = password;
        this.forceLoginWeb = forceLoginWeb;
        this.preference = LocalDatabaseHelper.getInstance().getPreference();
        this.apiHelper = new DatabaseHelper(new Configuration());
    }

    @Override
    protected void doWork() {
        if (!SIMULATE_LOGIN) {
            try {
                LOGGER.info("Login instagram using api");
                Instagram instagram = UserPool.getInstagram(preference.getUserId(), username, password, apiHelper);
                if (instagram != null) {
                    LOGGER.info("Login instagram using api success");
                    InstagramLoginResponse loginResponse = instagram.getInstagramLoginResponse();
                    if (loginResponse != null) {
                        InstagramUserInfo userInfo = loginResponse.getUserInfo();
                        if (userInfo != null) {
                            LocalDatabaseHelper localDatabaseHelper = LocalDatabaseHelper.getInstance();
                            Preference preference = localDatabaseHelper.getPreference();
                            InstagramDb instagramDb = Main.getInstance().getInstagramDb();
                            instagramDb.setUserId(preference.getUserId());
                            instagramDb.setUsername(userInfo.getUsername());
                            instagramDb.setPassword(password);
                            instagramDb.setInstagramUserId(userInfo.getPk());
                            instagramDb.setAvatarUrl(userInfo.getProfilePicUrl());
                            localDatabaseHelper.updateInstagram(instagramDb);
                            preference.setInstagramUsername(userInfo.getUsername());
                            localDatabaseHelper.updatePreference(preference);
                            sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_SUCCESS, forceLoginWeb));
                        } else {
                            sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_FAILURE));
                        }
                    } else {
                        sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_FAILURE));
                    }
                } else {
                    LOGGER.info("Login instagram using api failure");
                    sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_FAILURE));
                }
            } catch (InstagramCannotAccessException e) {
                LOGGER.info("Login instagram using api failure", e);
                sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_FAILURE));
            }
        } else {
            sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_SUCCESS, forceLoginWeb));
        }
    }

    @Override
    protected void onExceptionOccur(Exception e) {
        sendMessage(Message.obtain(Const.Msg.MSG_LOGIN_APP_FAILURE));
    }
}
