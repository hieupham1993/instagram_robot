package jp.co.rilarc.instagram_robot.tasks;

import javafx.application.Platform;
import jp.co.rilarc.instagram_robot.messenger.Message;
import jp.co.rilarc.instagram_robot.messenger.Messenger;

/**
 * Created by HieuPT on 11/29/2017.
 */
public abstract class BroadcastTask extends HandledExceptionTask {

    private Messenger messenger;

    public final void setMessenger(Messenger messenger) {
        this.messenger = messenger;
    }

    public final Messenger getMessenger() {
        return messenger;
    }

    protected final void sendMessage(Message message) {
        Platform.runLater(() -> {
            if (messenger != null) {
                messenger.sendMessage(message);
            }
        });
    }
}
