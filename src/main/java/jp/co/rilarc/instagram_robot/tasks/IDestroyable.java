package jp.co.rilarc.instagram_robot.tasks;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface IDestroyable {

    default void destroy() {
    }
}
