/**
 * Created by nguyenthanhson on 7/4/17.
 */

package jp.co.rilarc.instagram_robot.instagram;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import jp.co.rilarc.instagram_robot.utils.Const;


public class Signatures {

    public static String generateSignature(Map<String, String> data) throws UnsupportedEncodingException {
        JSONObject jsonData = new JSONObject();

        for (Map.Entry<String, String> entry : data.entrySet()) {
            if ((Utils.isJSONOjectValid(entry.getValue()) || Utils.isJSONArrayValid(entry.getValue())) && !Objects.equals(entry.getKey(), "location")) {
                if (Utils.isJSONOjectValid(entry.getValue()))
                    jsonData.put(entry.getKey(), new JSONObject(entry.getValue()));
                else
                    jsonData.put(entry.getKey(), new JSONArray(entry.getValue()));

            } else {
                jsonData.put(entry.getKey(), entry.getValue());
            }
        }

        String hash = sha256HexBinaryString(jsonData.toString(), Const.IG_SIG_KEY);


        return "ig_sig_key_version=" + Const.SIG_KEY_VERSION + "&signed_body=" + hash + "." + URLEncoder.encode(jsonData.toString(), "UTF-8");
    }

    public static String generateDeviceId() {
        long currentNanoTime = System.nanoTime();

        String hashString = md5(Long.toString(currentNanoTime));


        return "android-" + hashString.substring(0, 16);
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public static String md5(String input) {
        String md5 = null;

        if (null == input) return null;

        try {

            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //Update input string in message digest
            digest.update(input.getBytes(), 0, input.length());

            //Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }

        return md5;
    }

    private static byte[] sha256(String data, String key) {
        byte[] hash = null;

        if (null == data) return null;

        try {

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            hash = sha256_HMAC.doFinal(data.getBytes());
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {

            e.printStackTrace();
        }

        return hash;
    }

    public static String sha256HexBinaryString(String data, String key) {
        return DatatypeConverter.printHexBinary(sha256(data, key)).toLowerCase();
    }

    public static String sha256Base64String(String data, String key) {
        return Base64.encodeBase64String(sha256(data, key));
    }

    public static int randomInt(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }
}
