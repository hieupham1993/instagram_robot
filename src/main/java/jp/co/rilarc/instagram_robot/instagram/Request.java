package jp.co.rilarc.instagram_robot.instagram;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.utils.Const;

public class Request {
    private String _url;
    private Map<String, String> _params = new HashMap<>();
    private Map<String, String> _posts = new HashMap<>();
    private Map<String, String> _files = new HashMap<>();
    private Map<String, String> _headers;
    private String _boundary;
    private BasicCookieStore _cookies = null;
    private Boolean _signedPost = true;
    private Proxy mProxy;



    public Request(String url, Map<String, String> headers, Proxy proxy) {
        _url = url;
        _headers = headers;

        mProxy = proxy;
    }

    public Request addParam(String key, String value) {
        _params.put(key, value);
        return this;
    }

    public Request addPost(String key, String value) {
        _posts.put(key, value);
        return this;
    }

    public Request addFile(String key, String value) {
        _files.put(key, value);
        return this;
    }

    public Request setBoundary(String boundary) {
        _boundary = boundary;
        return  this;
    }

    public Request setSignedPost(Boolean signedPost) {
        _signedPost = signedPost;
        return this;
    }

    public Request addHeader(String key, String value) {
        _headers.put(key, value);
        return this;
    }

    public Request setCookies(BasicCookieStore cookies) {
        _cookies = cookies;
        return this;
    }

    public BasicCookieStore getCookies() {
        return _cookies;
    }

    public HttpResponse getResponse() throws URISyntaxException, IOException {

        String endpoint = this.buildEndpoint(_url);

        // using proxy
        CloseableHttpClient client;
        RequestConfig config;
        if (mProxy != null && mProxy.isValidProxy()) {
            // init
            HttpHost proxy = new HttpHost(mProxy.getProxyAddress(), mProxy.getProxyPort());


            config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build();


            // if using auth proxy
            if (mProxy.needAuth()) {
                CredentialsProvider credsProvider = new BasicCredentialsProvider();
                credsProvider.setCredentials(
                        new AuthScope(mProxy.getProxyAddress(), mProxy.getProxyPort()),
                        new UsernamePasswordCredentials(mProxy.getProxyUsername(), mProxy.getProxyPassword()));
                client = HttpClients.custom()
                        .setDefaultCredentialsProvider(credsProvider).build();
            } else {
                client = HttpClients.createDefault();
            }
        } else {
            client = HttpClients.createDefault();
            config = RequestConfig.DEFAULT;
        }


        HttpClientContext context = HttpClientContext.create();
        HttpResponse response;

        if (_cookies != null)
            context.setCookieStore(_cookies);

        if (!_posts.isEmpty() || !_files.isEmpty()) //post method
        {
            HttpPost request = new HttpPost(endpoint);

            for (Map.Entry<String, String> entry : _headers.entrySet())
                request.addHeader(entry.getKey(), entry.getValue());

            HttpEntity entity = this.buildBodyEntity();
            request.setEntity(entity);

            request.setConfig(config);
            response = client.execute(request, context);
        }
        else //get method
        {
            HttpGet request = new HttpGet(endpoint);

            for (Map.Entry<String, String> entry : _headers.entrySet())
                request.setHeader(entry.getKey(), entry.getValue());

            request.setConfig(config);
            response = client.execute(request, context);
        }

        BasicCookieStore cookieStore = (BasicCookieStore) context.getCookieStore();
        if (!cookieStore.getCookies().isEmpty())
            this.setCookies(cookieStore);

        return response;
    }

    private String buildEndpoint(String url) throws URISyntaxException {
        if (url.contains("http"))
            return url;

        String endpoint = Const.INSTAGRAM_HOST + _url;

        URIBuilder builder = new URIBuilder()
                .setScheme(Const.INSTAGRAM_SCHEME)
                .setHost(endpoint);

        // create query string
        if (!_params.isEmpty()) {
            List<NameValuePair> queryParams = new ArrayList<>();
            for (Map.Entry<String, String> entry : _params.entrySet()) {
                queryParams.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            builder.setParameters(queryParams);
        }

        URI uri = builder.build();
        endpoint = uri.toString();

        return endpoint;
    }

    private HttpEntity buildBodyEntity() throws UnsupportedEncodingException {
        HttpEntity result;
        if (!_files.isEmpty()) { // post multipart

            MultipartEntityBuilder multipartBuilder = MultipartEntityBuilder.create();
            multipartBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            for (Map.Entry<String, String> entry : _files.entrySet()) {
                File file = new File(entry.getValue());
                FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY, "pending_media_" + System.currentTimeMillis() +".jpg");

                multipartBuilder.addPart(entry.getKey(), fileBody);
            }

            for (Map.Entry<String, String> entry : _posts.entrySet()) {
                StringBody stringBody = new StringBody(entry.getValue(), ContentType.MULTIPART_FORM_DATA);
                multipartBuilder.addPart(entry.getKey(), stringBody);
            }
            multipartBuilder.setBoundary(_boundary);
            result = multipartBuilder.build();
        }
        else //post form
        {
            if (_signedPost) {
                String postData = Signatures.generateSignature(_posts);

                result = new StringEntity(postData, "UTF-8");
            } else {
                List<NameValuePair> postBody = new ArrayList<>();
                for (Map.Entry<String, String> entry : _posts.entrySet())
                    postBody.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));

                result = new UrlEncodedFormEntity(postBody, "UTF-8");
            }
        }

        return result;
    }
}
