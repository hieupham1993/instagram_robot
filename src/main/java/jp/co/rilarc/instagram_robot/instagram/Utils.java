package jp.co.rilarc.instagram_robot.instagram;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;

/**
 * Created by nguyenthanhson on 10/24/17.
 */
class Utils {
    static boolean isImage(File imageFile) {
        boolean result = false;

        try {
            if (imageFile.exists() && !imageFile.isDirectory()) {
                Image image = ImageIO.read(imageFile);

                if (image != null)
                    result = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    static void throwIfIllegalMediaResolution(String fileType, String filePath, int width, int height) {
        // Check Resolution.
        if (Objects.equals(fileType, "photofile")) {
            // Validate photo resolution.
            if (width > ImageAutoResizer.MAX_WIDTH || height > ImageAutoResizer.MAX_HEIGHT) {
                throw new IllegalArgumentException("Instagram only accepts photos with a maximum resolution up to " + ImageAutoResizer.MAX_WIDTH + "x" + ImageAutoResizer.MAX_HEIGHT + ". " +
                        "Your file " + filePath + " has a " + width + "x" + height + " resolution.");
            }
        } else {
            // Validate video resolution. Instagram allows between 320px-1080px width.
            // NOTE: They have height-limits too, but we automatically enforce
            // those when validating the aspect ratio further down.
            if (width < 320 || width > 1080) {
                throw new IllegalArgumentException("Instagram only accepts videos that are between 320 and 1080 pixels wide. Your file " + filePath + " is " + width + " pixels wide.");
            }
        }

        // Check Aspect Ratio.
        // NOTE: This Instagram rule is the same for both videos and photos.
        // See ImageAutoResizer for the latest up-to-date allowed ratios.
        double aspectRatio = ((double) width / height);
        if (aspectRatio < ImageAutoResizer.MIN_RATIO || aspectRatio > ImageAutoResizer.MAX_RATIO) {
            throw new IllegalArgumentException("Instagram only accepts media with aspect ratios between " + ImageAutoResizer.MIN_RATIO + " and " + ImageAutoResizer.MAX_RATIO + ". Your file " + filePath + " has a " + aspectRatio + " aspect ratio.");
        }
    }

    static long generateUploadId() {
        return System.currentTimeMillis();
    }

    static boolean isJSONOjectValid(String text) {
        try {
            new JSONObject(text);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }

    static boolean isJSONArrayValid(String text) {
        try {
            new JSONArray(text);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }

    static String generateUserBreadcrumb (int size) {
        String key = "iN4$aGr0m";
        long date = System.currentTimeMillis();

        int term = Signatures.randomInt(2, 3) * 1000 + size * Signatures.randomInt(15, 20) * 100;

        int text_change_event_count = size / Signatures.randomInt(2, 3);
        if (text_change_event_count == 0)
            text_change_event_count = 1;

        String data = size + " " + term + " " + text_change_event_count + " " + date;

        return Signatures.sha256Base64String(data, key) + "\r\n" + Base64.encodeBase64String(data.getBytes()) + "\r\n";
    }

    static String downloadImage(String imageUrl) throws IOException {
        URL url = new URL(imageUrl);
        String fileName = url.getFile();
        String destName = "images/" + fileName.substring(fileName.lastIndexOf("/"));
        File imageFile = new File(destName);
        FileUtils.copyURLToFile(url, imageFile);

        return imageFile.getPath();
    }
}
