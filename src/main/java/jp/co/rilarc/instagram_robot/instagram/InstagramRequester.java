package jp.co.rilarc.instagram_robot.instagram;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.model.Action;

public class InstagramRequester {
	final private static Logger logger = LogManager.getLogger(InstagramRequester.class);
	
	private Instagram mInstagram;
	
    public InstagramRequester(Instagram instagram) {
        mInstagram = instagram;
    }

    public JSONObject searchMediaByHashtag(String hashtag, String maxId) throws InstagramCheckpointRequiredException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getHashtagFeed(hashtag, maxId);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                if (response.getStatusLine().getStatusCode() == 404)
                    logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": search hashtag failed");
                else
//                    logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + responseAsString);
                	processWhenGetResponseFailed(responseAsString);
            }

        } catch (IOException | URISyntaxException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }

        return result;
    }

    public JSONObject searchMediaByLocation(String locationId, String maxId) throws InstagramCheckpointRequiredException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getLocationFeed(locationId, maxId);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                processWhenGetResponseFailed(responseAsString);
            }

        } catch (IOException | URISyntaxException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }

        return result;
    }

    public JSONObject searchMediaFromTimeline(String maxId) throws InstagramCheckpointRequiredException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getTimelineFeed(maxId);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
//                logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + responseAsString);
            	processWhenGetResponseFailed(responseAsString);
            }

        } catch (IOException | URISyntaxException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }

        return result;
    }

    public JSONObject getUserInfo(String userId) throws InstagramCheckpointRequiredException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getUserInfo(userId);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
//                logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + responseAsString);
                processWhenGetResponseFailed(responseAsString);
            }

        } catch (IOException | URISyntaxException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }

        return result;
    }
    
	public void processWhenGetResponseFailed(String responseAsString) throws InstagramCheckpointRequiredException {
	    	if(responseAsString == null) {
	    		return;
	    	}
	    	
	    	if(responseAsString.contains("The link you followed may be broken")) {
    			logger.warn("["+ mInstagram.getInstagramAccount().getUsername() + "] The link you followed may be broken, or the page may have been removed.");
    		}else {
    			logger.error("["+ mInstagram.getInstagramAccount().getUsername() + "] : " + responseAsString);
    		}
	    	
	    	if(responseAsString.contains("checkpoint_required")) {
	    		throw new InstagramCheckpointRequiredException(
	        			String.format("account %s need to verify from instagram app", mInstagram.getInstagramAccount().getUsername()));
	    	}
    }
    
    public void processIfSpammed(String responseAsString, Action spamType) throws ActionBlockException {
	    	if(responseAsString == null) {
	    		return;
	    	}
	    	logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + responseAsString);
	    	
	    	if(responseAsString.contains("Action Blocked")) {
	    		throw new ActionBlockException(
	        			String.format("account %s was blocked because spam in action %s", mInstagram.getInstagramAccount().getUsername(), spamType.toString()));
	    	}
    }

    public JSONObject uploadPhoto(String imagePath, JSONObject externalMetadata) throws InstagramCheckpointRequiredException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.uploadTimelinePhoto(imagePath, externalMetadata);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                processWhenGetResponseFailed(responseAsString);
            }

        } catch (IOException | URISyntaxException | IllegalArgumentException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }

        return result;
    }

    public JSONObject sendComment(String mediaId, String commentContent) throws InstagramCheckpointRequiredException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.comment(mediaId, commentContent);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                processWhenGetResponseFailed(responseAsString);
            }

        } catch (IOException | URISyntaxException | IllegalArgumentException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }
        return result;
    }

    public JSONObject sendMessage(String instagramUserId, String message) throws ActionBlockException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.sendMessage(new String[]{instagramUserId}, message);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
            	processIfSpammed(responseAsString, Action.SEND_MESSAGE);
            }

        } catch (IOException | URISyntaxException | IllegalArgumentException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }
        return result;
    }

    public JSONObject getSelfUserFeed(String maxId, String minTimestamp) throws ActionBlockException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getSelfUserFeed(maxId, minTimestamp);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            }

        } catch (IOException | URISyntaxException | IllegalArgumentException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }
        return result;
    }

    public JSONObject getPopularFeed() throws ActionBlockException {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getPopularFeed();

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            }

        } catch (IOException | URISyntaxException | IllegalArgumentException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }
        return result;
    }

    public JSONObject searchLocation(String latitude, String longitude, String name) {
        JSONObject result = new JSONObject();
        try {
            HttpResponse response = mInstagram.searchLocation(latitude, longitude);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject fullResponse = new JSONObject(responseAsString);

                JSONArray locations = fullResponse.getJSONArray("venues");

                locations.forEach(item -> {
                    JSONObject location = (JSONObject) item;

                    if (location.getString("name").equals(name)) {
                        result.put("name", name);
                        result.put("external_id_source", location.getString("external_id_source"));
                        result.put("external_source", (String) null);
                        result.put("address", location.getString("address"));
                        result.put("lat", String.valueOf(location.opt("lat")));
                        result.put("lng", String.valueOf(location.opt("lng")));
                        result.put("external_id", String.valueOf(location.opt("external_id")));
                        result.put("facebook_places_id", (String) null);
                        result.put("city", (String) null);
                        result.put("pk", (String) null);
                        result.put("status", (String) null);
                        result.put("message", (String) null);
                    }
                });
            }

        } catch (IOException | URISyntaxException | IllegalArgumentException | JSONException e) {
            logger.error("account "+ mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }
        return result;
    }
}
