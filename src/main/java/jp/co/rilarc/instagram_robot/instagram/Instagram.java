package jp.co.rilarc.instagram_robot.instagram;

import com.google.gson.Gson;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.util.EntityUtils;
import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.imageio.ImageIO;

import jp.co.rilarc.instagram_robot.device.Device;
import jp.co.rilarc.instagram_robot.device.GoodDevices;
import jp.co.rilarc.instagram_robot.device.UserAgent;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.handler.UploadImageHandler;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.InstagramLoginResponse;
import jp.co.rilarc.instagram_robot.model.InstagramResponse;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.phantomjs.RunJs;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;

public class Instagram {

    private BasicCookieStore mCookies = new BasicCookieStore();

    private InstagramAccount mInstagramAccount;

    private InstagramLoginResponse instagramLoginResponse;

    private Device mDevice;

    private Proxy mProxy = null;

    final protected static Logger logger = LogManager.getLogger(Instagram.class);

    private InstagramWebController instagramWebController;

    @Deprecated
    public Instagram(InstagramAccount instagramAccount, RobotSetting robotSetting) {
        this(instagramAccount);
    }

    public Instagram(InstagramAccount instagramAccount) {
        mInstagramAccount = instagramAccount;

        mInstagramAccount.setAccountId(null);
        String deviceString = GoodDevices.getRandomGoodDevice();
        mDevice = new Device(deviceString);
        String userAgent = UserAgent.buildUserAgent(mDevice);
        mInstagramAccount.setDeviceString(GoodDevices.getRandomGoodDevice());
        mInstagramAccount.setUuid(Signatures.generateUUID());
        mInstagramAccount.setPhoneId(Signatures.generateUUID());
        mInstagramAccount.setDeviceId(Signatures.generateDeviceId());
        // mInstagramAccount.setDeviceId(Signatures.generateDeviceId());
        mInstagramAccount.setUserAgent(userAgent);

        // mRobotSetting = robotSetting;
    }

    public void setWebDriver(WebDriver webDriver) {
        instagramWebController = webDriver != null ? new InstagramWebController(webDriver) : null;
    }

    public boolean login(boolean forceLogin, boolean loginByPhantom) throws IOException, URISyntaxException,
            InstagramInvalidCredentialsException, InstagramCheckpointRequiredException,
            InstagramUserInvalidException, InstagramSentryBlockException {
        Request request;
        HttpResponse response;
        BasicCookieStore cookieStore;

        if (forceLogin)
            logger.info("account " + mInstagramAccount.getUsername() + " force login");
        else
            logger.info("account " + mInstagramAccount.getUsername() + " reset token");

        if (mInstagramAccount.getAccountId() == null || forceLogin) {
            logger.info("account " + mInstagramAccount.getUsername() + " login from begin");

            mCookies = new BasicCookieStore();

            this.syncFeatures(true);

            request = this.request("si/fetch_headers");
            request.addParam("challenge_type", "signup").addParam("guid", mInstagramAccount.getUuid()).getResponse();

            cookieStore = request.getCookies();
            this.mergeCookies(cookieStore);
            String csrfToken = this.getCookieValue("csrftoken");
            mInstagramAccount.setCsrfToken(csrfToken);

            request = this.request("accounts/login/");
            response = request.addPost("phone_id", mInstagramAccount.getPhoneId())
                    .addPost("_csrftoken", mInstagramAccount.getCsrfToken())
                    .addPost("username", mInstagramAccount.getUsername()).addPost("guid", mInstagramAccount.getUuid())
                    .addPost("device_id", mInstagramAccount.getDeviceId())
                    .addPost("password", mInstagramAccount.getPassword()).addPost("login_attempt_count", "0")
                    .getResponse();
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() != 200) {
                logger.info("account " + mInstagramAccount.getUsername() + " login failed: " + responseAsString);

                if (responseAsString.contains("The password you entered is incorrect")) {
                    VLogger.append("インスタグラムのパスワードが間違っている可能性があります。確認してください。");
                    throw new InstagramInvalidCredentialsException(
                            String.format("Incorrect password for %s", mInstagramAccount.getUsername()));
                } else if (responseAsString.contains("Please check your username and try again")) {
                    VLogger.append("インスタグラムのアカウントが正しくないです。確認してください。");
                    throw new InstagramInvalidCredentialsException(
                            "The username you entered doesn't appear to belong to an account. Please check your username and try again.");
                } else if (responseAsString.contains("checkpoint_required")) {
                    VLogger.append("ログインできませんでした。スマホにてインスタグラムアプリを開き、 「This was me」（白いボタン）をクリックしてから、もう一度、ボタンをクリックして、再度接続してください。");
                    throw new InstagramCheckpointRequiredException(String
                            .format("account %s need to verify from instagram app", mInstagramAccount.getUsername()));
                } else if (responseAsString.contains("Your account has been disabled for violating")) {
                    VLogger.append("あなたのインスタグラムアカウントは無効になっている可能性があります。ご確認ください。");
                    throw new InstagramUserInvalidException(
                            "Your account has been disabled for violating our terms. Learn how you may be able to restore your account.");
                } else if (responseAsString.contains("Sorry, there was a problem with your request.")) {
                    VLogger.append("インスタグラムに接続できませんでした。ウエブまたはモバイルアプリを開き、認証要求があるかどうかを確認してください。");
                    throw new InstagramSentryBlockException("Sorry, there was a problem with your request.");
                }

                return false;
            }
            if (!TextUtils.isEmpty(responseAsString)) {
                instagramLoginResponse = new Gson().fromJson(responseAsString, InstagramLoginResponse.class);
            }
            logger.info("account " + mInstagramAccount.getUsername() + " success login by API");

            cookieStore = request.getCookies();
            this.mergeCookies(cookieStore);
            csrfToken = this.getCookieValue("csrftoken");
            String accountId = this.getCookieValue("ds_user_id");

            mInstagramAccount.setCsrfToken(csrfToken);
            mInstagramAccount.setAccountId(accountId);
            mInstagramAccount.setRankToken(accountId + "_" + csrfToken);

            this.syncFeatures(false);
            this.getAutoCompleteUserList();
            this.getTimelineFeed(null);
            this.getRankedRecipients();
            this.getRecentRecipients();
            this.getMegaphoneLog();
            this.getV2Inbox();
            this.getRecentActivity();
            this.getReelsTrayFeed();
            this.getExplore();

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            long currentTimestamp = timestamp.getTime() % 1000;
            mInstagramAccount.setLastTimeLogin(currentTimestamp);

            // login by phantomjs
            if (loginByPhantom) {
                boolean loginPhantomResult = RunJs.login(mInstagramAccount, mProxy);
                /*
                int retry = 0;
				while (!loginPhantomResult && retry < 2) { // Thu login 2 lan
					loginPhantomResult = RunJs.login(mInstagramAccount, mProxy);
					retry++;
					jp.co.rilarc.instagram_robot.utils.Utils.waitFor(3); // wait 3 seconds
				}
				*/
                if (!loginPhantomResult) {
                    logger.error("account " + mInstagramAccount.getUsername() + " cannot login by phantomjs");
                }
            }
            return true;
        }

        response = this.getTimelineFeed(null);
        if (response.getStatusLine().getStatusCode() == 400) {
            logger.info("account " + mInstagramAccount.getUsername() + " get timeline feed failed");

            return this.login(true, loginByPhantom);
        }

        if (mInstagramAccount.isRefreshLogin()) {
            logger.info("account " + mInstagramAccount.getUsername() + " refresh login");

            this.getAutoCompleteUserList();
            this.getReelsTrayFeed();
            this.getRankedRecipients();
            this.getRecentRecipients();
            this.getMegaphoneLog();
            this.getV2Inbox();
            this.getRecentActivity();
            this.getExplore();

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            long currentTimestamp = timestamp.getTime() % 1000;
            mInstagramAccount.setLastTimeLogin(currentTimestamp);
        }

        return true;
    }

    public HttpResponse getHashtagFeed(String hashTag, String maxId) throws IOException, URISyntaxException {
        Request request = this.request("feed/tag/" + hashTag + "/");
        if (maxId != null)
            request.addParam("max_id", maxId);

        return request.getResponse();
    }

    public HttpResponse getLocationFeed(String locationId, String maxId) throws IOException, URISyntaxException {
        Request request = this.request("feed/location/" + locationId + "/");
        if (maxId != null)
            request.addParam("max_id", maxId);

        return request.getResponse();
    }

    public InstagramLoginResponse getInstagramLoginResponse() {
        return instagramLoginResponse;
    }

    public void syncFeatures(boolean preLogin) throws IOException, URISyntaxException {
        Request request = this.request("qe/sync/");

        if (preLogin) {
            request.addPost("id", Signatures.generateUUID()).addPost("experiments", Const.LOGIN_EXPERIMENTS)
                    .getResponse();
        } else {
            request.addPost("_uuid", mInstagramAccount.getUuid()).addPost("_uid", mInstagramAccount.getAccountId())
                    .addPost("id", mInstagramAccount.getAccountId())
                    .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("experiments", Const.EXPERIMENTS)
                    .getResponse();
        }

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getAutoCompleteUserList() throws IOException, URISyntaxException {
        Request request = this.request("friendships/autocomplete_user_list/");
        request.addParam("version", "2").getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);

    }

    public HttpResponse getTimelineFeed(String maxId) throws IOException, URISyntaxException {
        Request request = this.request("feed/timeline").addParam("rank_token", mInstagramAccount.getRankToken())
                .addParam("ranked_content", "true");

        if (maxId != null)
            request.addParam("max_id", maxId);

        HttpResponse response = request.getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);

        return response;
    }

    public void getRankedRecipients() throws IOException, URISyntaxException {
        Request request = this.request("direct_v2/ranked_recipients");
        request.addParam("show_threads", "true").getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getRecentRecipients() throws IOException, URISyntaxException {
        Request request = this.request("direct_share/recent_recipients/");
        request.getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getMegaphoneLog() throws IOException, URISyntaxException {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long currentTimestamp = timestamp.getTime() % 1000;

        Request request = this.request("megaphone/log/");
        request.setSignedPost(false).addPost("type", "feed_aysf").addPost("action", "seen").addPost("reason", "")
                .addPost("_uuid", mInstagramAccount.getUuid()).addPost("device_id", mInstagramAccount.getDeviceId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken())
                .addPost("uuid", Signatures.md5(String.valueOf(currentTimestamp))).getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getV2Inbox() throws IOException, URISyntaxException {
        Request request = this.request("direct_v2/inbox/");
        request.getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getRecentActivity() throws IOException, URISyntaxException {
        Request request = this.request("news/inbox/").addParam("activity_module", "all");
        request.getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getReelsTrayFeed() throws IOException, URISyntaxException {
        Request request = this.request("feed/reels_tray/");
        request.getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public void getExplore() throws IOException, URISyntaxException {
        Request request = this.request("discover/explore/");
        request.getResponse();

        BasicCookieStore cookieStore = request.getCookies();
        this.mergeCookies(cookieStore);
    }

    public HttpResponse like(String mediaId) throws IOException, URISyntaxException {
        return this.request("media/" + mediaId + "/like/").addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_uid", mInstagramAccount.getAccountId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("media_id", mediaId).getResponse();
    }

    public boolean likeMediaByJs(String mediaCode) {
        return instagramWebController != null && instagramWebController.likePost(mediaCode);
    }

    public boolean followUserByJs(String username) {
        return instagramWebController != null && instagramWebController.follow(username);
    }

    public boolean unfollowUserByJs(String username) {
        return instagramWebController != null && instagramWebController.unfollow(username);
    }

    public HttpResponse unlike(String mediaId) throws IOException, URISyntaxException {
        return this.request("media/" + mediaId + "/unlike/").addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_uid", mInstagramAccount.getAccountId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("media_id", mediaId).getResponse();
    }

    public HttpResponse getMediaInfo(String mediaId) throws IOException, URISyntaxException {
        return this.request("media/" + mediaId + "/info/").addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_uid", mInstagramAccount.getAccountId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("media_id", mediaId).getResponse();
    }

    public HttpResponse follow(String userId) throws IOException, URISyntaxException {
        return this.request("friendships/create/" + userId + "/").addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_uid", mInstagramAccount.getAccountId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("user_id", userId).getResponse();
    }

    public InstagramResponse unfollow(String userId) throws IOException, URISyntaxException, InstagramCannotAccessException {
        HttpResponse response = this.request("friendships/destroy/" + userId + "/").addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_uid", mInstagramAccount.getAccountId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("user_id", userId).getResponse();
        return processResponse(response);
    }

    public HttpResponse getSelfUserFollowers(String maxId) throws IOException, URISyntaxException {
        Request request = this.request("friendships/" + mInstagramAccount.getAccountId() + "/followers/")
                .addParam("rank_token", mInstagramAccount.getRankToken());

        if (maxId != null)
            request.addParam("max_id", maxId);

        return request.getResponse();
    }

    public HttpResponse getUserFollowings(String userId, String maxId) throws IOException, URISyntaxException {
        Request request = this.request("friendships/" + userId + "/following/").addParam("rank_token",
                mInstagramAccount.getRankToken());

        if (maxId != null)
            request.addParam("max_id", maxId);

        return request.getResponse();
    }

    public InstagramResponse getUsersFriendship(String userIds) throws IOException, URISyntaxException, InstagramCannotAccessException {
        HttpResponse response = this.request("friendships/show_many/").setSignedPost(false).addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("user_ids", userIds).getResponse();
        return processResponse(response);
    }

    public InstagramResponse getUserFriendship(String userId) throws IOException, URISyntaxException, InstagramCannotAccessException {
        Request request = this.request("friendships/show/" + userId);

        HttpResponse response = request.getResponse();
        return processResponse(response);
    }

    public HttpResponse getUserInfo(String userId) throws IOException, URISyntaxException {
        return this.request("users/" + userId + "/info/").getResponse();
    }

    public HttpResponse uploadTimelinePhoto(String imagePath, JSONObject externalMetadata)
            throws IOException, URISyntaxException {
        return this._uploadSinglePhoto("timeline", imagePath, externalMetadata);
    }

    private HttpResponse _uploadSinglePhoto(String targetFeed, String imagePath, JSONObject externalMetadata)
            throws IOException, IllegalArgumentException, URISyntaxException {
        // Make sure we only allow these particular feeds for this function.
        if (!Objects.equals(targetFeed, "timeline") && !Objects.equals(targetFeed, "story")) {
            throw new IllegalArgumentException("Bad target feed " + targetFeed + ".");
        }

        if (imagePath.contains("http")) {
            imagePath = Utils.downloadImage(imagePath);
        }

        // Verify that the file exists locally.
        File imageFile = new File(imagePath);
        if (!imageFile.exists() || imageFile.isDirectory()) {
            throw new IllegalArgumentException("The photo file " + imagePath + " does not exist on disk.");
        }

        // check is image file
        if (!Utils.isImage(imageFile)) {
            throw new IllegalArgumentException("The file " + imagePath + " could not be opened , it is not an image");
        }

        BufferedImage bufferedImage = ImageIO.read(imageFile);
        int photoWidth = bufferedImage.getWidth();
        int photoHeight = bufferedImage.getHeight();

        Utils.throwIfIllegalMediaResolution("photofile", imagePath, photoWidth, photoHeight);

        // upload image
        HttpResponse uploadResponse = this.uploadPhotoData(targetFeed, imagePath);

        if (uploadResponse.getStatusLine().getStatusCode() != 200)
            return uploadResponse;
        String responseAsString = EntityUtils.toString(uploadResponse.getEntity());
        UploadImageHandler uploadImageHandler = new UploadImageHandler(new JSONObject(responseAsString));

        BigInteger uploadId = uploadImageHandler.getUploadId();

        JSONObject internalMetadata = new JSONObject();
        internalMetadata.put("uploadId", String.valueOf(uploadId));
        internalMetadata.put("photoWidth", photoWidth);
        internalMetadata.put("photoHeight", photoHeight);
        System.out.println(internalMetadata.toString());

        return this.configureSinglePhoto(targetFeed, internalMetadata, externalMetadata);
    }

    private HttpResponse uploadPhotoData(String targetFeed, String imagePath) throws IOException, URISyntaxException {
        String boundary = mInstagramAccount.getUuid();
        long uploadId = Utils.generateUploadId();

        Request request = multipartRequest("upload/photo/", boundary);
        request.addPost("upload_id", String.valueOf(uploadId)).addPost("_uuid", boundary)
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken())
                .addPost("image_compression", "{\"lib_name\":\"jt\",\"lib_version\":\"1.3.0\",\"quality\":\"87\"}")
                .addFile("photo", imagePath);

        if (Objects.equals(targetFeed, "album")) {
            request.addPost("is_sidecar", "1");
        }

        return request.getResponse();
    }

    private HttpResponse configureSinglePhoto(String targetFeed, JSONObject internalMetadata,
                                              JSONObject externalMetadata) throws IOException, URISyntaxException {
        String endpoint;
        // Determine the target endpoint for the photo.
        switch (targetFeed) {
            case "timeline":
                endpoint = "media/configure/";
                break;
            case "story":
                endpoint = "media/configure_to_story/";
                break;
            default:
                throw new IllegalArgumentException("Bad target feed " + targetFeed);
        }

        // Available external metadata parameters:
        /** @var string|null Caption to use for the media. */
        String caption = externalMetadata.has("caption") ? externalMetadata.getString("caption") : "";

        /**
         * @var Response\Model\Location|null A Location object describing where the
         *      media was taken. NOT USED FOR STORY MEDIA!
         */
        JSONObject location = (externalMetadata.has("location") && !Objects.equals(targetFeed, "story"))
                ? externalMetadata.getJSONObject("location")
                : null;

        /** @var string The ID of the entry to configure. */
        String uploadId = internalMetadata.getString("uploadId");
        /** @var int|float Width of the photo. */
        int photoWidth = internalMetadata.getInt("photoWidth");
        /** @var int|float Height of the photo. */
        int photoHeight = internalMetadata.getInt("photoHeight");

        Request request = this.request(endpoint).addPost("_csrftoken", mInstagramAccount.getCsrfToken())
                .addPost("_uid", mInstagramAccount.getAccountId()).addPost("_uuid", mInstagramAccount.getUuid());

        // edits param
        JSONObject editsParam = new JSONObject();
        JSONArray crop_original_size = new JSONArray();
        crop_original_size.put(photoWidth);
        crop_original_size.put(photoHeight);
        editsParam.put("crop_original_size", crop_original_size);
        editsParam.put("crop_zoom", 1);
        JSONArray crop_center = new JSONArray();
        crop_center.put(0.0);
        crop_center.put(-0.0);
        editsParam.put("crop_center", crop_center);

        request.addPost("edits", editsParam.toString());

        // device param
        JSONObject deviceParam = new JSONObject();
        deviceParam.put("manufacturer", mDevice.getManufacturer());
        deviceParam.put("model", mDevice.getModel());
        deviceParam.put("android_version", mDevice.getAndroidVersion());
        deviceParam.put("android_release", mDevice.getAndroidRelease());
        request.addPost("device", deviceParam.toString());

        // extra param
        JSONObject extraParam = new JSONObject();
        extraParam.put("source_width", photoWidth);
        extraParam.put("source_height", photoHeight);
        request.addPost("extra", extraParam.toString());

        switch (targetFeed) {
            case "timeline":
                request.addPost("caption", caption).addPost("source_type", "4").addPost("media_folder", "Camera")
                        .addPost("upload_id", uploadId);
                break;
            case "story":
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                long currentTimestamp = timestamp.getTime() % 1000;
                request.addPost("client_shared_at", String.valueOf(currentTimestamp)).addPost("source_type", "3")
                        .addPost("configure_mode", "1").addPost("client_timestamp", String.valueOf(currentTimestamp))
                        .addPost("upload_id", uploadId);
                break;
            default:
                throw new IllegalArgumentException("Bad target feed " + targetFeed);
        }

        if (location != null) {
            JSONObject locationParam = new JSONObject();
            locationParam.put(location.getString("external_id_source") + "_id", location.getString("external_id"));
            locationParam.put("name", location.getString("name"));
            locationParam.put("lat", String.valueOf(location.getDouble("lat")));
            locationParam.put("lng", String.valueOf(location.getDouble("lng")));
            locationParam.put("address", location.getString("address"));
            locationParam.put("external_source", location.getString("external_id_source"));

            request.addPost("location", locationParam.toString()).addPost("geotag_enabled", "1")
                    .addPost("posting_latitude", String.valueOf(location.getDouble("lat")))
                    .addPost("posting_longitude", String.valueOf(location.getDouble("lng")))
                    .addPost("media_latitude", String.valueOf(location.getDouble("lat")))
                    .addPost("media_longitude", String.valueOf(location.getDouble("lng"))).addPost("av_latitude", "0.0")
                    .addPost("av_longitude", "0.0");
        }

        return request.getResponse();
    }

    public HttpResponse comment(String mediaId, String commentContent) throws IOException, URISyntaxException {
        Request request = request("media/" + mediaId + "/comment/")
                .addPost("user_breadcrumb", Utils.generateUserBreadcrumb(commentContent.length()))
                .addPost("idempotence_token", Signatures.generateUUID())
                .addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("_uid", mInstagramAccount.getAccountId())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken())
                .addPost("comment_text", commentContent)
                .addPost("containermodule", "comments_feed_timeline")
                .addPost("radio_type", "wifi-none");

        return request.getResponse();
    }

    public HttpResponse sendMessage(String[] userIds, String message) throws IOException, URISyntaxException {

        String url = jp.co.rilarc.instagram_robot.utils.Utils.extractUrl(message);
        // Neu message co kem theo link thi gui theo API broadcast link
        if (url != null) {
            return sendMessage(userIds, message, url);
        }

        Request request = request("direct_v2/threads/broadcast/text/")
                .addPost("recipient_users", "[[" + String.join(",", userIds) + "]]").addPost("text", message)
                .addPost("action", "send_item").addPost("client_context", Signatures.generateUUID())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("link_urls", "[https://google.com]").setSignedPost(false);

        return request.getResponse();
    }

    public HttpResponse sendMessage(String[] userIds, String message, String url)
            throws IOException, URISyntaxException {
        Request request = request("direct_v2/threads/broadcast/link/")
                .addPost("recipient_users", "[[" + String.join(",", userIds) + "]]").addPost("link_text", message)
                .addPost("action", "send_item").addPost("client_context", Signatures.generateUUID())
                .addPost("_csrftoken", mInstagramAccount.getCsrfToken()).addPost("_uuid", mInstagramAccount.getUuid())
                .addPost("link_urls", "[\"" + url + "\"]").setSignedPost(false);

        return request.getResponse();
    }

    public HttpResponse getSelfUserFeed(String maxId, String minTimestamp) throws IOException, URISyntaxException {
        return getUserFeed(mInstagramAccount.getAccountId(), maxId, minTimestamp);
    }

    public HttpResponse getUserFeed(String userId, String maxId, String minTimestamp) throws IOException, URISyntaxException {
        Request request = this.request("feed/user/" + userId)
                .addParam("rank_token", mInstagramAccount.getRankToken())
                .addParam("ranked_content", "true");
        if (!TextUtils.isEmpty(maxId)) {
            request.addParam("max_id", maxId);
        }
        if (!TextUtils.isEmpty(minTimestamp)) {
            request.addParam("min_timestamp", minTimestamp);
        }
        return request.getResponse();
    }

    public HttpResponse getPopularFeed() throws IOException, URISyntaxException {
        return this.request("feed/popular/")
                .addParam("rank_token", mInstagramAccount.getRankToken())
                .addParam("people_teaser_supported", "1")
                .addParam("rank_token", "true")
                .getResponse();
    }

    public HttpResponse searchLocation(String latitude, String longitude) throws IOException, URISyntaxException {
        return this.request("location_search/")
                .addParam("rank_token", mInstagramAccount.getRankToken())
                .addParam("latitude", latitude)
                .addParam("longitude", longitude)
                .addParam("timestamp", String.valueOf(System.currentTimeMillis()%1000))
                .getResponse();
    }

    private Request request(String url) {
        Map<String, String> headers = buildBaseHeaders();
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

        Request request = new Request(url, headers, mProxy);
        if (this.mCookies != null)
            request.setCookies(mCookies);

        return request;
    }

    private Request multipartRequest(String url, String boundary) {
        Map<String, String> headers = buildBaseHeaders();
        headers.put("Content-Type", "multipart/form-data; boundary=" + boundary);

        Request request = new Request(url, headers, mProxy);
        if (this.mCookies != null)
            request.setCookies(mCookies);
        request.setBoundary(boundary);

        return request;
    }

    private void mergeCookies(BasicCookieStore cookieStore) {
        for (Cookie c : cookieStore.getCookies())
            mCookies.addCookie(c);
    }

    private String getCookieValue(String cookieName) {
        String csrfToken = "";

        for (Cookie c : mCookies.getCookies()) {
            if (c.getName().equals(cookieName)) {
                csrfToken = c.getValue();
            }
        }

        return csrfToken;
    }

    private Map<String, String> buildBaseHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", mInstagramAccount.getUserAgent());
        headers.put("Connection", "keep-alive");
        headers.put("Accept", "*/*");
        headers.put("Accept-Encoding", Const.ACCEPT_ENCODING);
        headers.put("X-IG-Capabilities", Const.X_IG_Capabilities);
        headers.put("X-IG-Connection-Type", Const.X_IG_Connection_Type);
        int connectionSpeed = ((int) (Math.random() * (3700 - 1000))) + 1000;
        headers.put("X-IG-Connection-Speed", String.valueOf(connectionSpeed) + "kbps");
        headers.put("X-FB-HTTP-Engine", Const.X_FB_HTTP_Engine);
        headers.put("Accept-Language", Const.ACCEPT_LANGUAGE);

        return headers;
    }

    public InstagramAccount getInstagramAccount() {
        return mInstagramAccount;
    }

    public void setProxy(Proxy mProxy) {
        logger.info("Proxy " + mProxy);
        this.mProxy = mProxy;
    }

    public Proxy getProxy() {
        return this.mProxy;
    }

    private InstagramResponse processResponse(HttpResponse response) throws IOException, InstagramCannotAccessException {
        String responseAsString = EntityUtils.toString(response.getEntity());

        InstagramResponse iResponse = new InstagramResponse();
        iResponse.setStatusCode(response.getStatusLine().getStatusCode());
        iResponse.setResponseAsString(responseAsString);

        if (iResponse.getStatusCode() != 200) {
            if (responseAsString.contains("The password you entered is incorrect")) {
                throw new InstagramCannotAccessException(
                        String.format("Incorrect password for %s", mInstagramAccount.getUsername()));
            } else if (responseAsString.contains("Please check your username and try again")) {
                throw new InstagramCannotAccessException(
                        "The username you entered doesn't appear to belong to an account. Please check your username and try again.");
            } else if (responseAsString.contains("checkpoint_required")) {
                throw new InstagramCannotAccessException(String
                        .format("account %s need to verify from instagram app", mInstagramAccount.getUsername()));
            } else if (responseAsString.contains("Your account has been disabled for violating")) {
                throw new InstagramCannotAccessException(
                        "Your account has been disabled for violating our terms. Learn how you may be able to restore your account.");
            }
        }

        return iResponse;
    }
}
