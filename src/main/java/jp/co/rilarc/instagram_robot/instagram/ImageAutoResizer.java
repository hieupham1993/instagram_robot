package jp.co.rilarc.instagram_robot.instagram;

/**
 * Created by nguyenthanhson on 10/24/17.
 */
public class ImageAutoResizer {
    static final double MIN_RATIO = 0.8;
    static final double MAX_RATIO = 1.91;
    static final int MAX_WIDTH = 1080;
    static final int MAX_HEIGHT = 1350;
}
