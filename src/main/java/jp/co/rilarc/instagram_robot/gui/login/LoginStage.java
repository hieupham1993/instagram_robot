package jp.co.rilarc.instagram_robot.gui.login;

import java.io.IOException;

import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LoginStage {

    private static final String TAG = LoginStage.class.getSimpleName();

    public LoginStage(Stage stage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/layout/layout_login.fxml"));
            stage.setTitle("Login");
            stage.setResizable(false);
            FadeTransition ft = new FadeTransition(Duration.millis(1000), root);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();
            stage.setMinWidth(600);
            stage.setMinHeight(600);
            stage.setWidth(600);
            stage.setHeight(600);
            stage.centerOnScreen();
            stage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
