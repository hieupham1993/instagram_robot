package jp.co.rilarc.instagram_robot.gui.commentsetting;

/**
 * Created by HieuPT on 1/24/2018.
 */
public interface ICommentSettingController {

    void refreshScheduledMessages();
}
