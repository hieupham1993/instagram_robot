package jp.co.rilarc.instagram_robot.gui.main;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface IMainView {

    void startRobot();

    void stopRobot();

    void showProgress();

    void hideProgress();

    void showLogoutConfirmDialog();

    void displayLoginScreen();

    void fillProfileInfo(String systemEmail, String instagramUsername, String instagramAvatar);

    void setRobotState(boolean running);

    void openUrl(String url);

    void loginToWebInstagram(String email, String password);

    void showCommentSettingPanel();

    void showMessageSettingPanel();

    void hideSettingPanel();

    void loginToAppInstagram(String email, String password, boolean forceLoginWeb);

    void fillLoginInfo(String username, String password);

    void setLoginState(boolean loggedIn);

    void showChangeAccountConfirmDialog();

    void clearLoginForm();
}
