package jp.co.rilarc.instagram_robot.gui.login;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface ILoginView {

    void closeScreen();

    void displayMainScreen();

    void showLoginFailureDialog(String message);

    void showProgress(boolean shown);

    void enableLoginButton(boolean enable);

    void setAutoLoginEnable(boolean enable);

    void setAutoStartEnable(boolean enable);

    void fillLoginInfo(String email, String password);
}
