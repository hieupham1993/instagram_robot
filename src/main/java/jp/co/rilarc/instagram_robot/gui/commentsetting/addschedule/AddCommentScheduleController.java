package jp.co.rilarc.instagram_robot.gui.commentsetting.addschedule;

import com.hieupt.controlsfx.DateTimePicker;
import com.hieupt.controlsfx.EmojiEditor;
import com.hieupt.controlsfx.ImageButtonTableCell;
import com.hieupt.controlsfx.ImageViewTableCell;
import com.hieupt.controlsfx.LabelTableCell;
import com.hieupt.controlsfx.converter.IImageConverter;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXProgressBar;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.util.TextUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import jp.co.rilarc.instagram_robot.controlfx.ControlUtils;
import jp.co.rilarc.instagram_robot.gui.ConfirmAlertDialog;
import jp.co.rilarc.instagram_robot.gui.commentsetting.ICommentSettingController;
import jp.co.rilarc.instagram_robot.model.InstagramArticleTableItem;
import jp.co.rilarc.instagram_robot.presenters.AddCommentSchedulePresenter;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.ImageCache;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class AddCommentScheduleController implements Initializable, IAddCommentScheduleView {

    private static final String KEY_SEARCH_KEYWORD = "KEY_SEARCH_KEYWORD";

    private static final String KEY_NEXT_MAX_ID = "KEY_NEXT_MAX_ID";

    @FXML
    private ToggleButton self_post_button;

    @FXML
    private ToggleButton most_like_button;

    @FXML
    private ToggleButton timeline_button;

    @FXML
    private ToggleButton search_button;

    @FXML
    private TextField search_text_field;

    @FXML
    private StackPane progress_layout;

    @FXML
    private EmojiEditor emoji_editor;

    @FXML
    private DateTimePicker date_time_picker;

    @FXML
    private TableView<InstagramArticleTableItem> instagram_data_table_view;

    @FXML
    private JFXProgressBar table_view_progress;

    @FXML
    private JFXComboBox<Integer> delay_value_combo_box;

    private ICommentSettingController commentSettingController;

    private ScrollBar instagramTableScrollBar;

    private AddCommentSchedulePresenter presenter;

    private final ToggleGroup toggleGroup = new ToggleGroup();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        presenter = new AddCommentSchedulePresenter(this);
        setupInstagramTable();
        initDelayComboBox();
        setupActionButton();
        setupSearchBox();
    }

    private void setupSearchBox() {
        search_button.setDisable(true);
        search_text_field.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.isEmpty() && !newValue.contains(Const.SPACE_CHARACTER)) {
                search_button.setDisable(false);
            } else {
                search_button.setDisable(true);
            }
        });
    }

    private void setupActionButton() {
        toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (oldValue != null) {
                    oldValue.setSelected(false);
                }
            } else {
                oldValue.setSelected(true);
            }
        });
        toggleGroup.getToggles().addAll(timeline_button, most_like_button, self_post_button, search_button);
    }

    private void initDelayComboBox() {
        List<Integer> delayValueList = new ArrayList<>();
        for (int i = Const.MIN_DELAY_SCHEDULE; i <= Const.MAX_DELAY_SCHEDULE; i += Const.STEP_DELAY_SCHEDULE) {
            delayValueList.add(i);
        }
        delay_value_combo_box.setItems(FXCollections.observableList(delayValueList));
        delay_value_combo_box.setConverter(new StringConverter<Integer>() {

            @Override
            public String toString(Integer object) {
                if (object != null) {
                    return String.valueOf(object) + " 秒";
                }
                return Const.EMPTY_STRING;
            }

            @Override
            public Integer fromString(String string) {
                if (!TextUtils.isEmpty(string)) {
                    String valueStr = string.substring(0, string.lastIndexOf(" 秒"));
                    NumberUtils.toInt(valueStr, 0);
                }
                return 0;
            }
        });
        delay_value_combo_box.setValue(Const.DEFAULT_DELAY_SCHEDULE);
    }

    private void setupInstagramTable() {
        instagram_data_table_view.selectionModelProperty().bind(new SimpleObjectProperty<>());
        ObservableList<TableColumn<InstagramArticleTableItem, ?>> columns = instagram_data_table_view.getColumns();
        columns.add(createCheckColumn());
        columns.add(createPostTypeColumn());
        columns.add(createPostPictureColumn());
        columns.add(createCaptionColumn());
        columns.add(createLikeCountColumn());
        columns.add(createCommentCountColumn());
        columns.add(createOptionColumn());
        columns.forEach(column -> column.setSortable(false));
    }

    private void initInstagramTableScrollBar() {
        if (instagramTableScrollBar == null) {
            ScrollBar scrollBar = ControlUtils.getVerticalScrollbar(instagram_data_table_view);
            if (scrollBar != null) {
                instagramTableScrollBar = scrollBar;
                instagramTableScrollBar.valueProperty().addListener((observable, oldValue, newValue) -> {
                    double scrolledValue = newValue.doubleValue();
                    if (scrolledValue == instagramTableScrollBar.getMax()) {
                        double targetValue = scrolledValue * instagram_data_table_view.getItems().size();
                        instagramTableScrollBar.setUserData(targetValue);
                        onReachMaxScroll();
                    }
                });
            }
        }
    }

    private void onReachMaxScroll() {
        @SuppressWarnings("unchecked") Map<String, String> searchInfoMap = (Map<String, String>) instagram_data_table_view.getUserData();
        if (searchInfoMap != null) {
            String searchKeyword = searchInfoMap.getOrDefault(KEY_SEARCH_KEYWORD, Const.EMPTY_STRING);
            String nextMaxId = searchInfoMap.getOrDefault(KEY_NEXT_MAX_ID, Const.EMPTY_STRING);
            if (timeline_button.isSelected()) {
                presenter.onLoadMoreTimeLine(nextMaxId);
            } else if (search_button.isSelected()) {
                presenter.onLoadMoreHashtag(searchKeyword, nextMaxId);
            } else if (self_post_button.isSelected()) {
                presenter.onLoadMoreSelfPost(nextMaxId);
            }
        }
    }

    private TableColumn<InstagramArticleTableItem, Boolean> createCheckColumn() {
        TableColumn<InstagramArticleTableItem, Boolean> column = new TableColumn<>(Const.EMPTY_STRING);
        CheckBox checkBoxHeader = new CheckBox();
        checkBoxHeader.setOnAction(event -> {
            CheckBox checkBox = (CheckBox) event.getSource();
            boolean isChecked = checkBox.isSelected();
            instagram_data_table_view.getItems().forEach(item -> item.setChecked(isChecked));
        });
        SimpleBooleanProperty booleanProperty = new SimpleBooleanProperty();
        checkBoxHeader.selectedProperty().bindBidirectional(booleanProperty);
        column.setGraphic(checkBoxHeader);
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return item.checkedProperty();
        });
        column.setCellFactory(param -> {
            TableCell<InstagramArticleTableItem, Boolean> tableCell = new CheckBoxTableCell<>(index -> {
                ObservableList<InstagramArticleTableItem> items = instagram_data_table_view.getItems();
                if (index < 0 || index >= items.size()) {
                    return null;
                }
                recheckColumnHeaderState(booleanProperty);
                return items.get(index).checkedProperty();
            });
            tableCell.setAlignment(Pos.TOP_CENTER);
            return tableCell;
        });
        column.setMinWidth(50.0);
        column.setMaxWidth(50.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramArticleTableItem, String> createPostTypeColumn() {
        TableColumn<InstagramArticleTableItem, String> column = new TableColumn<>("タイプ");
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return new SimpleStringProperty(item.getPostTypeText());
        });
        column.setCellFactory(param -> {
            TableCell<InstagramArticleTableItem, String> tableCell = new LabelTableCell<>(new DefaultStringConverter());
            tableCell.setAlignment(Pos.TOP_CENTER);
            return tableCell;
        });
        column.setMinWidth(75.0);
        column.setMaxWidth(75.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramArticleTableItem, String> createPostPictureColumn() {
        TableColumn<InstagramArticleTableItem, String> column = new TableColumn<>("写真");
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return new SimpleStringProperty(item.getPostPicUrl());
        });
        column.setCellFactory(param -> {
            ImageViewTableCell<InstagramArticleTableItem, String> tableCell = new ImageViewTableCell<>(new IImageConverter<String>() {

                @Override
                public String fromImage(Image image) {
                    return image.toString();
                }

                @Override
                public Image toImage(String object) {
                    return ImageCache.getInstance().getImage(object);
                }
            });
            tableCell.setAlignment(Pos.TOP_CENTER);
            tableCell.setMaxHeight(150.0);
            tableCell.setMaxWidth(150.0);
            tableCell.setMinHeight(150.0);
            tableCell.setMinWidth(150.0);
            tableCell.setImageSize(125.0, 125.0);
            return tableCell;
        });
        column.setMinWidth(150.0);
        column.setMaxWidth(150.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramArticleTableItem, String> createCaptionColumn() {
        TableColumn<InstagramArticleTableItem, String> column = new TableColumn<>("キャプション");
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return new SimpleStringProperty(item.getCaption());
        });
        column.setCellFactory(param -> {
            LabelTableCell<InstagramArticleTableItem, String> tableCell = new LabelTableCell<>(new DefaultStringConverter());
            tableCell.setWrapContent(true);
            tableCell.setAlignment(Pos.TOP_LEFT);
            return tableCell;
        });
        return column;
    }

    private TableColumn<InstagramArticleTableItem, String> createLikeCountColumn() {
        TableColumn<InstagramArticleTableItem, String> column = new TableColumn<>("いいね");
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return new SimpleStringProperty(String.valueOf(item.getLikeCount()));
        });
        column.setCellFactory(param -> {
            TableCell<InstagramArticleTableItem, String> tableCell = new LabelTableCell<>(new DefaultStringConverter());
            tableCell.setAlignment(Pos.TOP_CENTER);
            return tableCell;
        });
        column.setMinWidth(75.0);
        column.setMaxWidth(75.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramArticleTableItem, String> createCommentCountColumn() {
        TableColumn<InstagramArticleTableItem, String> column = new TableColumn<>("コメント");
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return new SimpleStringProperty(String.valueOf(item.getCommentCount()));
        });
        column.setCellFactory(param -> {
            TableCell<InstagramArticleTableItem, String> tableCell = new LabelTableCell<>(new DefaultStringConverter());
            tableCell.setAlignment(Pos.TOP_CENTER);
            return tableCell;
        });
        column.setMinWidth(75.0);
        column.setMaxWidth(75.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramArticleTableItem, String> createOptionColumn() {
        TableColumn<InstagramArticleTableItem, String> column = new TableColumn<>("オプション");
        column.setCellValueFactory(param -> {
            InstagramArticleTableItem item = param.getValue();
            return new SimpleStringProperty(InstagramWebController.INSTAGRAM_BASE_POST_URL + item.getPostId());
        });
        column.setCellFactory(param -> {
            ImageButtonTableCell<InstagramArticleTableItem, String> tableCell = new ImageButtonTableCell<>(new IImageConverter<String>() {

                @Override
                public String fromImage(Image image) {
                    return image.toString();
                }

                @Override
                public Image toImage(String object) {
                    return new Image(getClass().getResource("/images/eye.png").toExternalForm());
                }
            });
            tableCell.setAlignment(Pos.TOP_CENTER);
            tableCell.setOnButtonClickListener(Utils::openBrowserSystem);
            return tableCell;
        });
        column.setMinWidth(60.0);
        column.setMaxWidth(60.0);
        column.setResizable(false);
        return column;
    }

    private void recheckColumnHeaderState(BooleanProperty booleanProperty) {
        boolean checked = true;
        for (InstagramArticleTableItem item : instagram_data_table_view.getItems()) {
            if (!item.isChecked()) {
                checked = false;
                break;
            }
        }
        booleanProperty.set(checked);
    }

    @Override
    public void showTableViewProgress(boolean shown) {
        table_view_progress.setVisible(shown);
    }

    @Override
    public void showInstagramDataTable(boolean shown) {
        instagram_data_table_view.setVisible(shown);
    }

    @Override
    public void showInstagramArticleData(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
        instagram_data_table_view.setItems(FXCollections.observableList(items));
        instagram_data_table_view.scrollTo(0);
        initInstagramTableScrollBar();
        instagram_data_table_view.setUserData(buildSearchMapInfo(searchKeyword, nextMaxId));
    }

    @Override
    public void showProgress(boolean shown) {
        progress_layout.setVisible(shown);
    }

    @Override
    public void closeDialog() {
        closeWindow();
    }

    @Override
    public void notifyCommentSettingRefresh() {
        if (commentSettingController != null) {
            commentSettingController.refreshScheduledMessages();
        }
    }

    @Override
    public void showMessageDialog(String message) {
        if (!TextUtils.isEmpty(message)) {
            Platform.runLater(() -> new ConfirmAlertDialog(getStage(), Alert.AlertType.INFORMATION, "警告", message, null));
        }
    }

    @Override
    public void addMoreInstagramArticleData(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
        instagram_data_table_view.getItems().addAll(FXCollections.observableList(items));
        instagram_data_table_view.setUserData(buildSearchMapInfo(searchKeyword, nextMaxId));
        resetInstagramScrollBarValue();
    }

    private Map<String, String> buildSearchMapInfo(String searchKeyword, String nextMaxId) {
        Map<String, String> map = new HashMap<>();
        map.put(KEY_SEARCH_KEYWORD, searchKeyword);
        map.put(KEY_NEXT_MAX_ID, nextMaxId);
        return map;
    }

    private void resetInstagramScrollBarValue() {
        double targetValue;
        try {
            targetValue = (double) instagramTableScrollBar.getUserData();
        } catch (ClassCastException e) {
            targetValue = -1.0;
        }
        if (targetValue >= 0.0) {
            instagramTableScrollBar.setValue(targetValue / instagram_data_table_view.getItems().size());
        }
    }

    public void setCommentSettingController(ICommentSettingController commentSettingController) {
        this.commentSettingController = commentSettingController;
    }

    private Stage getStage() {
        return (Stage) timeline_button.getScene().getWindow();
    }

    private void closeWindow() {
        Platform.runLater(() -> getStage().close());
    }

    public void onRegisterScheduleButtonClicked(ActionEvent actionEvent) {
        List<InstagramArticleTableItem> userItems = instagram_data_table_view.getItems().filtered(InstagramArticleTableItem::isChecked);
        String comment = emoji_editor.getText();
        long sendTimeMillis = date_time_picker.getTimeInMillis();
        int delay = delay_value_combo_box.getValue();
        presenter.onRegisterScheduleButtonClicked(userItems, comment, sendTimeMillis, delay);
    }

    public void onTimelineButtonClicked(ActionEvent actionEvent) {
        presenter.onTimelineButtonClicked();
    }

    public void onMostLikeButtonClicked(ActionEvent actionEvent) {
        presenter.onMostLikeButtonClicked();
    }

    public void onSelfPostButtonClicked(ActionEvent actionEvent) {
        presenter.onSelfPostButtonClicked();
    }

    public void onSearchButtonClicked(ActionEvent actionEvent) {
        String hashtag = search_text_field.getText();
        presenter.onSearchButtonClicked(hashtag);
    }
}
