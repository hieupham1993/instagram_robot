package jp.co.rilarc.instagram_robot.gui.messagesetting.addschedule;

import java.util.List;

import jp.co.rilarc.instagram_robot.model.InstagramUserTableItem;

/**
 * Created by HieuPT on 1/4/2018.
 */
public interface IAddMessageScheduleView {

    void showTableViewProgress(boolean shown);

    void showInstagramDataTable(boolean shown);

    void showInstagramUserData(List<InstagramUserTableItem> items, String nextMaxId);

    void showProgress(boolean shown);

    void closeDialog();

    void notifyMessageSettingRefresh();

    void addMoreInstagramUserData(List<InstagramUserTableItem> items, String nextMaxId);

    void showMessageDialog(String message);
}
