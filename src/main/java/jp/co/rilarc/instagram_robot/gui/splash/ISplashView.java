package jp.co.rilarc.instagram_robot.gui.splash;

/**
 * Created by HieuPT on 11/13/2017.
 */
public interface ISplashView {

    void displayLoginScreen();

    void displayMainScreen();
}
