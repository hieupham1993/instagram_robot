package jp.co.rilarc.instagram_robot.gui.splash;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.gui.login.LoginStage;
import jp.co.rilarc.instagram_robot.gui.main.MainStage;
import jp.co.rilarc.instagram_robot.presenters.SplashPresenter;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class SplashController implements Initializable, ISplashView, Main.IApplicationStateChangeListener {

    @FXML
    private AnchorPane root_layout;

    private Stage stage;

    private SplashPresenter presenter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Main.getInstance().registerAppStateChangeListener(this);
        presenter = new SplashPresenter(this);
    }

    private Stage getStage() {
        if (stage == null) {
            stage = (Stage) root_layout.getScene().getWindow();
        }
        return stage;
    }

    @Override
    public void displayLoginScreen() {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> new LoginStage(getStage()));
        new Thread(sleeper).start();
    }

    @Override
    public void displayMainScreen() {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> new MainStage(getStage()));
        new Thread(sleeper).start();
    }

    @Override
    public void onAppStart() {
        presenter.onStart();
    }
}
