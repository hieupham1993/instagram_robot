package jp.co.rilarc.instagram_robot.gui;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Window;

/**
 * Created by HieuPT on 11/15/2017.
 */
public class ConfirmAlertDialog extends Alert {

    public interface IConfirmAlertDialogResult {

        default void onOk() {

        }

        default void onCancel() {

        }
    }

    public ConfirmAlertDialog(Window owner, String title, String description, IConfirmAlertDialogResult callback) {
        this(owner, AlertType.CONFIRMATION, title, description, callback);
    }

    public ConfirmAlertDialog(Window owner, AlertType alertType, String title, String description, IConfirmAlertDialogResult callback) {
        super(alertType);
        setTitle(title);
        setHeaderText(description);
        initOwner(owner);
        Optional<ButtonType> result = showAndWait();
        if (callback != null) {
            if (result.isPresent() && result.get() == ButtonType.OK) {
                callback.onOk();
            } else {
                callback.onCancel();
            }
        }
    }
}
