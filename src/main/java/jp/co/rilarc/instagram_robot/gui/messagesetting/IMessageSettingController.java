package jp.co.rilarc.instagram_robot.gui.messagesetting;

/**
 * Created by HieuPT on 1/24/2018.
 */
public interface IMessageSettingController {

    void refreshScheduledMessages();
}
