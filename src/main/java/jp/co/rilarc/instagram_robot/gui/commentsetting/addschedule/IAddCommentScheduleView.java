package jp.co.rilarc.instagram_robot.gui.commentsetting.addschedule;

import java.util.List;

import jp.co.rilarc.instagram_robot.model.InstagramArticleTableItem;

/**
 * Created by HieuPT on 1/4/2018.
 */
public interface IAddCommentScheduleView {

    void showTableViewProgress(boolean shown);

    void showInstagramDataTable(boolean shown);

    void showInstagramArticleData(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId);

    void showProgress(boolean shown);

    void closeDialog();

    void notifyCommentSettingRefresh();

    void showMessageDialog(String message);

    void addMoreInstagramArticleData(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId);
}
