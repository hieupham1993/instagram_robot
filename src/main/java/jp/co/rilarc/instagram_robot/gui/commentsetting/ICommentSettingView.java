package jp.co.rilarc.instagram_robot.gui.commentsetting;

import java.util.List;

import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleCommentResponse;

/**
 * Created by HieuPT on 1/18/2018.
 */
public interface ICommentSettingView {

    void backToLogScreen();

    void showNoItemLabel(boolean shown);

    void showProgress();

    void hideProgress();

    void showScheduleComment(List<WssjGetListScheduleCommentResponse.Comment> messages);

    void showAddNewScheduleForm();

    void showDeleteAllConfirmDialog();

    void showMessageDialog(String message);

    void enableDeleteAllButton(boolean enable);
}
