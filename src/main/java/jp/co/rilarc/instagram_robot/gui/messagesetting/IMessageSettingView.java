package jp.co.rilarc.instagram_robot.gui.messagesetting;

import java.util.List;

import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse; /**
 * Created by HieuPT on 1/18/2018.
 */
public interface IMessageSettingView {

    void backToLogScreen();

    void showNoItemLabel(boolean shown);

    void showProgress();

    void hideProgress();

    void showScheduleMessage(List<WssjGetListScheduleMessageResponse.Message> messages);

    void showAddNewScheduleForm();

    void showDeleteAllConfirmDialog();

    void showMessageDialog(String message);

    void enableDeleteAllButton(boolean enable);
}
