package jp.co.rilarc.instagram_robot.gui.messagesetting.addschedule;

import com.hieupt.controlsfx.DateTimePicker;
import com.hieupt.controlsfx.EmojiEditor;
import com.hieupt.controlsfx.ImageButtonTableCell;
import com.hieupt.controlsfx.ImageViewTableCell;
import com.hieupt.controlsfx.LabelTableCell;
import com.hieupt.controlsfx.converter.IImageConverter;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXProgressBar;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.util.TextUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;
import jp.co.rilarc.instagram_robot.controlfx.ControlUtils;
import jp.co.rilarc.instagram_robot.gui.ConfirmAlertDialog;
import jp.co.rilarc.instagram_robot.gui.messagesetting.IMessageSettingController;
import jp.co.rilarc.instagram_robot.model.InstagramUserTableItem;
import jp.co.rilarc.instagram_robot.presenters.AddMessageSchedulePresenter;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class AddMessageScheduleController implements Initializable, IAddMessageScheduleView {

    @FXML
    private StackPane progress_layout;

    @FXML
    private EmojiEditor emoji_editor;

    @FXML
    private DateTimePicker date_time_picker;

    @FXML
    private TableView<InstagramUserTableItem> instagram_data_table_view;

    @FXML
    private JFXProgressBar table_view_progress;

    @FXML
    private JFXComboBox<Integer> delay_value_combo_box;

    @FXML
    private ToggleButton following_button;

    @FXML
    private ToggleButton follower_button;

    private IMessageSettingController messageSettingController;

    private ScrollBar instagramTableScrollBar;

    private AddMessageSchedulePresenter presenter;

    private final ToggleGroup toggleGroup = new ToggleGroup();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        presenter = new AddMessageSchedulePresenter(this);
        setupInstagramTable();
        initDelayComboBox();
        setupActionButton();
    }

    private void setupActionButton() {
        toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (oldValue != null) {
                    oldValue.setSelected(false);
                }
            } else {
                oldValue.setSelected(true);
            }
        });
        toggleGroup.getToggles().addAll(follower_button, following_button);
    }

    private void initDelayComboBox() {
        List<Integer> delayValueList = new ArrayList<>();
        for (int i = Const.MIN_DELAY_SCHEDULE; i <= Const.MAX_DELAY_SCHEDULE; i += Const.STEP_DELAY_SCHEDULE) {
            delayValueList.add(i);
        }
        delay_value_combo_box.setItems(FXCollections.observableList(delayValueList));
        delay_value_combo_box.setConverter(new StringConverter<Integer>() {

            @Override
            public String toString(Integer object) {
                if (object != null) {
                    return String.valueOf(object) + " 秒";
                }
                return Const.EMPTY_STRING;
            }

            @Override
            public Integer fromString(String string) {
                if (!TextUtils.isEmpty(string)) {
                    String valueStr = string.substring(0, string.lastIndexOf(" 秒"));
                    NumberUtils.toInt(valueStr, 0);
                }
                return 0;
            }
        });
        delay_value_combo_box.setValue(Const.DEFAULT_DELAY_SCHEDULE);
    }

    private void setupInstagramTable() {
        instagram_data_table_view.selectionModelProperty().bind(new SimpleObjectProperty<>());
        ObservableList<TableColumn<InstagramUserTableItem, ?>> columns = instagram_data_table_view.getColumns();
        columns.add(createCheckColumn());
        columns.add(createAvatarColumn());
        columns.add(createUsernameColumn());
        columns.add(createFullNameColumn());
        columns.add(createActionColumn());
        columns.forEach(column -> column.setSortable(false));
    }

    private void initInstagramTableScrollBar() {
        if (instagramTableScrollBar == null) {
            ScrollBar scrollBar = ControlUtils.getVerticalScrollbar(instagram_data_table_view);
            if (scrollBar != null) {
                instagramTableScrollBar = scrollBar;
                instagramTableScrollBar.valueProperty().addListener((observable, oldValue, newValue) -> {
                    double scrolledValue = newValue.doubleValue();
                    if (scrolledValue == instagramTableScrollBar.getMax()) {
                        double targetValue = scrolledValue * instagram_data_table_view.getItems().size();
                        instagramTableScrollBar.setUserData(targetValue);
                        if (following_button.isSelected()) {
                            presenter.onLoadMoreFollowing((String) instagram_data_table_view.getUserData());
                        } else {
                            presenter.onLoadMoreFollowers((String) instagram_data_table_view.getUserData());
                        }
                    }
                });
            }
        }
    }

    private TableColumn<InstagramUserTableItem, Boolean> createCheckColumn() {
        TableColumn<InstagramUserTableItem, Boolean> column = new TableColumn<>(Const.EMPTY_STRING);
        CheckBox checkBoxHeader = new CheckBox();
        checkBoxHeader.setOnAction(event -> {
            CheckBox checkBox = (CheckBox) event.getSource();
            boolean isChecked = checkBox.isSelected();
            instagram_data_table_view.getItems().forEach(item -> item.setChecked(isChecked));
        });
        SimpleBooleanProperty booleanProperty = new SimpleBooleanProperty();
        checkBoxHeader.selectedProperty().bindBidirectional(booleanProperty);
        column.setGraphic(checkBoxHeader);
        column.setCellValueFactory(param -> {
            InstagramUserTableItem item = param.getValue();
            return item.checkedProperty();
        });
        column.setCellFactory(param -> {
            TableCell<InstagramUserTableItem, Boolean> tableCell = new CheckBoxTableCell<>(index -> {
                ObservableList<InstagramUserTableItem> items = instagram_data_table_view.getItems();
                if (index < 0 || index >= items.size()) {
                    return null;
                }
                recheckColumnHeaderState(booleanProperty);
                return items.get(index).checkedProperty();
            });
            tableCell.setAlignment(Pos.TOP_CENTER);
            return tableCell;
        });
        column.setMinWidth(50.0);
        column.setMaxWidth(50.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramUserTableItem, String> createAvatarColumn() {
        TableColumn<InstagramUserTableItem, String> column = new TableColumn<>("写真");
        column.setCellValueFactory(param -> {
            InstagramUserTableItem item = param.getValue();
            return new SimpleStringProperty(item.getAvatarUrl());
        });
        column.setCellFactory(param -> {
            ImageViewTableCell<InstagramUserTableItem, String> tableCell = new ImageViewTableCell<>(new IImageConverter<String>() {

                @Override
                public String fromImage(Image image) {
                    return image.toString();
                }

                @Override
                public Image toImage(String object) {
                    return new Image(object, true);
                }
            });
            tableCell.setAlignment(Pos.TOP_CENTER);
            tableCell.setMaxHeight(150.0);
            tableCell.setMaxWidth(150.0);
            tableCell.setMinHeight(150.0);
            tableCell.setMinWidth(150.0);
            tableCell.setImageSize(125.0, 125.0);
            return tableCell;
        });
        column.setMinWidth(150.0);
        column.setMaxWidth(150.0);
        column.setResizable(false);
        return column;
    }

    private TableColumn<InstagramUserTableItem, String> createUsernameColumn() {
        TableColumn<InstagramUserTableItem, String> column = new TableColumn<>("ユーザー名");
        column.setCellValueFactory(param -> {
            InstagramUserTableItem item = param.getValue();
            return new SimpleStringProperty(item.getUsername());
        });
        column.setCellFactory(param -> {
            TableCell<InstagramUserTableItem, String> tableCell = new LabelTableCell<>(new DefaultStringConverter());
            tableCell.setAlignment(Pos.TOP_LEFT);
            return tableCell;
        });
        return column;
    }

    private TableColumn<InstagramUserTableItem, String> createFullNameColumn() {
        TableColumn<InstagramUserTableItem, String> column = new TableColumn<>("フルネーム");
        column.setCellValueFactory(param -> {
            InstagramUserTableItem item = param.getValue();
            return new SimpleStringProperty(item.getFullName());
        });
        column.setCellFactory(param -> {
            TableCell<InstagramUserTableItem, String> tableCell = new LabelTableCell<>(new DefaultStringConverter());
            tableCell.setAlignment(Pos.TOP_LEFT);
            return tableCell;
        });
        return column;
    }

    private TableColumn<InstagramUserTableItem, String> createActionColumn() {
        TableColumn<InstagramUserTableItem, String> column = new TableColumn<>("オプション");
        column.setCellValueFactory(param -> {
            InstagramUserTableItem item = param.getValue();
            return new SimpleStringProperty(InstagramWebController.INSTAGRAM_BASE_URL + "/" + item.getUsername());
        });
        column.setCellFactory(param -> {
            ImageButtonTableCell<InstagramUserTableItem, String> tableCell = new ImageButtonTableCell<>(new IImageConverter<String>() {

                @Override
                public String fromImage(Image image) {
                    return image.toString();
                }

                @Override
                public Image toImage(String object) {
                    return new Image(getClass().getResource("/images/eye.png").toExternalForm());
                }
            });
            tableCell.setAlignment(Pos.TOP_CENTER);
            tableCell.setOnButtonClickListener(Utils::openBrowserSystem);
            return tableCell;
        });
        column.setMinWidth(75.0);
        column.setMaxWidth(75.0);
        column.setResizable(false);
        return column;
    }

    private void recheckColumnHeaderState(BooleanProperty booleanProperty) {
        boolean checked = true;
        for (InstagramUserTableItem item : instagram_data_table_view.getItems()) {
            if (!item.isChecked()) {
                checked = false;
                break;
            }
        }
        booleanProperty.set(checked);
    }

    public void onRegisterScheduleButtonClicked(ActionEvent actionEvent) {
        List<InstagramUserTableItem> userItems = instagram_data_table_view.getItems().filtered(InstagramUserTableItem::isChecked);
        String message = emoji_editor.getText();
        long sendTimeMillis = date_time_picker.getTimeInMillis();
        int delay = delay_value_combo_box.getValue();
        presenter.onRegisterScheduleButtonClicked(userItems, message, sendTimeMillis, delay);
    }

    public void onFollowingButtonClicked(ActionEvent actionEvent) {
        presenter.onFollowingButtonClicked();
    }

    public void onFollowerButtonClicked(ActionEvent actionEvent) {
        presenter.onFollowerButtonClicked();
    }

    @Override
    public void showTableViewProgress(boolean shown) {
        table_view_progress.setVisible(shown);
    }

    @Override
    public void showInstagramDataTable(boolean shown) {
        instagram_data_table_view.setVisible(shown);
    }

    @Override
    public void showInstagramUserData(List<InstagramUserTableItem> items, String nextMaxId) {
        instagram_data_table_view.setItems(FXCollections.observableList(items));
        instagram_data_table_view.scrollTo(0);
        initInstagramTableScrollBar();
        instagram_data_table_view.setUserData(nextMaxId);
    }

    @Override
    public void showProgress(boolean shown) {
        progress_layout.setVisible(shown);
    }

    @Override
    public void closeDialog() {
        closeWindow();
    }

    @Override
    public void notifyMessageSettingRefresh() {
        if (messageSettingController != null) {
            messageSettingController.refreshScheduledMessages();
        }
    }

    @Override
    public void addMoreInstagramUserData(List<InstagramUserTableItem> items, String nextMaxId) {
        instagram_data_table_view.getItems().addAll(FXCollections.observableList(items));
        instagram_data_table_view.setUserData(nextMaxId);
        resetInstagramScrollBarValue();
    }

    @Override
    public void showMessageDialog(String message) {
        if (!TextUtils.isEmpty(message)) {
            Platform.runLater(() -> new ConfirmAlertDialog(getStage(), Alert.AlertType.INFORMATION, "警告", message, null));
        }
    }

    private void resetInstagramScrollBarValue() {
        double targetValue;
        try {
            targetValue = (double) instagramTableScrollBar.getUserData();
        } catch (ClassCastException e) {
            targetValue = -1.0;
        }
        if (targetValue >= 0.0) {
            instagramTableScrollBar.setValue(targetValue / instagram_data_table_view.getItems().size());
        }
    }

    public void setMessageSettingController(IMessageSettingController messageSettingController) {
        this.messageSettingController = messageSettingController;
    }

    private Stage getStage() {
        return (Stage) follower_button.getScene().getWindow();
    }

    private void closeWindow() {
        Platform.runLater(() -> getStage().close());
    }
}
