package jp.co.rilarc.instagram_robot.gui.messagesetting.addschedule;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.gui.messagesetting.IMessageSettingController;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class AddMessageScheduleStage extends Stage {

    public AddMessageScheduleStage(Stage owner, IMessageSettingController messageSettingController) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/layout_add_message_schedule.fxml"));
            Parent root = loader.load();
            AddMessageScheduleController controller = loader.getController();
            controller.setMessageSettingController(messageSettingController);
            setTitle("Add message schedule");
            setResizable(true);
            setMinWidth(820.0);
            setMinHeight(690.0);
            initModality(Modality.WINDOW_MODAL);
            initOwner(owner);
            getIcons().add(new Image(getClass().getResource("/images/chat.png").toExternalForm()));
            setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
