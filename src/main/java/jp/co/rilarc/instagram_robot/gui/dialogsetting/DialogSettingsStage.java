package jp.co.rilarc.instagram_robot.gui.dialogsetting;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class DialogSettingsStage extends Stage {

    public DialogSettingsStage(Stage owner) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/layout/layout_dialog_settings.fxml"));
            setTitle("Setting");
            setResizable(false);
            initModality(Modality.WINDOW_MODAL);
            initOwner(owner);
            getIcons().add(new Image(getClass().getResource("/images/settings.png").toExternalForm()));
            setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
