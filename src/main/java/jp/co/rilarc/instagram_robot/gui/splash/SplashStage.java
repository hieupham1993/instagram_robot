package jp.co.rilarc.instagram_robot.gui.splash;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class SplashStage extends Stage {

    private static final String TAG = SplashStage.class.getSimpleName();

    public SplashStage() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/layout/layout_splash.fxml"));
            setResizable(false);
            setMinWidth(600);
            setMinHeight(600);
            setWidth(600);
            setHeight(600);
            getIcons().add(new Image(getClass().getResource("/images/hash_strike_logo.png").toExternalForm()));
            setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
