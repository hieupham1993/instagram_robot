package jp.co.rilarc.instagram_robot.gui.dialogsetting;

/**
 * Created by HieuPT on 1/4/2018.
 */
public interface IDialogSettingView {

    void setAutoLoginEnable(boolean enable);

    void setAutoStartEnable(boolean enable);

    void closeDialog();
}
