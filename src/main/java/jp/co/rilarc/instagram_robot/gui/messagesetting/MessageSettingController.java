package jp.co.rilarc.instagram_robot.gui.messagesetting;

import org.controlsfx.control.GridView;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse;
import jp.co.rilarc.instagram_robot.gui.ConfirmAlertDialog;
import jp.co.rilarc.instagram_robot.gui.IMainController;
import jp.co.rilarc.instagram_robot.controlfx.MessageCell;
import jp.co.rilarc.instagram_robot.gui.messagesetting.addschedule.AddMessageScheduleStage;
import jp.co.rilarc.instagram_robot.presenters.MessageSettingPresenter;

/**
 * Created by HieuPT on 1/17/2018.
 */
public class MessageSettingController implements Initializable, IMessageSettingView, IMessageSettingController {

    @FXML
    private MenuItem delete_all_button;

    @FXML
    private Button add_new_button;

    @FXML
    private StackPane layoutProgressLoading;

    @FXML
    private Label no_items_label;

    @FXML
    private GridView<WssjGetListScheduleMessageResponse.Message> item_grid_view;

    private IMainController externalEventListener;

    private MessageSettingPresenter presenter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        presenter = new MessageSettingPresenter(this);
        item_grid_view.setCellFactory(parent -> new MessageCell());
        presenter.getListScheduleMessage();
    }

    public void setExternalEventListener(IMainController externalEventListener) {
        this.externalEventListener = externalEventListener;
    }

    public void onBackButtonClicked(MouseEvent mouseEvent) {
        presenter.onBackButtonClicked();
    }

    @Override
    public void backToLogScreen() {
        if (externalEventListener != null) {
            externalEventListener.backToLogScreen();
        }
    }

    @Override
    public void showNoItemLabel(boolean shown) {
        item_grid_view.setVisible(!shown);
        no_items_label.setVisible(shown);
    }

    @Override
    public void showProgress() {
        layoutProgressLoading.setVisible(true);
    }

    @Override
    public void hideProgress() {
        layoutProgressLoading.setVisible(false);
    }

    @Override
    public void showScheduleMessage(List<WssjGetListScheduleMessageResponse.Message> messages) {
        Platform.runLater(() -> {
            item_grid_view.setItems(FXCollections.observableArrayList());
            item_grid_view.setItems(FXCollections.observableList(messages));
            item_grid_view.getItems().addListener((ListChangeListener<WssjGetListScheduleMessageResponse.Message>) c -> {
                c.next();
                if (c.wasRemoved() && c.getList().isEmpty()) {
                    showNoItemLabel(true);
                    enableDeleteAllButton(false);
                }
            });
        });
    }

    @Override
    public void showAddNewScheduleForm() {
        new AddMessageScheduleStage(getStage(), this).show();
    }

    @Override
    public void showDeleteAllConfirmDialog() {
        new ConfirmAlertDialog(getStage(), "警告", "削除してもよろしいですか", new ConfirmAlertDialog.IConfirmAlertDialogResult() {

            @Override
            public void onOk() {
                presenter.onDeleteAllScheduleConfirmed();
            }
        });
    }

    @Override
    public void showMessageDialog(String message) {
        new ConfirmAlertDialog(getStage(), Alert.AlertType.INFORMATION, "警告", message, null);
    }

    @Override
    public void enableDeleteAllButton(boolean enable) {
        delete_all_button.setDisable(!enable);
    }

    private Stage getStage() {
        return (Stage) layoutProgressLoading.getScene().getWindow();
    }

    public void onAddNewButtonClicked(ActionEvent actionEvent) {
        presenter.onAddNewButtonClicked();
    }

    @Override
    public void refreshScheduledMessages() {
        presenter.getListScheduleMessage();
    }

    public void onDeleteAllScheduleButtonClicked(ActionEvent actionEvent) {
        presenter.onDeleteAllScheduleButtonClicked();
    }
}
