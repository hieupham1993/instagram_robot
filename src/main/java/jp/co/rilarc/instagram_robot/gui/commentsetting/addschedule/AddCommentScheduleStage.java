package jp.co.rilarc.instagram_robot.gui.commentsetting.addschedule;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.gui.commentsetting.ICommentSettingController;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class AddCommentScheduleStage extends Stage {

    public AddCommentScheduleStage(Stage owner, ICommentSettingController commentSettingController) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/layout_add_comment_schedule.fxml"));
            Parent root = loader.load();
            AddCommentScheduleController controller = loader.getController();
            controller.setCommentSettingController(commentSettingController);
            setTitle("Add comment schedule");
            setResizable(true);
            setMinWidth(820.0);
            setMinHeight(690.0);
            initModality(Modality.WINDOW_MODAL);
            initOwner(owner);
            getIcons().add(new Image(getClass().getResource("/images/chat.png").toExternalForm()));
            setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
