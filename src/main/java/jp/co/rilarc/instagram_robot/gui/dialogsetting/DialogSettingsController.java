package jp.co.rilarc.instagram_robot.gui.dialogsetting;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.presenters.DialogSettingPresenter;

/**
 * Created by Nguyen Huu Ta on 19/11/2017.
 */

public class DialogSettingsController implements Initializable, IDialogSettingView {

    @FXML
    private CheckBox auto_login_check_box;

    @FXML
    private CheckBox auto_start_check_box;

    private DialogSettingPresenter presenter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        presenter = new DialogSettingPresenter(this);
        presenter.onStart();
    }

    private Stage getStage() {
        return (Stage) auto_login_check_box.getScene().getWindow();
    }

    private void closeWindow() {
        getStage().close();
    }

    public void onSaveButtonClicked(ActionEvent actionEvent) {
        presenter.onSaveButtonClicked(auto_login_check_box.isSelected(), auto_start_check_box.isSelected());
    }

    @Override
    public void setAutoLoginEnable(boolean enable) {
        auto_login_check_box.setSelected(enable);
    }

    @Override
    public void setAutoStartEnable(boolean enable) {
        auto_start_check_box.setSelected(enable);
    }

    @Override
    public void closeDialog() {
        closeWindow();
    }
}
