package jp.co.rilarc.instagram_robot.gui.login;

import org.apache.http.util.TextUtils;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.BuildConfig;
import jp.co.rilarc.instagram_robot.gui.main.MainStage;
import jp.co.rilarc.instagram_robot.presenters.LoginPresenter;
import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LoginController implements Initializable, ILoginView {

    @FXML
    private AnchorPane layoutProgressLoading;

    @FXML
    private Label version_label;

    @FXML
    private Label app_name_label;

    @FXML
    private TextField email_text_field;

    @FXML
    private TextField password_text_field;

    @FXML
    private CheckBox auto_login_checkbox;

    @FXML
    private CheckBox auto_start_checkbox;

    @FXML
    private Button login_button;

    private Stage stage;

    private LoginPresenter presenter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        app_name_label.setText(BuildConfig.APP_NAME);
        version_label.setText(String.format(Const.VERSION_TEXT, BuildConfig.VERSION_NAME));
        presenter = new LoginPresenter(this);
        presenter.onStart();
    }

    public void onLoginButtonClicked(ActionEvent actionEvent) {
        String email = email_text_field.getText();
        String password = password_text_field.getText();
        boolean isAutoLogin = auto_login_checkbox.isSelected();
        boolean isAutoStart = auto_start_checkbox.isSelected();
        presenter.onLoginButtonClicked(email, password, isAutoLogin, isAutoStart);
    }

    private void showDialog(String message) {
        if (!TextUtils.isEmpty(message)) {
            ButtonType buttonOk = new ButtonType("確認", ButtonBar.ButtonData.OK_DONE);
            Alert alert = new Alert(Alert.AlertType.INFORMATION, message, buttonOk);
            alert.setTitle("通知");
            DialogPane dialogPane = alert.getDialogPane();
            dialogPane.getStylesheets().add(getClass().getResource("/stylesheet/style.css").toExternalForm());
            dialogPane.getStyleClass().add("dialog");
            alert.setHeaderText(null);
            alert.showAndWait();
        }
    }

    private Stage getStage() {
        if (stage == null) {
            stage = (Stage) login_button.getScene().getWindow();
        }
        return stage;
    }

    @Override
    public void closeScreen() {
        Platform.runLater(() -> getStage().close());
    }

    @Override
    public void displayMainScreen() {
        Task<Void> sleeper = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> {
            showProgress(false);
            enableLoginButton(true);
            new MainStage(getStage());
        });
        new Thread(sleeper).start();

    }

    @Override
    public void showLoginFailureDialog(String message) {
        Platform.runLater(() -> showDialog(message));
    }

    @Override
    public void showProgress(boolean shown) {
        layoutProgressLoading.setVisible(shown);
    }

    @Override
    public void enableLoginButton(boolean enable) {
        login_button.setDisable(!enable);
    }

    @Override
    public void setAutoLoginEnable(boolean enable) {
        auto_login_checkbox.setSelected(enable);
    }

    @Override
    public void setAutoStartEnable(boolean enable) {
        auto_start_checkbox.setSelected(enable);
    }

    @Override
    public void fillLoginInfo(String email, String password) {
        email_text_field.setText(email);
        password_text_field.setText(password);
    }
}
