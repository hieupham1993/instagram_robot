package jp.co.rilarc.instagram_robot.gui.main;

import org.apache.commons.lang3.SystemUtils;
import org.apache.http.util.TextUtils;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import jp.co.rilarc.instagram_robot.BuildConfig;
import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.Robot3;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.gui.ConfirmAlertDialog;
import jp.co.rilarc.instagram_robot.gui.IMainController;
import jp.co.rilarc.instagram_robot.gui.commentsetting.CommentSettingController;
import jp.co.rilarc.instagram_robot.gui.dialogsetting.DialogSettingsStage;
import jp.co.rilarc.instagram_robot.gui.login.LoginStage;
import jp.co.rilarc.instagram_robot.gui.messagesetting.MessageSettingController;
import jp.co.rilarc.instagram_robot.jsinterface.ILogPanelJs;
import jp.co.rilarc.instagram_robot.messenger.Message;
import jp.co.rilarc.instagram_robot.messenger.MessageHandler;
import jp.co.rilarc.instagram_robot.messenger.Messenger;
import jp.co.rilarc.instagram_robot.presenters.MainPresenter;
import jp.co.rilarc.instagram_robot.tasks.LoginInstagramAppTask;
import jp.co.rilarc.instagram_robot.tasks.LoginInstagramWebTask;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import netscape.javascript.JSObject;

public class MainController
        implements Initializable, Main.IApplicationStateChangeListener, IMainView,
        ILogPanelJs, IMainController {

    private static final String TAG = MainController.class.getSimpleName();

    @FXML
    private Button comment_setting_button;

    @FXML
    private Button message_setting_button;

    @FXML
    private AnchorPane robot_control_panel;

    @FXML
    private AnchorPane profile_info_panel;

    @FXML
    private AnchorPane login_form;

    @FXML
    private TextField instagram_username_text_field;

    @FXML
    private PasswordField instagram_password_field;

    @FXML
    private AnchorPane other_screen_panel;

    @FXML
    private Label system_information_label;

    @FXML
    private VBox progressLoading;

    @FXML
    private WebView log_web_view;

    @FXML
    private ImageView avatar_image_view;

    @FXML
    private Label instagram_user_name_label;

    @FXML
    private Label email_label;

    @FXML
    private Label status_label;

    @FXML
    private Button start_button;

    @FXML
    private Button stop_button;

    private ExecutorService loginWebService, loginAppService;

    private LoginInstagramWebTask loginInstagramWebTask;

    private LoginInstagramAppTask loginInstagramAppTask;

    private MainPresenter presenter;

    private Robot3 robot;

    private final Messenger messenger = new Messenger(new MessageHandler() {

        @Override
        protected void onMessageReceived(Message msg) {
            Platform.runLater(() -> {
                switch (msg.what) {
                    case Const.Msg.MSG_LOGIN_WEB_SUCCESS:
                        onLoginWebSuccess();
                        break;
                    case Const.Msg.MSG_LOGIN_WEB_FAILURE:
                        onLoginWebFailure();
                        break;
                    case Const.Msg.MSG_LOGIN_APP_SUCCESS:
                        boolean forceLoginWeb;
                        try {
                            forceLoginWeb = (boolean) msg.objects[0];
                        } catch (ClassCastException e) {
                            forceLoginWeb = false;
                        }
                        onLoginAppSuccess(forceLoginWeb);
                        break;
                    case Const.Msg.MSG_LOGIN_APP_FAILURE:
                        onLoginAppFailure();
                        break;
                    case Const.Msg.MSG_STOP_ROBOT:
                        onStopRobotButtonClicked(null);
                        break;
                }
            });
        }
    });

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Main.getInstance().registerAppStateChangeListener(this);
        presenter = new MainPresenter(this);
        initSystemInfo();
        setupLogPanel();
        initRobot();
        presenter.onStart();
    }

    private void initRobot() {
        Preference preference = LocalDatabaseHelper.getInstance().getPreference();
        robot = new Robot3(preference.getEmail());
        robot.setMessenger(messenger);
    }

    private void setupLogPanel() {
        log_web_view.setContextMenuEnabled(false);
        WebEngine webEngine = log_web_view.getEngine();
        webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            if (Worker.State.SUCCEEDED.equals(newValue)) {
                JSObject window = (JSObject) webEngine.executeScript("window");
                window.setMember("jsInterface", MainController.this);
            }
        });
        webEngine.load(getClass().getResource("/log_panel.html").toExternalForm());
        VLogger.setup(webEngine);
    }

    private void initSystemInfo() {
        String systemInfo = "OS name: " + SystemUtils.OS_NAME +
                " - " +
                "OS version: " + SystemUtils.OS_VERSION +
                " - " +
                "OS arch: " + SystemUtils.OS_ARCH +
                " - " +
                "App version: " + BuildConfig.VERSION_NAME;
        system_information_label.setText(systemInfo);
    }

    private void onLoginWebSuccess() {
        presenter.onLoginWebSuccess();
    }

    private void onLoginWebFailure() {
        presenter.onLoginFailure();
    }

    private void onLoginAppSuccess(boolean forceLoginWeb) {
        presenter.onLoginAppSuccess(forceLoginWeb);
    }

    private void onLoginAppFailure() {
        presenter.onLoginFailure();
    }

    @Override
    public void startRobot() {
        robot.start();
    }

    @Override
    public void stopRobot() {
        robot.stop();
    }

    @Override
    public void showProgress() {
        progressLoading.setVisible(true);
    }

    @Override
    public void hideProgress() {
        progressLoading.setVisible(false);
    }

    @Override
    public void showLogoutConfirmDialog() {
        new ConfirmAlertDialog(getStage(), Alert.AlertType.CONFIRMATION, "警告", "アカウントを変更すると、実施しているタスクが停止されます。よろしいですか？",
                new ConfirmAlertDialog.IConfirmAlertDialogResult() {

                    @Override
                    public void onOk() {
                        presenter.onLogoutConfirmed();
                    }
                });
    }

    @Override
    public void displayLoginScreen() {
        new LoginStage(getStage());
    }

    @Override
    public void fillProfileInfo(String systemEmail, String instagramUsername, String instagramAvatar) {
        email_label.setText(systemEmail);
        if (!TextUtils.isEmpty(instagramAvatar)) {
            avatar_image_view.setImage(new Image(instagramAvatar, true));
        }
        instagram_user_name_label.setText(instagramUsername);
    }

    @Override
    public void setRobotState(boolean running) {
        if (running) {
            status_label.setText("稼働中");
            start_button.setDisable(true);
            stop_button.setDisable(false);
        } else {
            status_label.setText("待機中");
            start_button.setDisable(false);
            stop_button.setDisable(true);
        }
    }

    @Override
    public void openUrl(String url) {
        Utils.openBrowserSystem(url);
    }

    @Override
    public void loginToWebInstagram(String email, String password) {
        loginWebService = Executors.newSingleThreadExecutor();
        loginInstagramWebTask = new LoginInstagramWebTask(email, password);
        loginInstagramWebTask.setMessenger(messenger);
        loginWebService.execute(loginInstagramWebTask);
    }

    @Override
    public void showCommentSettingPanel() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/layout_comment_setting.fxml"));
            Parent root = loader.load();
            CommentSettingController commentController = loader.getController();
            if (commentController != null) {
                commentController.setExternalEventListener(this);
            }
            showPanel(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMessageSettingPanel() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/layout_message_setting.fxml"));
            Parent root = loader.load();
            MessageSettingController messageSettingController = loader.getController();
            if (messageSettingController != null) {
                messageSettingController.setExternalEventListener(this);
            }
            showPanel(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hideSettingPanel() {
        ObservableList<Node> children = other_screen_panel.getChildren();
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(300), other_screen_panel);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        fadeTransition.setOnFinished(event -> {
            children.clear();
            other_screen_panel.setVisible(false);
        });
        fadeTransition.play();
    }

    @Override
    public void loginToAppInstagram(String username, String password, boolean forceLoginWeb) {
        loginAppService = Executors.newSingleThreadExecutor();
        loginInstagramAppTask = new LoginInstagramAppTask(username, password, forceLoginWeb);
        loginInstagramAppTask.setMessenger(messenger);
        loginAppService.execute(loginInstagramAppTask);
    }

    @Override
    public void fillLoginInfo(String username, String password) {
        instagram_username_text_field.setText(username);
        instagram_password_field.setText(password);
    }

    @Override
    public void setLoginState(boolean loggedIn) {
        login_form.setVisible(!loggedIn);
        profile_info_panel.setVisible(loggedIn);
        robot_control_panel.setDisable(!loggedIn);
        comment_setting_button.setDisable(!loggedIn);
        message_setting_button.setDisable(!loggedIn);
    }

    @Override
    public void showChangeAccountConfirmDialog() {
        new ConfirmAlertDialog(getStage(), Alert.AlertType.CONFIRMATION, "警告", "アカウントを変更すると、実施しているタスクが停止されます。よろしいですか？",
                new ConfirmAlertDialog.IConfirmAlertDialogResult() {

                    @Override
                    public void onOk() {
                        presenter.onChangeInstagramAccountConfirmed();
                    }
                });
    }

    @Override
    public void clearLoginForm() {
        instagram_username_text_field.setText(Const.EMPTY_STRING);
        instagram_password_field.setText(Const.EMPTY_STRING);
        instagram_username_text_field.requestFocus();
    }

    private void showPanel(Node node) {
        other_screen_panel.setOpacity(1.0);
        other_screen_panel.setVisible(true);
        ObservableList<Node> children = other_screen_panel.getChildren();
        children.add(node);
        AnchorPane.setLeftAnchor(node, 0.0d);
        AnchorPane.setTopAnchor(node, 0.0d);
        AnchorPane.setRightAnchor(node, 0.0d);
        AnchorPane.setBottomAnchor(node, 0.0d);
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(300), node);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);
        fadeTransition.setOnFinished(event -> {
            children.clear();
            children.add(node);
        });
        fadeTransition.play();
    }

    @Override
    public void onAppStopping() {
        stopLogging();
        stopRobot();
    }

    private void stopLogging() {
        if (loginInstagramWebTask != null) {
            loginInstagramWebTask.destroy();
        }
        Utils.stopService(loginWebService);
        if (loginInstagramAppTask != null) {
            loginInstagramAppTask.destroy();
        }
        Utils.stopService(loginAppService);
    }

    public void onSettingButtonClicked(MouseEvent mouseEvent) {
        new DialogSettingsStage(getStage()).show();
    }

    private Stage getStage() {
        return (Stage) start_button.getScene().getWindow();
    }

    public void onLogoutButtonClicked(MouseEvent mouseEvent) {
        presenter.onLogoutButtonClicked();
    }

    public void onStartRobotButtonClicked(ActionEvent actionEvent) {
        presenter.onStartRobotButtonClicked();
    }

    public void onStopRobotButtonClicked(ActionEvent actionEvent) {
        presenter.onStopRobotButtonClicked();
    }

    public void onInstagramButtonClicked(MouseEvent mouseEvent) {
        presenter.onInstagramButtonClicked();
    }

    public void onOpenTaskSettingButtonClicked(ActionEvent actionEvent) {
        presenter.onOpenTaskSettingButtonClicked();
    }

    public void onCommentSettingButtonClicked(ActionEvent actionEvent) {
        presenter.onCommentSettingButtonClicked();
    }

    public void onMessageSettingButtonClicked(ActionEvent actionEvent) {
        presenter.onMessageSettingButtonClicked();
    }

    @Override
    public void backToLogScreen() {
        presenter.onBackButtonClicked();
    }

    public void onLoginButtonClicked(ActionEvent actionEvent) {
        String username = instagram_username_text_field.getText();
        String password = instagram_password_field.getText();
        presenter.onLoginButtonClicked(username, password);
    }

    public void onChangeInstagramAccountButtonClicked(MouseEvent event) {
        presenter.onChangeInstagramAccountButtonClicked();
    }
}

