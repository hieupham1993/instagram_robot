package jp.co.rilarc.instagram_robot.model;

public class FollowingUser {
    private int mId;
    private int mUserId;
    private String mInstagramUsername;
    private String mAvatarUrl;
    private String mFollowingUserId;
    private boolean mHasChecked;

    public FollowingUser(int mId, int mUserId, String mInstagramUsername, String mAvatarUrl, String mFollowingUserId, boolean mHasChecked) {
        this.mId = mId;
        this.mUserId = mUserId;
        this.mInstagramUsername = mInstagramUsername;
        this.mAvatarUrl = mAvatarUrl;
        this.mFollowingUserId = mFollowingUserId;
        this.mHasChecked = mHasChecked;
    }

    public String getFollowingUserId() {
        return mFollowingUserId;
    }

    public int getId() {
        return mId;
    }

    @Override
    public String toString() {
        return ("Id:" + this.mId+
                " User id: "+ this.mUserId +
                " Following user id: "+ this.mFollowingUserId +
                " Has checked : " + this.mHasChecked);
    }

    public String getInstagramUsername() {
        return mInstagramUsername;
    }

    public void setInstagramUsername(String mInstagramUsername) {
        this.mInstagramUsername = mInstagramUsername;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String mAvatarUrl) {
        this.mAvatarUrl = mAvatarUrl;
    }
}
