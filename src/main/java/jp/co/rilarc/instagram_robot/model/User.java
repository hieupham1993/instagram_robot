package jp.co.rilarc.instagram_robot.model;

public class User {

    private InstagramAccount instagramAccount;

    private RobotSetting robotSetting;

    private int id;

    private String email;

    private boolean isRobotWorking;

    private int proxyId;

    private String rememberToken;

    private String accessToken;

    public InstagramAccount getInstagramAccount() {
        return instagramAccount;
    }

    public void setInstagramAccount(InstagramAccount instagramAccount) {
        this.instagramAccount = instagramAccount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RobotSetting getRobotSetting() {
        return robotSetting;
    }

    public void setRobotSetting(RobotSetting robotSetting) {
        this.robotSetting = robotSetting;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isRobotWorking() {
        return isRobotWorking;
    }

    public void setRobotWorking(boolean robotWorking) {
        isRobotWorking = robotWorking;
    }

    public int getProxyId() {
        return proxyId;
    }

    public void setProxyId(int proxyId) {
        this.proxyId = proxyId;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
