package jp.co.rilarc.instagram_robot.model;

public class LikeHashtag {
    private int mId;

    private int mUserId;

    private String mHashtag;


    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        this.mUserId = userId;
    }

    public String getHashtag() {
        return mHashtag;
    }

    public void setHashtag(String hashtag) {
        this.mHashtag = hashtag;
    }
}
