package jp.co.rilarc.instagram_robot.model;

import java.sql.Date;
import java.util.Calendar;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class RobotSetting {

    final private static Logger logger = LogManager.getLogger(RobotSetting.class);

    private int id;
    private int userId;

    // like setting
    private boolean mLikesHashtagAutoLike;
    private boolean mLikesHashtagMinLikeFilterMode;
    private int mLikesHashtagMinLikeFilter;
    private boolean mLikesHashtagMaxLikeFilterMode;
    private int mLikesHashtagMaxLikeFilter;
    private boolean mLikesHashtagMaxHashtagFilterMode;
    private int mLikesHashtagMaxHashtagFilter;
    private boolean mLikesHashtagEnableSpamFilter;
    private boolean mLikesHashtagAutoDisableBadTags;

    private boolean mLikesLocationAutoLike;
    private boolean mLikesLocationMinLikeFilterMode;
    private int mLikesLocationMinLikeFilter;
    private boolean mLikesLocationMaxLikeFilterMode;
    private int mLikesLocationMaxLikeFilter;
    private boolean mLikesLocationMaxHashtagFilterMode;
    private int mLikesLocationMaxHashtagFilter;
    private boolean mLikesLocationEnableSpamFilter;

    private boolean mLikesFollowingAutoLikeFeed;
    private boolean mLikesFollowingEnableSpaFilter;
    private int mLikesAutoLikeSpeed;
    private boolean mDisableLike;
    private boolean mDisableFollow;

    // follow setting
    private boolean mFollowsHashtagAutoFollow;
    private boolean mFollowsHashtagEnableSpamFilter;

    private boolean mFollowsLocationAutoFollow;

    private boolean mFollowsFollowersAutoFollowBack;

    private boolean mFollowsDoNotFollowPreviouslyUnfollowedUsers;
    private boolean mFollowsDoNotFollowPrivateUsers;
    private boolean mFollowsMaxHashtagFilterMode;
    private int mFollowsMaxHashtagFilter;

    private int mFollowsAutoFollowSpeed;

    // unfollows setting
    private boolean mUnfollowsNotFollowingBackAutoUnfollow;
    private boolean mUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode;
    private int mUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow;
    private boolean mUnfollowsBoostUnfollow;
    private int mUnfollowsBoostAmount;
    private int mUnfollowsAutoUnfollowSpeed;

    // global setting
    private boolean mGlobalRobotStatus;
    private boolean mGlobalSlowStartState;
    private boolean mGlobalSleepMode;
    private int mGlobalSleepStart;
    private int mGlobalSleepEnd;
    private boolean mGlobalFilter;
    private boolean mGlobalCompleAccount;

    // proxy
    private String mProxyAddress;
    private int mProxyPort;
    private String mProxyUsername;
    private String mProxyPassword;

    private Date mUpdatedAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isLikesHashtagAutoLike() {
        return mLikesHashtagAutoLike;
    }

    public void setLikesHashtagAutoLike(boolean likesHashtagAutoLike) {
        this.mLikesHashtagAutoLike = likesHashtagAutoLike;
    }

    public boolean isLikesHashtagMinLikeFilterMode() {
        return mLikesHashtagMinLikeFilterMode;
    }

    public void setLikesHashtagMinLikeFilterMode(boolean likesHashtagMinLikeFilterMode) {
        this.mLikesHashtagMinLikeFilterMode = likesHashtagMinLikeFilterMode;
    }

    public int getLikesHashtagMinLikeFilter() {
        return mLikesHashtagMinLikeFilter;
    }

    public void setLikesHashtagMinLikeFilter(int likesHashtagMinLikeFilter) {
        this.mLikesHashtagMinLikeFilter = likesHashtagMinLikeFilter;
    }

    public boolean isLikesHashtagMaxLikeFilterMode() {
        return mLikesHashtagMaxLikeFilterMode;
    }

    public void setLikesHashtagMaxLikeFilterMode(boolean likesHashtagMaxLikeFilterMode) {
        this.mLikesHashtagMaxLikeFilterMode = likesHashtagMaxLikeFilterMode;
    }

    public int getLikesHashtagMaxLikeFilter() {
        return mLikesHashtagMaxLikeFilter;
    }

    public void setLikesHashtagMaxLikeFilter(int likesHashtagMaxLikeFilter) {
        this.mLikesHashtagMaxLikeFilter = likesHashtagMaxLikeFilter;
    }

    public boolean isLikesHashtagMaxHashtagFilterMode() {
        return mLikesHashtagMaxHashtagFilterMode;
    }

    public void setLikesHashtagMaxHashtagFilterMode(boolean likesHashtagMaxHashtagFilterMode) {
        this.mLikesHashtagMaxHashtagFilterMode = likesHashtagMaxHashtagFilterMode;
    }

    public int getLikesHashtagMaxHashtagFilter() {
        return mLikesHashtagMaxHashtagFilter;
    }

    public void setLikesHashtagMaxHashtagFilter(int likesHashtagMaxHashtagFilter) {
        this.mLikesHashtagMaxHashtagFilter = likesHashtagMaxHashtagFilter;
    }

    public boolean isLikesHashtagEnableSpamFilter() {
        return mLikesHashtagEnableSpamFilter;
    }

    public void setLikesHashtagEnableSpamFilter(boolean likesHashtagEnableSpamFilter) {
        this.mLikesHashtagEnableSpamFilter = likesHashtagEnableSpamFilter;
    }

    public boolean isLikesHashtagAutoDisableBadTags() {
        return mLikesHashtagAutoDisableBadTags;
    }

    public void setLikesHashtagAutoDisableBadTags(boolean likesHashtagAutoDisableBadTags) {
        this.mLikesHashtagAutoDisableBadTags = likesHashtagAutoDisableBadTags;
    }

    public boolean isLikesLocationAutoLike() {
        return mLikesLocationAutoLike;
    }

    public void setLikesLocationAutoLike(boolean likesLocationAutoLike) {
        this.mLikesLocationAutoLike = likesLocationAutoLike;
    }

    public boolean isLikesLocationMinLikeFilterMode() {
        return mLikesLocationMinLikeFilterMode;
    }

    public void setLikesLocationMinLikeFilterMode(boolean likesLocationMinLikeFilterMode) {
        this.mLikesLocationMinLikeFilterMode = likesLocationMinLikeFilterMode;
    }

    public int getLikesLocationMinLikeFilter() {
        return mLikesLocationMinLikeFilter;
    }

    public void setLikesLocationMinLikeFilter(int likesLocationMinLikeFilter) {
        this.mLikesLocationMinLikeFilter = likesLocationMinLikeFilter;
    }

    public boolean isLikesLocationMaxLikeFilterMode() {
        return mLikesLocationMaxLikeFilterMode;
    }

    public void setLikesLocationMaxLikeFilterMode(boolean likesLocationMaxLikeFilterMode) {
        this.mLikesLocationMaxLikeFilterMode = likesLocationMaxLikeFilterMode;
    }

    public int getLikesLocationMaxLikeFilter() {
        return mLikesLocationMaxLikeFilter;
    }

    public void setLikesLocationMaxLikeFilter(int likesLocationMaxLikeFilter) {
        this.mLikesLocationMaxLikeFilter = likesLocationMaxLikeFilter;
    }

    public boolean isLikesLocationMaxHashtagFilterMode() {
        return mLikesLocationMaxHashtagFilterMode;
    }

    public void setLikesLocationMaxHashtagFilterMode(boolean likesLocationMaxHashtagFilterMode) {
        this.mLikesLocationMaxHashtagFilterMode = likesLocationMaxHashtagFilterMode;
    }

    public int getLikesLocationMaxHashtagFilter() {
        return mLikesLocationMaxHashtagFilter;
    }

    public void setLikesLocationMaxHashtagFilter(int likesLocationMaxHashtagFilter) {
        this.mLikesLocationMaxHashtagFilter = likesLocationMaxHashtagFilter;
    }

    public boolean isLikesLocationEnableSpamFilter() {
        return mLikesLocationEnableSpamFilter;
    }

    public void setLikesLocationEnableSpamFilter(boolean likesLocationEnableSpamFilter) {
        this.mLikesLocationEnableSpamFilter = likesLocationEnableSpamFilter;
    }

    public boolean isLikesFollowingAutoLikeFeed() {
        return mLikesFollowingAutoLikeFeed;
    }

    public void setLikesFollowingAutoLikeFeed(boolean likesFollowingAutoLikeFeed) {
        this.mLikesFollowingAutoLikeFeed = likesFollowingAutoLikeFeed;
    }

    public boolean isLikesFollowingEnableSpaFilter() {
        return mLikesFollowingEnableSpaFilter;
    }

    public void setLikesFollowingEnableSpaFilter(boolean likesFollowingEnableSpaFilter) {
        this.mLikesFollowingEnableSpaFilter = likesFollowingEnableSpaFilter;
    }

    public int getLikesAutoLikeSpeed() {
        return mLikesAutoLikeSpeed;
    }

    public void setLikesAutoLikeSpeed(int likesAutoLikeSpeed) {
        this.mLikesAutoLikeSpeed = likesAutoLikeSpeed;
    }

    public boolean isFollowsHashtagAutoFollow() {
        return mFollowsHashtagAutoFollow;
    }

    public void setFollowsHashtagAutoFollow(boolean followsHashtagAutoFollow) {
        this.mFollowsHashtagAutoFollow = followsHashtagAutoFollow;
    }

    public boolean isFollowsDoNotFollowPreviouslyUnfollowedUsers() {
        return mFollowsDoNotFollowPreviouslyUnfollowedUsers;
    }

    public void setFollowsDoNotFollowPreviouslyUnfollowedUsers(boolean followsDoNotFollowPreviouslyUnfollowedUsers) {
        this.mFollowsDoNotFollowPreviouslyUnfollowedUsers = followsDoNotFollowPreviouslyUnfollowedUsers;
    }

    public boolean isFollowsDoNotFollowPrivateUsers() {
        return mFollowsDoNotFollowPrivateUsers;
    }

    public void setFollowsDoNotFollowPrivateUsers(boolean followsDoNotFollowPrivateUsers) {
        this.mFollowsDoNotFollowPrivateUsers = followsDoNotFollowPrivateUsers;
    }

    public boolean isFollowsMaxHashtagFilterMode() {
        return mFollowsMaxHashtagFilterMode;
    }

    public void setFollowsMaxHashtagFilterMode(boolean followsMaxHashtagFilterMode) {
        this.mFollowsMaxHashtagFilterMode = followsMaxHashtagFilterMode;
    }

    public int getFollowsMaxHashtagFilter() {
        return mFollowsMaxHashtagFilter;
    }

    public void setFollowsMaxHashtagFilter(int followsMaxHashtagFilter) {
        this.mFollowsMaxHashtagFilter = followsMaxHashtagFilter;
    }

    public boolean isFollowsHashtagEnableSpamFilter() {
        return mFollowsHashtagEnableSpamFilter;
    }

    public void setFollowsHashtagEnableSpamFilter(boolean followsHashtagEnableSpamFilter) {
        this.mFollowsHashtagEnableSpamFilter = followsHashtagEnableSpamFilter;
    }

    public boolean isFollowsLocationAutoFollow() {
        return mFollowsLocationAutoFollow;
    }

    public void setFollowsLocationAutoFollow(boolean followsLocationAutoFollow) {
        this.mFollowsLocationAutoFollow = followsLocationAutoFollow;
    }

    public boolean isFollowsFollowersAutoFollowBack() {
        return mFollowsFollowersAutoFollowBack;
    }

    public void setFollowsFollowersAutoFollowBack(boolean followsFollowersAutoFollow) {
        this.mFollowsFollowersAutoFollowBack = followsFollowersAutoFollow;
    }

    public int getFollowsAutoFollowSpeed() {
        return mFollowsAutoFollowSpeed;
    }

    public void setFollowsAutoFollowSpeed(int followsAutoFollowSpeed) {
        this.mFollowsAutoFollowSpeed = followsAutoFollowSpeed;
    }

    public boolean isUnfollowsNotFollowingBackAutoUnfollow() {
        return mUnfollowsNotFollowingBackAutoUnfollow;
    }

    public void setUnfollowsNotFollowingBackAutoUnfollow(boolean unfollowsNotFollowingBackAutoUnfollow) {
        this.mUnfollowsNotFollowingBackAutoUnfollow = unfollowsNotFollowingBackAutoUnfollow;
    }

    public boolean isUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode() {
        return mUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode;
    }

    public void setUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode(boolean unfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode) {
        this.mUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode = unfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode;
    }

    public int getUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow() {
        return mUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow;
    }

    public void setUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow(int unfollowsNotFollowingBackDaysToWaitBeforeUnfollow) {
        this.mUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow = unfollowsNotFollowingBackDaysToWaitBeforeUnfollow;
    }

    public boolean isUnfollowsBoostUnfollow() {
        return mUnfollowsBoostUnfollow;
    }

    public void setUnfollowsBoostUnfollow(boolean unfollowsBoostUnfollow) {
        this.mUnfollowsBoostUnfollow = unfollowsBoostUnfollow;
    }

    public int getUnfollowsAutoUnfollowSpeed() {
        return mUnfollowsAutoUnfollowSpeed;
    }

    public void setUnfollowsAutoUnfollowSpeed(int unfollowsAutoUnfollowSpeed) {
        this.mUnfollowsAutoUnfollowSpeed = unfollowsAutoUnfollowSpeed;
    }

    public boolean getGlobalRobotStatus() {
        return mGlobalRobotStatus;
    }

    public void setGlobalRobotStatus(boolean globalRobotStatus) {
        this.mGlobalRobotStatus = globalRobotStatus;
    }

    public boolean isGlobalSlowStartState() {
        return mGlobalSlowStartState;
    }

    public void setGlobalSlowStartState(boolean globalSlowStartState) {
        this.mGlobalSlowStartState = globalSlowStartState;
    }

    public boolean isGlobalSleepMode() {
        return mGlobalSleepMode;
    }

    public void setGlobalSleepMode(boolean globalSleepMode) {
        this.mGlobalSleepMode = globalSleepMode;
    }

    public int getGlobalSleepStart() {
        return mGlobalSleepStart;
    }

    public void setGlobalSleepStart(int globalSleepStart) {
        this.mGlobalSleepStart = globalSleepStart;
    }

    public int getGlobalSleepEnd() {
        return mGlobalSleepEnd;
    }

    public void setGlobalSleepEnd(int globalSleepEnd) {
        this.mGlobalSleepEnd = globalSleepEnd;
    }

    public boolean isGlobalFilter() {
        return mGlobalFilter;
    }

    public void setGlobalFilter(boolean globalFilter) {
        this.mGlobalFilter = globalFilter;
    }

    public boolean isGlobalCompleAccount() {
        return mGlobalCompleAccount;
    }

    public void setGlobalCompleAccount(boolean globalCompleAccount) {
        this.mGlobalCompleAccount = globalCompleAccount;
    }

    public boolean isSleepTime()
    {
//        logger.info("mGlobalSleepMode = " + mGlobalSleepMode);
//        logger.info("mGlobalSleepStart = " + mGlobalSleepStart);
//        logger.info("mGlobalSleepEnd = " + mGlobalSleepEnd);
        if (!mGlobalSleepMode) {
            return false;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        if (mGlobalSleepStart <= mGlobalSleepEnd) {
            if (currentHour >= mGlobalSleepStart && currentHour < mGlobalSleepEnd)
                return true;
        }
        // VD: start = 22h,  end = 4h
        else {
            //if ((currentHour >= mGlobalSleepStart && currentHour > mGlobalSleepEnd) ||
            //    (currentHour <= mGlobalSleepStart && currentHour < mGlobalSleepEnd))
            //    return true;
            if (currentHour >= mGlobalSleepStart || currentHour < mGlobalSleepEnd)
                return true;
        }

        return false;
    }

    public int getUnfollowsBoostAmount() {
        return mUnfollowsBoostAmount;
    }

    public void setUnfollowsBoostAmount(int mUnfollowsBoostAmount) {
        this.mUnfollowsBoostAmount = mUnfollowsBoostAmount;
    }

    public boolean isDisableLike() {
        return mDisableLike;
    }

    public void setDisableLike(boolean mDisableLike) {
        this.mDisableLike = mDisableLike;
    }

	public boolean isDisableFollow() {
		return mDisableFollow;
	}

	public void setDisableFollow(boolean mDisableFollow) {
		this.mDisableFollow = mDisableFollow;
	}

	public Date getmUpdatedAt() {
		return mUpdatedAt;
	}

	public void setmUpdatedAt(Date mUpdatedAt) {
		this.mUpdatedAt = mUpdatedAt;
	}

    public String getProxyAddress() {
        return mProxyAddress;
    }

    public void setProxyAddress(String mProxyAddress) {
        this.mProxyAddress = mProxyAddress;
    }

    public int getProxyPort() {
        return mProxyPort;
    }

    public void setProxyPort(int mProxyPort) {
        this.mProxyPort = mProxyPort;
    }

    public String getProxyUsername() {
        return mProxyUsername;
    }

    public void setProxyUsername(String mProxyUsername) {
        this.mProxyUsername = mProxyUsername;
    }

    public String getProxyPassword() {
        return mProxyPassword;
    }

    public void setProxyPassword(String mProxyPassword) {
        this.mProxyPassword = mProxyPassword;
    }
}