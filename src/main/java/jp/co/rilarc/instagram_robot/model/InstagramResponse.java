/**
 * 
 */
package jp.co.rilarc.instagram_robot.model;

/**
 * @author nghilt
 *
 */
public class InstagramResponse {
	
	private int statusCode;
	private String responseAsString;
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getResponseAsString() {
		return responseAsString;
	}
	public void setResponseAsString(String responseAsString) {
		this.responseAsString = responseAsString;
	}
}
