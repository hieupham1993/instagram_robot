package jp.co.rilarc.instagram_robot.model;

public class LocalUser {
	private int userId;
	private boolean active;
	private boolean robotWorking;
	private boolean robotActive;
	private boolean instagramConnected;
	private Proxy proxy;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isRobotWorking() {
		return robotWorking;
	}
	public void setRobotWorking(boolean robotWorking) {
		this.robotWorking = robotWorking;
	}
	public boolean isRobotActive() {
		return robotActive;
	}
	public void setRobotActive(boolean robotActive) {
		this.robotActive = robotActive;
	}
	public boolean isInstagramConnected() {
		return instagramConnected;
	}
	public void setInstagramConnected(boolean instagramConnected) {
		this.instagramConnected = instagramConnected;
	}
	public Proxy getProxy() {
		return proxy;
	}
	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}
}
