package jp.co.rilarc.instagram_robot.model;

/**
 * Created by nguyenthanhson on 10/28/17.
 */
public class ScheduleComment extends ScheduleBase {
    public static final int STATUS_WAITING = 0;
    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_FINISH = 2;
    public static final int STATUS_CANCEL = 3;

    private String mediaId;
    private String commentContent;
    private String mediaCode;

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getMediaCode() {
        return mediaCode;
    }

    public void setMediaCode(String mediaCode) {
        this.mediaCode = mediaCode;
    }

	@Override
	public String toString() {
		return "ScheduleComment [mediaId=" + mediaId + ", commentContent=(not display), delay=" + getDelay()
				+ ", mediaCode=" + mediaCode + ", "
				+ super.toString() + "]";
	}
    
}
