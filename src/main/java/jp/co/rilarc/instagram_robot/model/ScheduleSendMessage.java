package jp.co.rilarc.instagram_robot.model;

/**
 * Created by nguyenthanhson on 10/31/17.
 */
public class ScheduleSendMessage extends ScheduleBase {
    public static final int STATUS_WAITING = 0;
    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_FINISH = 2;
    public static final int STATUS_CANCEL = 3;

    private String instagramUserId;
    private String message;
    private String instagramName;

    public String getInstagramUserId() {
        return instagramUserId;
    }

    public void setInstagramUserId(String instagramUserId) {
        this.instagramUserId = instagramUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInstagramName() {
        return instagramName;
    }

    public void setInstagramName(String instagramName) {
        this.instagramName = instagramName;
    }

	@Override
	public String toString() {
//		return "ScheduleSendMessage [instagramUserId=" + instagramUserId + ", message=(not display), instagramName="
//				+ instagramName + ", "
//				+ super.toString() + "]";
		return "[" + super.toString() + "]";
	}
}
