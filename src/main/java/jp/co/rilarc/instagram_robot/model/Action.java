/**
 * 
 */
package jp.co.rilarc.instagram_robot.model;

/**
 * @author nghilt
 *
 */
public enum Action {
	LIKE,
	FOLLOW,
	UNFOLLOW,
    UPLOAD_PHOTO,
    SEND_COMMENT,
    SEND_MESSAGE
}
