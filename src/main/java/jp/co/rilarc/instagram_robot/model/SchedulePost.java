package jp.co.rilarc.instagram_robot.model;

import java.sql.Timestamp;

/**
 * Created by nguyenthanhson on 10/27/17.
 */
public class SchedulePost extends ScheduleBase {
    public static final int STATUS_WAITING = 0;
    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_FINISH = 2;
    public static final int STATUS_CANCEL = 3;

    private String imagePath;
    private String caption;
    private String location;
    private boolean deleteSchedule;
    private int repeatTime;
    private boolean postLoop;
    private Timestamp dayEndSchedule;
    private String linkAfterPost;
    private Timestamp lastestPost;
    private Timestamp nextTimeUpload;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isDeleteSchedule() {
        return deleteSchedule;
    }

    public void setDeleteSchedule(boolean deleteSchedule) {
        this.deleteSchedule = deleteSchedule;
    }

    public Timestamp getDayEndSchedule() {
        return dayEndSchedule;
    }

    public void setDayEndSchedule(Timestamp dayEndSchedule) {
        this.dayEndSchedule = dayEndSchedule;
    }

    public String getLinkAfterPost() {
        return linkAfterPost;
    }

    public void setLinkAfterPost(String linkAfterPost) {
        this.linkAfterPost = linkAfterPost;
    }

    public Timestamp getLastestPost() {
        return lastestPost;
    }

    public void setLastestPost(Timestamp lastestPost) {
        this.lastestPost = lastestPost;
    }

    public boolean isPostLoop() {
        return postLoop;
    }

    public void setPostLoop(boolean postLoop) {
        this.postLoop = postLoop;
    }

    public int getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(int repeatTime) {
        this.repeatTime = repeatTime;
    }

    public Timestamp getNextTimeUpload() {
        return nextTimeUpload;
    }

    public void setNextTimeUpload(Timestamp nextTimeUpload) {
        this.nextTimeUpload = nextTimeUpload;
    }

	@Override
	public String toString() {
		return "SchedulePost [imagePath=" + imagePath + ", caption=(not display), location=" + location
				+ ", deleteSchedule=" + deleteSchedule + ", repeatTime=" + repeatTime + ", postLoop=" + postLoop
				+ ", dayEndSchedule=" + dayEndSchedule + ", linkAfterPost=" + linkAfterPost + ", lastestPost="
				+ lastestPost + ", nextTimeUpload=" + nextTimeUpload + ", "
				+ super.toString() + "]";
	}
}
