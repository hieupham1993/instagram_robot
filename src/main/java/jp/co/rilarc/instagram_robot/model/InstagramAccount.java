package jp.co.rilarc.instagram_robot.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jp.co.rilarc.instagram_robot.utils.Const;

import java.sql.Timestamp;

public class InstagramAccount {
	@JsonProperty("id")
	private int id;
    
	@JsonProperty("user_id")
	private int userId;

    @JsonProperty("username")
	private String username;
    
    @JsonProperty("instagram_avatar")
	private String instagramAvatar;

    @JsonProperty("password")
	private String password;

    @JsonProperty("device_id")
	private String deviceId;

    @JsonProperty("phone_id")
	private String phoneId;

    @JsonProperty("csrf_token")
	private String csrfToken;

    @JsonProperty("uuid")
	private String uuid;

    @JsonProperty("user_agent")
	private String userAgent;

    @JsonProperty("device_string")
	private String deviceString;

    @JsonProperty("account_id")
	private String accountId;

    @JsonProperty("last_time_login")
	private Long lastTimeLogin;

    @JsonProperty("rank_token")
	private String rankToken;

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInstagramAvatar() {
		return instagramAvatar;
	}

	public void setInstagramAvatar(String instagramAvatar) {
		this.instagramAvatar = instagramAvatar;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public String getCsrfToken() {
		return csrfToken;
	}

	public void setCsrfToken(String csrfToken) {
		this.csrfToken = csrfToken;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getDeviceString() {
		return deviceString;
	}

	public void setDeviceString(String deviceString) {
		this.deviceString = deviceString;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Long getLastTimeLogin() {
		return lastTimeLogin;
	}

	public void setLastTimeLogin(Long lastTimeLogin) {
		this.lastTimeLogin = lastTimeLogin;
	}

	public boolean isRefreshLogin()
	{
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long currentTimestamp = timestamp.getTime()%1000;

		return (this.lastTimeLogin == null ||
				(currentTimestamp - this.lastTimeLogin) >= Const.APP_REFRESH_INTERVAL);
	}

	public String getRankToken() {
		return rankToken;
	}

	public void setRankToken(String rankToken) {
		this.rankToken = rankToken;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
