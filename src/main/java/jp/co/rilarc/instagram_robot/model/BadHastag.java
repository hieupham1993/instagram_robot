package jp.co.rilarc.instagram_robot.model;

import jp.co.rilarc.instagram_robot.utils.Const;

import java.sql.Timestamp;

public class BadHastag {
    private int mId;
    private String mHashtag;
    private int mNotFoundTimes;
    private Long mMarkedTime;

    public BadHastag(int mId, String mHashtag, int mNotFoundTimes, Long mMarkedTime) {
        this.mId = mId;
        this.mHashtag = mHashtag;
        this.mNotFoundTimes = mNotFoundTimes;
        this.mMarkedTime = mMarkedTime;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getHashtag() {
        return mHashtag;
    }

    public void setHashtag(String mHashtag) {
        this.mHashtag = mHashtag;
    }

    public int getNotFoundTimes() {
        return mNotFoundTimes;
    }

    public void setNotFoundTimes(int mNotFoundTimes) {
        this.mNotFoundTimes = mNotFoundTimes;
    }

    public Long getMarkedTime() {
        return mMarkedTime;
    }

    public void setMarkedTime(Long mMarkedTime) {
        this.mMarkedTime = mMarkedTime;
    }

    public boolean isBlock()
    {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long currentTimestamp = timestamp.getTime()%1000;

        return ((currentTimestamp - mMarkedTime) >= Const.MARK_BAD_HASHTAG_TIME);
    }

}
