package jp.co.rilarc.instagram_robot.model;

/**
 * Created by nguyenthanhson on 10/17/17.
 */
public class Proxy {
    private int mId;
    private String mProxyAddress;
    private int mProxyPort;
    private String mProxyUsername;
    private String mProxyPassword;
    
    public Proxy() {
    	
    }
    
    public Proxy(String address, int port) {
    	mProxyAddress = address;
        mProxyPort = port;
    }
    
    public Proxy(String address, int port, String username, String password) {
    	this(address, port);
        mProxyUsername = username;
        mProxyPassword = password;
    }
    
    public boolean isValidProxy() {
    	// Tam thoi chi check mot so port thong dung
    	return mProxyAddress != null && !mProxyAddress.equals("") && (mProxyPort != 0 && mProxyPort != 80);
    }
    
    public boolean needAuth() {
    	return mProxyUsername != null && !mProxyUsername.equals("") && mProxyPassword != null && !mProxyPassword.equals("");
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getProxyAddress() {
        return mProxyAddress;
    }

    public void setProxyAddress(String mProxyAddress) {
        this.mProxyAddress = mProxyAddress;
    }

    public int getProxyPort() {
        return mProxyPort;
    }

    public void setProxyPort(int mProxyPort) {
        this.mProxyPort = mProxyPort;
    }

    public String getProxyUsername() {
        return mProxyUsername;
    }

    public void setProxyUsername(String mProxyUsername) {
        this.mProxyUsername = mProxyUsername;
    }

    public String getProxyPassword() {
        return mProxyPassword;
    }

    public void setProxyPassword(String mProxyPassword) {
        this.mProxyPassword = mProxyPassword;
    }

	@Override
	public String toString() {
		return "Proxy [mId=" + mId + ", mProxyAddress=" + mProxyAddress + "]";
	}

}
