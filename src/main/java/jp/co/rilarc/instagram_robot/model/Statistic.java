package jp.co.rilarc.instagram_robot.model;

public class Statistic {
    private int mId;
    private int mUserId;
    private int mNumLikes;
    private int mNumFollowings;
    private int mNumFollowers;
    private int mNumUnfollows;
    private String mLastLikeMethod;
    private String mLastFollowMethod;
    private String mDate;

    public static final String LIKE_METHOD_HASHTAG = "hashtag";
    public static final String LIKE_METHOD_LOCATION = "location";
    public static final String LIKE_METHOD_FOLLOWING = "following";

    public static final String FOLLOW_METHOD_HASHTAG = "hashtag";
    public static final String FOLLOW_METHOD_LOCATION = "location";
    public static final String FOLLOW_METHOD_FOLLOWER = "follower";
    public static final String FOLLOW_METHOD_RELATED_ACCOUNT = "related_account";

    public static final String UNFOLLOW_METHOD_NOT_FOLLOWING_BACK = "not_following_back";
    public static final String UNFOLLOW_METHOD_UNFOLLOW_BOOST = "boost";

    public Statistic() {
        resetStatitics();
    }

    public Statistic(int id, int userId, int numLikes, int numFollowings, int numFollowers, int numUnfollows, String date, String lastLikeMethod, String lastFollowMethod) {
        this.mId = id;
        this.mUserId = userId;
        this.mNumLikes = numLikes;
        this.mNumFollowings = numFollowings;
        this.mNumFollowers = numFollowers;
        this.mNumUnfollows = numUnfollows;
        this.mLastLikeMethod = lastLikeMethod;
        this.mLastFollowMethod = lastFollowMethod;
        this.mDate = date;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int mUserId) {
        this.mUserId = mUserId;
    }

    public int getNumLikes() {
        return mNumLikes;
    }

    public int getNumFollowings() {
        return mNumFollowings;
    }

    public int getNumUnfollows() {
        return mNumUnfollows;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getLastLikeMethod() {
        return mLastLikeMethod;
    }

    public void setLastLikeMethod(String mLastLikeMethod) {
        this.mLastLikeMethod = mLastLikeMethod;
    }

    public String getLastFollowMethod() {
        return mLastFollowMethod;
    }

    public void setLastFollowMethod(String mLastFollowMethod) {
        this.mLastFollowMethod = mLastFollowMethod;
    }

    public void increaseNumFollowing() {
        this.mNumFollowings++;
    }

    public void increaseNumLikes() {
        this.mNumLikes++;
    }

    public void increaseNumUnfollows() {
        this.mNumUnfollows++;
    }

    public void resetStatitics() {
        this.mNumLikes = 0;
        this.mNumFollowings = 0;
        this.mNumUnfollows = 0;
    }
}
