package jp.co.rilarc.instagram_robot.model;

public class AdvanceSetting {
    private String mConflict;
    private String mWhiteList;
    private String mBlackList;
    private boolean mMode;


    public AdvanceSetting(int mId, String mConflict, String mWhiteList, String mBlackList, boolean mMode) {
        this.mConflict = mConflict;
        this.mWhiteList = mWhiteList;
        this.mBlackList = mBlackList;
        this.mMode = mMode;
    }

    public String getConflict() {
        return mConflict;
    }

    public void setConflict(String mConflict) {
        this.mConflict = mConflict;
    }

    public String getWhiteList() {
        return mWhiteList;
    }

    public void setWhiteList(String mWhiteList) {
        this.mWhiteList = mWhiteList;
    }

    public String getBlackList() {
        return mBlackList;
    }

    public void setBlackList(String mBlackList) {
        this.mBlackList = mBlackList;
    }

    public boolean isActive() {
        return mMode;
    }

    public void setMode(boolean mMode) {
        this.mMode = mMode;
    }
}
