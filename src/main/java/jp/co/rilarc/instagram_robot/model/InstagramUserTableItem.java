package jp.co.rilarc.instagram_robot.model;

import javafx.beans.property.SimpleBooleanProperty;

/**
 * Created by HieuPT on 1/22/2018.
 */
public class InstagramUserTableItem {

    private SimpleBooleanProperty checkedProperty;

    private long accountId;

    private String avatarUrl;

    private String username;

    private String fullName;

    public InstagramUserTableItem() {
        checkedProperty = new SimpleBooleanProperty(false);
    }

    public boolean isChecked() {
        return checkedProperty.get();
    }

    public void setChecked(boolean checked) {
        checkedProperty.set(checked);
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public SimpleBooleanProperty checkedProperty() {
        return checkedProperty;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
}
