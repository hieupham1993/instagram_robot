package jp.co.rilarc.instagram_robot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/25/2018.
 */
public class InstagramUserInfo {

    @SerializedName("allow_contacts_sync")
    private boolean isAllowContactSync;

    @SerializedName("can_see_organic_insights")
    private boolean canSeeOrganicInsights;

    @SerializedName("country_code")
    private int countryCode;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("has_anonymous_profile_picture")
    private boolean hasAnonymousProfilePicture;

    @SerializedName("is_business")
    private boolean isBusiness;

    @SerializedName("is_private")
    private boolean isPrivate;

    @SerializedName("is_verified")
    private boolean isVerified;

    @SerializedName("national_number")
    private long nationalNumber;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("pk")
    private long pk;

    @SerializedName("profile_pic_id")
    private String profilePicId;

    @SerializedName("profile_pic_url")
    private String profilePicUrl;

    @SerializedName("show_insights_terms")
    private boolean isShowInsights;

    @SerializedName("username")
    private String username;

    public boolean isAllowContactSync() {
        return isAllowContactSync;
    }

    public boolean isCanSeeOrganicInsights() {
        return canSeeOrganicInsights;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isHasAnonymousProfilePicture() {
        return hasAnonymousProfilePicture;
    }

    public boolean isBusiness() {
        return isBusiness;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public long getNationalNumber() {
        return nationalNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public long getPk() {
        return pk;
    }

    public String getProfilePicId() {
        return profilePicId;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public boolean isShowInsights() {
        return isShowInsights;
    }

    public String getUsername() {
        return username;
    }
}
