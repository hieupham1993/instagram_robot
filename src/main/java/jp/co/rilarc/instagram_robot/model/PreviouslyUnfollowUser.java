package jp.co.rilarc.instagram_robot.model;

public class PreviouslyUnfollowUser {
    private int mId;
    private int mUserId;
    private String mUnfollowedUsers;


    public PreviouslyUnfollowUser(int mId, int mUserId, String mUnfollowedUsers) {
        this.mId = mId;
        this.mUserId = mUserId;
        this.mUnfollowedUsers = mUnfollowedUsers;
    }


    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        this.mUserId = userId;
    }

    public String getUnfollowedUsers() {
        return mUnfollowedUsers;
    }

    public void setUnfollowedUsers(String unfollowedUsers) {
        this.mUnfollowedUsers = unfollowedUsers;
    }

}
