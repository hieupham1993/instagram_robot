package jp.co.rilarc.instagram_robot.model;

public class RelatedAccount {
    private int mId;
    private String mInstagramId;
    private String mInstagramUsername;
    private boolean mStatus;

    public RelatedAccount(int mId, String mInstagramId, String mInstagramUsername, boolean mStatus) {
        this.mId = mId;
        this.mInstagramId = mInstagramId;
        this.mInstagramUsername = mInstagramUsername;
        this.mStatus = mStatus;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getInstagramId() {
        return mInstagramId;
    }

    public void setInstagramId(String mInstagramId) {
        this.mInstagramId = mInstagramId;
    }

    public String getInstagramUsername() {
        return mInstagramUsername;
    }

    public void setInstagramUsername(String mInstagramUsername) {
        this.mInstagramUsername = mInstagramUsername;
    }

    public boolean isRunning() {
        return mStatus;
    }

    public void setStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }
}
