package jp.co.rilarc.instagram_robot.model;

public class LikeLocation {
    private int mId;

    private int mUserId;

    private String mLocationId;

    private String mLocationName;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        this.mUserId = userId;
    }

    public String getLocationId() {
        return mLocationId;
    }

    public void setLocationId(String location) {
        this.mLocationId = location;
    }

    public String getLocationName() {
        return mLocationName;
    }

    public void setLocationName(String mLocationName) {
        this.mLocationName = mLocationName;
    }
}
