package jp.co.rilarc.instagram_robot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/25/2018.
 */
public class InstagramLoginResponse {

    @SerializedName("logged_in_user")
    private InstagramUserInfo userInfo;

    @SerializedName("status")
    private String status;

    public InstagramUserInfo getUserInfo() {
        return userInfo;
    }

    public String getStatus() {
        return status;
    }
}
