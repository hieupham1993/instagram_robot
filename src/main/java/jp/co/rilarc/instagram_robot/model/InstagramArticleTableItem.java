package jp.co.rilarc.instagram_robot.model;

import javafx.beans.property.SimpleBooleanProperty;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramMediaOrAd;

/**
 * Created by HieuPT on 1/22/2018.
 */
public class InstagramArticleTableItem {

    private static final String POST_TYPE_PIC_TEXT = "写真";

    private static final String POST_TYPE_VIDEO_TEXT = "ビデオ";

    private SimpleBooleanProperty checkedProperty;

    private int postType;

    private String userProfilePicUrl;

    private String postPicUrl;

    private String username;

    private String caption;

    private int likeCount;

    private int commentCount;

    private String postId;

    private long pk;

    public InstagramArticleTableItem() {
        checkedProperty = new SimpleBooleanProperty(false);
    }

    public boolean isChecked() {
        return checkedProperty.get();
    }

    public void setChecked(boolean checked) {
        checkedProperty.set(checked);
    }

    public SimpleBooleanProperty checkedProperty() {
        return checkedProperty;
    }

    public int getPostType() {
        return postType;
    }

    public void setPostType(int postType) {
        this.postType = postType;
    }

    public String getPostTypeText() {
        switch (postType) {
            case InstagramMediaOrAd.MEDIA_TYPE_VIDEO:
                return POST_TYPE_VIDEO_TEXT;
            default:
                return POST_TYPE_PIC_TEXT;
        }
    }

    public String getPostPicUrl() {
        return postPicUrl;
    }

    public void setPostPicUrl(String postPicUrl) {
        this.postPicUrl = postPicUrl;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public long getPk() {
        return pk;
    }

    public void setPk(long pk) {
        this.pk = pk;
    }

    public String getUserProfilePicUrl() {
        return userProfilePicUrl;
    }

    public void setUserProfilePicUrl(String userProfilePicUrl) {
        this.userProfilePicUrl = userProfilePicUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
