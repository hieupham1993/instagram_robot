package jp.co.rilarc.instagram_robot.model;

import java.sql.Timestamp;

/**
 * Created by nguyenthanhson on 10/31/17.
 */
public class ScheduleBase {
    public static final int STATUS_WAITING = 0;
    public static final int STATUS_RUNNING = 1;
    public static final int STATUS_FINISH = 2;
    public static final int STATUS_CANCEL = 3;

    private int id;
    private int userId;
    private Timestamp timeToAction;
    private int delay;// second
    private int status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Timestamp getTimeToAction() {
		return timeToAction;
	}
	public void setTimeToAction(Timestamp timeToAction) {
		this.timeToAction = timeToAction;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	@Override
	public String toString() {
		return String.format("id=%s, userId=%s, timeToAction=%s, delay=%s, status=%s", id, userId,
				timeToAction, delay, status);
	}
}
