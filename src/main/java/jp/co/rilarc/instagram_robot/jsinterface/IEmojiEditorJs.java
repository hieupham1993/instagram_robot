package jp.co.rilarc.instagram_robot.jsinterface;

/**
 * Created by HieuPT on 1/8/2018.
 */
public interface IEmojiEditorJs {

    void onTextChanged(String text);
}
