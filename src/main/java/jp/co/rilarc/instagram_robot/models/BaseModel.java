package jp.co.rilarc.instagram_robot.models;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.concurrent.Task;

abstract class BaseModel {

    protected interface IWorkerTask<T> {

        T doWork();

        void onFinish(T result);
    }

    protected <T> void runOnWorkerThread(IWorkerTask<T> action) {
        WorkerTask<T> workerTask = new WorkerTask<>(action);
        workerTask.execute();
    }

    private static class WorkerTask<Result> extends Task<Result> {

        private final IWorkerTask<Result> action;

        private ExecutorService executor;

        WorkerTask(IWorkerTask<Result> action) {
            this.action = action;
            this.executor = Executors.newSingleThreadExecutor();
            setOnSucceeded(event -> action.onFinish(WorkerTask.this.getValue()));
        }

        @Override
        protected Result call() throws Exception {
            if (action != null) {
                return action.doWork();
            }
            return null;
        }

        final void execute() {
            executor.submit(this);
        }
    }

}
