package jp.co.rilarc.instagram_robot.models;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.Cookie;

import java.util.Set;

import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 1/3/2018.
 */
public class PreferencesModel extends BaseModel {

    public interface ICheckAutoLoginResponse {

        void onEnabled(String email, String password);

        void onDisabled();
    }

    public interface ICheckAutoStartResponse {

        void onEnabled();

        void onDisabled();
    }

    public interface IGetLoginInformationResponse {

        void onExist(String email, String password);
    }

    public interface IGetAccessTokenResponse {

        void onSuccess(String token);

        void onFailure();
    }

    public interface ICheckCookiesResponse {

        void onExist(Set<Cookie> cookies);

        void onEmpty();
    }

    public interface IGetInstagramDbResponse {

        void onSuccess(InstagramDb instagramDb);

        void onFailure();
    }

    public interface IGetAccountInformationResponse {

        void onSuccess(String systemEmail, String instagramUsername, String avatarUrl);
    }

    private static final String TAG = PreferencesModel.class.getSimpleName();

    private final LocalDatabaseHelper databaseHelper;

    public PreferencesModel() {
        databaseHelper = LocalDatabaseHelper.getInstance();
    }

    public void checkAutoLogin(ICheckAutoLoginResponse callback) {
        if (callback != null) {
            Preference preference = databaseHelper.getPreference();
            if (preference.isAutoLogin()) {
                callback.onEnabled(preference.getEmail(), preference.getPassword());
            } else {
                callback.onDisabled();
            }
        }
    }

    public void checkAutoStart(ICheckAutoStartResponse callback) {
        if (callback != null) {
            Preference preference = databaseHelper.getPreference();
            if (preference.isAutoStart()) {
                callback.onEnabled();
            } else {
                callback.onDisabled();
            }
        }
    }

    public void getLoginInformation(IGetLoginInformationResponse callback) {
        if (callback != null) {
            Preference preference = databaseHelper.getPreference();
            if (!TextUtils.isEmpty(preference.getEmail())) {
                callback.onExist(preference.getEmail(), preference.getPassword());
            }
        }
    }

    public void updateAutoLogin(boolean enable) {
        Preference preference = databaseHelper.getPreference();
        preference.setAutoLogin(enable);
        databaseHelper.updatePreference(preference);
    }

    public void updateAutoStart(boolean enable) {
        Preference preference = databaseHelper.getPreference();
        preference.setAutoStart(enable);
        databaseHelper.updatePreference(preference);
    }

    public void getAccessToken(IGetAccessTokenResponse callback) {
        String token = databaseHelper.getPreference().getAccessToken();
        if (!TextUtils.isEmpty(token)) {
            token = token.replace("Bearer ", Const.EMPTY_STRING);
            callback.onSuccess(token);
        } else {
            callback.onFailure();
        }
    }

    public void checkCookies(int userId, String username, ICheckCookiesResponse callback) {
        if (callback != null) {
            InstagramDb instagramDb = databaseHelper.getInstagram(userId, username);
            Set<Cookie> cookies = instagramDb.getCookies();
            if (cookies != null && !cookies.isEmpty()) {
                callback.onExist(cookies);
            } else {
                callback.onEmpty();
            }
        }
    }

    public void updatePreference(String email, String password, int userId, String instagramUsername,
                                 String accessToken, String tokenType, long expireTime) {
        Preference preference = databaseHelper.getPreference();
        updatePreference(email, password, userId, instagramUsername, preference.isAutoLogin(), preference.isAutoStart(), accessToken, tokenType, expireTime);
    }

    public void updatePreference(String email, String password, int userId, String instagramUsername,
                                 boolean isAutoLogin, boolean isAutoStart, String accessToken, String tokenType, long expireTime) {
        Preference preference = databaseHelper.getPreference();
        preference.setEmail(email);
        preference.setPassword(password);
        preference.setUserId(userId);
        preference.setInstagramUsername(instagramUsername);
        preference.setAccessToken(accessToken);
        preference.setTokenType(tokenType);
        preference.setTokenExpireTime(expireTime);
        preference.setAutoLogin(isAutoLogin);
        preference.setAutoStart(isAutoStart);
        databaseHelper.updatePreference(preference);
    }

    public void updateInstagram(int userId, String username, String password, long instagramUserId, String avatarUrl) {
        InstagramDb instagramDb = databaseHelper.getInstagram(userId, username);
        if (!instagramDb.getUsername().equals(username)
                || !instagramDb.getPassword().equals(password)) {
            instagramDb.setCookies(null);
        }
        instagramDb.setUserId(userId);
        instagramDb.setUsername(username);
        instagramDb.setInstagramUserId(instagramUserId);
        instagramDb.setPassword(password);
        instagramDb.setAvatarUrl(avatarUrl);
        databaseHelper.updateInstagram(instagramDb);
    }

    public void getInstagramDb(long userId, String username, IGetInstagramDbResponse callback) {
        if (callback != null) {
            InstagramDb instagramDb = databaseHelper.getInstagram(userId, username);
            if (!TextUtils.isEmpty(instagramDb.getUsername())) {
                callback.onSuccess(instagramDb);
            } else {
                callback.onFailure();
            }
        }
    }

    public void getAccountInformation(IGetAccountInformationResponse callback) {
        if (callback != null) {
            Preference preference = databaseHelper.getPreference();
            if (!TextUtils.isEmpty(preference.getEmail())) {
                InstagramDb instagramDb = databaseHelper.getInstagram(preference.getUserId(), preference.getInstagramUsername());
                if (!TextUtils.isEmpty(instagramDb.getUsername())) {
                    callback.onSuccess(preference.getEmail(), instagramDb.getUsername(), instagramDb.getAvatarUrl());
                }
            }
        }
    }
}
