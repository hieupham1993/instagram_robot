package jp.co.rilarc.instagram_robot.models;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import jp.co.rilarc.instagram_robot.action.UserPool;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramFeedItem;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramFollowListResponse;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramHashtagFeedResponse;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramMediaOrAd;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramPopularFeedResponse;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramSelfUserFeedResponse;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramTimelineFeedResponse;
import jp.co.rilarc.instagram_robot.jsonobject.InstagramUserData;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.InstagramArticleTableItem;
import jp.co.rilarc.instagram_robot.model.InstagramUserTableItem;

/**
 * Created by HieuPT on 1/22/2018.
 */
public class InstagramDataModel extends BaseModel {

    public interface IGetFollowListCallback {

        void onSuccess(List<InstagramUserTableItem> items, String maxId);

        void onFailure();
    }

    public interface ICheckInstagramConnectionCallback {

        void onConnected();

        void onDisconnected();
    }

    public interface IGetArticlesCallback {

        void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String maxId);

        void onFailure();
    }

    public interface IValidateHashtagCallback {

        void onValid(String hashtag);

        void onInvalid(String message);
    }

    private static final Logger LOGGER = LogManager.getLogger(InstagramDataModel.class);

    private LocalDatabaseHelper databaseHelper;

    private Gson gson;

    public InstagramDataModel() {
        databaseHelper = LocalDatabaseHelper.getInstance();
        gson = new Gson();
    }

    public void checkInstagramConnection(ICheckInstagramConnectionCallback callback) {
        Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
        if (instagram != null) {
            callback.onConnected();
        } else {
            callback.onDisconnected();
        }
    }

    public void getFollowingList(String maxId, IGetFollowListCallback callback) {
        runOnWorkerThread(new IWorkerTask<InstagramFollowListResponse>() {

            @Override
            public InstagramFollowListResponse doWork() {
                Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
                if (instagram != null) {
                    InstagramAccount instagramAccount = instagram.getInstagramAccount();
                    if (instagramAccount != null) {
                        String userId = instagramAccount.getAccountId();
                        if (!TextUtils.isEmpty(userId)) {
                            try {
                                HttpResponse httpResponse = instagram.getUserFollowings(userId, maxId);
                                String responseAsString = EntityUtils.toString(httpResponse.getEntity());
                                if (httpResponse.getStatusLine().getStatusCode() == 200) {
                                    InstagramFollowListResponse responseObject = gson.fromJson(responseAsString, InstagramFollowListResponse.class);
                                    if (responseObject != null && responseObject.isOk()) {
                                        return responseObject;
                                    }
                                } else {
                                    LOGGER.debug("getUserFollowings response: " + responseAsString);
                                }
                            } catch (IOException | URISyntaxException e) {
                                LOGGER.warn("Get user following fail", e);
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            public void onFinish(InstagramFollowListResponse result) {
                if (result != null) {
                    List<InstagramUserData> users = result.getUsers();
                    if (users != null && !users.isEmpty()) {
                        List<InstagramUserTableItem> items = users.stream().filter(Objects::nonNull).map(instagramFollowUserData -> {
                            InstagramUserTableItem item = new InstagramUserTableItem();
                            item.setAvatarUrl(instagramFollowUserData.getProfilePicUrl());
                            item.setUsername(instagramFollowUserData.getUsername());
                            item.setAccountId(instagramFollowUserData.getPk());
                            item.setFullName(instagramFollowUserData.getFullName());
                            return item;
                        }).collect(Collectors.toList());
                        callback.onSuccess(items, result.getNextMaxId());
                        return;
                    }
                }
                callback.onFailure();
            }
        });
    }

    public void getFollowerList(String maxId, IGetFollowListCallback callback) {
        runOnWorkerThread(new IWorkerTask<InstagramFollowListResponse>() {

            @Override
            public InstagramFollowListResponse doWork() {
                Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
                if (instagram != null) {
                    try {
                        HttpResponse httpResponse = instagram.getSelfUserFollowers(maxId);
                        String responseAsString = EntityUtils.toString(httpResponse.getEntity());
                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                            InstagramFollowListResponse responseObject = gson.fromJson(responseAsString, InstagramFollowListResponse.class);
                            if (responseObject != null && responseObject.isOk()) {
                                return responseObject;
                            }
                        } else {
                            LOGGER.debug("getUserFollowers response: " + responseAsString);
                        }
                    } catch (IOException | URISyntaxException e) {
                        LOGGER.warn("Get user follower fail", e);
                    }
                }
                return null;
            }

            @Override
            public void onFinish(InstagramFollowListResponse result) {
                if (result != null) {
                    List<InstagramUserData> users = result.getUsers();
                    if (users != null && !users.isEmpty()) {
                        List<InstagramUserTableItem> items = users.stream().filter(Objects::nonNull).map(instagramFollowUserData -> {
                            InstagramUserTableItem item = new InstagramUserTableItem();
                            item.setAvatarUrl(instagramFollowUserData.getProfilePicUrl());
                            item.setUsername(instagramFollowUserData.getUsername());
                            item.setAccountId(instagramFollowUserData.getPk());
                            item.setFullName(instagramFollowUserData.getFullName());
                            return item;
                        }).collect(Collectors.toList());
                        callback.onSuccess(items, result.getNextMaxId());
                        return;
                    }
                }
                callback.onFailure();
            }
        });
    }

    public void getTimelineArticles(String maxId, IGetArticlesCallback callback) {
        runOnWorkerThread(new IWorkerTask<InstagramTimelineFeedResponse>() {

            @Override
            public InstagramTimelineFeedResponse doWork() {
                Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
                if (instagram != null) {
                    try {
                        HttpResponse httpResponse = instagram.getTimelineFeed(maxId);
                        String responseAsString = EntityUtils.toString(httpResponse.getEntity());
                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                            InstagramTimelineFeedResponse responseObject = gson.fromJson(responseAsString, InstagramTimelineFeedResponse.class);
                            if (responseObject != null && responseObject.isOk()) {
                                return responseObject;
                            }
                        } else {
                            LOGGER.debug("timeline response: " + responseAsString);
                        }
                    } catch (IOException | URISyntaxException e) {
                        LOGGER.warn("Get timeline article fail", e);
                    }
                }
                return null;
            }

            @Override
            public void onFinish(InstagramTimelineFeedResponse result) {
                if (result != null) {
                    List<InstagramFeedItem> feedItems = result.getFeedItems();
                    if (feedItems != null && !feedItems.isEmpty()) {
                        List<InstagramArticleTableItem> items = feedItems.stream().filter(Objects::nonNull).map(feed -> {
                            InstagramArticleTableItem item = null;
                            InstagramMediaOrAd mediaOrAd = feed.getMediaOrAd();
                            if (mediaOrAd != null) {
                                item = new InstagramArticleTableItem();
                                item.setCaption(mediaOrAd.getCaptionText());
                                item.setLikeCount(mediaOrAd.getLikeCount());
                                item.setCommentCount(mediaOrAd.getCommentCount());
                                item.setPostId(mediaOrAd.getCode());
                                item.setPostType(mediaOrAd.getMediaType());
                                item.setPostPicUrl(mediaOrAd.getThumbnailUrl());
                                item.setPk(mediaOrAd.getPk());
                                item.setUsername(mediaOrAd.getUsername());
                                item.setUserProfilePicUrl(mediaOrAd.getUserProfilePicUrl());
                            }
                            return item;
                        }).filter(Objects::nonNull).collect(Collectors.toList());
                        callback.onSuccess(items, null, result.getNextMaxId());
                        return;
                    }
                }
                callback.onFailure();
            }
        });
    }

    public void getHastagArticles(String hashtag, String maxId, IGetArticlesCallback callback) {
        runOnWorkerThread(new IWorkerTask<InstagramHashtagFeedResponse>() {

            @Override
            public InstagramHashtagFeedResponse doWork() {
                Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
                if (instagram != null) {
                    try {
                        HttpResponse httpResponse = instagram.getHashtagFeed(hashtag, maxId);
                        String responseAsString = EntityUtils.toString(httpResponse.getEntity());
                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                            InstagramHashtagFeedResponse responseObject = gson.fromJson(responseAsString, InstagramHashtagFeedResponse.class);
                            if (responseObject != null && responseObject.isOk()) {
                                return responseObject;
                            }
                        } else {
                            LOGGER.debug("search hashtag response: " + responseAsString);
                        }
                    } catch (IOException | URISyntaxException e) {
                        LOGGER.warn("Get hashtag article fail", e);
                    }
                }
                return null;
            }

            @Override
            public void onFinish(InstagramHashtagFeedResponse result) {
                if (result != null) {
                    List<InstagramMediaOrAd> feedItems = result.getItems();
                    if (feedItems != null && !feedItems.isEmpty()) {
                        List<InstagramArticleTableItem> items = feedItems.stream().filter(Objects::nonNull).map(feed -> {
                            InstagramArticleTableItem item = new InstagramArticleTableItem();
                            item.setCaption(feed.getCaptionText());
                            item.setLikeCount(feed.getLikeCount());
                            item.setCommentCount(feed.getCommentCount());
                            item.setPostId(feed.getCode());
                            item.setPostType(feed.getMediaType());
                            item.setPostPicUrl(feed.getThumbnailUrl());
                            item.setPk(feed.getPk());
                            item.setUsername(feed.getUsername());
                            item.setUserProfilePicUrl(feed.getUserProfilePicUrl());
                            return item;
                        }).collect(Collectors.toList());
                        callback.onSuccess(items, hashtag, result.getNextMaxId());
                        return;
                    }
                }
                callback.onFailure();
            }
        });
    }

    public void getSelfArticles(String maxId, IGetArticlesCallback callback) {
        runOnWorkerThread(new IWorkerTask<InstagramSelfUserFeedResponse>() {

            @Override
            public InstagramSelfUserFeedResponse doWork() {
                Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
                if (instagram != null) {
                    try {
                        HttpResponse httpResponse = instagram.getSelfUserFeed(maxId, null);
                        String responseAsString = EntityUtils.toString(httpResponse.getEntity());
                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                            InstagramSelfUserFeedResponse responseObject = gson.fromJson(responseAsString, InstagramSelfUserFeedResponse.class);
                            if (responseObject != null && responseObject.isOk()) {
                                return responseObject;
                            }
                        } else {
                            LOGGER.debug("self articles response: " + responseAsString);
                        }
                    } catch (IOException | URISyntaxException e) {
                        LOGGER.warn("Get self article fail", e);
                    }
                }
                return null;
            }

            @Override
            public void onFinish(InstagramSelfUserFeedResponse result) {
                if (result != null) {
                    List<InstagramMediaOrAd> feedItems = result.getItems();
                    if (feedItems != null && !feedItems.isEmpty()) {
                        List<InstagramArticleTableItem> items = feedItems.stream().filter(Objects::nonNull).map(feed -> {
                            InstagramArticleTableItem item = new InstagramArticleTableItem();
                            item.setCaption(feed.getCaptionText());
                            item.setLikeCount(feed.getLikeCount());
                            item.setCommentCount(feed.getCommentCount());
                            item.setPostId(feed.getCode());
                            item.setPostType(feed.getMediaType());
                            item.setPostPicUrl(feed.getThumbnailUrl());
                            item.setPk(feed.getPk());
                            item.setUsername(feed.getUsername());
                            item.setUserProfilePicUrl(feed.getUserProfilePicUrl());
                            return item;
                        }).collect(Collectors.toList());
                        callback.onSuccess(items, null, result.getNextMaxId());
                        return;
                    }
                }
                callback.onFailure();
            }
        });
    }

    public void getPopularArticles(IGetArticlesCallback callback) {
        runOnWorkerThread(new IWorkerTask<InstagramPopularFeedResponse>() {

            @Override
            public InstagramPopularFeedResponse doWork() {
                Instagram instagram = UserPool.getCachedInstagram(databaseHelper.getPreference().getUserId());
                if (instagram != null) {
                    try {
                        HttpResponse httpResponse = instagram.getPopularFeed();
                        String responseAsString = EntityUtils.toString(httpResponse.getEntity());
                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                            InstagramPopularFeedResponse responseObject = gson.fromJson(responseAsString, InstagramPopularFeedResponse.class);
                            if (responseObject != null && responseObject.isOk()) {
                                return responseObject;
                            }
                        } else {
                            LOGGER.debug("popular articles response: " + responseAsString);
                        }
                    } catch (IOException | URISyntaxException e) {
                        LOGGER.warn("Get popular article fail", e);
                    }
                }
                return null;
            }

            @Override
            public void onFinish(InstagramPopularFeedResponse result) {
                if (result != null) {
                    List<InstagramMediaOrAd> feedItems = result.getItems();
                    if (feedItems != null && !feedItems.isEmpty()) {
                        List<InstagramArticleTableItem> items = feedItems.stream().filter(Objects::nonNull).map(feed -> {
                            InstagramArticleTableItem item = new InstagramArticleTableItem();
                            item.setCaption(feed.getCaptionText());
                            item.setLikeCount(feed.getLikeCount());
                            item.setCommentCount(feed.getCommentCount());
                            item.setPostId(feed.getCode());
                            item.setPostType(feed.getMediaType());
                            item.setPostPicUrl(feed.getThumbnailUrl());
                            item.setPk(feed.getPk());
                            item.setUsername(feed.getUsername());
                            item.setUserProfilePicUrl(feed.getUserProfilePicUrl());
                            return item;
                        }).collect(Collectors.toList());
                        callback.onSuccess(items, null, null);
                        return;
                    }
                }
                callback.onFailure();
            }
        });
    }
}
