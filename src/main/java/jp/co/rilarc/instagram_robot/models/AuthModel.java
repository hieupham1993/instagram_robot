package jp.co.rilarc.instagram_robot.models;

import org.apache.http.util.TextUtils;

import jp.co.rilarc.instagram_robot.api.IWssjServices;
import jp.co.rilarc.instagram_robot.api.requests.WssjLoginRequest;
import jp.co.rilarc.instagram_robot.api.responses.WssjLoginResponse;
import jp.co.rilarc.instagram_robot.utils.RequestQueue;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class AuthModel extends BaseModel {

    private interface IFailureCallback {

        void onFailure(String message);
    }

    public interface ILoginCallback extends IFailureCallback {

        void onSuccess(WssjLoginResponse.Data response);
    }

    public interface IValidateLoginInfoCallback {

        void onValid(String email, String password);

        void onInvalid(String message);
    }

    private static final String TAG = AuthModel.class.getSimpleName();

    public void validateLoginInfo(String email, String password, IValidateLoginInfoCallback callback) {
        if (callback != null) {
            if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
                callback.onValid(email, password);
            } else {
                callback.onInvalid("Email and password must not be empty");
            }
        }
    }

    public void login(String email, String password, ILoginCallback callback) {
        RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.login(new WssjLoginRequest(email, password)), new Callback<WssjLoginResponse>() {

            @Override
            public void onResponse(Call<WssjLoginResponse> call, Response<WssjLoginResponse> response) {
                WssjLoginResponse responseBody = response.body();
                if (responseBody != null) {
                    if (responseBody.isSuccess()) {
                        WssjLoginResponse.Data responseData = responseBody.getData();
                        if (responseData != null) {
                            callback.onSuccess(responseData);
                        } else {
                            callback.onFailure(responseBody.getMessage());
                        }
                    } else {
                        callback.onFailure(responseBody.getMessage());
                    }
                } else {
                    callback.onFailure("メールアドレスまたはパスワードが間違っています。");
                }
            }

            @Override
            public void onFailure(Call<WssjLoginResponse> call, Throwable t) {
                callback.onFailure("メールアドレスまたはパスワードが間違っています。");
            }
        });
    }
}
