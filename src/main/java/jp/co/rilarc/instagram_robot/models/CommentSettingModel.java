package jp.co.rilarc.instagram_robot.models;

import org.apache.http.util.TextUtils;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.List;

import jp.co.rilarc.instagram_robot.api.IWssjServices;
import jp.co.rilarc.instagram_robot.api.requests.WssjAddCommentScheduleRequest;
import jp.co.rilarc.instagram_robot.api.responses.WssjBaseResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleCommentResponse;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.model.InstagramArticleTableItem;
import jp.co.rilarc.instagram_robot.utils.RequestQueue;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 1/19/2018.
 */
public class CommentSettingModel extends BaseModel {

    public interface IGetListScheduleCommentCallback {

        void onSuccess(List<WssjGetListScheduleCommentResponse.Comment> comments);

        void onEmpty();

        void onFailure();
    }

    public interface IDeleteAllScheduleCommentCallback {

        void onSuccess();

        void onFailure(String message);
    }

    public interface IValidateAddScheduleCommentRequestCallback {

        void onValid(List<InstagramArticleTableItem> items, String comment, long sendAt, int delay);

        void onInvalid(String message);
    }

    public interface IAddScheduleCommentCallback {

        void onSuccess();

        void onFailure(String message);
    }

    private final IWssjServices services;

    private final LocalDatabaseHelper databaseHelper;

    public CommentSettingModel() {
        services = IWssjServices.SERVICES;
        databaseHelper = LocalDatabaseHelper.getInstance();
    }

    public void getListScheduleComment(CommentSettingModel.IGetListScheduleCommentCallback callback) {
        Preference preference = databaseHelper.getPreference();
        RequestQueue.getInstance().addRequest(services.getListScheduleComment(preference.getAccessToken()), new Callback<WssjGetListScheduleCommentResponse>() {

            @Override
            public void onResponse(Call<WssjGetListScheduleCommentResponse> call, Response<WssjGetListScheduleCommentResponse> response) {
                WssjGetListScheduleCommentResponse commentResponse = response.body();
                if (commentResponse != null) {
                    WssjGetListScheduleCommentResponse.Data data = commentResponse.getData();
                    if (data != null) {
                        List<WssjGetListScheduleCommentResponse.Comment> comments = data.getComments();
                        if (comments != null) {
                            if (!comments.isEmpty()) {
                                callback.onSuccess(comments);
                            } else {
                                callback.onEmpty();
                            }
                            return;
                        }
                    }
                }
                callback.onFailure();
            }

            @Override
            public void onFailure(Call<WssjGetListScheduleCommentResponse> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    public void deleteAllScheduleComment(CommentSettingModel.IDeleteAllScheduleCommentCallback callback) {
        Preference preference = databaseHelper.getPreference();
        RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.deleteAllComment(preference.getAccessToken()), new Callback<WssjBaseResponse<Object>>() {

            @Override
            public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                WssjBaseResponse<Object> responseBody = response.body();
                if (responseBody != null) {
                    if (responseBody.isSuccess()) {
                        callback.onSuccess();
                    } else {
                        callback.onFailure(responseBody.getMessage());
                    }
                } else {
                    callback.onFailure("Something went wrong. Please try again later");
                }
            }

            @Override
            public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                callback.onFailure("Something went wrong. Please try again later");
            }
        });
    }

    public void validateAddScheduleRequestBody(List<InstagramArticleTableItem> items, String comment, long sendAt, int delay, CommentSettingModel.IValidateAddScheduleCommentRequestCallback callback) {
        if (TextUtils.isEmpty(comment)) {
            callback.onInvalid("メッセージを入力してください。");
        } else if (sendAt < 0) {
            callback.onInvalid("時間を入力してください。");
        } else if (sendAt < DateTime.now().withSecondOfMinute(0).withMillisOfSecond(0).getMillis()) {
            callback.onInvalid("将来の時間を選択してください。");
        } else if (items == null || items.isEmpty()) {
            callback.onInvalid("インスタグラムユーザーを選択してください。");
        } else {
            callback.onValid(items, comment, sendAt, delay);
        }
    }

    public void addScheduleComment(List<InstagramArticleTableItem> items, String comment, long sendAt, int delay, IAddScheduleCommentCallback callback) {
        WssjAddCommentScheduleRequest requestBody = new WssjAddCommentScheduleRequest();
        requestBody.setComment(comment);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        requestBody.setSendAt(dateFormat.format(DateTime.now().withMillis(sendAt).toDate()));
        requestBody.setDelay(delay);
        items.forEach(item -> {
            WssjAddCommentScheduleRequest.PostContent postContent = new WssjAddCommentScheduleRequest.PostContent();
            postContent.setPostCaption(item.getCaption());
            postContent.setPostId(item.getPk());
            postContent.setPostCode(item.getPostId());
            postContent.setPostPicUrl(item.getPostPicUrl());
            postContent.setUsername(item.getUsername());
            postContent.setUserProfilePic(item.getUserProfilePicUrl());
            requestBody.addInstagramUserId(postContent);
        });
        Preference preference = databaseHelper.getPreference();
        RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.addCommentSchedule(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

            @Override
            public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                WssjBaseResponse<Object> responseBody = response.body();
                if (responseBody != null) {
                    if (responseBody.isSuccess()) {
                        callback.onSuccess();
                    } else {
                        callback.onFailure(responseBody.getMessage());
                    }
                } else {
                    callback.onFailure("Something went wrong. Please try again later");
                }
            }

            @Override
            public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                callback.onFailure("Something went wrong. Please try again later");
            }
        });
    }
}
