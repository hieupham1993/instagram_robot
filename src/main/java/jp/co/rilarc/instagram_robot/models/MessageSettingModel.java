package jp.co.rilarc.instagram_robot.models;

import org.apache.http.util.TextUtils;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.List;

import jp.co.rilarc.instagram_robot.api.IWssjServices;
import jp.co.rilarc.instagram_robot.api.requests.WssjAddMessageScheduleRequest;
import jp.co.rilarc.instagram_robot.api.responses.WssjBaseResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.model.InstagramUserTableItem;
import jp.co.rilarc.instagram_robot.utils.RequestQueue;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 1/19/2018.
 */
public class MessageSettingModel extends BaseModel {

    public interface IGetListScheduleMessageCallback {

        void onSuccess(List<WssjGetListScheduleMessageResponse.Message> messages);

        void onEmpty();

        void onFailure();
    }

    public interface IValidateAddScheduleMessageRequestCallback {

        void onValid(List<InstagramUserTableItem> userItems, String message, long sendAt, int delay);

        void onInvalid(String message);
    }

    public interface IAddScheduleMessageCallback {

        void onSuccess();

        void onFailure(String message);
    }

    public interface IDeleteAllScheduleMessageCallback {

        void onSuccess();

        void onFailure(String message);
    }

    private final IWssjServices services;

    private final LocalDatabaseHelper databaseHelper;

    public MessageSettingModel() {
        services = IWssjServices.SERVICES;
        databaseHelper = LocalDatabaseHelper.getInstance();
    }

    public void getListScheduleMessage(IGetListScheduleMessageCallback callback) {
        Preference preference = databaseHelper.getPreference();
        RequestQueue.getInstance().addRequest(services.getListScheduleSendMessage(preference.getAccessToken()), new Callback<WssjGetListScheduleMessageResponse>() {

            @Override
            public void onResponse(Call<WssjGetListScheduleMessageResponse> call, Response<WssjGetListScheduleMessageResponse> response) {
                WssjGetListScheduleMessageResponse messageResponse = response.body();
                if (messageResponse != null) {
                    WssjGetListScheduleMessageResponse.Data data = messageResponse.getData();
                    if (data != null) {
                        List<WssjGetListScheduleMessageResponse.Message> messages = data.getMessages();
                        if (messages != null) {
                            if (!messages.isEmpty()) {
                                callback.onSuccess(messages);
                            } else {
                                callback.onEmpty();
                            }
                            return;
                        }
                    }
                }
                callback.onFailure();
            }

            @Override
            public void onFailure(Call<WssjGetListScheduleMessageResponse> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    public void validateAddScheduleRequestBody(List<InstagramUserTableItem> userItems, String message, long sendAt, int delay, IValidateAddScheduleMessageRequestCallback callback) {
        if (TextUtils.isEmpty(message)) {
            callback.onInvalid("メッセージを入力してください。");
        } else if (sendAt < 0) {
            callback.onInvalid("時間を入力してください。");
        } else if (sendAt < DateTime.now().withSecondOfMinute(0).withMillisOfSecond(0).getMillis()) {
            callback.onInvalid("将来の時間を選択してください。");
        } else if (userItems == null || userItems.isEmpty()) {
            callback.onInvalid("インスタグラムユーザーを選択してください。");
        } else {
            callback.onValid(userItems, message, sendAt, delay);
        }
    }

    public void addScheduleMessage(List<InstagramUserTableItem> userItems, String message, long sendAt, int delay, IAddScheduleMessageCallback callback) {
        WssjAddMessageScheduleRequest requestBody = new WssjAddMessageScheduleRequest();
        requestBody.setDescription(message);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        requestBody.setSendAt(dateFormat.format(DateTime.now().withMillis(sendAt).toDate()));
        requestBody.setDelay(delay);
        userItems.forEach(item -> requestBody.addInstagramUserId(String.valueOf(item.getAccountId()), item.getUsername()));
        Preference preference = databaseHelper.getPreference();
        RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.addMessageSchedule(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

            @Override
            public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                WssjBaseResponse<Object> responseBody = response.body();
                if (responseBody != null) {
                    if (responseBody.isSuccess()) {
                        callback.onSuccess();
                    } else {
                        callback.onFailure(responseBody.getMessage());
                    }
                } else {
                    callback.onFailure("Something went wrong. Please try again later");
                }
            }

            @Override
            public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                callback.onFailure("Something went wrong. Please try again later");
            }
        });
    }

    public void deleteAllScheduleMessage(IDeleteAllScheduleMessageCallback callback) {
        Preference preference = databaseHelper.getPreference();
        RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.deleteAllMessage(preference.getAccessToken()), new Callback<WssjBaseResponse<Object>>() {

            @Override
            public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                WssjBaseResponse<Object> responseBody = response.body();
                if (responseBody != null) {
                    if (responseBody.isSuccess()) {
                        callback.onSuccess();
                    } else {
                        callback.onFailure(responseBody.getMessage());
                    }
                } else {
                    callback.onFailure("Something went wrong. Please try again later");
                }
            }

            @Override
            public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                callback.onFailure("Something went wrong. Please try again later");
            }
        });
    }
}
