package jp.co.rilarc.instagram_robot.database.model;

import org.apache.http.util.TextUtils;

import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class Preference {

    private int userId;

    private String email;

    private String password;

    private String instagramUsername;

    private boolean isAutoLogin;

    private boolean isAutoStart;

    private String accessToken;

    private String tokenType;

    private long tokenExpireTime;

    public String getEmail() {
        return !TextUtils.isEmpty(email) ? email : Const.EMPTY_STRING;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return !TextUtils.isEmpty(password) ? password : Const.EMPTY_STRING;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAutoLogin() {
        return isAutoLogin;
    }

    public void setAutoLogin(boolean autoLogin) {
        isAutoLogin = autoLogin;
    }

    public boolean isAutoStart() {
        return isAutoStart;
    }

    public void setAutoStart(boolean autoStart) {
        isAutoStart = autoStart;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getTokenExpireTime() {
        return tokenExpireTime;
    }

    public void setTokenExpireTime(long tokenExpireTime) {
        this.tokenExpireTime = tokenExpireTime;
    }

    public void setInstagramUsername(String instagramUsername) {
        this.instagramUsername = instagramUsername;
    }

    public String getInstagramUsername() {
        return !TextUtils.isEmpty(instagramUsername) ? instagramUsername : Const.EMPTY_STRING;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
