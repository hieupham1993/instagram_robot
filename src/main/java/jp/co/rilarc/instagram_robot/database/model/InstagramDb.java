package jp.co.rilarc.instagram_robot.database.model;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.Cookie;

import java.util.Set;

import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 1/10/2018.
 */
public class InstagramDb {

    private int userId;

    private String username;

    private String password;

    private String avatarUrl;

    private long instagramUserId;

    private Set<Cookie> cookies;

    public String getUsername() {
        return !TextUtils.isEmpty(username) ? username : Const.EMPTY_STRING;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return !TextUtils.isEmpty(password) ? password : Const.EMPTY_STRING;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatarUrl() {
        return !TextUtils.isEmpty(avatarUrl) ? avatarUrl : Const.EMPTY_STRING;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Set<Cookie> getCookies() {
        return cookies;
    }

    public void setCookies(Set<Cookie> cookies) {
        this.cookies = cookies;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getInstagramUserId() {
        return instagramUserId;
    }

    public void setInstagramUserId(long instagramUserId) {
        this.instagramUserId = instagramUserId;
    }
}
