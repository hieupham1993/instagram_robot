package jp.co.rilarc.instagram_robot.database;

/**
 * Created by HieuPT on 11/13/2017.
 */
class DataContract {

    static class PreferenceEntry {

        public static final String TABLE_NAME = "Preference";

        public static final String COLUMN_EMAIL = "Email";

        public static final String COLUMN_PASSWORD = "Password";

        public static final String COLUMN_USER_ID = "UserId";

        public static final String COLUMN_INSTAGRAM_USERNAME = "InstagramUsername";

        public static final String COLUMN_AUTO_LOGIN = "AutoLogin";

        public static final String COLUMN_AUTO_START = "AutoStart";

        public static final String COLUMN_ACCESS_TOKEN = "AccessToken";

        public static final String COLUMN_TOKEN_TYPE = "TokenType";

        public static final String COLUMN_TOKEN_EXPIRE_TIME = "TokenExpireTime";

        private PreferenceEntry() {
            //no instance
        }
    }

    static class InstagramEntry {

        public static final String TABLE_NAME = "Instagram";

        public static final String COLUMN_USER_ID = "UserId";

        public static final String COLUMN_USERNAME = "Username";

        public static final String COLUMN_PASSWORD = "Password";

        public static final String COLUMN_AVATAR_URL = "AvatarUrl";

        public static final String COLUMN_INSTAGRAM_USER_ID = "InstagramUserId";

        public static final String COLUMN_COOKIES = "Cookies";

        private InstagramEntry() {
            //no instance
        }
    }

    private DataContract() {
        //no instance
    }
}
