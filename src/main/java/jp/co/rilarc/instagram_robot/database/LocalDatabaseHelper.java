package jp.co.rilarc.instagram_robot.database;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.Cookie;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LocalDatabaseHelper {

    private static final String TAG = LocalDatabaseHelper.class.getSimpleName();

    private static final String DIRECTORY = "data";

    private static final String DB_NAME = "data.db";

    private static LocalDatabaseHelper instance;

    public static LocalDatabaseHelper getInstance() {
        if (instance == null) {
            instance = new LocalDatabaseHelper();
        }
        return instance;
    }

    private Connection connection;

    private Gson gson;

    private LocalDatabaseHelper() {
        try {
            Class.forName("org.sqlite.JDBC");
            ensureDbDirectoryExist();
            connection = DriverManager.getConnection("jdbc:sqlite:" + DIRECTORY + "/" + DB_NAME);
            gson = new Gson();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void ensureDbDirectoryExist() {
        File dbDirectory = new File(DIRECTORY);
        if (!dbDirectory.exists()) {
            dbDirectory.mkdir();
        }
    }

    public void createTable() {
        try {
            Statement statement = connection.createStatement();
            createPreferenceTable(statement);
            createInstagramTable(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createPreferenceTable(Statement statement) {
        try {
            String sql = "CREATE TABLE IF NOT EXISTS " + DataContract.PreferenceEntry.TABLE_NAME
                    + "(" + DataContract.PreferenceEntry.COLUMN_AUTO_LOGIN + " INTEGER,"
                    + DataContract.PreferenceEntry.COLUMN_AUTO_START + " INTEGER,"
                    + DataContract.PreferenceEntry.COLUMN_EMAIL + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_PASSWORD + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_USER_ID + " INTEGER,"
                    + DataContract.PreferenceEntry.COLUMN_INSTAGRAM_USERNAME + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN + " TEXT,"
                    + DataContract.PreferenceEntry.COLUMN_TOKEN_TYPE + " VARCHAR,"
                    + DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME + " INTEGER"
                    + ")";
            statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createInstagramTable(Statement statement) {
        try {
            String sql = "CREATE TABLE IF NOT EXISTS " + DataContract.InstagramEntry.TABLE_NAME
                    + "(" + DataContract.InstagramEntry.COLUMN_USER_ID + " INTEGER NOT NULL,"
                    + DataContract.InstagramEntry.COLUMN_USERNAME + " VARCHAR NOT NULL,"
                    + DataContract.InstagramEntry.COLUMN_PASSWORD + " VARCHAR,"
                    + DataContract.InstagramEntry.COLUMN_INSTAGRAM_USER_ID + " INTEGER,"
                    + DataContract.InstagramEntry.COLUMN_AVATAR_URL + " VARCHAR,"
                    + DataContract.InstagramEntry.COLUMN_COOKIES + " TEXT,"
                    + "PRIMARY KEY ("
                    + DataContract.InstagramEntry.COLUMN_USER_ID + ", "
                    + DataContract.InstagramEntry.COLUMN_USERNAME
                    + ")"
                    + ")";
            statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePreference(Preference preference) {
        if (preference != null) {
            try {
                Statement statement = connection.createStatement();
                clearPreference();
                int autoLogin = preference.isAutoLogin() ? 1 : 0;
                int autoStart = preference.isAutoStart() ? 1 : 0;
                String sql = "INSERT OR IGNORE INTO " + DataContract.PreferenceEntry.TABLE_NAME
                        + " ("
                        + DataContract.PreferenceEntry.COLUMN_EMAIL + ","
                        + DataContract.PreferenceEntry.COLUMN_PASSWORD + ","
                        + DataContract.PreferenceEntry.COLUMN_USER_ID + ","
                        + DataContract.PreferenceEntry.COLUMN_INSTAGRAM_USERNAME + ","
                        + DataContract.PreferenceEntry.COLUMN_AUTO_LOGIN + ","
                        + DataContract.PreferenceEntry.COLUMN_AUTO_START + ","
                        + DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN + ","
                        + DataContract.PreferenceEntry.COLUMN_TOKEN_TYPE + ","
                        + DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME
                        + ")"
                        + " VALUES"
                        + " ("
                        + "'" + preference.getEmail() + "',"
                        + "'" + preference.getPassword() + "',"
                        + preference.getUserId() + ","
                        + "'" + preference.getInstagramUsername() + "',"
                        + autoLogin + ","
                        + autoStart + ","
                        + "'" + preference.getAccessToken() + "',"
                        + "'" + preference.getTokenType() + "',"
                        + preference.getTokenExpireTime()
                        + ")";
                statement.execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateAccessToken(String accessToken, long tokenExpireTime) {
        try {
            Statement statement = connection.createStatement();
            String sql = "UPDATE " + DataContract.PreferenceEntry.TABLE_NAME
                    + " SET "
                    + DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN + "=" + "'" + accessToken + "',"
                    + DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME + "=" + tokenExpireTime;
            statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Preference getPreference() {
        try {
            Statement statement = connection.createStatement();
            String sql = "SELECT *"
                    + " FROM " + DataContract.PreferenceEntry.TABLE_NAME;
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet != null && !resultSet.isClosed()) {
                Preference preference = new Preference();
                preference.setEmail(resultSet.getString(DataContract.PreferenceEntry.COLUMN_EMAIL));
                preference.setPassword(resultSet.getString(DataContract.PreferenceEntry.COLUMN_PASSWORD));
                preference.setUserId(resultSet.getInt(DataContract.PreferenceEntry.COLUMN_USER_ID));
                preference.setInstagramUsername(resultSet.getString(DataContract.PreferenceEntry.COLUMN_INSTAGRAM_USERNAME));
                preference.setAccessToken(resultSet.getString(DataContract.PreferenceEntry.COLUMN_ACCESS_TOKEN));
                preference.setAutoLogin(resultSet.getInt(DataContract.PreferenceEntry.COLUMN_AUTO_LOGIN) == 1);
                preference.setAutoStart(resultSet.getInt(DataContract.PreferenceEntry.COLUMN_AUTO_START) == 1);
                preference.setTokenType(resultSet.getString(DataContract.PreferenceEntry.COLUMN_TOKEN_TYPE));
                preference.setTokenExpireTime(resultSet.getLong(DataContract.PreferenceEntry.COLUMN_TOKEN_EXPIRE_TIME));
                resultSet.close();
                return preference;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new Preference();
    }

    public void updateInstagram(InstagramDb instagramDb) {
        if (instagramDb != null && !TextUtils.isEmpty(instagramDb.getUsername())) {
            Set<Cookie> cookies = instagramDb.getCookies();
            String jsonCookies = Const.EMPTY_STRING;
            if (cookies != null && !cookies.isEmpty()) {
                jsonCookies = gson.toJson(cookies);
            }
            try {
                Statement statement = connection.createStatement();
                String sql = "INSERT OR REPLACE INTO " + DataContract.InstagramEntry.TABLE_NAME
                        + " ("
                        + DataContract.InstagramEntry.COLUMN_USER_ID + ","
                        + DataContract.InstagramEntry.COLUMN_USERNAME + ","
                        + DataContract.InstagramEntry.COLUMN_PASSWORD + ","
                        + DataContract.InstagramEntry.COLUMN_INSTAGRAM_USER_ID + ","
                        + DataContract.InstagramEntry.COLUMN_AVATAR_URL + ","
                        + DataContract.InstagramEntry.COLUMN_COOKIES
                        + ")"
                        + " VALUES"
                        + " ("
                        + instagramDb.getUserId() + ","
                        + "'" + instagramDb.getUsername() + "',"
                        + "'" + instagramDb.getPassword() + "',"
                        + "'" + instagramDb.getInstagramUserId() + "',"
                        + "'" + instagramDb.getAvatarUrl() + "',"
                        + "'" + jsonCookies + "'"
                        + ")";
                statement.execute(sql);
            } catch (SQLException e) {
                System.err.println("SQLException: " + e.getMessage());
            }
        }
    }

    public InstagramDb getInstagram(long userId, String username) {
        InstagramDb instagramDb = new InstagramDb();
        if (!TextUtils.isEmpty(username)) {
            try {
                Statement statement = connection.createStatement();
                String sql = "SELECT *"
                        + " FROM " + DataContract.InstagramEntry.TABLE_NAME
                        + " WHERE ("
                        + DataContract.InstagramEntry.COLUMN_USERNAME + "=" + "'" + username + "'"
                        + " AND "
                        + DataContract.InstagramEntry.COLUMN_USER_ID + "=" + userId
                        + ")";
                ResultSet resultSet = statement.executeQuery(sql);
                if (resultSet != null && !resultSet.isClosed()) {
                    instagramDb.setUserId(resultSet.getInt(DataContract.InstagramEntry.COLUMN_USER_ID));
                    instagramDb.setUsername(resultSet.getString(DataContract.InstagramEntry.COLUMN_USERNAME));
                    instagramDb.setPassword(resultSet.getString(DataContract.InstagramEntry.COLUMN_PASSWORD));
                    instagramDb.setInstagramUserId(resultSet.getLong(DataContract.InstagramEntry.COLUMN_INSTAGRAM_USER_ID));
                    instagramDb.setAvatarUrl(resultSet.getString(DataContract.InstagramEntry.COLUMN_AVATAR_URL));
                    String cookie = resultSet.getString(DataContract.InstagramEntry.COLUMN_COOKIES);
                    resultSet.close();
                    if (!TextUtils.isEmpty(cookie)) {
                        instagramDb.setCookies(gson.fromJson(cookie, new TypeToken<Set<Cookie>>() {

                        }.getType()));
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instagramDb;
    }

    public void clearCookie() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM " + DataContract.InstagramEntry.TABLE_NAME);
        } catch (SQLException e) {
            System.err.println("SQLException: " + e.getMessage());
        }
    }

    public void clearPreference() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DELETE FROM " + DataContract.PreferenceEntry.TABLE_NAME);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
