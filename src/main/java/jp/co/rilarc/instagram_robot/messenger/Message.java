package jp.co.rilarc.instagram_robot.messenger;

public class Message {

    public int what;

    public Object[] objects;

    public static Message obtain(int what) {
        return obtain(what, (Object[]) null);
    }

    public static Message obtain(int what, Object... objects) {
        return new Message(what, objects);
    }

    private Message(int what, Object... objects) {
        this.what = what;
        this.objects = objects;
    }
}
