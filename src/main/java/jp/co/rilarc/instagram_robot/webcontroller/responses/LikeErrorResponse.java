package jp.co.rilarc.instagram_robot.webcontroller.responses;

public class LikeErrorResponse extends ErrorResponse {

    public static final int CODE_MISSING_REACT_PERMISSION = 1;

    public static final int CODE_LIKED_ALREADY = 2;

    public static final int CODE_CANNOT_INTERACTIVE = 3;

    public LikeErrorResponse(int code) {
        super(code);
    }

    public LikeErrorResponse(int code, String message) {
        super(code, message);
    }
}
