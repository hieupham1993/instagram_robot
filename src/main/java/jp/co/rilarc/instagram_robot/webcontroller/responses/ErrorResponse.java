package jp.co.rilarc.instagram_robot.webcontroller.responses;

import jp.co.rilarc.instagram_robot.utils.Const;

public class ErrorResponse {

    public static final int CODE_INVALID_URL = -1000;

    public static final int CODE_LOAD_PAGE_ERROR = -1001;

    public static final int CODE_UNKNOWN = -9999;

    private int code;

    private String message;

    public ErrorResponse(int code) {
        this(code, Const.EMPTY_STRING);
    }

    public ErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
