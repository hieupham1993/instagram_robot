package jp.co.rilarc.instagram_robot.webcontroller;

import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.webcontroller.responses.IActionResponse;
import jp.co.rilarc.instagram_robot.webcontroller.responses.LoginErrorResponse;

/**
 * Created by HieuPT on 11/11/2017.
 */
public class InstagramWebController extends BaseWebController {

    public static final String INSTAGRAM_BASE_URL = "https://www.instagram.com";

    public static final String INSTAGRAM_LOGIN_URL = INSTAGRAM_BASE_URL + "/accounts/login/";

    public static final String INSTAGRAM_BASE_POST_URL = INSTAGRAM_BASE_URL + "/p/";

    public static final String INSTAGRAM_EXPLORE_TAGS_BASE_URL = INSTAGRAM_BASE_URL + "/explore/tags";

    private static final Logger LOGGER = LogManager.getLogger(InstagramWebController.class);

    public InstagramWebController(WebDriver webDriver) {
        super(webDriver);
    }

    public final void login(String account, String password, IActionResponse<Void, LoginErrorResponse> response) {
        if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
            doLogin(account, password, response);
        } else {
            IActionResponse.onError(response, new LoginErrorResponse(LoginErrorResponse.CODE_USER_NAME_OR_PASSWORD_EMPTY));
        }
    }

    private void doLogin(String userId, String password, IActionResponse<Void, LoginErrorResponse> response) {
        openUrl(INSTAGRAM_LOGIN_URL);
        wait(3, TimeUnit.SECONDS);
        WebElement emailElem = getElementByName("username");
        WebElement passElem = getElementByName("password");
        if (emailElem != null && passElem != null) {
            wait(5, TimeUnit.SECONDS);
            simulateSendKeys(emailElem, userId);
            wait(Utils.getRandomNumber(300, 600), TimeUnit.MILLISECONDS);
            new Actions(getWebDriver()).sendKeys(Keys.TAB).build().perform();
            simulateSendKeys(passElem, password);
            wait(Utils.getRandomNumber(300, 600), TimeUnit.MILLISECONDS);
            new Actions(getWebDriver()).sendKeys(Keys.ENTER).build().perform();
            checkLogin(response);
            return;
        }
        IActionResponse.onError(response, new LoginErrorResponse(LoginErrorResponse.CODE_LOAD_PAGE_ERROR));
    }

    private void checkLogin(IActionResponse<Void, LoginErrorResponse> response) {
        wait(3, TimeUnit.SECONDS);
        if (isElementExist(By.id("slfErrorAlert"))) {
            IActionResponse.onError(response, new LoginErrorResponse(LoginErrorResponse.CODE_USER_NAME_OR_PASSWORD_INVALID));
        } else {
            String currentUrl = getWebDriver().getCurrentUrl();
            if (!TextUtils.isEmpty(currentUrl) && currentUrl.contains("challenge")) {
                LOGGER.info("------------------------------Challenge required---------------------------");
                IActionResponse.onError(response, new LoginErrorResponse(LoginErrorResponse.CODE_SECURE_CHECK_POINT));
            } else {
                openUrl(INSTAGRAM_BASE_URL);
                wait(5, TimeUnit.SECONDS);
                if (isElementExist("a.coreSpriteDesktopNavProfile")) {
                    IActionResponse.onSuccess(response);
                } else {
                    IActionResponse.onError(response, new LoginErrorResponse(LoginErrorResponse.CODE_UNKNOWN));
                }
            }
        }
    }

    public boolean likePost(String postId) {
        if (!TextUtils.isEmpty(postId)) {
            openUrl(INSTAGRAM_BASE_POST_URL + postId);
            wait(5, TimeUnit.SECONDS);
            WebElement coreSpriteHeartOpenElement = getElementByCssSelector("span.glyphsSpriteHeart__outline__24__grey_9");
            if (coreSpriteHeartOpenElement != null) {
                closeMobileBannerSuggestion();
                clickElement(coreSpriteHeartOpenElement);
                WebElement coreSpriteHeartFullElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span.glyphsSpriteHeart__filled__24__red_5")));
                return coreSpriteHeartFullElement != null;
            } else {
                WebElement coreSpriteHeartFullElement = getElementByCssSelector("span.glyphsSpriteHeart__filled__24__red_5");
                return coreSpriteHeartFullElement != null;
            }
        }
        return false;
    }

    private void closeMobileBannerSuggestion() {
        List<WebElement> suggestMobileAppElements = getElementsByCssSelector("div.pLTDo.N8xpH");
        if (suggestMobileAppElements != null && !suggestMobileAppElements.isEmpty()) {
            WebElement suggestMobileAppElement = suggestMobileAppElements.get(0);
            if (suggestMobileAppElement != null) {
                WebElement closeBannerButtonElement = getElementByCssSelector(suggestMobileAppElement, "span.uDNXD");
                clickElement(closeBannerButtonElement);
            }
        }
    }

    public boolean follow(String username) {
        if (!TextUtils.isEmpty(username)) {
            openUrl(INSTAGRAM_BASE_URL + "/" + username);
            wait(5, TimeUnit.SECONDS);
            WebElement followButtonElement = getElementByCssSelector("button._5f5mN.jIbKX._6VtSN.yZn4P");
            if (followButtonElement != null) {
                clickElement(followButtonElement);
                WebElement followingButtonElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button._5f5mN.-fzfL._6VtSN.yZn4P")));
                return followingButtonElement != null;
            } else {
                WebElement followingButtonElement = getElementByCssSelector("button._5f5mN.-fzfL._6VtSN.yZn4P");
                return followingButtonElement != null;
            }
        }
        return false;
    }

    public boolean unfollow(String username) {
        if (!TextUtils.isEmpty(username)) {
            openUrl(INSTAGRAM_BASE_URL + "/" + username);
            wait(5, TimeUnit.SECONDS);
            WebElement unfollowButtonElement = getElementByCssSelector("button._5f5mN.-fzfL._6VtSN.yZn4P");
            if (unfollowButtonElement != null) {
                clickElement(unfollowButtonElement);
                WebElement dialogConfirmElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.pbNvD.fPMEg")));
                if (dialogConfirmElement != null) {
                    WebElement confirmUnfollowButtonElement = getElementByCssSelector(dialogConfirmElement, "button.aOOlW.-Cab_");
                    if (confirmUnfollowButtonElement != null) {
                        clickElement(confirmUnfollowButtonElement);
                        WebElement followButtonElement = waitUntil(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button._5f5mN.jIbKX._6VtSN.yZn4P")));
                        return followButtonElement != null;
                    }
                }
            } else {
                WebElement followButtonElement = getElementByCssSelector("button._5f5mN.jIbKX._6VtSN.yZn4P");
                return followButtonElement != null;
            }
        }
        return false;
    }
}
