package jp.co.rilarc.instagram_robot.webcontroller;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.co.rilarc.instagram_robot.utils.Utils;

/**
 * Created by HieuPT on 11/11/2017.
 */
public abstract class BaseWebController {

    private static final String TAG = BaseWebController.class.getSimpleName();

    private static final int WEB_DRIVER_WAIT_TIMEOUT = 10;

    private static final int DEFAULT_WAIT_TIME = 500;

    private final WebDriver webDriver;

    private final WebDriverWait webDriverWait;

    public BaseWebController(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, WEB_DRIVER_WAIT_TIMEOUT);
    }

    protected WebDriver getWebDriver() {
        return webDriver;
    }

    public final JavascriptExecutor getJavascriptExecutor() {
        if (webDriver instanceof JavascriptExecutor) {
            return (JavascriptExecutor) webDriver;
        } else {
            throw new IllegalArgumentException("This web driver does not support javascript");
        }
    }

    public final void waitDefault() {
        wait(DEFAULT_WAIT_TIME, TimeUnit.MILLISECONDS);
    }

    public final void wait(long time, TimeUnit timeUnit) {
        if (time > 0) {
            try {
                Thread.sleep(timeUnit.toMillis(time));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public final void scrollToElement(WebElement element) {
        getJavascriptExecutor().executeScript("arguments[0].scrollIntoView();", element);
    }

    public final void scrollToTop() {
        getJavascriptExecutor().executeScript("window.scrollTo(0, 0);");
    }

    public final void scrollToBottom() {
        getJavascriptExecutor().executeScript("window.scrollTo(0, document.body.scrollHeight);");
    }

    public final void simulateSendKeys(WebElement element, String text) {
        if (element != null && !TextUtils.isEmpty(text)) {
            element.clear();
            element.click();
            Actions actions = new Actions(webDriver);
            for (char c : text.toCharArray()) {
                actions.sendKeys(Character.toString(c));
                actions.pause(Utils.getRandomNumber(50, 300));
            }
            actions.build().perform();
        }
    }

    public final void clickElement(WebElement element) {
        if (element != null) {
            waitDefault();
            try {
                if (isElementPresentAndDisplayed(element)) {
                    element.click();
                }
            } catch (WebDriverException e) {
                e.printStackTrace();
            }
        }
    }

    public final void moveToElement(WebElement element) {
        if (element != null) {
            waitDefault();
            try {
                Actions actions = new Actions(webDriver);
                actions.moveToElement(element);
                actions.build().perform();
            } catch (WebDriverException e) {
                e.printStackTrace();
            }
        }
    }

    public final boolean isElementPresentAndDisplayed(WebElement element) {
        try {
            if (element != null) {
                return element.isDisplayed();
            }
        } catch (WebDriverException e) {
            e.printStackTrace();
        }
        return false;
    }

    public final void openUrl(String url) {
        webDriver.get(url);
    }

    public final Object executeJavaScript(String script, Object... objects) {
        return getJavascriptExecutor().executeScript(script, objects);
    }

    public final <E extends WebElement> E getElementByCondition(ExpectedCondition<E> expectedCondition) {
        try {
            return webDriverWait.until(expectedCondition);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final <T> T waitUntil(ExpectedCondition<T> expectedCondition) {
        try {
            return webDriverWait.until(expectedCondition);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByCssSelector(WebElement parentElement, String selector) {
        try {
            return parentElement.findElement(By.cssSelector(selector));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<WebElement> getElementsByCssSelector(WebElement parentElement, String selector) {
        try {
            return parentElement.findElements(By.cssSelector(selector));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<WebElement> getElementsByCssSelector(String selector) {
        try {
            return webDriver.findElements(By.cssSelector(selector));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByCssSelector(String selector) {
        try {
            return webDriver.findElement(By.cssSelector(selector));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByClassName(String className) {
        try {
            return webDriver.findElement(By.className(className));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByClassName(WebElement parentElement, String className) {
        try {
            return parentElement.findElement(By.className(className));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<WebElement> getElementsByClassName(String className) {
        try {
            return webDriver.findElements(By.className(className));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementById(String id) {
        try {
            return webDriver.findElement(By.id(id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementById(WebElement parentElement, String id) {
        try {
            return parentElement.findElement(By.id(id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<WebElement> getElementsById(String id) {
        try {
            return webDriver.findElements(By.id(id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<WebElement> getElementsById(WebElement parentElement, String id) {
        try {
            return parentElement.findElements(By.id(id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByName(String name) {
        try {
            return webDriver.findElement(By.name(name));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByTagName(String name) {
        try {
            return webDriver.findElement(By.tagName(name));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final WebElement getElementByTagName(WebElement parentElement, String name) {
        try {
            return parentElement.findElement(By.tagName(name));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final boolean isElementExist(String cssSelector) {
        try {
            List<WebElement> elements = webDriver.findElements(By.cssSelector(cssSelector));
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final boolean isElementExist(WebElement parentElement, String cssSelector) {
        try {
            List<WebElement> elements = parentElement.findElements(By.cssSelector(cssSelector));
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final boolean isElementExist(By by) {
        try {
            List<WebElement> elements = webDriver.findElements(by);
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final boolean isElementExist(WebElement parentElement, By by) {
        try {
            List<WebElement> elements = parentElement.findElements(by);
            return elements != null && !elements.isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
