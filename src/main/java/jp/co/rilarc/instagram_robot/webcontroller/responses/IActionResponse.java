package jp.co.rilarc.instagram_robot.webcontroller.responses;

public interface IActionResponse<T, E extends ErrorResponse> {

    static <T, E extends ErrorResponse> void onSuccess(IActionResponse<T, E> delegate) {
        onSuccess(delegate, null);
    }

    static <T, E extends ErrorResponse> void onSuccess(IActionResponse<T, E> delegate, T data) {
        if (delegate != null) {
            delegate.onSuccess(data);
        }
    }

    static <T, E extends ErrorResponse> void onError(IActionResponse<T, E> delegate, E error) {
        if (delegate != null) {
            delegate.onError(error);
        }
    }

    default void onSuccess(T data) {
    }

    default void onError(E errorResponse) {
    }
}
