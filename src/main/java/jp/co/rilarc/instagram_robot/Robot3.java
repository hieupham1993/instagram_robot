package jp.co.rilarc.instagram_robot;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.action1user.ScheduledAutoThreadForUser;
import jp.co.rilarc.instagram_robot.action1user.UserAutoThread;
import jp.co.rilarc.instagram_robot.helpers.APIHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.messenger.Message;
import jp.co.rilarc.instagram_robot.messenger.Messenger;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public class Robot3 {

    private final String email;

    final private IAPIHelper dbHelper;

    final private Configuration config;

    final private static Logger logger = LogManager.getLogger(Robot3.class);

    private Messenger messenger;

    private UserAutoThread userAutoThread;

    private ScheduledAutoThreadForUser scheduledAutoThreadForUser;

    public Robot3(String email) {
        this.email = email;
        config = new Configuration();
        dbHelper = new APIHelper(config);
        System.setProperty("file.encoding", "UTF-8");
    }

    public void setMessenger(Messenger messenger) {
        this.messenger = messenger;
    }

    public void start() {
        logger.info("Start robot");
        VLogger.append("タスクを実行します");
        try {
            User user = getUser();
            if (user == null) {
                logger.error("Cannot get info of user : " + email);
                if (messenger != null) {
                    messenger.sendMessage(Message.obtain(Const.Msg.MSG_STOP_ROBOT));
                }
                return;
            }

            // Start thread to auto like, follow, unfollow
            startThreadForUser(user);

            // Start thread to post, commentsetting and sendmessage
            startScheduledActionThread(user);
        } catch (Exception e) {
            logger.error("Unknown Error: ", e);
        }
    }

    public void stop() {
        logger.info("Stop robot");
        if (userAutoThread != null) {
            userAutoThread.terminate();
        }
        if (scheduledAutoThreadForUser != null) {
            scheduledAutoThreadForUser.terminate();
        }
        VLogger.append("タスクを停止しました");
    }

    private void startThreadForUser(User user) {
        userAutoThread = new UserAutoThread(user, dbHelper, config);
        userAutoThread.start();
    }

    private void startScheduledActionThread(User user) {
        scheduledAutoThreadForUser = new ScheduledAutoThreadForUser(user, dbHelper, config);
        scheduledAutoThreadForUser.start();
    }

    private User getUser() {
        return dbHelper.getUserByToken();
    }
}