package jp.co.rilarc.instagram_robot.follow;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;

import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.handler.ListFollowHandler;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.InstagramResponse;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.utils.Const;

public class FollowByFollower extends Follow {

    public FollowByFollower(Instagram instagram, RobotSetting robotSetting, int userId, IAPIHelper dbHelper,
                            AdvanceSetting advanceSetting, Statistic statistic, int numberUsersToFollow) {
        super(instagram, robotSetting, userId, dbHelper, advanceSetting, statistic, numberUsersToFollow);
    }

    @Override
    public int autoFollow() throws ActionBlockException, InstagramCannotAccessException {
//        boolean result = false;
        int totalUsersFollowed = 0;

        VLogger.append("「あなたをフォローしたフォロワーに自動的にフォローする」タスクを実行します");

        String maxId = null;
        for (int loopCount = 0; loopCount< Const.MAX_SEARCH_PAGE && totalUsersFollowed < numberUsersToFollow; loopCount++) {
            // search media

            JSONObject listFollowers = this.getListFollowers(maxId);

            if (listFollowers == null)
                break;

            ListFollowHandler listFollowHandler = new ListFollowHandler(listFollowers);
            if (!Objects.equals(listFollowHandler.getStatus(), "ok"))
                break;

            // get list user id
            ArrayList<String> listUserIds = listFollowHandler.getListIds();

            // get list friendship
            JSONObject usersFriendship = this.getUsersFriendship(listUserIds);
            if (usersFriendship == null || !Objects.equals(usersFriendship.getString("status"), "ok"))
                break;

            JSONObject friendshipStatuses = usersFriendship.getJSONObject("friendship_statuses");

            // check friendship status
//            for (String userId : listUserIds) {
            for (int i=0; i < listUserIds.size() && totalUsersFollowed < numberUsersToFollow; i++) {
                String userId = listUserIds.get(i);

                if (friendshipStatuses.has(userId)) {
                    JSONObject friendshipStatus = friendshipStatuses.getJSONObject(userId);

                    // if not following or send follow request
                    if (!friendshipStatus.getBoolean("following") && !friendshipStatus.getBoolean("outgoing_request")) {
                        // get user info
                        JSONObject userInfo = null;
//						try {
							userInfo = mInstagramRequester.getUserInfo(userId);
//						} catch (InstagramCheckpointRequiredException e1) {
//							logger.info(e1.getMessage());
//							mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//						}
                        if (userInfo == null || !Objects.equals(userInfo.getString("status"), "ok"))
                            continue;

                        // check conflict filter
                        if (mAdvanceSetting != null && mAdvanceSetting.isActive()) {
                            logger.info(buildLog("checking user " + userId));

                            if (this.isBlackUser(userId) || !this.checkConflictWordFilter(userId)) {
                                try {
                                    Thread.sleep(Signatures.randomInt(10, 20) * 1000);
                                } catch (InterruptedException e) {
                                    logger.error(buildLog(), e);
                                }

                                continue;
                            }
                        }

                        String usernameFollow = listFollowHandler.searchUsername(userId);

                        boolean result;
//                        try {
                            result = this.followUser(userId,usernameFollow);
//                        } catch (ActionBlockException e) {
//                            logger.error(e.getMessage());
//                            break outerLoop;
//                        }

                        if (result) {
                            String username = userInfo.getJSONObject("user").getString("username");
                            String avatarUrl = userInfo.getJSONObject("user").getString("profile_pic_url");

                            mDbHelper.saveFollowingUser(userId, username, avatarUrl);

                            //insert activity
                            mDbHelper.insertActivity(Const.ACTIVITY_TYPE_FOLLOW, mInstagram.getInstagramAccount().getUsername() ,username, avatarUrl, null, null, null, null);

                            totalUsersFollowed++;
                        }

                        try {
                            Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
                        } catch (InterruptedException e) {
                            logger.error(buildLog(), e);
                        }
                    }
                }
            }


            //check has next page
            if (!listFollowHandler.hasNext())
                break;

            //next page id
            maxId = listFollowHandler.getNextPageId();
        }

        return totalUsersFollowed;
    }

    private JSONObject getListFollowers(String maxId) {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getSelfUserFollowers(maxId);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                logger.info(buildLog(": " + responseAsString));
            }

        } catch (IOException | URISyntaxException e) {
            logger.error(buildLog(), e);
        }

        return result;
    }

    private JSONObject getUsersFriendship(ArrayList<String> listUserIds) throws InstagramCannotAccessException {
        String userIds = String.join(",", listUserIds);

        JSONObject result = null;
        try {
        		InstagramResponse response = mInstagram.getUsersFriendship(userIds);

            //get response body
            String responseAsString = response.getResponseAsString();
            if (response.getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                logger.info(buildLog(": " + responseAsString));
            }

        } catch (IOException | URISyntaxException e) {
            logger.error(buildLog(": " + e.getMessage()));
        }

        return result;
    }

    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {

    }
    
    protected StringBuilder buildLog() {
		return super.buildLog("[byFollower]");
	}
}
