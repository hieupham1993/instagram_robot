package jp.co.rilarc.instagram_robot.follow;

import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.SearchMediaHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.FollowHashtag;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Const;

import jp.co.rilarc.instagram_robot.utils.VLogger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class FollowByHashtag extends Follow {
	
	final protected static Logger logger = LogManager.getLogger(FollowByHashtag.class);

    public FollowByHashtag(Instagram instagram, RobotSetting robotSetting, int userId, IAPIHelper dbHelper,
                           AdvanceSetting advanceSetting, Statistic statistic, int numberUsersToFollow) {
        super(instagram, robotSetting, userId, dbHelper, advanceSetting, statistic, numberUsersToFollow);
    }

    @Override
    public int autoFollow() throws ActionBlockException, InstagramCheckpointRequiredException {
//        boolean result = false;
        int totalUsersFollowed = 0;

        // get list hashtags
        ArrayList<FollowHashtag> followHashtags = mDbHelper.getFollowHashtagsByToken();
        if (followHashtags.size() == 0) {
            logger.debug(buildLog(": have no follow_hashtag record"));
            return 0;
        }

        // get random location id in db
        String maxId = null;
        int rnd = new Random().nextInt(followHashtags.size());
        FollowHashtag followHashtag = followHashtags.get(rnd);

        VLogger.append("「登録したハッシュタグ【" + followHashtag.getHashtag() + "】を含む写真を投稿したユーザーを自動でフォローする」タスクを実行します");

        for (int loopCount = 0; loopCount< Const.MAX_SEARCH_PAGE; loopCount++) {
            // search media

            JSONObject searchMediaResponse = null;
//			try {
				searchMediaResponse = mInstagramRequester.searchMediaByHashtag(followHashtag.getHashtag(), maxId);
//			} catch (InstagramCheckpointRequiredException e1) {
//				logger.info(e1.getMessage());
//				mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//			}

            if (searchMediaResponse == null)
                break;

            SearchMediaHandler searchMediaHandler = new SearchMediaHandler(searchMediaResponse);
            if (!Objects.equals(searchMediaHandler.getStatus(), "ok"))
                break;

            // get list media
            JSONArray medias = searchMediaHandler.getMedias();

            int numUserFollowed = this.followUserFromList(medias, followHashtag.getHashtag(), numberUsersToFollow);
            mDbHelper.increaseFollowHashtagCount(followHashtag.getId(), numUserFollowed);

//            if (result) {
//                mDbHelper.increaseFollowHashtagCount(followHashtag.getId());
//                break;
//            }

            totalUsersFollowed += numUserFollowed;
            if (numUserFollowed < numberUsersToFollow)
                numberUsersToFollow -= numUserFollowed;
            else
                break;

            //check has next page
            if (!searchMediaHandler.hasNext())
                break;

            //next page id
            maxId = searchMediaHandler.getNextPageId();

            try {
                Thread.sleep(Signatures.randomInt(2, 3) * 1000);
            } catch (InterruptedException e) {
                logger.error("ERROR: ", e);
            }
        }

        return totalUsersFollowed;
    }

    @Override
    protected boolean checkFollowFilter(MediaHandler media) {
        JSONArray hashtags = media.getListHashtags();
        //check max hashtag
        if (mRobotSetting.isFollowsMaxHashtagFilterMode() && hashtags.length() >= mRobotSetting.getFollowsMaxHashtagFilter())
            return false;

        //check spam hashtag
        if (mRobotSetting.isLikesHashtagEnableSpamFilter() && hashtags.length() > 0) {
            JSONArray spamHashtags = mDbHelper.getSpamHashtagsByToken();

            if (spamHashtags != null && spamHashtags.length() > 0) {
                for (int i=0; i<hashtags.length(); i++)
                    for (int j=0; j<spamHashtags.length(); j++)
                        if (Objects.equals(hashtags.getString(i).toLowerCase(), spamHashtags.getString(j).toLowerCase()))
                            return false;
            }
        }

        return super.checkFollowFilter(media);
    }

    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {
        String userName = mediaHandler.getUsername();
        String avatarUrl = mediaHandler.getAvatarUrl();
        mDbHelper.insertActivity(Const.ACTIVITY_TYPE_FOLLOW, mInstagram.getInstagramAccount().getUsername(), userName, avatarUrl, key, null, null, null);
    }
    
    protected StringBuilder buildLog() {
		return super.buildLog("[byHashtag]");
	}
}
