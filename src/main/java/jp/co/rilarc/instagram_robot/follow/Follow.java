package jp.co.rilarc.instagram_robot.follow;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.FollowBlockedException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.InstagramUser;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.UserHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.InstagramRequester;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.utils.VLogger;

public abstract class Follow {

    protected Instagram mInstagram;

    final protected RobotSetting mRobotSetting;

    final protected AdvanceSetting mAdvanceSetting;

    final protected IAPIHelper mDbHelper;

    final protected Statistic statistic;

    final protected static Logger logger = LogManager.getLogger(Follow.class);

    protected InstagramRequester mInstagramRequester;

    protected int numberUsersToFollow;

    final protected int mUserId;

    final protected String instagramUsername;

    public Follow(Instagram instagram, RobotSetting robotSetting, int userId, IAPIHelper dbHelper,
                  AdvanceSetting advanceSetting, Statistic statistic, int numberUsersToFollow) {
        mInstagram = instagram;
        mRobotSetting = robotSetting;
        mAdvanceSetting = advanceSetting;
        mUserId = userId;
        instagramUsername = mInstagram.getInstagramAccount().getUsername();
        mDbHelper = dbHelper;
        this.statistic = statistic;
        mInstagramRequester = new InstagramRequester(instagram);
        this.numberUsersToFollow = numberUsersToFollow;
    }

    public abstract int autoFollow() throws ActionBlockException, InstagramCannotAccessException;

    protected boolean checkFollowFilter(MediaHandler media) {
        if (mRobotSetting.isFollowsDoNotFollowPrivateUsers() && media.isFromPrivateUser())
            return false;

        if (mRobotSetting.isFollowsDoNotFollowPreviouslyUnfollowedUsers()) {
            JSONArray unfollowedUsers = mDbHelper.getPreviouslyUnfollowUsers();

            if (unfollowedUsers == null || unfollowedUsers.length() == 0)
                return true;

            for (int i = 0; i < unfollowedUsers.length(); i++)
                if (Objects.equals(unfollowedUsers.getString(i), media.getUserId()))
                    return false;
        }

        return true;
    }

    protected boolean followUser(String userId, String username) throws ActionBlockException {
        // follow user
        logger.debug(buildLog(": follow user " + username));
        boolean result = false;
        if (!TextUtils.isEmpty(username)) {
            result = mInstagram.followUserByJs(username);
            logger.info("[Follow] " + username + " by js result: " + result);
        }
        if (!result) {
            result = this.followUserByAPI(userId);
        }

        if (result) {
            statistic.increaseNumFollowing();
            mDbHelper.increaseNumFollowsStatistic(statistic.getId(), 1);

            logger.info(buildLog("[successful] user " + username + ". Total follow = " + statistic.getNumFollowings()));
            VLogger.append(statistic.getNumFollowings() + "回目のフォロー： https://www.instagram.com/" + username);
        }

        return result;
    }

    protected boolean followUserByAPI(String userId) throws ActionBlockException {
        boolean result = false;

        try {
            HttpResponse response = mInstagram.follow(userId);

            if (response.getStatusLine().getStatusCode() == 400) {
                logger.info(buildLog(": user " + userId + " not found"));
                return false;
            }

            //check like result
            String responseAsString = EntityUtils.toString(response.getEntity());
            JSONObject responseObject = new JSONObject(responseAsString);
            if (response.getStatusLine().getStatusCode() == 200
                    && Objects.equals(responseObject.getString("status"), "ok")
                    && responseObject.getJSONObject("friendship_status").getBoolean("following")) {
                result = true;
            } else {
                if (responseAsString.contains("The link you followed may be broken")) {
                    logger.warn(buildLog("The link you followed may be broken, or the page may have been removed."));
                } else {
                    logger.info(buildLog("responseAsString " + responseAsString));
                }

                // Change flag diable_follow = 1
                if ((responseObject.has("spam") && responseObject.getBoolean("spam"))
                        || responseAsString.contains("following the max limit of accounts")) {

                    String reason = "";
                    if ((responseObject.has("spam") && responseObject.getBoolean("spam"))) {
                        reason = "Disabled because spam";
                    } else {
                        reason = "Disabled because following the max limit of accounts";
                    }

                    logger.error(String.format("Disable auto follow function of user %d (instagram_id = %s)", mUserId, userId));

                    throw new FollowBlockedException(reason);
                }
            }
        } catch (IOException | URISyntaxException | JSONException e) {
            logger.error("account " + mInstagram.getInstagramAccount().getUsername() + ": " + e.getMessage());
        }

        return result;
    }

    protected int followUserFromList(JSONArray medias, String key, int numberUsersToLike) throws ActionBlockException, InstagramCheckpointRequiredException {
//        boolean result = false;
        List<String> requestedUser = new ArrayList<>();
        int numUsersFollowed = 0;

        for (int i = 0; i < medias.length() && numUsersFollowed < numberUsersToLike; i++) {
            JSONObject media = medias.getJSONObject(i);

            MediaHandler mediaHandler = new MediaHandler(media);

            //check like filter
            if (mediaHandler.isMedia() && !mediaHandler.hasFollowedOrRequested()
                    && this.checkFollowFilter(mediaHandler) && !requestedUser.contains(mediaHandler.getUserId())) {
                // check conflict filter
                if (mAdvanceSetting != null && mAdvanceSetting.isActive()) {
                    logger.info(buildLog(" checking user " + mediaHandler.getUserId()));

                    if (this.isBlackUser(mediaHandler.getUserId()) || !this.checkConflictWordFilter(mediaHandler.getUserId())) {
//                        try {
//                            Thread.sleep(Signatures.randomInt(10, 20) * 1000);
//                        } catch (InterruptedException e) {
//                            logger.error("ERROR: ", e);
//                        }
                        Utils.waitFor(10, 20);
                        continue;
                    }
                }

                //like media
                logger.debug(buildLog(" will follow user " + mediaHandler.getUsername()));

                boolean result = this.followUser(mediaHandler.getUserId(), mediaHandler.getUsername());

                if (result) {
                    // like media
                    // TODO de tranh bi spam thi ko follow user khong like bai post cua user do nua
//                    if (!mediaHandler.hasLiked())
//                        mInstagram.likeMediaByJs(mediaHandler.getMediaPost());

                    mDbHelper.saveFollowingUser(mediaHandler.getUserId(), mediaHandler.getUsername(), mediaHandler.getAvatarUrl());
                    this.createActivity(mediaHandler, key);

                    requestedUser.add(mediaHandler.getUserId());
                    numUsersFollowed++;
                } else {
                    logger.info(buildLog("[failed] user " + mediaHandler.getUsername()));
                    VLogger.append("https://www.instagram.com/" + mediaHandler.getUsername() + "をフォロー失敗しました");
                }

                try {
                    Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
                } catch (InterruptedException e) {
                    logger.error("ERROR: ", e);
                }
            }
        }

        return numUsersFollowed;
    }

    protected abstract void createActivity(MediaHandler mediaHandler, String key);

    protected boolean checkConflictWordFilter(String userId) throws InstagramCheckpointRequiredException {
        String conflictWords = mAdvanceSetting.getConflict();

        if (conflictWords != null) {
            JSONArray conflictArr = new JSONArray(conflictWords);

            // get user description

            JSONObject userInfo = null;
//			try {
            userInfo = mInstagramRequester.getUserInfo(userId);
//			} catch (InstagramCheckpointRequiredException e1) {
//				logger.info(e1.getMessage());
//				mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//			}
            if (userInfo == null || !Objects.equals(userInfo.getString("status"), "ok"))
                return false;
            UserHandler userHandler = new UserHandler(userInfo);
            String biography = userHandler.getBiography();

            for (int i = 0; i < conflictArr.length(); i++) {
                String conflictWord = conflictArr.getString(i);

                if (biography.toLowerCase().contains(conflictWord.toLowerCase())) {
                    logger.info(buildLog(": user " + userId + " is conflict"));

                    return false;
                }
            }
        }

        return true;
    }

    protected boolean isBlackUser(String userId) {
        String blackList = mAdvanceSetting.getBlackList();

        if (blackList != null) {
            InstagramUser instagramUser = mDbHelper.getInstagramUser(userId);

            if (instagramUser != null) {
                JSONArray blackArr = new JSONArray(blackList);

                for (int i = 0; i < blackArr.length(); i++) {
                    if (Objects.equals(blackArr.getString(i), instagramUser.getUsername())) {
                        logger.info(buildLog(": user " + userId + " is black user"));

                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected StringBuilder buildLog(String msg) {
        return new StringBuilder("[" + mUserId + "][" + instagramUsername + "][Follow]").append(msg);
    }
}
