package jp.co.rilarc.instagram_robot.follow;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;

import jp.co.rilarc.instagram_robot.model.*;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.handler.ListFollowHandler;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.utils.Const;

public class FollowByRelatedAccount extends Follow {

	final protected static Logger logger = LogManager.getLogger(FollowByRelatedAccount.class);

    private RelatedAccount mRelatedAccount;

    public FollowByRelatedAccount(Instagram instagram, RobotSetting robotSetting, int userId, IAPIHelper dbHelper,
                                  AdvanceSetting advanceSetting, RelatedAccount relatedAccount, Statistic statistic, int numberUsersToFollow) {
        super(instagram, robotSetting, userId, dbHelper, advanceSetting, statistic, numberUsersToFollow);
        mRelatedAccount = relatedAccount;
    }

    @Override
    public int autoFollow() throws ActionBlockException, InstagramCannotAccessException {
        int totalUsersFollowed = 0;

        String maxId = null;
//        outerLoop:
        for (int loopCount = 0; loopCount< Const.MAX_SEARCH_PAGE; loopCount++) {
            // search media

            JSONObject listFollowers = this.getListFollowings(mRelatedAccount.getInstagramId(), maxId);
//            logger.info("FOLLOWING RELATED ACCOUNT: " + mRelatedAccount.getInstagramUsername());

            logger.debug(listFollowers);

            if (listFollowers == null) {
//                logger.info("FOLLOWING RELATED ACCOUNT: get list followers failed");

                break;
            }

            ListFollowHandler listFollowingsHandler = new ListFollowHandler(listFollowers);
            if (!Objects.equals(listFollowingsHandler.getStatus(), "ok")) {
//                logger.info("FOLLOWING RELATED ACCOUNT: get list followers failed (status not ok)");

                break;
            }

            // get list user id
            ArrayList<String> listUserIds = listFollowingsHandler.getListIds();
//            logger.info("FOLLOWING RELATED ACCOUNT: follow " + listUserIds.size() + " accounts");
//            logger.info("FOLLOWING RELATED ACCOUNT: " + listUserIds.toString());

            // get list friendship
            JSONObject usersFriendship = this.getUsersFriendship(listUserIds);
            if (usersFriendship == null || !Objects.equals(usersFriendship.getString("status"), "ok")) {
//                logger.info("FOLLOWING RELATED ACCOUNT: get list friendship failed (status not ok)");

                break;
            }
//            logger.info("FOLLOWING RELATED ACCOUNT: friendships result " + usersFriendship.toString());


            JSONObject friendshipStatuses = usersFriendship.getJSONObject("friendship_statuses");

            // check friendship status
            for (int i=0; i<listUserIds.size() && totalUsersFollowed < numberUsersToFollow; i++) {

                String userId = listUserIds.get(i);

//                logger.info("FOLLOWING RELATED ACCOUNT: follow user " + userId + "(" +i+ ")");


                if (friendshipStatuses.has(userId)) {
                    JSONObject friendshipStatus = friendshipStatuses.getJSONObject(userId);

                    // if not following or send follow request
                    if (!friendshipStatus.getBoolean("following") && !friendshipStatus.getBoolean("outgoing_request")) {
                        // get user info
                        JSONObject userInfo = null;
//						try {
							userInfo = mInstagramRequester.getUserInfo(userId);
//						} catch (InstagramCheckpointRequiredException e1) {
//							logger.info(e1.getMessage());
//							mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//						}
                        if (userInfo == null || !Objects.equals(userInfo.getString("status"), "ok")) {
//                            logger.info("FOLLOWING RELATED ACCOUNT: get info " + userId + " failed");
//                            logger.info("FOLLOWING RELATED ACCOUNT: " + userInfo);

                            try {
                                Thread.sleep( 60 * 1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            continue;
                        }

                        // check conflict filter
                        if (this.isBlackUser(userId) || !this.checkConflictWordFilter(userId)) {
                            try {
                                Thread.sleep(Signatures.randomInt(2, 3) * 1000);
                            } catch (InterruptedException e) {
                                logger.error("ERROR: ", e);
                            }

                            continue;
                        }

                        String usernameFollow = listFollowingsHandler.searchUsername(userId);
                        // follow user
                        logger.info(buildLog(" user " + usernameFollow));

                        boolean result;
//                        try {
                            result = this.followUser(userId, usernameFollow);
//                        } catch (ActionBlockException e) {
//                            logger.error(e.getMessage());
//                            break outerLoop;
//                        }

                        if (result) {
                            String username = userInfo.getJSONObject("user").getString("username");
                            String avatarUrl = userInfo.getJSONObject("user").getString("profile_pic_url");

                            mDbHelper.saveFollowingUser(userId, username, avatarUrl);

                            //insert activity
                            mDbHelper.insertActivity(Const.ACTIVITY_TYPE_FOLLOW, mInstagram.getInstagramAccount().getUsername() ,username, avatarUrl, null, null, null, null);

                            // check is the last user
                            if (i == listUserIds.size()-1 && !listFollowingsHandler.hasNext()) {
//                                logger.info("FOLLOWING RELATED ACCOUNT: followed last user in list (" + userId + ") index " + i);

                                mDbHelper.updateRelatedAccountStatus(mRelatedAccount.getId(), false);
                            }

                            totalUsersFollowed++;

                            logger.info(buildLog("[successful] user " + usernameFollow));
//                            logger.info("FOLLOWING RELATED ACCOUNT: done index " + i);

                            if (totalUsersFollowed == numberUsersToFollow)
                                return totalUsersFollowed;

                        }

                        try {
                            Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
                        } catch (InterruptedException e) {
                            logger.error("ERROR: ", e);
                        }
                    } else {
//                        logger.info("FOLLOWING RELATED ACCOUNT: followed " + userId + " user");
                    }
                } else {
//                    logger.info("FOLLOWING RELATED ACCOUNT: friendship status " + userId + " not found");
                }
            }

            //check has next page
            if (!listFollowingsHandler.hasNext()) {
                // update related account status
//                logger.info("FOLLOWING RELATED ACCOUNT: followed last user in list");

                mDbHelper.updateRelatedAccountStatus(mRelatedAccount.getId(), false);
                break;
            }

            //next page id
            maxId = listFollowingsHandler.getNextPageId();
        }

        return totalUsersFollowed;
    }

    private JSONObject getListFollowings(String userId, String maxId) {
        JSONObject result = null;
        try {
            HttpResponse response = mInstagram.getUserFollowings(userId, maxId);

            //get response body
            String responseAsString = EntityUtils.toString(response.getEntity());
            if (response.getStatusLine().getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                logger.info(buildLog(": " + responseAsString));
            }

        } catch (IOException | URISyntaxException e) {
            logger.error(buildLog(), e);
        }

        return result;
    }

    private JSONObject getUsersFriendship(ArrayList<String> listUserIds) throws InstagramCannotAccessException {
        String userIds = String.join(",", listUserIds);

        JSONObject result = null;
        try {
        		InstagramResponse response = mInstagram.getUsersFriendship(userIds);

            //get response body
            String responseAsString = response.getResponseAsString();
            if (response.getStatusCode() == 200) {
                result = new JSONObject(responseAsString);
            } else {
                logger.info(buildLog(": " + responseAsString));
            }

        } catch (IOException | URISyntaxException e) {
            logger.error(buildLog(": " + e.getMessage()));
        }

        return result;
    }

    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {

    }
    
    protected StringBuilder buildLog() {
		return super.buildLog("[byRelatedAccount]");
	}
}
