package jp.co.rilarc.instagram_robot.follow;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.MediaHandler;
import jp.co.rilarc.instagram_robot.handler.SearchMediaHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.FollowLocation;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.utils.Const;

public class FollowByLocation extends Follow {
	
	final protected static Logger logger = LogManager.getLogger(FollowByLocation.class);
	
    public FollowByLocation(Instagram instagram, RobotSetting robotSetting, int userId, IAPIHelper dbHelper,
                            AdvanceSetting advanceSetting, Statistic statistic, int numberUsersToFollow) {
        super(instagram, robotSetting, userId, dbHelper, advanceSetting, statistic, numberUsersToFollow);
    }

    @Override
    public int autoFollow() throws ActionBlockException, InstagramCheckpointRequiredException {
//        boolean result = false;
        int totalUsersFollowed = 0;

        // get list location ids
        ArrayList<FollowLocation> followLocations = mDbHelper.getFollowLocationsByToken();
        if (followLocations.size() == 0) {
            logger.info(buildLog(": have no like_locations record"));
            return 0;
        }

        // get random location id in db
        String maxId = null;
        int rnd = new Random().nextInt(followLocations.size());
        FollowLocation followLocation = followLocations.get(rnd);

        VLogger.append("「登録したロケーション【" + followLocation.getLocationName() + "】に一致する投稿を投稿された人に対して自動的にフォローする」タスクを実行します");

        for (int loopCount = 0; loopCount< Const.MAX_SEARCH_PAGE; loopCount++) {
            // search media

            JSONObject searchMediaResponse = null;
//			try {
				searchMediaResponse = mInstagramRequester.searchMediaByLocation(followLocation.getLocationId(), maxId);
//			} catch (InstagramCheckpointRequiredException e1) {
//				logger.info(e1.getMessage());
//				mDbHelper.disableInstagramConnectStatus(mUserId, e1.getMessage());
//			}

            if (searchMediaResponse == null)
                break;

            SearchMediaHandler searchMediaHandler = new SearchMediaHandler(searchMediaResponse);
            if (!Objects.equals(searchMediaHandler.getStatus(), "ok"))
                break;

            // get list media
            JSONArray medias = searchMediaHandler.getMedias();

            int numUserFollowed = this.followUserFromList(medias, followLocation.getLocationId(), numberUsersToFollow);
            mDbHelper.increaseFollowLocationCount(followLocation.getId(), numUserFollowed);

//            if (result) {
//                mDbHelper.increaseFollowLocationCount(followLocation.getId());
//                break;
//            }

            totalUsersFollowed += numUserFollowed;
            if (numUserFollowed < numberUsersToFollow)
                numberUsersToFollow -= numUserFollowed;
            else
                break;

            //check has next page
            if (!searchMediaHandler.hasNext())
                break;

            //next page id
            maxId = searchMediaHandler.getNextPageId();

            try {
                Thread.sleep(Signatures.randomInt(10, 30) * 1000);
            } catch (InterruptedException e) {
                logger.error("ERROR: ", e);
            }
        }

        return totalUsersFollowed;
    }

    @Override
    protected boolean checkFollowFilter(MediaHandler media) {
        JSONArray hashtags = media.getListHashtags();

        //check max hashtag
        if (mRobotSetting.isFollowsMaxHashtagFilterMode() && hashtags.length() >= mRobotSetting.getFollowsMaxHashtagFilter())
            return false;

        return super.checkFollowFilter(media);
    }

    @Override
    protected void createActivity(MediaHandler mediaHandler, String key) {
        String userName = mediaHandler.getUsername();
        String avatarUrl = mediaHandler.getAvatarUrl();

        mDbHelper.insertActivity(Const.ACTIVITY_TYPE_FOLLOW, mInstagram.getInstagramAccount().getUsername(), userName, avatarUrl, null, key, null, null);
    }
    
    protected StringBuilder buildLog() {
		return super.buildLog("[byLocation]");
	}
}
