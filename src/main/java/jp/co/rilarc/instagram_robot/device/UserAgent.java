package jp.co.rilarc.instagram_robot.device;

import jp.co.rilarc.instagram_robot.utils.Const;

public class UserAgent {
    private static final String USER_AGENT_FORMAT = "Instagram %s Android (%s/%s; %s; %s; %s; %s; %s; %s; %s)";

    public static String buildUserAgent(Device device) {
        String manufacturerWithBrand = device.getManufacturer();
        if (device.getBrand() != null)
            manufacturerWithBrand += "/" + device.getBrand();

        return String.format(
                USER_AGENT_FORMAT,
                Const.VERSION,
                device.getAndroidVersion(),
                device.getAndroidRelease(),
                device.getDpi(),
                device.getResolution(),
                manufacturerWithBrand,
                device.getModel(),
                device.getDevice(),
                device.getCpu(),
                Const.USER_AGENT_LOCALE
        );
    }
}
