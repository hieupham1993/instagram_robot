package jp.co.rilarc.instagram_robot.device;

public class Device {
    private String mDeviceString;
    private String mAndroidVersion;
    private String mAndroidRelease;
    private String mDpi;
    private String mResolution;
    private String mManufacturer;
    private String mBrand;
    private String mModel;
    private String mDevice;
    private String mCpu;
    private String mUserAgent;

    public Device(String deviceString) {
        String[] parts = deviceString.split(";");
        String[] androidOS = parts[0].split("/", 2);
        String[] manufacturerAndBrand = parts[3].split("/", 2);

        mDeviceString = deviceString;
        mAndroidVersion = androidOS[0];
        mAndroidRelease = androidOS[1];
        mDpi = parts[1];
        mResolution = parts[2];
        mManufacturer = manufacturerAndBrand[0];
        mBrand = (manufacturerAndBrand.length >= 2) ? manufacturerAndBrand[1] : null;
        mModel = parts[4];
        mDevice = parts[5];
//        mDevice = parts[5];
        mCpu = parts[6];
    }

    public String getDeviceString() {
        return mDeviceString;
    }

    public void setDeviceString(String _deviceString) {
        this.mDeviceString = _deviceString;
    }

    public String getAndroidVersion() {
        return mAndroidVersion;
    }

    public void setAndroidVersion(String _androidVersion) {
        this.mAndroidVersion = _androidVersion;
    }

    public String getAndroidRelease() {
        return mAndroidRelease;
    }

    public void setAndroidRelease(String mAndroidRelease) {
        this.mAndroidRelease = mAndroidRelease;
    }

    public String getDpi() {
        return mDpi;
    }

    public void setDpi(String mDpi) {
        this.mDpi = mDpi;
    }

    public String getResolution() {
        return mResolution;
    }

    public void setResolution(String mResolution) {
        this.mResolution = mResolution;
    }

    public String getManufacturer() {
        return mManufacturer;
    }

    public void setManufacturer(String mManufacturer) {
        this.mManufacturer = mManufacturer;
    }

    public String getBrand() {
        return mBrand;
    }

    public void setBrand(String mBrand) {
        this.mBrand = mBrand;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String mModel) {
        this.mModel = mModel;
    }

    public String getDevice() {
        return mDevice;
    }

    public void setDevice(String mDevice) {
        this.mDevice = mDevice;
    }

    public String getCpu() {
        return mCpu;
    }

    public void setCpu(String mCpu) {
        this.mCpu = mCpu;
    }

    public String getUserAgent() {
        return mUserAgent;
    }

    public void setUserAgent(String mUserAgent) {
        this.mUserAgent = mUserAgent;
    }
}
