package jp.co.rilarc.instagram_robot;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.chrome.ChromeDriverService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.gui.splash.SplashStage;
import jp.co.rilarc.instagram_robot.utils.Utils;

public class Main extends Application {

    public interface IApplicationStateChangeListener {

        default void onAppStart() {
        }

        default void onAppStopping() {
        }
    }

    private static final LocalDatabaseHelper DATABASE;

    static {
        String userDir = System.getProperty("user.dir");
        setupWebDriver(userDir);
        DATABASE = LocalDatabaseHelper.getInstance();
    }

    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    private List<IApplicationStateChangeListener> stateListeners;

    private InstagramDb instagramDb;

    @Override
    public void start(Stage primaryStage) {
        setInstance(this);
        stateListeners = new ArrayList<>();
        new SplashStage().show();
        notifyAppStarted();
    }

    @Override
    public void stop() {
        DATABASE.closeConnection();
        notifyAppStopping();
    }

    private void setInstance(Main instance) {
        Main.instance = instance;
    }

    public void registerAppStateChangeListener(IApplicationStateChangeListener listener) {
        if (!stateListeners.contains(listener)) {
            stateListeners.add(listener);
        }
    }

    private void notifyAppStarted() {
        List<IApplicationStateChangeListener> listeners = new ArrayList<>(stateListeners);
        for (IApplicationStateChangeListener listener : listeners) {
            if (listener != null) {
                listener.onAppStart();
            }
        }
    }

    private void notifyAppStopping() {
        List<IApplicationStateChangeListener> listeners = new ArrayList<>(stateListeners);
        for (IApplicationStateChangeListener listener : listeners) {
            if (listener != null) {
                listener.onAppStopping();
            }
        }
    }

    public InstagramDb getInstagramDb() {
        return instagramDb;
    }

    public void setInstagramDb(InstagramDb instagramDb) {
        this.instagramDb = instagramDb;
    }

    private static void setupWebDriver(String userDir) {
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, userDir + "/webdriver/chromedriver.exe");
        } else if (SystemUtils.IS_OS_MAC) {
            File webDriver = new File(userDir, "/webdriver/chromedriver");
            Utils.grantFilePermission(webDriver);
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, userDir + "/webdriver/chromedriver");
        }
    }

    public static void main(String[] args) {
        DATABASE.createTable();
        launch(args);
    }
}
