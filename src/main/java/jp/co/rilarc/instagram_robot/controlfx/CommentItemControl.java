package jp.co.rilarc.instagram_robot.controlfx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import jp.co.rilarc.instagram_robot.api.IWssjServices;
import jp.co.rilarc.instagram_robot.api.requests.WssjCancelCommentRequest;
import jp.co.rilarc.instagram_robot.api.responses.WssjBaseResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleCommentResponse;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.gui.ConfirmAlertDialog;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.ImageCache;
import jp.co.rilarc.instagram_robot.utils.RequestQueue;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 1/18/2018.
 */
public class CommentItemControl extends StackPane implements Initializable {

    public interface IDeleteButtonClickListener {

        void onClicked(int messageId);
    }

    @FXML
    private StackPane post_image_box;

    @FXML
    private Label status_label;

    @FXML
    private HBox action_panel;

    @FXML
    private StackPane instagram_target_link_button;

    @FXML
    private StackPane delete_button;

    @FXML
    private StackPane stop_schedule_button;

    @FXML
    private Label title_label;

    @FXML
    private ImageView instagram_profile_pic_image_view;

    @FXML
    private Label instagram_username_label;

    @FXML
    private ImageView post_picture_image_view;

    @FXML
    private Label comment_label;

    @FXML
    private Label time_label;

    private IDeleteButtonClickListener deleteListener;

    private SimpleIntegerProperty commentIdProperty;

    private WssjGetListScheduleCommentResponse.Comment commentItem;

    public CommentItemControl() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/layout/layout_comment_item.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            commentIdProperty = new SimpleIntegerProperty();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void updateItem(WssjGetListScheduleCommentResponse.Comment item) {
        commentItem = item;
        commentIdProperty.set(item.getId());
        setSendTime(item.getSendAt());
        setInstagramTargetUser(item.getInstagramUsername());
        setStatus(item.getStatus());
        setProfilePicImage(item.getInstagramProfilePic());
        setPostPicImage(item.getInstagramPostPic());
        setComment(item.getComment());
    }

    public void setTitle(String text) {
        title_label.setText(text);
    }

    public String getTitle() {
        return title_label.getText();
    }

    public void setSendTime(String sendTime) {
        time_label.setText(sendTime);
    }

    public String getSendTime() {
        return time_label.getText();
    }

    public void setInstagramTargetUser(String username) {
        instagram_username_label.setText(username);
    }

    public String getInstagramTargetUser() {
        return instagram_username_label.getText();
    }

    public void setProfilePicImage(String url) {
        instagram_profile_pic_image_view.setImage(ImageCache.getInstance().getImage(url));
    }

    public void setPostPicImage(String url) {
        post_picture_image_view.setImage(ImageCache.getInstance().getImage(url));
    }

    public void setComment(String comment) {
        comment_label.setText(comment);
        Tooltip.install(comment_label, new Tooltip(comment));
    }

    public void setDeleteListener(IDeleteButtonClickListener deleteListener) {
        this.deleteListener = deleteListener;
    }

    /**
     * status can be one of these value
     * {@link ScheduleSendMessage#STATUS_WAITING}
     * {@link ScheduleSendMessage#STATUS_RUNNING}
     * {@link ScheduleSendMessage#STATUS_FINISH}
     * {@link ScheduleSendMessage#STATUS_CANCEL}
     *
     * @param status
     */
    public void setStatus(int status) {
        commentItem.setStatus(status);
        Platform.runLater(() -> {
            status_label.getStyleClass().removeIf(s -> s.startsWith("status-"));
            switch (status) {
                case ScheduleSendMessage.STATUS_FINISH:
                    status_label.setText(Const.SCHEDULE_STATUS_FINISHED_TEXT);
                    status_label.getStyleClass().add("status-finish");
                    stop_schedule_button.setVisible(false);
                    break;
                case ScheduleSendMessage.STATUS_RUNNING:
                    status_label.setText(Const.SCHEDULE_STATUS_RUNNING_TEXT);
                    status_label.getStyleClass().add("status-running");
                    stop_schedule_button.setVisible(true);
                    break;
                case ScheduleSendMessage.STATUS_CANCEL:
                    status_label.setText(Const.SCHEDULE_STATUS_CANCEL_TEXT);
                    status_label.getStyleClass().add("status-cancel");
                    stop_schedule_button.setVisible(false);
                    break;
                default:
                    status_label.setText(Const.SCHEDULE_STATUS_WAITING_TEXT);
                    status_label.getStyleClass().add("status-waiting");
                    stop_schedule_button.setVisible(true);
                    break;
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        stop_schedule_button.managedProperty().bindBidirectional(stop_schedule_button.visibleProperty());
        title_label.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_URL + "/" + title_label.getText()));
        instagram_profile_pic_image_view.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_URL + "/" + commentItem.getInstagramUsername()));
        instagram_username_label.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_URL + "/" + commentItem.getInstagramUsername()));
        instagram_target_link_button.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_POST_URL + commentItem.getPostCode()));
        post_image_box.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_POST_URL + commentItem.getPostCode()));
        status_label.setOnMouseClicked(Event::consume);
        stop_schedule_button.setOnMouseClicked(event -> {
            Preference preference = LocalDatabaseHelper.getInstance().getPreference();
            WssjCancelCommentRequest requestBody = new WssjCancelCommentRequest(commentIdProperty.get());
            RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.cancelComment(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

                @Override
                public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                    WssjBaseResponse<Object> responseBody = response.body();
                    if (responseBody != null && responseBody.isSuccess()) {
                        setStatus(ScheduleSendMessage.STATUS_CANCEL);
                    } else {
                        showDialog("警告", "Something went wrong. Please try again later", null);
                    }
                }

                @Override
                public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                    showDialog("警告", "Something went wrong. Please try again later", null);
                }
            });
        });
        delete_button.setOnMouseClicked(event -> {
            showDialog(Alert.AlertType.CONFIRMATION, "警告", "削除してもよろしいですか", new ConfirmAlertDialog.IConfirmAlertDialogResult() {

                @Override
                public void onOk() {
                    Preference preference = LocalDatabaseHelper.getInstance().getPreference();
                    WssjCancelCommentRequest requestBody = new WssjCancelCommentRequest(commentIdProperty.get());
                    RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.deleteComment(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

                        @Override
                        public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                            WssjBaseResponse<Object> responseBody = response.body();
                            if (responseBody != null && responseBody.isSuccess()) {
                                if (deleteListener != null) {
                                    deleteListener.onClicked(requestBody.getCommentId());
                                }
                            } else {
                                showDialog("警告", "Something went wrong. Please try again later", null);
                            }
                        }

                        @Override
                        public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                            showDialog("警告", "Something went wrong. Please try again later", null);
                        }
                    });
                }
            });
        });
    }

    private void showDialog(String title, String description, ConfirmAlertDialog.IConfirmAlertDialogResult callback) {
        showDialog(Alert.AlertType.INFORMATION, title, description, callback);
    }

    private void showDialog(Alert.AlertType alertType, String title, String description, ConfirmAlertDialog.IConfirmAlertDialogResult callback) {
        Platform.runLater(() -> new ConfirmAlertDialog(stop_schedule_button.getScene().getWindow(), alertType, title, description, callback));
    }
}
