package jp.co.rilarc.instagram_robot.controlfx;

import org.controlsfx.control.GridCell;

import javafx.application.Platform;
import javafx.scene.Node;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;

/**
 * Created by HieuPT on 1/18/2018.
 */
public class MessageCell extends GridCell<WssjGetListScheduleMessageResponse.Message> {

    private MessageItemControl itemControl;

    private Preference preference;

    public MessageCell() {
        itemControl = new MessageItemControl();
        preference = LocalDatabaseHelper.getInstance().getPreference();
    }

    @Override
    protected void updateItem(WssjGetListScheduleMessageResponse.Message item, boolean empty) {
        super.updateItem(item, empty);
        Node node = null;
        if (!empty && item != null) {
            itemControl.updateItem(item);
            itemControl.setTitle(preference.getInstagramUsername());
            itemControl.setDeleteListener(messageId -> Platform.runLater(() -> getGridView().getItems().removeIf(message -> message.getId() == messageId)));
            node = itemControl;
        }
        setGraphic(node);
    }
}
