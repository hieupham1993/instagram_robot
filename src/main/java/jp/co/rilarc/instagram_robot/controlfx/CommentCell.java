package jp.co.rilarc.instagram_robot.controlfx;

import org.controlsfx.control.GridCell;

import javafx.application.Platform;
import javafx.scene.Node;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleCommentResponse;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;

/**
 * Created by HieuPT on 1/18/2018.
 */
public class CommentCell extends GridCell<WssjGetListScheduleCommentResponse.Comment> {

    private CommentItemControl itemControl;

    private Preference preference;

    public CommentCell() {
        itemControl = new CommentItemControl();
        preference = LocalDatabaseHelper.getInstance().getPreference();
    }

    @Override
    protected void updateItem(WssjGetListScheduleCommentResponse.Comment item, boolean empty) {
        super.updateItem(item, empty);
        Node node = null;
        if (!empty && item != null) {
            itemControl.updateItem(item);
            itemControl.setTitle(preference.getInstagramUsername());
            itemControl.setDeleteListener(commentId -> Platform.runLater(() -> getGridView().getItems().removeIf(comment -> comment.getId() == commentId)));
            node = itemControl;
        }
        setGraphic(node);
    }
}
