package jp.co.rilarc.instagram_robot.controlfx;

import org.joda.time.DateTime;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import jp.co.rilarc.instagram_robot.api.IWssjServices;
import jp.co.rilarc.instagram_robot.api.requests.WssjCancelMessageRequest;
import jp.co.rilarc.instagram_robot.api.responses.WssjBaseResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse;
import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.gui.ConfirmAlertDialog;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.RequestQueue;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 1/18/2018.
 */
public class MessageItemControl extends StackPane implements Initializable {

    public interface IDeleteButtonClickListener {

        void onClicked(int messageId);
    }

    @FXML
    private Label message_label;

    @FXML
    private Label status_label;

    @FXML
    private Label instagram_target_label;

    @FXML
    private HBox action_panel;

    @FXML
    private StackPane instagram_target_link_button;

    @FXML
    private StackPane delete_button;

    @FXML
    private StackPane stop_schedule_button;

    @FXML
    private StackPane reactive_schedule_button;

    @FXML
    private Label title_label;

    @FXML
    private Label time_label;

    private IDeleteButtonClickListener deleteListener;

    private SimpleIntegerProperty messageIdProperty;

    private WssjGetListScheduleMessageResponse.Message messageItem;

    private long sendTimeMillis;

    public MessageItemControl() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/layout/layout_message_item.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            messageIdProperty = new SimpleIntegerProperty();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void updateItem(WssjGetListScheduleMessageResponse.Message item) {
        messageItem = item;
        messageIdProperty.set(item.getId());
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(item.getSendAt());
            sendTimeMillis = date.getTime();
        } catch (ParseException e) {
            sendTimeMillis = 0;
        }
        setSendTime(item.getSendAt());
        setMessage(item.getMessage());
        setInstagramTargetUser(item.getInstagramName());
        setStatus(item.getStatus());
    }

    public void setTitle(String text) {
        title_label.setText(text);
    }

    public String getTitle() {
        return title_label.getText();
    }

    public void setSendTime(String sendTime) {
        time_label.setText(sendTime);
    }

    public String getSendTime() {
        return time_label.getText();
    }

    public void setMessage(String message) {
        message_label.setText(message);
        Tooltip.install(message_label, new Tooltip(message));
    }

    public void setInstagramTargetUser(String username) {
        instagram_target_label.setText(username);
    }

    public String getInstagramTargetUser() {
        return instagram_target_label.getText();
    }

    public void setDeleteListener(IDeleteButtonClickListener deleteListener) {
        this.deleteListener = deleteListener;
    }

    /**
     * status can be one of these value
     * {@link ScheduleSendMessage#STATUS_WAITING}
     * {@link ScheduleSendMessage#STATUS_RUNNING}
     * {@link ScheduleSendMessage#STATUS_FINISH}
     * {@link ScheduleSendMessage#STATUS_CANCEL}
     *
     * @param status
     */
    public void setStatus(int status) {
        messageItem.setStatus(status);
        Platform.runLater(() -> {
            status_label.getStyleClass().removeIf(s -> s.startsWith("status-"));
            switch (status) {
                case ScheduleSendMessage.STATUS_FINISH:
                    status_label.setText(Const.SCHEDULE_STATUS_FINISHED_TEXT);
                    status_label.getStyleClass().add("status-finish");
                    stop_schedule_button.setVisible(false);
                    reactive_schedule_button.setVisible(false);
                    break;
                case ScheduleSendMessage.STATUS_RUNNING:
                    status_label.setText(Const.SCHEDULE_STATUS_RUNNING_TEXT);
                    status_label.getStyleClass().add("status-running");
                    stop_schedule_button.setVisible(true);
                    reactive_schedule_button.setVisible(false);
                    break;
                case ScheduleSendMessage.STATUS_CANCEL:
                    status_label.setText(Const.SCHEDULE_STATUS_CANCEL_TEXT);
                    status_label.getStyleClass().add("status-cancel");
                    stop_schedule_button.setVisible(false);
                    reactive_schedule_button.setVisible(sendTimeMillis > DateTime.now().getMillis());
                    break;
                default:
                    status_label.setText(Const.SCHEDULE_STATUS_WAITING_TEXT);
                    status_label.getStyleClass().add("status-waiting");
                    stop_schedule_button.setVisible(true);
                    reactive_schedule_button.setVisible(false);
                    break;
            }
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        stop_schedule_button.managedProperty().bindBidirectional(stop_schedule_button.visibleProperty());
        reactive_schedule_button.managedProperty().bindBidirectional(reactive_schedule_button.visibleProperty());
        title_label.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_URL + "/" + title_label.getText()));
        instagram_target_link_button.setOnMouseClicked(event -> Utils.openBrowserSystem(InstagramWebController.INSTAGRAM_BASE_URL + "/" + instagram_target_label.getText()));
        stop_schedule_button.setOnMouseClicked(event -> {
            Preference preference = LocalDatabaseHelper.getInstance().getPreference();
            WssjCancelMessageRequest requestBody = new WssjCancelMessageRequest(messageIdProperty.get());
            RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.cancelMessage(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

                @Override
                public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                    WssjBaseResponse<Object> responseBody = response.body();
                    if (responseBody != null && responseBody.isSuccess()) {
                        setStatus(ScheduleSendMessage.STATUS_CANCEL);
                    } else {
                        showDialog("警告", "Something went wrong. Please try again later", null);
                    }
                }

                @Override
                public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                    showDialog("警告", "Something went wrong. Please try again later", null);
                }
            });
        });
        reactive_schedule_button.setOnMouseClicked(event -> {
            Preference preference = LocalDatabaseHelper.getInstance().getPreference();
            WssjCancelMessageRequest requestBody = new WssjCancelMessageRequest(messageIdProperty.get());
            RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.reactiveMessage(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

                @Override
                public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                    WssjBaseResponse<Object> responseBody = response.body();
                    if (responseBody != null && responseBody.isSuccess()) {
                        setStatus(ScheduleSendMessage.STATUS_WAITING);
                    } else {
                        showDialog("警告", "Something went wrong. Please try again later", null);
                    }
                }

                @Override
                public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                    showDialog("警告", "Something went wrong. Please try again later", null);
                }
            });
        });
        delete_button.setOnMouseClicked(event -> showDialog(Alert.AlertType.CONFIRMATION, "警告", "削除してもよろしいですか", new ConfirmAlertDialog.IConfirmAlertDialogResult() {

            @Override
            public void onOk() {
                Preference preference = LocalDatabaseHelper.getInstance().getPreference();
                WssjCancelMessageRequest requestBody = new WssjCancelMessageRequest(messageIdProperty.get());
                RequestQueue.getInstance().addRequest(IWssjServices.SERVICES.deleteMessage(preference.getAccessToken(), requestBody), new Callback<WssjBaseResponse<Object>>() {

                    @Override
                    public void onResponse(Call<WssjBaseResponse<Object>> call, Response<WssjBaseResponse<Object>> response) {
                        WssjBaseResponse<Object> responseBody = response.body();
                        if (responseBody != null && responseBody.isSuccess()) {
                            if (deleteListener != null) {
                                deleteListener.onClicked(requestBody.getMessageId());
                            }
                        } else {
                            showDialog("警告", "Something went wrong. Please try again later", null);
                        }
                    }

                    @Override
                    public void onFailure(Call<WssjBaseResponse<Object>> call, Throwable t) {
                        showDialog("警告", "Something went wrong. Please try again later", null);
                    }
                });
            }
        }));
    }

    private void showDialog(String title, String description, ConfirmAlertDialog.IConfirmAlertDialogResult callback) {
        showDialog(Alert.AlertType.INFORMATION, title, description, callback);
    }

    private void showDialog(Alert.AlertType alertType, String title, String description, ConfirmAlertDialog.IConfirmAlertDialogResult callback) {
        Platform.runLater(() -> new ConfirmAlertDialog(stop_schedule_button.getScene().getWindow(), alertType, title, description, callback));
    }
}
