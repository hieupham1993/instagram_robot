package jp.co.rilarc.instagram_robot.presenters;

import org.joda.time.DateTime;

import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.api.responses.WssjLoginResponse;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.gui.login.ILoginView;
import jp.co.rilarc.instagram_robot.models.AuthModel;
import jp.co.rilarc.instagram_robot.models.PreferencesModel;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class LoginPresenter {

    private final ILoginView view;

    private final AuthModel authModel;

    private final PreferencesModel preferencesModel;

    public LoginPresenter(ILoginView view) {
        this.view = view;
        authModel = new AuthModel();
        preferencesModel = new PreferencesModel();
    }

    public void onStart() {
        preferencesModel.checkAutoLogin(new PreferencesModel.ICheckAutoLoginResponse() {

            @Override
            public void onEnabled(String email, String password) {
                view.setAutoLoginEnable(true);
            }

            @Override
            public void onDisabled() {
                view.setAutoLoginEnable(false);
            }
        });
        preferencesModel.checkAutoStart(new PreferencesModel.ICheckAutoStartResponse() {

            @Override
            public void onEnabled() {
                view.setAutoStartEnable(true);
            }

            @Override
            public void onDisabled() {
                view.setAutoStartEnable(false);
            }
        });
        preferencesModel.getLoginInformation(view::fillLoginInfo);
    }

    public void onLoginButtonClicked(String email, String password, boolean isAutoLogin, boolean isAutoStart) {
        view.showProgress(true);
        view.enableLoginButton(false);
        authModel.validateLoginInfo(email, password, new AuthModel.IValidateLoginInfoCallback() {

            @Override
            public void onValid(String email, String password) {
                authModel.login(email, password, new AuthModel.ILoginCallback() {

                    @Override
                    public void onSuccess(WssjLoginResponse.Data response) {
                        preferencesModel.updatePreference(email, password, response.getId(),
                                response.getInstagramName(), isAutoLogin, isAutoStart,
                                "Bearer " + response.getAccessToken(), response.getTokenType(),
                                DateTime.now().plusMinutes(response.getExpireIn()).getMillis());
                        preferencesModel.updateInstagram(response.getId(), response.getInstagramName(), response.getInstagramPassword(),
                                response.getInstagramUserId(), response.getInstagramAvatar());
                        preferencesModel.getInstagramDb(response.getId(), response.getInstagramName(), new PreferencesModel.IGetInstagramDbResponse() {

                            @Override
                            public void onSuccess(InstagramDb instagramDb) {
                                Main.getInstance().setInstagramDb(instagramDb);
                            }

                            @Override
                            public void onFailure() {
                                Main.getInstance().setInstagramDb(new InstagramDb());
                            }
                        });
                        view.displayMainScreen();
                    }

                    @Override
                    public void onFailure(String message) {
                        view.showProgress(false);
                        view.enableLoginButton(true);
                        view.showLoginFailureDialog(message);
                    }
                });
            }

            @Override
            public void onInvalid(String message) {
                view.showProgress(false);
                view.enableLoginButton(true);
                view.showLoginFailureDialog(message);
            }
        });
    }
}
