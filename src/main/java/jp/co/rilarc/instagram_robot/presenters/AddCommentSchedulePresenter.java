package jp.co.rilarc.instagram_robot.presenters;

import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import jp.co.rilarc.instagram_robot.gui.commentsetting.addschedule.IAddCommentScheduleView;
import jp.co.rilarc.instagram_robot.model.InstagramArticleTableItem;
import jp.co.rilarc.instagram_robot.models.CommentSettingModel;
import jp.co.rilarc.instagram_robot.models.InstagramDataModel;

/**
 * Created by HieuPT on 1/22/2018.
 */
public class AddCommentSchedulePresenter {

    private static final Logger LOGGER = LogManager.getLogger(AddCommentSchedulePresenter.class);

    private final IAddCommentScheduleView view;

    private final InstagramDataModel instagramModel;

    private final CommentSettingModel commentModel;

    public AddCommentSchedulePresenter(IAddCommentScheduleView view) {
        this.view = view;
        this.instagramModel = new InstagramDataModel();
        this.commentModel = new CommentSettingModel();
    }

    public void onTimelineButtonClicked() {
        instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

            @Override
            public void onConnected() {
                view.showInstagramDataTable(false);
                view.showTableViewProgress(true);
                instagramModel.getTimelineArticles(null, new InstagramDataModel.IGetArticlesCallback() {

                    @Override
                    public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(items, searchKeyword, nextMaxId);
                    }

                    @Override
                    public void onFailure() {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(new ArrayList<>(), null, null);
                    }
                });
            }

            @Override
            public void onDisconnected() {
                LOGGER.info("Not connect to instagram app");
            }
        });
    }

    public void onLoadMoreTimeLine(String maxId) {
        if (!TextUtils.isEmpty(maxId)) {
            instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

                @Override
                public void onConnected() {
                    view.showTableViewProgress(true);
                    instagramModel.getTimelineArticles(maxId, new InstagramDataModel.IGetArticlesCallback() {

                        @Override
                        public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                            view.showTableViewProgress(false);
                            view.addMoreInstagramArticleData(items, searchKeyword, nextMaxId);
                        }

                        @Override
                        public void onFailure() {
                            view.showTableViewProgress(false);
                        }
                    });
                }

                @Override
                public void onDisconnected() {
                    LOGGER.info("Instagram api disconnected");
                }
            });
        }
    }

    public void onRegisterScheduleButtonClicked(List<InstagramArticleTableItem> userItems, String comment, long sendTimeMillis, int delay) {
        commentModel.validateAddScheduleRequestBody(userItems, comment, sendTimeMillis, delay, new CommentSettingModel.IValidateAddScheduleCommentRequestCallback() {

            @Override
            public void onValid(List<InstagramArticleTableItem> items, String comment, long sendAt, int delay) {
                view.showProgress(true);
                commentModel.addScheduleComment(items, comment, sendAt, delay, new CommentSettingModel.IAddScheduleCommentCallback() {

                    @Override
                    public void onSuccess() {
                        view.showProgress(false);
                        view.notifyCommentSettingRefresh();
                        view.closeDialog();
                    }

                    @Override
                    public void onFailure(String message) {
                        view.showProgress(false);
                        view.showMessageDialog(message);
                    }
                });
            }

            @Override
            public void onInvalid(String message) {
                view.showMessageDialog(message);
            }
        });
    }

    public void onSearchButtonClicked(String hashtag) {
        instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

            @Override
            public void onConnected() {
                view.showInstagramDataTable(false);
                view.showTableViewProgress(true);
                instagramModel.getHastagArticles(hashtag, null, new InstagramDataModel.IGetArticlesCallback() {

                    @Override
                    public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(items, searchKeyword, nextMaxId);
                    }

                    @Override
                    public void onFailure() {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(new ArrayList<>(), null, null);
                    }
                });
            }

            @Override
            public void onDisconnected() {
                LOGGER.info("Not connect to instagram app");
            }
        });
    }

    public void onLoadMoreHashtag(String searchKeyword, String nextMaxId) {
        if (!TextUtils.isEmpty(searchKeyword) && !TextUtils.isEmpty(nextMaxId)) {
            instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

                @Override
                public void onConnected() {
                    view.showTableViewProgress(true);
                    instagramModel.getHastagArticles(searchKeyword, nextMaxId, new InstagramDataModel.IGetArticlesCallback() {

                        @Override
                        public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                            view.showTableViewProgress(false);
                            view.addMoreInstagramArticleData(items, searchKeyword, nextMaxId);
                        }

                        @Override
                        public void onFailure() {
                            view.showTableViewProgress(false);
                        }
                    });
                }

                @Override
                public void onDisconnected() {
                    LOGGER.info("Instagram api disconnected");
                }
            });
        }
    }

    public void onSelfPostButtonClicked() {
        instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

            @Override
            public void onConnected() {
                view.showInstagramDataTable(false);
                view.showTableViewProgress(true);
                instagramModel.getSelfArticles(null, new InstagramDataModel.IGetArticlesCallback() {

                    @Override
                    public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(items, searchKeyword, nextMaxId);
                    }

                    @Override
                    public void onFailure() {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(new ArrayList<>(), null, null);
                    }
                });
            }

            @Override
            public void onDisconnected() {
                LOGGER.info("Not connect to instagram app");
            }
        });
    }

    public void onLoadMoreSelfPost(String nextMaxId) {
        if (!TextUtils.isEmpty(nextMaxId)) {
            instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

                @Override
                public void onConnected() {
                    view.showTableViewProgress(true);
                    instagramModel.getSelfArticles(nextMaxId, new InstagramDataModel.IGetArticlesCallback() {

                        @Override
                        public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                            view.showTableViewProgress(false);
                            view.addMoreInstagramArticleData(items, searchKeyword, nextMaxId);
                        }

                        @Override
                        public void onFailure() {
                            view.showTableViewProgress(false);
                        }
                    });
                }

                @Override
                public void onDisconnected() {
                    LOGGER.info("Instagram api disconnected");
                }
            });
        }
    }

    public void onMostLikeButtonClicked() {
        instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

            @Override
            public void onConnected() {
                view.showInstagramDataTable(false);
                view.showTableViewProgress(true);
                instagramModel.getPopularArticles(new InstagramDataModel.IGetArticlesCallback() {

                    @Override
                    public void onSuccess(List<InstagramArticleTableItem> items, String searchKeyword, String nextMaxId) {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(items, searchKeyword, nextMaxId);
                    }

                    @Override
                    public void onFailure() {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramArticleData(new ArrayList<>(), null, null);
                    }
                });
            }

            @Override
            public void onDisconnected() {
                LOGGER.info("Not connect to instagram app");
            }
        });
    }
}
