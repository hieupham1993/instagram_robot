package jp.co.rilarc.instagram_robot.presenters;

import org.apache.http.util.TextUtils;
import org.openqa.selenium.Cookie;

import java.util.Set;

import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.gui.main.IMainView;
import jp.co.rilarc.instagram_robot.models.PreferencesModel;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class MainPresenter {

    private IMainView view;

    private PreferencesModel preferencesModel;

    public MainPresenter(IMainView view) {
        this.view = view;
        preferencesModel = new PreferencesModel();
    }

    public void onLogoutButtonClicked() {
        view.showLogoutConfirmDialog();
    }

    public void onLogoutConfirmed() {
        view.displayLoginScreen();
    }

    public void onStart() {
        view.setLoginState(false);
        InstagramDb cachedInstagramDb = Main.getInstance().getInstagramDb();
        preferencesModel.getInstagramDb(cachedInstagramDb.getUserId(), cachedInstagramDb.getUsername(), new PreferencesModel.IGetInstagramDbResponse() {

            @Override
            public void onSuccess(InstagramDb instagramDb) {
                view.showProgress();
                view.fillLoginInfo(instagramDb.getUsername(), instagramDb.getPassword());
                view.loginToAppInstagram(instagramDb.getUsername(), instagramDb.getPassword(), false);
            }

            @Override
            public void onFailure() {
            }
        });
    }

    public void onInstagramButtonClicked() {
        view.openUrl(InstagramWebController.INSTAGRAM_BASE_URL);
    }

    public void onStartRobotButtonClicked() {
        view.startRobot();
        view.setRobotState(true);
    }

    public void onStopRobotButtonClicked() {
        view.stopRobot();
        view.setRobotState(false);
    }

    public void onOpenTaskSettingButtonClicked() {
        preferencesModel.getAccessToken(new PreferencesModel.IGetAccessTokenResponse() {

            @Override
            public void onSuccess(String token) {
                view.openUrl(Const.BASE_WSSJ_URL + "/user/login?token=" + token);
            }

            @Override
            public void onFailure() {

            }
        });

    }

    public void onLoginWebSuccess() {
        preferencesModel.getAccountInformation((systemEmail, instagramUsername, avatarUrl) -> view.fillProfileInfo(systemEmail, instagramUsername, avatarUrl));
        view.hideProgress();
        view.setLoginState(true);
        preferencesModel.checkAutoStart(new PreferencesModel.ICheckAutoStartResponse() {

            @Override
            public void onEnabled() {
                view.setRobotState(true);
                view.startRobot();
            }

            @Override
            public void onDisabled() {
                view.setRobotState(false);
            }
        });
    }

    public void onLoginFailure() {
        view.hideProgress();
        view.setLoginState(false);
    }

    public void onLoginAppSuccess(boolean forceLoginWeb) {
        InstagramDb cachedInstagramDb = Main.getInstance().getInstagramDb();
        if (forceLoginWeb) {
            view.loginToWebInstagram(cachedInstagramDb.getUsername(), cachedInstagramDb.getPassword());
        } else {
            preferencesModel.checkCookies(cachedInstagramDb.getUserId(), cachedInstagramDb.getUsername(), new PreferencesModel.ICheckCookiesResponse() {

                @Override
                public void onExist(Set<Cookie> cookies) {
                    preferencesModel.getAccountInformation((systemEmail, instagramUsername, avatarUrl) -> view.fillProfileInfo(systemEmail, instagramUsername, avatarUrl));
                    view.hideProgress();
                    view.setLoginState(true);
                    VLogger.append("アカウント【" + cachedInstagramDb.getUsername() + "】でインスタグラムにログインしました!");
                    preferencesModel.checkAutoStart(new PreferencesModel.ICheckAutoStartResponse() {

                        @Override
                        public void onEnabled() {
                            view.setRobotState(true);
                            view.startRobot();
                        }

                        @Override
                        public void onDisabled() {
                            view.setRobotState(false);
                        }
                    });
                }

                @Override
                public void onEmpty() {
                    view.loginToWebInstagram(cachedInstagramDb.getUsername(), cachedInstagramDb.getPassword());
                }
            });
        }
    }

    public void onCommentSettingButtonClicked() {
        view.showCommentSettingPanel();
    }

    public void onMessageSettingButtonClicked() {
        view.showMessageSettingPanel();
    }

    public void onBackButtonClicked() {
        view.hideSettingPanel();
    }

    public void onLoginButtonClicked(String username, String password) {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            view.showProgress();
            view.loginToAppInstagram(username, password, true);
        }
    }

    public void onChangeInstagramAccountButtonClicked() {
        view.showChangeAccountConfirmDialog();
    }

    public void onChangeInstagramAccountConfirmed() {
        view.stopRobot();
        view.clearLoginForm();
        view.setLoginState(false);
    }
}
