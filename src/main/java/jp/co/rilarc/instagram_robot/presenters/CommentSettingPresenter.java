package jp.co.rilarc.instagram_robot.presenters;

import java.util.ArrayList;
import java.util.List;

import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleCommentResponse;
import jp.co.rilarc.instagram_robot.gui.commentsetting.ICommentSettingView;
import jp.co.rilarc.instagram_robot.models.CommentSettingModel;

/**
 * Created by HieuPT on 1/18/2018.
 */
public class CommentSettingPresenter {

    private ICommentSettingView view;

    private CommentSettingModel commentSettingModel;

    public CommentSettingPresenter(ICommentSettingView view) {
        this.view = view;
        this.commentSettingModel = new CommentSettingModel();
    }

    public void onBackButtonClicked() {
        view.backToLogScreen();
    }

    public void getListScheduleMessage() {
        view.showProgress();
        commentSettingModel.getListScheduleComment(new CommentSettingModel.IGetListScheduleCommentCallback() {

            @Override
            public void onSuccess(List<WssjGetListScheduleCommentResponse.Comment> comments) {
                view.hideProgress();
                view.showNoItemLabel(false);
                view.showScheduleComment(comments);
            }

            @Override
            public void onEmpty() {
                view.hideProgress();
                view.showNoItemLabel(true);
            }

            @Override
            public void onFailure() {
                view.hideProgress();
                view.showNoItemLabel(true);
            }
        });
    }

    public void onAddNewButtonClicked() {
        view.showAddNewScheduleForm();
    }

    public void onDeleteAllScheduleButtonClicked() {
        view.showDeleteAllConfirmDialog();
    }

    public void onDeleteAllScheduleConfirmed() {
        commentSettingModel.deleteAllScheduleComment(new CommentSettingModel.IDeleteAllScheduleCommentCallback() {

            @Override
            public void onSuccess() {
                view.showNoItemLabel(true);
                view.showScheduleComment(new ArrayList<>());
            }

            @Override
            public void onFailure(String message) {
                view.showMessageDialog(message);
            }
        });
    }
}
