package jp.co.rilarc.instagram_robot.presenters;

import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import jp.co.rilarc.instagram_robot.gui.messagesetting.addschedule.IAddMessageScheduleView;
import jp.co.rilarc.instagram_robot.model.InstagramUserTableItem;
import jp.co.rilarc.instagram_robot.models.InstagramDataModel;
import jp.co.rilarc.instagram_robot.models.MessageSettingModel;

/**
 * Created by HieuPT on 1/22/2018.
 */
public class AddMessageSchedulePresenter {

    private static final Logger LOGGER = LogManager.getLogger(AddMessageSchedulePresenter.class);

    private final IAddMessageScheduleView view;

    private final InstagramDataModel instagramModel;

    private final MessageSettingModel messageModel;

    public AddMessageSchedulePresenter(IAddMessageScheduleView view) {
        this.view = view;
        this.instagramModel = new InstagramDataModel();
        this.messageModel = new MessageSettingModel();
    }

    public void onFollowingButtonClicked() {
        instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

            @Override
            public void onConnected() {
                view.showInstagramDataTable(false);
                view.showTableViewProgress(true);
                instagramModel.getFollowingList(null, new InstagramDataModel.IGetFollowListCallback() {

                    @Override
                    public void onSuccess(List<InstagramUserTableItem> items, String nextMaxId) {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramUserData(items, nextMaxId);
                    }

                    @Override
                    public void onFailure() {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramUserData(new ArrayList<>(), null);
                    }
                });
            }

            @Override
            public void onDisconnected() {
                LOGGER.info("Not connect to instagram app");
            }
        });
    }

    public void onFollowerButtonClicked() {
        instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

            @Override
            public void onConnected() {
                view.showInstagramDataTable(false);
                view.showTableViewProgress(true);
                instagramModel.getFollowerList(null, new InstagramDataModel.IGetFollowListCallback() {

                    @Override
                    public void onSuccess(List<InstagramUserTableItem> items, String nextMaxId) {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramUserData(items, nextMaxId);
                    }

                    @Override
                    public void onFailure() {
                        view.showInstagramDataTable(true);
                        view.showTableViewProgress(false);
                        view.showInstagramUserData(new ArrayList<>(), null);
                    }
                });
            }

            @Override
            public void onDisconnected() {
                LOGGER.info("Not connect to instagram app");
            }
        });
    }

    public void onRegisterScheduleButtonClicked(List<InstagramUserTableItem> userItems, String message, long sendAt, int delay) {
        messageModel.validateAddScheduleRequestBody(userItems, message, sendAt, delay, new MessageSettingModel.IValidateAddScheduleMessageRequestCallback() {

            @Override
            public void onValid(List<InstagramUserTableItem> userItems, String message, long sendAt, int delay) {
                view.showProgress(true);
                messageModel.addScheduleMessage(userItems, message, sendAt, delay, new MessageSettingModel.IAddScheduleMessageCallback() {

                    @Override
                    public void onSuccess() {
                        view.showProgress(false);
                        view.notifyMessageSettingRefresh();
                        view.closeDialog();
                    }

                    @Override
                    public void onFailure(String message) {
                        view.showProgress(false);
                        view.showMessageDialog(message);
                    }
                });
            }

            @Override
            public void onInvalid(String message) {
                view.showMessageDialog(message);
            }
        });
    }

    public void onLoadMoreFollowing(String maxId) {
        if (!TextUtils.isEmpty(maxId)) {
            instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

                @Override
                public void onConnected() {
                    view.showTableViewProgress(true);
                    instagramModel.getFollowingList(maxId, new InstagramDataModel.IGetFollowListCallback() {

                        @Override
                        public void onSuccess(List<InstagramUserTableItem> items, String nextMaxId) {
                            view.showTableViewProgress(false);
                            view.addMoreInstagramUserData(items, nextMaxId);
                        }

                        @Override
                        public void onFailure() {
                            view.showTableViewProgress(false);
                        }
                    });
                }

                @Override
                public void onDisconnected() {

                }
            });
        }
    }

    public void onLoadMoreFollowers(String maxId) {
        if (!TextUtils.isEmpty(maxId)) {
            instagramModel.checkInstagramConnection(new InstagramDataModel.ICheckInstagramConnectionCallback() {

                @Override
                public void onConnected() {
                    view.showTableViewProgress(true);
                    instagramModel.getFollowerList(maxId, new InstagramDataModel.IGetFollowListCallback() {

                        @Override
                        public void onSuccess(List<InstagramUserTableItem> items, String nextMaxId) {
                            view.showTableViewProgress(false);
                            view.addMoreInstagramUserData(items, nextMaxId);
                        }

                        @Override
                        public void onFailure() {
                            view.showTableViewProgress(false);
                        }
                    });
                }

                @Override
                public void onDisconnected() {

                }
            });
        }
    }
}
