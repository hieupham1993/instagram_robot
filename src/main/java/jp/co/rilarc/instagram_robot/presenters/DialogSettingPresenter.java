package jp.co.rilarc.instagram_robot.presenters;

import jp.co.rilarc.instagram_robot.gui.dialogsetting.IDialogSettingView;
import jp.co.rilarc.instagram_robot.models.PreferencesModel;

/**
 * Created by HieuPT on 1/4/2018.
 */
public class DialogSettingPresenter {

    private final IDialogSettingView view;

    private final PreferencesModel preferencesModel;

    public DialogSettingPresenter(IDialogSettingView view) {
        this.view = view;
        preferencesModel = new PreferencesModel();
    }

    public void onStart() {
        preferencesModel.checkAutoLogin(new PreferencesModel.ICheckAutoLoginResponse() {

            @Override
            public void onEnabled(String email, String password) {
                view.setAutoLoginEnable(true);
            }

            @Override
            public void onDisabled() {
                view.setAutoLoginEnable(false);
            }
        });
        preferencesModel.checkAutoStart(new PreferencesModel.ICheckAutoStartResponse() {

            @Override
            public void onEnabled() {
                view.setAutoStartEnable(true);
            }

            @Override
            public void onDisabled() {
                view.setAutoStartEnable(false);
            }
        });
    }

    public void onSaveButtonClicked(boolean isAutoLogin, boolean isAutoStart) {
        preferencesModel.updateAutoLogin(isAutoLogin);
        preferencesModel.updateAutoStart(isAutoStart);
        view.closeDialog();
    }
}
