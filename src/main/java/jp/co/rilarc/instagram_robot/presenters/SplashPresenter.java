package jp.co.rilarc.instagram_robot.presenters;

import org.joda.time.DateTime;

import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.api.responses.WssjLoginResponse;
import jp.co.rilarc.instagram_robot.database.model.InstagramDb;
import jp.co.rilarc.instagram_robot.gui.splash.ISplashView;
import jp.co.rilarc.instagram_robot.models.AuthModel;
import jp.co.rilarc.instagram_robot.models.PreferencesModel;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class SplashPresenter {

    private final ISplashView view;

    private final AuthModel authModel;

    private final PreferencesModel preferencesModel;

    public SplashPresenter(ISplashView view) {
        this.view = view;
        preferencesModel = new PreferencesModel();
        authModel = new AuthModel();
    }

    public void onStart() {
        preferencesModel.checkAutoLogin(new PreferencesModel.ICheckAutoLoginResponse() {

            @Override
            public void onEnabled(String email, String password) {
                authModel.login(email, password, new AuthModel.ILoginCallback() {

                    @Override
                    public void onSuccess(WssjLoginResponse.Data response) {
                        preferencesModel.updatePreference(email, password, response.getId(),
                                response.getInstagramName(), "Bearer " + response.getAccessToken(),
                                response.getTokenType(), DateTime.now().plusMinutes(response.getExpireIn()).getMillis());
                        preferencesModel.updateInstagram(response.getId(), response.getInstagramName(), response.getInstagramPassword(),
                                response.getInstagramUserId(), response.getInstagramAvatar());
                        preferencesModel.getInstagramDb(response.getId(), response.getInstagramName(), new PreferencesModel.IGetInstagramDbResponse() {

                            @Override
                            public void onSuccess(InstagramDb instagramDb) {
                                Main.getInstance().setInstagramDb(instagramDb);
                            }

                            @Override
                            public void onFailure() {
                                Main.getInstance().setInstagramDb(new InstagramDb());
                            }
                        });
                        view.displayMainScreen();
                    }

                    @Override
                    public void onFailure(String message) {
                        view.displayLoginScreen();
                    }
                });
            }

            @Override
            public void onDisabled() {
                view.displayLoginScreen();
            }
        });
    }
}
