package jp.co.rilarc.instagram_robot.presenters;

import java.util.ArrayList;
import java.util.List;

import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse;
import jp.co.rilarc.instagram_robot.gui.messagesetting.IMessageSettingView;
import jp.co.rilarc.instagram_robot.models.MessageSettingModel;

/**
 * Created by HieuPT on 1/18/2018.
 */
public class MessageSettingPresenter {

    private IMessageSettingView view;

    private MessageSettingModel messageSettingModel;

    public MessageSettingPresenter(IMessageSettingView view) {
        this.view = view;
        this.messageSettingModel = new MessageSettingModel();
    }

    public void onBackButtonClicked() {
        view.backToLogScreen();
    }

    public void getListScheduleMessage() {
        view.showProgress();
        messageSettingModel.getListScheduleMessage(new MessageSettingModel.IGetListScheduleMessageCallback() {

            @Override
            public void onSuccess(List<WssjGetListScheduleMessageResponse.Message> messages) {
                view.hideProgress();
                view.showNoItemLabel(false);
                view.enableDeleteAllButton(true);
                view.showScheduleMessage(messages);
            }

            @Override
            public void onEmpty() {
                view.hideProgress();
                view.showNoItemLabel(true);
                view.enableDeleteAllButton(false);
            }

            @Override
            public void onFailure() {
                view.hideProgress();
                view.showNoItemLabel(true);
                view.enableDeleteAllButton(false);
            }
        });
    }

    public void onAddNewButtonClicked() {
        view.showAddNewScheduleForm();
    }

    public void onDeleteAllScheduleButtonClicked() {
        view.showDeleteAllConfirmDialog();
    }

    public void onDeleteAllScheduleConfirmed() {
        messageSettingModel.deleteAllScheduleMessage(new MessageSettingModel.IDeleteAllScheduleMessageCallback() {

            @Override
            public void onSuccess() {
                view.showNoItemLabel(true);
                view.enableDeleteAllButton(false);
                view.showScheduleMessage(new ArrayList<>());
            }

            @Override
            public void onFailure(String message) {
                view.showMessageDialog(message);
            }
        });
    }
}
