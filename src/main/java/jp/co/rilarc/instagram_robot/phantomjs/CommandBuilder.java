package jp.co.rilarc.instagram_robot.phantomjs;

public class CommandBuilder {
	
	private final String SPACE = " ";
	
	private String phantomjsPath;
	private String cookieFile;
	private String proxyAddress;
	private String proxyCredential;
	private String scriptFilename;
	private String[] fileParams;
	
	public CommandBuilder(String phantomjsPath, String fileName) {
		this.phantomjsPath = phantomjsPath;
		this.scriptFilename = fileName;
	}
	
	public CommandBuilder cookie(String cookieFilename) {
		cookieFile = cookieFilename;
		return this;
	}
	
	public CommandBuilder params(String... params) {
		fileParams = new String[params.length];
		int i = 0;
		for(String param : params) {
			fileParams[i++] = param;
		}
		return this;
	}
	
	public CommandBuilder proxy(String address, int port) {
		proxyAddress = address + ":" + port;
		return this;
	}
	
	public CommandBuilder proxyAuth(String username, String password) {
		if(username != null && password != null) {
			proxyCredential = username + ":" + password;
		}
		return this;
	}
	
	public String buildCommand() {
		String command = phantomjsPath;
		
		// Add cookie file
		if(cookieFile != null) {
			command += " --cookies-file=" + cookieFile;
		}
		
		// Add proxy
		if(proxyAddress != null) {
			command += " --proxy=" + proxyAddress;
			
			if(proxyCredential != null) {
				command += " --proxy-auth=" + proxyCredential;
			}
		}
		
		// Add script file name
		command += SPACE + scriptFilename;
		
		// Add param
		for(String param : fileParams) {
			command += SPACE + param;
		}
		
		return command;
	}
}
