package jp.co.rilarc.instagram_robot.phantomjs;

import java.io.BufferedInputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class RunJs {
	
	final protected static Logger logger = LogManager.getLogger(RunJs.class);
	
    private static final Configuration config = new Configuration();

    public static boolean login(InstagramAccount account, Proxy proxy) {
		String response = runScript(account, proxy, "login.js", account.getUsername(), account.getPassword());
		return Boolean.parseBoolean(response);
	}
	
	public static boolean like(InstagramAccount account, String postId, Proxy proxy) {
		String response = runScript(account, proxy, "like.js", postId);
		return Boolean.parseBoolean(response);
	}
	
	public static boolean follow(InstagramAccount account, String username, Proxy proxy) {
		String response = runScript(account, proxy, "follow.js", username);
		return Boolean.parseBoolean(response);
	}

	public static String runScript(InstagramAccount account, Proxy proxy, String scriptFile, String... params) {
		String instaUsername = account.getUsername();
		String cookieFilename = String.format("cookies/%s_cookie.txt", instaUsername); 
		CommandBuilder commandBuilder = new CommandBuilder(config.getPhantomjsPath(), "phantomjs_script/" + scriptFile)
				.cookie(cookieFilename)
				.params(params);
		
		if(proxy != null) {
			commandBuilder.proxy(proxy.getProxyAddress(), proxy.getProxyPort())
				.proxyAuth(proxy.getProxyUsername(), proxy.getProxyPassword());
		}
		
		String command = commandBuilder.buildCommand();
		
		System.out.println("command = " + command);
		logger.debug("command = " + command);
		
		return runScript(command);
	}
	
	public static String runScript(String command) {
		Runtime rt = Runtime.getRuntime();
	    try{
	    	Process pr = rt.exec(command);
	        BufferedInputStream bis = new BufferedInputStream(pr.getInputStream());
	        
	        byte[] contents = new byte[1024];
	        int bytesRead = 0;
	        String strFileContents = ""; 
	        while((bytesRead = bis.read(contents)) != -1) { 
	            strFileContents += new String(contents, 0, bytesRead);              
	        }

	        // remove last enter
			strFileContents = strFileContents.substring(0, strFileContents.length() - 1);

			logger.debug("output = " + strFileContents);
			System.out.println("output = " + strFileContents);
			bis.close();
	        
	        return strFileContents;
	    }catch(Exception ex){
	        ex.printStackTrace();
	    }
	    
	    return "";
	}
}
