package jp.co.rilarc.instagram_robot;

import jp.co.rilarc.instagram_robot.action.ProxyAutoThread;
import jp.co.rilarc.instagram_robot.action.ScheduledAutoThread;
import jp.co.rilarc.instagram_robot.helpers.DatabaseHelper;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class Robot {

	final private IAPIHelper dbHelper;
	final private Configuration config;

	final private static Logger logger = LogManager.getLogger(Robot.class);

	public Robot() {
		config = new Configuration();
		dbHelper = new DatabaseHelper(config);

		System.setProperty("file.encoding", "UTF-8");
	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Tokyo"));

		Robot robot = new Robot();
		logger.info("Start robot");

		// Start thread to auto like, follow, unfollow
		robot.resetAllUser();
		try {
			List<Proxy> listProxy = robot.getActiveProxies();

			for (Proxy proxy : listProxy) {
				robot.startThreadForProxy(proxy);
				
				Utils.waitFor(5, 20);   // wait for 5 ~ 20s
			}
		} catch (Exception e) {
			logger.error("Unknown Error: ", e);
		}
		
		// Start thread to post, commentsetting and sendmessage
		robot.startScheduledActionThread();
		
		// Neu sang ngay moi thi set inactive cho toan bo user
		Calendar oldDay = Calendar.getInstance(Locale.JAPAN);
		while(true) {
			if(isNewDay(oldDay)) {
				robot.resetAllUser();
				oldDay = Calendar.getInstance(Locale.JAPAN);
			}
			Utils.waitForMinutes(4, 5);
		}
	}
	
	private static boolean isNewDay(Calendar oldDay) {
		Calendar nowTime = Calendar.getInstance(Locale.JAPAN);
		return (nowTime.get(Calendar.DAY_OF_YEAR) != oldDay.get(Calendar.DAY_OF_YEAR)) ? true : false;
	}

	private void startThreadForProxy(Proxy proxy) {
		ProxyAutoThread autoThread = new ProxyAutoThread(proxy, dbHelper, config);
		autoThread.start();
	}
	
	private void startScheduledActionThread() {
		ScheduledAutoThread autoThread = new ScheduledAutoThread(dbHelper, config);
		autoThread.start();
	}

	private List<Proxy> getActiveProxies() {
		List<Proxy> listProxy = dbHelper.getActiveProxies();
		Collections.shuffle(listProxy);
		return listProxy;
	}

	public void resetAllUser() {
		dbHelper.resetAllUser();
	}
}