/**
 *
 */
package jp.co.rilarc.instagram_robot.action1user;

import jp.co.rilarc.instagram_robot.model.*;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import jp.co.rilarc.instagram_robot.action.ScheduledAction;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;

/**
 * @author nghilt
 */
public class ScheduledAutoThreadForUser extends Thread {

    final private static Logger logger = LogManager.getLogger(ScheduledAutoThreadForUser.class);

    private final User user;

    private final IAPIHelper dbHelper;

    private final Configuration config;

    private final List<ScheduleBase> listSchedule;

    private boolean running = true;

    private boolean isSleeping = false;

    public ScheduledAutoThreadForUser(User user, IAPIHelper IAPIHelper, Configuration config) {
        this.user = user;
        this.dbHelper = IAPIHelper;
        this.config = config;
        listSchedule = new ArrayList<>();
    }

    public void terminate() {
        running = false;
        interrupt();
    }

    @Override
    public void run() {
        logger.info("Start scan schedule and do");
        RobotSetting robotSetting = null;
        while (running) {

            robotSetting = dbHelper.getRobotSettingByToken();

            // check sleep mode
            if (robotSetting.isSleepTime()) {
                if(!isSleeping) {
                    VLogger.append("夜間停止中");
                    logger.info("夜間停止中");
                }
                isSleeping = true;
                return;
            }
            isSleeping = false;

            getSchedules(user);

            // Lan luot gui message
            boolean success = false;
            try {
                for (ScheduleBase schedule : listSchedule) {

                    Timestamp sendTime = schedule.getTimeToAction();
                    int duration = (int) (sendTime.getTime() - new Date().getTime());
                    if (duration <= 0) {
                        duration = 0;
                    }

                    Utils.waitForMiliSecond(duration, duration);

                    if (schedule instanceof ScheduleSendMessage) {
                        success = ScheduledAction.sendMessage((ScheduleSendMessage) schedule, dbHelper, config);
                    } else if (schedule instanceof SchedulePost) {
                        success = ScheduledAction.post((SchedulePost) schedule, dbHelper, config);
                    } else if (schedule instanceof ScheduleComment) {
                        success = ScheduledAction.comment((ScheduleComment) schedule, dbHelper, config);
                    }

                    if (!success) {
                        break;
                    }

                    Utils.waitFor(30, 70); // wait for 30~70s before next action
                }
            } catch (Exception e) {
                logger.error("error", e);
            }

            // Wait for 1 minute before start new user
            Utils.waitForMinutes(1, 5);
        }
    }

    private void getSchedules(User user) {
        listSchedule.clear();    // Xoa toan bo schedule da gui

        // Get Uptime ScheduleSendMessage
        ScheduleSendMessage scheduleSendMessage = getSendMessageSchedule(user);
        SchedulePost schedulePost = getPostSchedule(user);
        ScheduleComment scheduleComment = getCommentSchedule(user);

        // schedule post va commentsetting
        if (scheduleSendMessage != null) {
            listSchedule.add(scheduleSendMessage);
        }
        if (schedulePost != null) {
            listSchedule.add(schedulePost);
        }
        if (scheduleComment != null) {
            listSchedule.add(scheduleComment);
        }

        if (listSchedule.isEmpty()) {
            logger.info("There are no schedule registered");
            return;
        }

        // sort
        Collections.sort(listSchedule, new Comparator<ScheduleBase>() {

            @Override
            public int compare(ScheduleBase o1, ScheduleBase o2) {
                return o1.getTimeToAction().before(o2.getTimeToAction()) ? -1 : 1;
            }
        });

        logger.debug("List schedule " + listSchedule);
        logger.info("Process " + listSchedule.size() + " schedules");
    }

    private ScheduleComment getCommentSchedule(User user) {
        return dbHelper.getCommentSchedule();
    }

    private SchedulePost getPostSchedule(User user) {
        return dbHelper.getPostSchedule();
    }

    protected ScheduleSendMessage getSendMessageSchedule(User user) {
        return dbHelper.getSendMessageSchedule();
    }
}
