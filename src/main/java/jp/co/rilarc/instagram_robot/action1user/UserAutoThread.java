/**
 *
 */
package jp.co.rilarc.instagram_robot.action1user;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.UnableToSetCookieException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import jp.co.rilarc.instagram_robot.Main;
import jp.co.rilarc.instagram_robot.action.BaseAction;
import jp.co.rilarc.instagram_robot.action.FollowAction;
import jp.co.rilarc.instagram_robot.action.LikeAction;
import jp.co.rilarc.instagram_robot.action.UnfollowAction;
import jp.co.rilarc.instagram_robot.action.UserPool;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.exception.WebDriverNotFoundException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.Action;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RelatedAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;
import jp.co.rilarc.instagram_robot.utils.VLogger;
import jp.co.rilarc.instagram_robot.utils.WebDriverUtils;
import jp.co.rilarc.instagram_robot.webcontroller.InstagramWebController;

/**
 * @author nghilt
 */
public class UserAutoThread extends Thread {

    final private static Logger logger = LogManager.getLogger(UserAutoThread.class);

    private final IAPIHelper dbHelper;

    private final Configuration config;

    private final User user;

    private Statistic statistic;

    private boolean running = true;

    private boolean isSleeping = false;

    private WebDriver webDriver;

    public UserAutoThread(User user, IAPIHelper IAPIHelper, Configuration config) {
//		logger.info("Start thread for proxy [" + proxy.getId() + "][" + proxy.getProxyAddress() + "]");
        this.user = user;
        this.dbHelper = IAPIHelper;
        this.config = config;
    }

    private void initWebDriver() throws WebDriverNotFoundException {
        webDriver = WebDriverUtils.createWebDriver();
        webDriver.manage().window().maximize();
        webDriver.get(InstagramWebController.INSTAGRAM_BASE_URL);
        Set<Cookie> cookies = Main.getInstance().getInstagramDb().getCookies();
        if (cookies != null && !cookies.isEmpty()) {
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    try {
                        webDriver.manage().addCookie(cookie);
                    } catch (UnableToSetCookieException e) {
                        logger.warn("UnableToSetCookieException: " + e.getMessage());
                    }
                }
            }
        }
        webDriver.get(InstagramWebController.INSTAGRAM_BASE_URL);
    }

    @Override
    public void run() {
        if (webDriver == null) {
            try {
                initWebDriver();
            } catch (WebDriverNotFoundException e) {
                e.printStackTrace();
            }
        }
        while (running) {
            runUser(user);
            Utils.waitForMinutes(1, 2);    // wait 1 ~ 2 munites
        }
    }

    public void terminate() {
        running = false;
        if (webDriver != null) {
            try {
                webDriver.close();
                webDriver.quit();
            } catch (WebDriverException e) {
                logger.warn("WebDriverException: " + e.getMessage());
            } finally {
                webDriver = null;
            }
        }
        interrupt();
    }

    private void runUser(User user) {
        int userId = user.getId();

        dbHelper.updateRobotStatus(true);

        RobotSetting robotSetting = dbHelper.getRobotSettingByToken();

        // check sleep mode
        if (robotSetting.isSleepTime()) {
            if(!isSleeping) {
                VLogger.append("夜間停止中");
                logger.info("夜間停止中");
            }
            isSleeping = true;
            return;
        }
        isSleeping = false;

        List<Action> actionList = getListAction(robotSetting);

        // User ko setting action nao thi stop ngay
        if (actionList.isEmpty()) {
            VLogger.append("あなたは（いいね・フォロー・アンフォロー）タスクを設定していないようです。");
            logger.info("[" + user.getId() + "][" + user.getEmail() + "] not setting any actions. So Stopped!");
            return;
        }

        Instagram instagram;
        try {
            instagram = UserPool.getInstagram(userId, null, dbHelper, config);
            if (instagram != null) {
                instagram.setWebDriver(webDriver);
                String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                this.statistic = dbHelper.getOrCreateStatistic(instagram.getInstagramAccount().getUsername(), today);

                AdvanceSetting advanceSetting = dbHelper.getAdvanceSetting();

                for (Action action : actionList) {

                    // check sleep mode
                    if (robotSetting.isSleepTime()) {
                        if(!isSleeping) {
                            VLogger.append("夜間停止中");
                            logger.info("夜間停止中");
                        }
                        isSleeping = true;
                        return;
                    }
                    isSleeping = false;

                    BaseAction nextAction = null;
                    switch (action) {
                        case LIKE:
                            nextAction = new LikeAction(instagram, robotSetting, advanceSetting, dbHelper, config, statistic);
                            break;
                        case FOLLOW:
                            nextAction = new FollowAction(instagram, robotSetting, advanceSetting, dbHelper, config, statistic);
                            break;
                        case UNFOLLOW:
                            nextAction = new UnfollowAction(instagram, robotSetting, advanceSetting, dbHelper, config, statistic);
                            break;
                        default:
                            break;
                    }

                    if (nextAction != null) {
                        nextAction.runAction();

                        // Wait for 1 ~ 3 minute before start new action
                        //Utils.waitForMinutes(1, 3);
                        Utils.waitFor(40, 110);
                    }
                }
            }
        } catch (InstagramCannotAccessException e) {
            logger.error("[" + user.getId() + "][" + user.getEmail() + "] cannot access to instagram. So Stopped!");
            VLogger.append("タスクを停止しました");
        }
    }

    private List<Action> getListAction(RobotSetting robotSetting) {
        List<Action> listAction = new ArrayList<>();

        int likeActionNumber = 0;
        int followActionNumber = 0;
        int unfollowActionNumber = 0;

        // Uu tien thuc hien boost unfollow, conflict account, sau do moi den cac kieu like, follow thong thuong
        if(robotSetting.isUnfollowsBoostUnfollow()) {
            VLogger.append("ブーストフォロー解除\nフォローしているユーザーが全てが対象で、古いフォローユーザーから順に入力された人数のフォローを解除していきます。");
            unfollowActionNumber++;
        }else {
            RelatedAccount relatedAccount = dbHelper.getRelatedAccount();
            if (relatedAccount != null) {
                VLogger.append("設定されている競合アカウント【" + relatedAccount.getInstagramUsername() + "】で自動フォローを実施します。");
                //likeActionNumber++;
                followActionNumber++;
            }else {
                if (!robotSetting.isDisableLike()) {
                    if (robotSetting.isLikesHashtagAutoLike()) {
                        likeActionNumber++;
                    }

                    if (robotSetting.isLikesLocationAutoLike()) {
                        likeActionNumber++;
                    }

                    if (robotSetting.isLikesFollowingAutoLikeFeed()) {
                        likeActionNumber++;
                    }
                } else {
                    logger.warn("[" + user.getId() + "][" + user.getEmail() + "] いいねアクションがブロックされています。");
                    VLogger.append("いいねアクションがブロックされています。");
                }

                //if (!robotSetting.isDisableFollow() && !robotSetting.isUnfollowsBoostUnfollow()) {
                if (!robotSetting.isDisableFollow()) {
                    if (robotSetting.isFollowsHashtagAutoFollow()) {
                        followActionNumber++;
                    }

                    if (robotSetting.isFollowsLocationAutoFollow()) {
                        followActionNumber++;
                    }

                    if (robotSetting.isFollowsFollowersAutoFollowBack()) {
                        followActionNumber++;
                    }
                } else if (robotSetting.isDisableFollow()) {
                    logger.warn("[" + user.getId() + "][" + user.getEmail() + "] フォローアクションがブロックされています。");
                    VLogger.append("フォローアクションがブロックされています。");
                }

                //if (robotSetting.isUnfollowsNotFollowingBackAutoUnfollow() || robotSetting.isUnfollowsBoostUnfollow()) {
                if (robotSetting.isUnfollowsNotFollowingBackAutoUnfollow()) {
                    unfollowActionNumber++;
                }
            }
        }

        if (likeActionNumber > 0)
            listAction.add(Action.LIKE);
        if (followActionNumber > 0)
            listAction.add(Action.FOLLOW);
        if (unfollowActionNumber > 0)
            listAction.add(Action.UNFOLLOW);

        Collections.shuffle(listAction); // Random action
        return listAction;
    }
}
