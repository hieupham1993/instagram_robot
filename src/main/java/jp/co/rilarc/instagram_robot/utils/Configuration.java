package jp.co.rilarc.instagram_robot.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Created on 2017/6/17.
 */
public class Configuration {

    private String host;

    private String database;

    private int port;

    private String username;

    private String password;

    //like speed
    private int likeSpeed1;

    private int likeSpeed2;

    private int likeSpeed3;

    private int likeSpeed4;

    private int likeSpeed5;

    //follow speed
    private int followSpeed1;

    private int followSpeed2;

    private int followSpeed3;

    private int followSpeed4;

    private int followSpeed5;

    //unfollow speed
    private int unfollowSpeed1;

    private int unfollowSpeed2;

    private int unfollowSpeed3;

    private int unfollowSpeed4;

    private int unfollowSpeed5;

    //proxy
//    private boolean isUsingProxy;
//    private String proxyAddress;
//    private int proxyPort;
//    private String proxyUsername;
//    private String proxyPassword;

    private String phantomjsPath;

    private boolean enablePhantomjs;

    final private static Logger logger = LogManager.getLogger(Configuration.class);

    public Configuration() {
        setDefaultConfig();
    }

    public String getHost() {
        return host;
    }

    public String getDatabase() {
        return database;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getLikeSpeed(int speedId) {
        switch (speedId) {
            case 1:
                return likeSpeed1;
            case 2:
                return likeSpeed2;
            case 3:
                return likeSpeed3;
            case 4:
                return likeSpeed4;
            case 5:
                return likeSpeed5;
            default:
                return likeSpeed1;
        }
    }

    public int getFollowSpeed(int speedId) {
        switch (speedId) {
            case 1:
                return followSpeed1;
            case 2:
                return followSpeed2;
            case 3:
                return followSpeed3;
            case 4:
                return followSpeed4;
            case 5:
                return followSpeed5;
            default:
                return followSpeed1;
        }
    }

    public int getUnfollowSpeed(int speedId) {
        switch (speedId) {
            case 1:
                return unfollowSpeed1;
            case 2:
                return unfollowSpeed2;
            case 3:
                return unfollowSpeed3;
            case 4:
                return unfollowSpeed4;
            case 5:
                return unfollowSpeed5;
            default:
                return unfollowSpeed1;
        }
    }

//    public boolean isUsingProxy() {
//        return isUsingProxy;
//    }
//
//    public String getProxyAddress() {
//        return proxyAddress;
//    }
//
//    public int getProxyPort() {
//        return proxyPort;
//    }
//
//    public String getProxyUsername() {
//        return proxyUsername;
//    }
//
//    public String getProxyPassword() {
//        return proxyPassword;
//    }

    public String getPhantomjsPath() {
        return phantomjsPath;
    }

    public boolean isEnablePhantomjs() {
        return enablePhantomjs;
    }

    private void setDefaultConfig() {
        logger.info("Use default config");

        host = "150.95.135.254";
        port = 3306;
        database = "instagram";
        username = "nghi-admin";
        password = "secret";

//        //like speed
//        likeSpeed1 = 250;
//        likeSpeed2 = 320;
//        likeSpeed3 = 400;
//        likeSpeed4 = 500;
//        likeSpeed5 = 600;
//
//        //follow speed
//        followSpeed1 = 96;
//        followSpeed2 = 120;
//        followSpeed3 = 180;
//        followSpeed4 = 250;
//        followSpeed5 = 320;
//
//        //unfollow speed
//        unfollowSpeed1 = 72;
//        unfollowSpeed2 = 96;
//        unfollowSpeed3 = 144;
//        unfollowSpeed4 = 180;
//        unfollowSpeed5 = 288;

        //like speed
        likeSpeed1 = 100;
        likeSpeed2 = 100;
        likeSpeed3 = 100;
        likeSpeed4 = 100;
        likeSpeed5 = 100;

        //follow speed
        followSpeed1 = 100;
        followSpeed2 = 100;
        followSpeed3 = 100;
        followSpeed4 = 100;
        followSpeed5 = 100;

        //unfollow speed
        unfollowSpeed1 = 150;
        unfollowSpeed2 = 150;
        unfollowSpeed3 = 150;
        unfollowSpeed4 = 150;
        unfollowSpeed5 = 150;

        phantomjsPath = "/Users/nghilt/Works/Watermelon/Projects/shell_script/phantomjs-2.1.1-macosx/bin/phantomjs";
        enablePhantomjs = true;
    }
}
