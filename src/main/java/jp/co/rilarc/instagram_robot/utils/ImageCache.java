package jp.co.rilarc.instagram_robot.utils;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;

/**
 * Created by HieuPT on 1/26/2018.
 */
public final class ImageCache {

    private final Map<String, Image> imageCaches;

    private static ImageCache instance;

    public static ImageCache getInstance() {
        if (instance == null) {
            instance = new ImageCache();
        }
        return instance;
    }

    private ImageCache() {
        imageCaches = new HashMap<>();
    }

    public Image getImage(String url) {
        Image image = imageCaches.get(url);
        if (image == null) {
            image = new Image(url, true);
            imageCaches.put(url, image);
        }
        return image;
    }
}
