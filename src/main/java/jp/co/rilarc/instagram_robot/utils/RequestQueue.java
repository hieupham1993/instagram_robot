package jp.co.rilarc.instagram_robot.utils;

import org.apache.http.client.HttpResponseException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HieuPT on 8/29/2017.
 */
public final class RequestQueue {

    public interface SyncRequestCallback<T> {

        default void onResponse(Call<T> request, T responseData) {

        }

        default void onFailure(Call<T> request, Throwable throwable) {

        }
    }

    private static final Logger logger = LogManager.getLogger(RequestQueue.class);

    private static final String TAG = RequestQueue.class.getSimpleName();

    private final LinkedList<Request<?>> requestQueue;

    public static RequestQueue getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private RequestQueue() {
        requestQueue = new LinkedList<>();
    }

    public <T> void addRequest(Call<T> request, Callback<T> callback) {
        if (request != null) {
            logger.debug("#addRequest: " + request.request().toString());
            Request<T> requestWrap = new Request<>(request, callback);
            requestQueue.add(requestWrap);
            if (requestQueue.size() == 1) {
                requestWrap.execute();
            }
        }
    }

    public <T> void addRequestToFrontQueue(Call<T> request, Callback<T> callback) {
        if (request != null) {
            logger.debug("#addRequestToFrontQueue: " + request.request().toString());
            Request<T> requestWrap = new Request<>(request, callback);
            requestQueue.addFirst(requestWrap);
            if (requestQueue.size() == 1) {
                requestWrap.execute();
            }
        }
    }

    public <T> T executeRequest(Call<T> request) {
        return executeRequest(request, null);
    }

    public <T> T executeRequest(Call<T> request, SyncRequestCallback<T> callback) {
        if (request != null) {
            logger.debug("#executeRequest: " + request.request().toString());
            try {
                Response<T> response = request.execute();
                T responseData = response.body();
                if (callback != null) {
                    if (response.isSuccessful()) {
                        callback.onResponse(request, responseData);
                    } else {
                        callback.onFailure(request, new HttpResponseException(response.code(), response.toString()));
                    }
                }
                return responseData;
            } catch (IOException e) {
                logger.warn("IOException", e);
                if (callback != null) {
                    callback.onFailure(request, e);
                }
            }
        }
        return null;
    }

    private final class Request<T> {

        private Call<T> request;

        private Callback<T> callback;

        private Request(Call<T> request, Callback<T> callback) {
            this.request = request;
            this.callback = callback;
        }

        private void execute() {
            request.enqueue(new CallbackDecorator<>(this));
        }
    }

    private final class CallbackDecorator<T> implements Callback<T> {

        private final Request<T> request;

        private final Callback<T> callback;

        private CallbackDecorator(Request<T> request) {
            this.request = request;
            this.callback = request.callback;
        }

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            if (callback != null) {
                callback.onResponse(call, response);
            }
            performNextRequest();
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            if (callback != null) {
                callback.onFailure(call, t);
            }
            performNextRequest();
        }

        private void performNextRequest() {
            requestQueue.remove(request);
            if (!requestQueue.isEmpty()) {
                Request<?> request = requestQueue.peek();
                request.execute();
            }
        }
    }

    private static final class InstanceHolder {

        private static final RequestQueue INSTANCE = new RequestQueue();

        private InstanceHolder() {
            //no instance
        }
    }
}
