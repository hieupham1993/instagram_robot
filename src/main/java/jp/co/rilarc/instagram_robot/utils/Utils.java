package jp.co.rilarc.instagram_robot.utils;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import jp.co.rilarc.instagram_robot.instagram.Signatures;

public final class Utils {

    final private static Logger logger = LogManager.getLogger(Utils.class);

    public static String hashMac(String text, String secretKey) {
        try {
            Key sk = new SecretKeySpec(secretKey.getBytes(), HASH_ALGORITHM);
            Mac mac = Mac.getInstance(sk.getAlgorithm());
            mac.init(sk);
            final byte[] hmac = mac.doFinal(text.getBytes());
            return toHexString(hmac);// call toHexString Methods....
        } catch (NoSuchAlgorithmException e1) {
            // throw an exception or pick a different encryption method
            logger.error("error building signature, no such algorithm in device " + HASH_ALGORITHM);
            return null;
        } catch (InvalidKeyException e) {
            logger.error("error building signature, invalid key " + HASH_ALGORITHM);
            return null;
        }
    }

    private static final String HASH_ALGORITHM = "HmacSHA256";

    // toHexString Method...
    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);

        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        formatter.close();
        return sb.toString();

    }

    public static String extractUrl(String text) {
//		String urlRegex = "http.*?(\\s|$)";
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";

        Pattern pattern = Pattern.compile(urlRegex);
        Matcher urlMatcher = pattern.matcher(text);

        if (urlMatcher.find()) {
            return text.substring(urlMatcher.start(0), urlMatcher.end(0));
        }

        return null;
    }

    public static Date getCurrentZeroTimeDate() {
        Date res = new Date();
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(res);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        res = calendar.getTime();

        return res;
    }

    public static void waitForMinutes(int min, int max) {
        waitFor(min * 60, max * 60);
    }

    public static void waitFor(int min, int max) {
        try {
            Thread.sleep(Signatures.randomInt(min, max) * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitForMiliSecond(int min, int max) {
        try {
            Thread.sleep(Signatures.randomInt(min, max));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static int getRandomNumber(int min, int max) {
        if (max > min) {
            Random random = new Random();
            return random.nextInt(max - min + 1) + min;
        } else {
            throw new IllegalArgumentException("Max must be greater than min");
        }
    }

    public static void captureScreenshot(WebDriver webDriver) {
        try {
            File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("screenshot", String.valueOf(System.currentTimeMillis()) + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void stopService(ExecutorService service) {
        if (service != null) {
            try {
                service.shutdown();
                service.awaitTermination(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                service.shutdownNow();
            }
        }
    }

    public static void openBrowserSystem(String url) {
        try {
            Desktop.getDesktop().browse(URI.create(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void grantFilePermission(File file) {
        if (file != null && file.exists()) {
            try {
                Set<PosixFilePermission> perms = new HashSet<>();
                perms.add(PosixFilePermission.OWNER_READ);
                perms.add(PosixFilePermission.OWNER_WRITE);
                perms.add(PosixFilePermission.OWNER_EXECUTE);
                perms.add(PosixFilePermission.GROUP_READ);
                perms.add(PosixFilePermission.GROUP_WRITE);
                perms.add(PosixFilePermission.GROUP_EXECUTE);
                perms.add(PosixFilePermission.OTHERS_READ);
                perms.add(PosixFilePermission.OTHERS_WRITE);
                perms.add(PosixFilePermission.OTHERS_EXECUTE);
                Files.setPosixFilePermissions(file.toPath(), perms);
            } catch (IOException e) {
                logger.warn("#grantFilePermission: IOException", e);
            }
        }
    }
}
