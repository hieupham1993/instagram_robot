package jp.co.rilarc.instagram_robot.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javafx.application.Platform;
import javafx.scene.web.WebEngine;

/**
 * VLogger = Visual Logger
 */
public class VLogger {

    private static final Logger LOGGER = LogManager.getLogger(VLogger.class);

    private static VLogger logger;

    public static void append(String message) {
        LOGGER.info(message);
        logger.print(message);
    }

    public static synchronized void setup(WebEngine context) {
        if (logger == null) {
            logger = new VLogger();
        }
        logger.setWebEngine(context);
    }

    private WebEngine webEngine;

    private VLogger() {
        //no instance
    }

    private void setWebEngine(WebEngine webEngine) {
        this.webEngine = webEngine;
    }

    private String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return formatter.format(Calendar.getInstance().getTime());
    }

    private void print(String message) {
        synchronized (this) {
            if (webEngine != null) {
                Platform.runLater(() -> {
                    String logMessage = "[" + getCurrentTime() + "]: " + message;
                    String formattedMessage = logMessage.replaceAll("\\r\\n|\\r|\\n", Const.SPACE_CHARACTER);
                    String script = "appendLog('" + formattedMessage + "');";
                    webEngine.executeScript(script);
                });
            }
        }
    }


}
