package jp.co.rilarc.instagram_robot.utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by HieuPT on 8/23/2017.
 */
public final class RetrofitClient {

    private final Map<String, Retrofit> clients;

    public static RetrofitClient getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private RetrofitClient() {
        clients = new HashMap<>();
    }

    private Retrofit getClient(String baseUrl) {
        Retrofit retrofit = clients.get(baseUrl);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            clients.put(baseUrl, retrofit);
        }
        return retrofit;
    }

    public <T> T getService(String baseUrl, Class<T> service) {
        Retrofit retrofit = getClient(baseUrl);
        return retrofit.create(service);
    }

    private static final class InstanceHolder {

        private static final RetrofitClient INSTANCE = new RetrofitClient();

        private InstanceHolder() {
            //no instance
        }
    }
}
