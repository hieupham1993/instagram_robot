package jp.co.rilarc.instagram_robot.helpers;

import jp.co.rilarc.instagram_robot.database.LocalDatabaseHelper;
import jp.co.rilarc.instagram_robot.database.model.Preference;
import jp.co.rilarc.instagram_robot.wssj_api.WssjService;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.http.util.TextUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.json.JSONArray;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.rilarc.instagram_robot.handler.InstagramUser;
import jp.co.rilarc.instagram_robot.instagram.Security;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.BadHastag;
import jp.co.rilarc.instagram_robot.model.FollowHashtag;
import jp.co.rilarc.instagram_robot.model.FollowLocation;
import jp.co.rilarc.instagram_robot.model.FollowingUser;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.LikeHashtag;
import jp.co.rilarc.instagram_robot.model.LikeLocation;
import jp.co.rilarc.instagram_robot.model.LocalUser;
import jp.co.rilarc.instagram_robot.model.PreviouslyUnfollowUser;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.RelatedAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;
import jp.co.rilarc.instagram_robot.model.SchedulePost;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Const;
import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseHelper implements IAPIHelper {

    final protected static Logger logger = LogManager.getLogger(IAPIHelper.class);

    public static final boolean INSTAGRAM_NOT_CONNECTED = false;

    public static final boolean INSTAGRAM_CONNECTED = true;

    public static final boolean INVALID_USER = false;

    protected final LocalDatabaseHelper localDatabase = LocalDatabaseHelper.getInstance();

    protected final WssjService wssjService;

//    final private Configuration config;
//
//    public IAPIHelper(Configuration _config) {
//        this.config = _config;
//    }

//    protected ConnectionPool connectionPool = null;

    public DatabaseHelper(Configuration config) {
//        connectionPool = new ConnectionPool(config);
        wssjService = new WssjService();
    }

//    private Connection getConnection() throws SQLException {
//        return DriverManager.getConnection("jdbc:mysql://" + config.getHost() + "/" + config.getDatabase() + "?useUnicode=true&characterEncoding=UTF-8&verifyServerCertificate=false&useSSL=false&user=" + config.getUsername() + "&password=" + config.getPassword());
//    }

    private String refreshToken() {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.refreshToken(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.getBoolean("success")) {
                    String newToken = result.getJSONObject("data").getString("access_token");
                    int expireIn = result.getJSONObject("data").getInt("expire_in");

                    localDatabase.updateAccessToken("Bearer " + newToken, DateTime.now().plusMinutes(expireIn).getMillis());
                    return newToken;
                }
            } else if (response.getStatusLine().getStatusCode() == 400) {
                String responseAsString = EntityUtils.toString(response.getEntity());
                JSONObject result = new JSONObject(responseAsString);
                logger.error("ERROR: " + result.getString("message"));

            }
        } catch (IOException | URISyntaxException e) {
            logger.error("ERROR: ", e);
        }

        return null;
    }

    private Date stringToDate(String dateInString) {
        if (TextUtils.isEmpty(dateInString))
            return null;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

            java.util.Date date = formatter.parse(dateInString);

            return new java.sql.Date(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Timestamp stringToTimestamp(String dateInString) {
        if (TextUtils.isEmpty(dateInString))
            return null;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

            java.util.Date date = formatter.parse(dateInString);

            return new java.sql.Timestamp(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public User loginToSystem(String email, String password) {
        User user = getUserByToken();
        if (user == null) {
            return null;
        }
        InstagramAccount instagramAccount = getInstagramAccountByToken();
        user.setInstagramAccount(instagramAccount);

        return user;
    }

    public void resetAllUser() {
        Connection connection = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "UPDATE users SET is_active_robot = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setBoolean(1, false);
//            preparedStatement.executeUpdate();
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }
    }

    public InstagramAccount getInstagramAccountByToken() {
        return getInstagramAccountByToken(true);
    }

    private InstagramAccount getInstagramAccountByToken(boolean retry) {
        InstagramAccount instagramAccount = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getInstagramAccount(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                instagramAccount = new InstagramAccount();

                instagramAccount.setId(result.getJSONObject("data").getInt("id"));
                instagramAccount.setUsername(result.getJSONObject("data").getString("username"));
                instagramAccount.setPassword(Security.decrypt(result.getJSONObject("data").getString("password"), Const.SECURITY_KEY));
                instagramAccount.setAccountId(result.getJSONObject("data").getString("instagram_userid"));
                instagramAccount.setInstagramAvatar(result.getJSONObject("data").getString("instagram_avatar"));
                instagramAccount.setUserId(result.getJSONObject("data").getInt("user_id"));
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getInstagramAccountByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return instagramAccount;
    }

    public RobotSetting getRobotSettingByToken() {
        return getRobotSettingByToken(true);
    }

    private RobotSetting getRobotSettingByToken(boolean retry) {
        RobotSetting robotSetting = null;

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getRobotSetting(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                robotSetting = new RobotSetting();

                robotSetting.setId(result.getJSONObject("data").getInt("id"));
                robotSetting.setUserId(result.getJSONObject("data").getInt("user_id"));

                robotSetting.setLikesHashtagAutoLike(result.getJSONObject("data").getInt("likes_hashtag_auto_like") == 1);
                robotSetting.setLikesHashtagMinLikeFilterMode(result.getJSONObject("data").getInt("likes_hashtag_min_like_filter_mode") == 1);
                robotSetting.setLikesHashtagMinLikeFilter(result.getJSONObject("data").getInt("likes_hashtag_min_like_filter"));
                robotSetting.setLikesHashtagMaxLikeFilterMode(result.getJSONObject("data").getInt("likes_hashtag_max_like_filter_mode") == 1);
                robotSetting.setLikesHashtagMaxLikeFilter(result.getJSONObject("data").getInt("likes_hashtag_max_like_filter"));
                robotSetting.setLikesHashtagMaxHashtagFilterMode(result.getJSONObject("data").getInt("likes_hashtag_max_hashtag_filter_mode") == 1);
                robotSetting.setLikesHashtagMaxHashtagFilter(result.getJSONObject("data").getInt("likes_hashtag_max_hashtag_filter"));
                robotSetting.setLikesHashtagEnableSpamFilter(result.getJSONObject("data").getInt("likes_hashtag_enable_spam_filter") == 1);
                robotSetting.setLikesHashtagAutoDisableBadTags(result.getJSONObject("data").getInt("likes_hashtag_auto_disable_bad_tags") == 1);

                robotSetting.setLikesLocationAutoLike(result.getJSONObject("data").getInt("likes_location_auto_like") == 1);
                robotSetting.setLikesLocationMinLikeFilterMode(result.getJSONObject("data").getInt("likes_location_min_like_filter_mode") == 1);
                robotSetting.setLikesLocationMinLikeFilter(result.getJSONObject("data").getInt("likes_location_min_like_filter"));
                robotSetting.setLikesLocationMaxLikeFilterMode(result.getJSONObject("data").getInt("likes_location_max_like_filter_mode") == 1);
                robotSetting.setLikesLocationMaxLikeFilter(result.getJSONObject("data").getInt("likes_location_max_like_filter"));
                robotSetting.setLikesLocationMaxHashtagFilterMode(result.getJSONObject("data").getInt("likes_location_max_hashtag_filter_mode") == 1);
                robotSetting.setLikesLocationMaxHashtagFilter(result.getJSONObject("data").getInt("likes_location_max_hashtag_filter"));
                robotSetting.setLikesLocationEnableSpamFilter(result.getJSONObject("data").getInt("likes_location_enable_spam_filter") == 1);

                robotSetting.setLikesFollowingAutoLikeFeed(result.getJSONObject("data").getInt("likes_following_auto_like_feed") == 1);
                robotSetting.setLikesFollowingEnableSpaFilter(result.getJSONObject("data").getInt("likes_following_enable_spam_filter") == 1);
                robotSetting.setLikesAutoLikeSpeed(result.getJSONObject("data").getInt("likes_auto_like_speed"));

                robotSetting.setFollowsHashtagAutoFollow(result.getJSONObject("data").getInt("follows_hashtag_auto_follow") == 1);
                robotSetting.setFollowsHashtagEnableSpamFilter(result.getJSONObject("data").getInt("follows_hashtag_enable_spam_filter") == 1);

                robotSetting.setFollowsLocationAutoFollow(result.getJSONObject("data").getInt("follows_hashtag_location_auto_follow") == 1);

                robotSetting.setFollowsFollowersAutoFollowBack(result.getJSONObject("data").getInt("follows_followers_auto_follow_back") == 1);

                robotSetting.setFollowsDoNotFollowPreviouslyUnfollowedUsers(result.getJSONObject("data").getInt("follows_do_not_follow_previously_unfollowed_users") == 1);
                robotSetting.setFollowsDoNotFollowPrivateUsers(result.getJSONObject("data").getInt("follows_do_not_follow_private_users") == 1);
                robotSetting.setFollowsMaxHashtagFilterMode(result.getJSONObject("data").getInt("follows_max_hashtag_filter_mode") == 1);
                robotSetting.setFollowsMaxHashtagFilter(result.getJSONObject("data").getInt("follows_max_hashtag_filter"));
                robotSetting.setFollowsAutoFollowSpeed(result.getJSONObject("data").getInt("follows_auto_follow_speed"));

                robotSetting.setUnfollowsNotFollowingBackAutoUnfollow(result.getJSONObject("data").getInt("unfollows_not_following_back_auto_unfollow") == 1);
                robotSetting.setUnfollowsNotFollowingBackDaysToWaitBeforeUnfollowMode(result.getJSONObject("data").getInt("unfollows_not_following_back_days_to_wait_before_unfollow_mode") == 1);
                robotSetting.setUnfollowsNotFollowingBackDaysToWaitBeforeUnfollow(result.getJSONObject("data").getInt("unfollows_not_following_back_days_to_wait_before_unfollow"));
                robotSetting.setUnfollowsBoostUnfollow(result.getJSONObject("data").getInt("unfollows_boost_unfollow") == 1);
                robotSetting.setUnfollowsBoostAmount(result.getJSONObject("data").getInt("unfollows_boost_amount"));
                robotSetting.setUnfollowsAutoUnfollowSpeed(result.getJSONObject("data").getInt("unfollows_auto_unfollow_speed"));

                robotSetting.setGlobalRobotStatus(result.getJSONObject("data").getInt("global_robot_status") == 1);
                robotSetting.setGlobalSlowStartState(result.getJSONObject("data").getInt("global_slow_start_state") == 1);
                robotSetting.setGlobalSleepMode(result.getJSONObject("data").getInt("global_sleep_mode") == 1);
                robotSetting.setGlobalSleepStart(result.getJSONObject("data").getInt("global_sleep_start"));
                robotSetting.setGlobalSleepEnd(result.getJSONObject("data").getInt("global_sleep_end"));
                robotSetting.setGlobalFilter(result.getJSONObject("data").getInt("global_filter") == 1);
                robotSetting.setGlobalCompleAccount(result.getJSONObject("data").getInt("global_comple_account") == 1);
                robotSetting.setDisableLike(result.getJSONObject("data").getInt("disable_like") == 1);
                robotSetting.setDisableFollow(result.getJSONObject("data").getInt("disable_follow") == 1);


                if (result.getJSONObject("data").isNull("proxy_address"))
                    robotSetting.setProxyAddress(null);
                else
                    robotSetting.setProxyAddress(result.getJSONObject("data").getString("proxy_address"));


                if (result.getJSONObject("data").isNull("proxy_port"))
                    robotSetting.setProxyPort(0);
                else
                    robotSetting.setProxyPort(result.getJSONObject("data").getInt("proxy_port"));


                if (result.getJSONObject("data").isNull("proxy_username"))
                    robotSetting.setProxyUsername(null);
                else
                    robotSetting.setProxyUsername(result.getJSONObject("data").getString("proxy_username"));


                if (result.getJSONObject("data").isNull("proxy_password"))
                    robotSetting.setProxyPassword(null);
                else
                    robotSetting.setProxyPassword(result.getJSONObject("data").getString("proxy_password"));


                if (result.getJSONObject("data").isNull("updated_at"))
                    robotSetting.setmUpdatedAt(null);
                else
                    robotSetting.setmUpdatedAt(stringToDate(result.getJSONObject("data").getString("updated_at")));
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getRobotSettingByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return robotSetting;
    }

    public ArrayList<User> getListUnactiveRobotUsers() {
        Connection connection = null;
        ArrayList<User> users = new ArrayList<>();

//        logger.debug("Start get list user!");
//
//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT * FROM users " +
//                    "WHERE is_active_robot = ? and is_instagram_connected = ? and active = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setBoolean(1, false);
//            preparedStatement.setBoolean(2, true);
//            preparedStatement.setBoolean(3, true);
//            ResultSet rs = preparedStatement.executeQuery();
//            while (rs.next()) {
//                logger.debug("get user " + rs.getString("email"));
//
//                User user = new User();
//                user.setId(rs.getInt("id"));
//                user.setEmail(rs.getString("email"));
//                user.setRobotWorking(rs.getBoolean("is_robot_working"));
//                users.add(user);
//            }
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return users;
    }


    public void updateRobotStatus(boolean robotStatus) {
        updateRobotStatus(robotStatus, true);
    }

    private void updateRobotStatus(boolean robotStatus, boolean retry) {
        Preference preference = localDatabase.getPreference();

        logger.debug("Update robot status user " + preference.getEmail());

        String token = preference.getAccessToken();

        try {
            HttpResponse response = wssjService.updateRobotStatus(token, robotStatus);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateRobotStatus(robotStatus, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public void updateRobotWorkingStatus(int userId, boolean robotStatus) {
        Connection connection = null;

        logger.debug("Update robot working status user " + userId);

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "UPDATE users SET is_robot_working = ?, updated_at = NOW() " +
//                    "WHERE id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setBoolean(1, robotStatus);
//            preparedStatement.setInt(2, userId);
//            preparedStatement.executeUpdate();
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }
    }


    public void updateInstagramConnectStatus(boolean connectStatus, String notes) {
        updateInstagramConnectStatus(connectStatus, notes, true);
    }

    private void updateInstagramConnectStatus(boolean connectStatus, String notes, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateInstagramConnectStatus(token, connectStatus, notes);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateInstagramConnectStatus(connectStatus, notes, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void disableUser(String notes) {
        disableUser(notes, true);
    }

    private void disableUser(String notes, boolean retry) {
        Preference preference = localDatabase.getPreference();
        logger.debug("Disable user due to invalid " + preference.getEmail());

        String token = preference.getAccessToken();

        try {
            HttpResponse response = wssjService.disableUser(token, notes);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    disableUser(notes, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public ArrayList<LikeHashtag> getLikeHashtagsByToken() {
        return getLikeHashtagsByToken(true);
    }

    private ArrayList<LikeHashtag> getLikeHashtagsByToken(boolean retry) {
        ArrayList<LikeHashtag> likeHashtags = new ArrayList<>();

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getLikeHashtags(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                JSONArray likeHashtagsArr = result.getJSONArray("data");

                likeHashtagsArr.forEach(item -> {
                    JSONObject likeHashtagJO = (JSONObject) item;

                    LikeHashtag likeHashtag = new LikeHashtag();

                    likeHashtag.setId(likeHashtagJO.getInt("id"));
                    likeHashtag.setUserId(likeHashtagJO.getInt("user_id"));
                    likeHashtag.setHashtag(likeHashtagJO.getString("hashtag"));

                    likeHashtags.add(likeHashtag);
                });
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getLikeHashtagsByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return likeHashtags;
    }

    public JSONArray getSpamHashtagsByToken() {
        return getSpamHashtagsByToken(true);
    }

    private JSONArray getSpamHashtagsByToken(boolean retry) {
        JSONArray spamHashtags = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getSpamHashtags(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return null;

                spamHashtags = new JSONArray(result.getString("data"));
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getSpamHashtagsByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return spamHashtags;
    }

    public BadHastag getBadHashtag(String hashtag) {
        return getBadHashtag(hashtag, true);
    }

    private BadHastag getBadHashtag(String hashtag, boolean retry) {
        BadHastag badHastag = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getBadHashtag(token, hashtag);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return  null;
                
                badHastag = new BadHastag(
                    result.getJSONObject("data").getInt("id"),
                    result.getJSONObject("data").getString("hashtag"),
                    result.getJSONObject("data").getInt("not_found_times"),
                    result.getJSONObject("data").getLong("marked_time")
                );
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getBadHashtag(hashtag,false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return badHastag;
    }

    public void markBadHashtag(String hashtag) {
        markBadHashtag(hashtag, true);
    }

    private void markBadHashtag(String hashtag, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.markBadHashtag(token, hashtag);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    markBadHashtag(hashtag, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void insertBadHashtag(String hashtag) {
        Connection connection = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "INSERT INTO bad_hashtags" +
//                    " (id, hashtag, not_found_times, marked_time)" +
//                    " VALUES (NULL, ?, '1', NOW())";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setString(1, hashtag);
//            preparedStatement.execute();
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }
    }

    public void deleteBadHashtag(int badHashtagId) {
        deleteBadHashtag(badHashtagId, true);
    }

    private void deleteBadHashtag(int badHashtagId, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.deleteBadHashtag(token, badHashtagId);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    deleteBadHashtag(badHashtagId,false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public Statistic getOrCreateStatistic(String instagramUsername, String date) {
        return getOrCreateStatistic(instagramUsername, date, true);
    }

    private Statistic getOrCreateStatistic(String instagramUsername, String date, boolean retry) {
        Statistic statistic = null;

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getOrCreateStatistic(token, instagramUsername, date);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if(!result.isNull("data")) {
                    statistic = new Statistic(
                            result.getJSONObject("data").getInt("id"),
                            result.getJSONObject("data").getInt("user_id"),
                            result.getJSONObject("data").getInt("num_likes"),
                            result.getJSONObject("data").getInt("num_followings"),
                            result.getJSONObject("data").getInt("num_followers"),
                            result.getJSONObject("data").getInt("num_unfollows"),
                            result.getJSONObject("data").getString("date"),
                            (result.getJSONObject("data").isNull("last_like_method")) ? null : result.getJSONObject("data").getString("last_like_method"),
                            (result.getJSONObject("data").isNull("last_follow_method")) ? null : result.getJSONObject("data").getString("last_follow_method")
                    );
                }
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getOrCreateStatistic(instagramUsername, date,false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return statistic;
    }

    public void increaseNumLikesStatistic(int id, int numLikes) {
        increaseNumLikesStatistic(id, numLikes, true);
    }

    private void increaseNumLikesStatistic(int id, int numLikes, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseNumLikesStatistic(token, id, numLikes);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseNumLikesStatistic(id, numLikes, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void updateLastLikeMethod(int id, String lastMethod) {
        Connection connection = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "UPDATE statistics SET last_like_method = ? " +
//                    "WHERE id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setString(1, lastMethod);
//            preparedStatement.setInt(2, id);
//            preparedStatement.executeUpdate();
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }
    }


    public ArrayList<LikeLocation> getLikeLocationsByToken() {
        return getLikeLocationsByToken(true);
    }

    private ArrayList<LikeLocation> getLikeLocationsByToken(boolean retry) {
        ArrayList<LikeLocation> likeLocations = new ArrayList<>();

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getLikeLocations(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                JSONArray locations = result.getJSONArray("data");

                locations.forEach(item -> {
                    JSONObject location = (JSONObject) item;

                    LikeLocation likeLocation = new LikeLocation();

                    likeLocation.setId(location.getInt("id"));
                    likeLocation.setUserId(location.getInt("user_id"));
                    likeLocation.setLocationId(location.getString("location_id"));
                    likeLocation.setLocationName((location.isNull("location")) ? "" : location.getJSONObject("location").getString("name"));

                    likeLocations.add(likeLocation);
                });
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getLikeLocationsByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return likeLocations;
    }


    public ArrayList<FollowHashtag> getFollowHashtagsByToken() {
        return getFollowHashtagsByToken(true);
    }

    private ArrayList<FollowHashtag> getFollowHashtagsByToken(boolean retry) {
        ArrayList<FollowHashtag> followHashtags = new ArrayList<>();
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getFollowHashtags(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                JSONArray hashtags = result.getJSONArray("data");

                hashtags.forEach(item -> {
                    JSONObject hashtag = (JSONObject) item;
                    FollowHashtag followHashtag = new FollowHashtag();

                    followHashtag.setId(hashtag.getInt("id"));
                    followHashtag.setUserId(hashtag.getInt("user_id"));
                    followHashtag.setHashtag(hashtag.getString("hashtag"));

                    followHashtags.add(followHashtag);
                });
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getFollowHashtagsByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return followHashtags;
    }


    public void increaseNumFollowsStatistic(int id, int numFollows) {
        increaseNumFollowsStatistic(id, numFollows, true);
    }

    private void increaseNumFollowsStatistic(int id, int numFollows, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseNumFollowsStatistic(token, id, numFollows);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseNumFollowsStatistic(id, numFollows, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public JSONArray getPreviouslyUnfollowUsers() {
        return getPreviouslyUnfollowUsers(true);
    }

    private JSONArray getPreviouslyUnfollowUsers(boolean retry) {
        JSONArray unfollowUsers = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getPreviouslyUnfollowUsers(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return null;

                unfollowUsers = new JSONArray(result.getString("data"));
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getPreviouslyUnfollowUsers(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return unfollowUsers;
    }

    public void updateLastFollowMethod(int id, String lastMethod) {
        Connection connection = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "UPDATE statistics SET last_follow_method = ? " +
//                    "WHERE id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setString(1, lastMethod);
//            preparedStatement.setInt(2, id);
//            preparedStatement.executeUpdate();
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }
    }


    public ArrayList<FollowLocation> getFollowLocationsByToken() {
        return getFollowLocationsByToken(true);
    }

    private ArrayList<FollowLocation> getFollowLocationsByToken(boolean retry) {
        ArrayList<FollowLocation> followLocations = new ArrayList<>();
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getFollowLocations(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                JSONArray locations = result.getJSONArray("data");

                locations.forEach(item -> {
                    JSONObject location = (JSONObject) item;

                    FollowLocation followLocation = new FollowLocation();

                    followLocation.setId(location.getInt("id"));
                    followLocation.setUserId(location.getInt("user_id"));
                    followLocation.setLocationId(location.getString("location_id"));
                    followLocation.setLocationName((location.isNull("location")) ? "" : location.getJSONObject("location").getString("name"));

                    followLocations.add(followLocation);
                });
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getFollowLocationsByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return followLocations;
    }

    public void saveFollowingUser(String followingUserId, String followingUsername, String followingAvatarUrl) {
        saveFollowingUser(followingUserId, followingUsername, followingAvatarUrl, true);
    }

    private void saveFollowingUser(String followingUserId, String followingUsername, String followingAvatarUrl, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.saveFollowingUser(token, followingUserId, followingUsername, followingAvatarUrl);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    saveFollowingUser(followingUserId, followingUsername, followingAvatarUrl, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public ArrayList<FollowingUser> getListFollowingUser(Integer daysToCheckUnfollow) {
        return getListFollowingUser(daysToCheckUnfollow, true);
    }

    private ArrayList<FollowingUser> getListFollowingUser(Integer daysToCheckUnfollow, boolean retry) {
        ArrayList<FollowingUser> followingUsers = new ArrayList<>();
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getListFollowingUser(token, daysToCheckUnfollow);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                JSONArray users = result.getJSONArray("data");

                users.forEach(item -> {
                    JSONObject user = (JSONObject) item;

                    FollowingUser followingUser = new FollowingUser(
                        user.getInt("id"),
                        user.getInt("user_id"),
                        user.getString("instagram_username"),
                        user.getString("instagram_avatar_url"),
                        user.getString("following_user_id"),
                        user.getInt("has_checked") == 1
                    );
                    followingUsers.add(followingUser);
                });
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getListFollowingUser(daysToCheckUnfollow, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return followingUsers;
    }


    public void removeFollowingUser(int id) {
        removeFollowingUser(id, true);
    }

    private void removeFollowingUser(int id, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.removeFollowingUser(token, id);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    removeFollowingUser(id, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void updateCheckedFollowingUser(int id) {
        updateCheckedFollowingUser(id, true);
    }

    private void updateCheckedFollowingUser(int id, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateCheckedFollowingUser(token, id);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateCheckedFollowingUser(id, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void increaseNumUnfollowsStatistic(int id) {
        increaseNumUnfollowsStatistic(id, true);
    }

    private void increaseNumUnfollowsStatistic(int id, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseNumUnfollowsStatistic(token, id);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseNumUnfollowsStatistic(id, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public PreviouslyUnfollowUser getPreviouslyUnfollowUser() {
        return getPreviouslyUnfollowUser(true);
    }

    private PreviouslyUnfollowUser getPreviouslyUnfollowUser(boolean retry) {
        PreviouslyUnfollowUser previouslyUnfollowUser = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getPreviouslyUnfollowUser(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if(!result.isNull("data")) {
                    JSONObject data = result.getJSONObject("data");
                    previouslyUnfollowUser = new PreviouslyUnfollowUser(
                            data.getInt("id"),
                            data.getInt("user_id"),
                            (data.isNull("unfollowed_users")) ? null : data.getString("unfollowed_users")
                    );
                }
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getPreviouslyUnfollowUser(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return previouslyUnfollowUser;
    }


    public void updateUnfollowedUsers(String unfollowedUsers) {
        updateUnfollowedUsers(unfollowedUsers, true);
    }

    private void updateUnfollowedUsers(String unfollowedUsers, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateUnfollowedUsers(token, unfollowedUsers);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateUnfollowedUsers(unfollowedUsers, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public void insertActivity(int type, String instagramNameApp, String instagramUsername, String instagramAvatar, String hashtag, String locationId, String mediaPost, String imagePost) {
        insertActivity(type, instagramNameApp, instagramUsername, instagramAvatar, hashtag, locationId, mediaPost, imagePost, true);
    }

    private void insertActivity(int type, String instagramNameApp, String instagramUsername, String instagramAvatar, String hashtag, String locationId, String mediaPost, String imagePost, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.insertActivity(token, type, instagramNameApp, instagramUsername, instagramAvatar, hashtag, locationId, mediaPost, imagePost);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    insertActivity(type, instagramNameApp, instagramUsername, instagramAvatar, hashtag, locationId, mediaPost, imagePost, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public LocalUser getUserStatus(int userId) {
        LocalUser user = null;
        Connection connection = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT *" +
//                    " FROM users" +
//                    " WHERE id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setInt(1, userId);
//            ResultSet rs = preparedStatement.executeQuery();
//            if (rs.next()) {
//                user = new LocalUser();
//                user.setUserId(rs.getInt("id"));
//                user.setActive(rs.getBoolean("active"));
//                user.setInstagramConnected(rs.getBoolean("is_instagram_connected"));
//            }
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return user;
    }



    public void updateUnfollowBoostStatus(boolean status) {
        updateUnfollowBoostStatus(status, true);
    }

    private void updateUnfollowBoostStatus(boolean status, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateUnfollowBoostStatus(token, status);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateUnfollowBoostStatus(status, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void reduceBoostAmount() {
        reduceBoostAmount(true);
    }

    private void reduceBoostAmount(boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.reduceBoostAmount(token);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    reduceBoostAmount(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public AdvanceSetting getAdvanceSetting() {
        return getAdvanceSetting(true);
    }

    private AdvanceSetting getAdvanceSetting(boolean retry) {
        AdvanceSetting advanceSetting = null;

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getAdvanceSetting(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return null;

                advanceSetting = new AdvanceSetting(
                    result.getJSONObject("data").getInt("id"),
                    (result.getJSONObject("data").isNull("conflict")) ? null : result.getJSONObject("data").getString("conflict"),
                    (result.getJSONObject("data").isNull("white_list")) ? null : result.getJSONObject("data").getString("white_list"),
                    (result.getJSONObject("data").isNull("black_list")) ? null : result.getJSONObject("data").getString("black_list"),
                    result.getJSONObject("data").getInt("mode") == 1
                );
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getAdvanceSetting(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return advanceSetting;
    }

    public InstagramUser getInstagramUser(String userId) {
        return getInstagramUser(userId, true);
    }

    private InstagramUser getInstagramUser(String userId, boolean retry) {
        InstagramUser instagramUser = null;

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getInstagramUser(token, userId);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return  null;

                instagramUser = new InstagramUser(
                    result.getJSONObject("data").getInt("id"),
                    result.getJSONObject("data").getString("instagram_user_id"),
                    result.getJSONObject("data").getString("username")
                );
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getInstagramUser(userId,false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return instagramUser;
    }


    
    public RelatedAccount getRelatedAccount() {
        return getRelatedAccount(true);
    }
    
    private RelatedAccount getRelatedAccount(boolean retry) {
        RelatedAccount relatedAccount = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getRelatedAccount(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return  null;
                
                relatedAccount = new RelatedAccount(
                        result.getJSONObject("data").getInt("id"),
                        result.getJSONObject("data").getString("instagram_id"),
                        result.getJSONObject("data").getString("instagram_username"),
                        result.getJSONObject("data").getInt("status") == 1
                );
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getRelatedAccount(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return relatedAccount;
    }


    public void updateRelatedAccountStatus(int id, boolean status) {
        updateRelatedAccountStatus(id, status, true);
    }

    private void updateRelatedAccountStatus(int id, boolean status, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateRelatedAccountStatus(token, id, status);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateRelatedAccountStatus(id, status, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public void increaseLikeHashtagCount(int id, int numberMediasLiked) {
        increaseLikeHashtagCount(id, numberMediasLiked, true);
    }

    private void increaseLikeHashtagCount(int id, int numberMediasLiked, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseLikeHashtagCount(token, id, numberMediasLiked);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseLikeHashtagCount(id, numberMediasLiked, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void increaseLikeLocationCount(int id, int numLiked) {
        increaseLikeLocationCount(id, numLiked, true);
    }

    private void increaseLikeLocationCount(int id, int numLiked, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseLikeLocationCount(token, id, numLiked);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseLikeLocationCount(id, numLiked, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void increaseFollowHashtagCount(int id, int numFollows) {
        increaseFollowHashtagCount(id, numFollows, true);
    }

    private void increaseFollowHashtagCount(int id, int numFollows, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseFollowHashtagCount(token, id, numFollows);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseFollowHashtagCount(id, numFollows, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public void increaseFollowLocationCount(int id, int numFollows) {
        increaseFollowLocationCount(id, numFollows, true);
    }

    private void increaseFollowLocationCount(int id, int numFollows, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.increaseFollowLocationCount(token, id, numFollows);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    increaseFollowLocationCount(id, numFollows, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public void disableAction(String action, String notes) {
        disableAction(action, notes, true);
    }

    private void disableAction(String action, String notes, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.disableAction(token, action, notes);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    disableAction(action, notes, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }


    public boolean getDisableSendMessageFlg() {
        return getDisableSendMessageFlg(true);
    }

    private boolean getDisableSendMessageFlg(boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getRobotSetting(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                return !result.isNull("data") && result.getJSONObject("data").getInt("disable_send_message") == 1;
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getDisableSendMessageFlg(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return false;
    }

    public Proxy getProxy(int id) {
        Connection connection = null;
        Proxy proxy = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT *" +
//                    " FROM proxies" +
//                    " WHERE id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setInt(1, id);
//            preparedStatement.setBoolean(2, true);
//            ResultSet rs = preparedStatement.executeQuery();
//            if (rs.next()) {
//                proxy = new Proxy();
//                proxy.setId(rs.getInt("id"));
//                proxy.setProxyAddress(rs.getString("proxy_address"));
//                proxy.setProxyPort(rs.getInt("proxy_port"));
//                proxy.setProxyUsername(rs.getString("proxy_username"));
//                proxy.setProxyPassword(rs.getString("proxy_password"));
//            }
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                logger.error("ERROR: ", ex);
//            }
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return proxy;
    }

    public Proxy getProxyByUserId(int userId) {
        Connection connection = null;
        Proxy proxy = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT proxies.*" +
//                    " FROM proxies" +
//                    " INNER JOIN users on users.proxy_id = proxies.id" +
//                    " WHERE users.id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setInt(1, userId);
//            ResultSet rs = preparedStatement.executeQuery();
//            if (rs.next()) {
//                proxy = new Proxy();
//                proxy.setId(rs.getInt("id"));
//                proxy.setProxyAddress(rs.getString("proxy_address"));
//                proxy.setProxyPort(rs.getInt("proxy_port"));
//                proxy.setProxyUsername(rs.getString("proxy_username"));
//                proxy.setProxyPassword(rs.getString("proxy_password"));
//            }
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return proxy;
    }

    public SchedulePost getPostSchedule() {
        return getPostSchedule(true);
    }

    private SchedulePost getPostSchedule(boolean retry) {
        SchedulePost schedulePost = null;

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getPostSchedule(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return  null;

                JSONObject data = result.getJSONObject("data");
                
                schedulePost = new SchedulePost();
                schedulePost.setId(data.getInt("id"));
                schedulePost.setUserId(data.getInt("user_id"));
                schedulePost.setImagePath(Const.BASE_WSSJ_URL + "/" + data.getString("img_src"));

                if (!data.isNull("caption")) schedulePost.setCaption(data.getString("caption"));
                else schedulePost.setCaption(null);

                if (!data.isNull("location")) schedulePost.setLocation(data.getString("location"));
                else schedulePost.setLocation(null);

                schedulePost.setTimeToAction(stringToTimestamp(data.getString("time_to_post")));

                schedulePost.setDeleteSchedule(data.getInt("delete_schedule") == 1);

                if (!data.isNull("repeat_time")) schedulePost.setRepeatTime(data.getInt("repeat_time"));
                else schedulePost.setRepeatTime(0);

                schedulePost.setPostLoop(data.getInt("post_loop") == 1);

                if (!data.isNull("day_end_schedule")) schedulePost.setDayEndSchedule(stringToTimestamp(data.getString("day_end_schedule")));
                else schedulePost.setDayEndSchedule(null);

                schedulePost.setStatus(data.getInt("status"));

                if (!data.isNull("link_after_post")) schedulePost.setLinkAfterPost(data.getString("link_after_post"));
                else schedulePost.setLinkAfterPost(null);

                if (!data.isNull("lastest_post")) schedulePost.setLastestPost(stringToTimestamp(data.getString("lastest_post")));
                else schedulePost.setLastestPost(null);

                if (!data.isNull("next_time_upload")) schedulePost.setNextTimeUpload(stringToTimestamp(data.getString("next_time_upload")));
                else schedulePost.setNextTimeUpload(null);
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getPostSchedule(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return schedulePost;
    }


    public void updatePostScheduleStatus(int postSchedule, int status) {
        updatePostScheduleStatus(postSchedule, status, true);
    }

    private void updatePostScheduleStatus(int postSchedule, int status, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updatePostSchedule(token, postSchedule, status, null,
                    null, null, null);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updatePostScheduleStatus(postSchedule, status, false);
            }
        } catch (IOException | URISyntaxException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void deletePostSchedule(int scheduleId) {
        deletePostSchedule(scheduleId, true);
    }

    private void deletePostSchedule(int scheduleId, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.deletePostSchedule(token, scheduleId);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    deletePostSchedule(scheduleId, false);
            }
        } catch (IOException | URISyntaxException e) {
            logger.error("ERROR: ", e);
        }
    }

    public void updatePostSchedule(SchedulePost schedulePost) {
        updatePostSchedule(schedulePost, true);
    }

    private void updatePostSchedule(SchedulePost schedulePost, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updatePostSchedule(token,
                    schedulePost.getId(),
                    schedulePost.getStatus(),
                    schedulePost.getLastestPost().getTime(),
                    schedulePost.getLinkAfterPost(),
                    schedulePost.getNextTimeUpload() == null ? null : schedulePost.getNextTimeUpload().getTime(),
                    schedulePost.getTimeToAction().getTime());

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updatePostSchedule(schedulePost, false);
            }
        } catch (IOException | URISyntaxException e) {
            logger.error("ERROR: ", e);
        }
    }

    public ScheduleComment getCommentSchedule() {
        return getCommentSchedule(true);
    }

    private ScheduleComment getCommentSchedule(boolean retry) {
        ScheduleComment scheduleComment = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getCommentSchedule(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return  null;

                scheduleComment = new ScheduleComment();
                scheduleComment.setId(result.getJSONObject("data").getInt("id"));
                scheduleComment.setUserId(result.getJSONObject("data").getInt("user_id"));
                scheduleComment.setMediaId(String.valueOf(result.getJSONObject("data").opt("post_id")));
                scheduleComment.setCommentContent(result.getJSONObject("data").getString("content_comment"));
                scheduleComment.setDelay(result.getJSONObject("data").getInt("delay"));
                scheduleComment.setTimeToAction(stringToTimestamp(result.getJSONObject("data").getString("time_post")));
                scheduleComment.setMediaCode(result.getJSONObject("data").getString("post_code"));
                scheduleComment.setStatus(result.getJSONObject("data").getInt("status"));
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getCommentSchedule(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return scheduleComment;
    }

    public void updateCommentScheduleStatus(int commentScheduleId, int status) {
        updateCommentScheduleStatus(commentScheduleId, status, true);
    }

    private void updateCommentScheduleStatus(int commentScheduleId, int status, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateCommentScheduleStatus(token, commentScheduleId, status);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateCommentScheduleStatus(commentScheduleId, status, false);
            }
        } catch (IOException | URISyntaxException e) {
            logger.error("ERROR: ", e);
        }
    }


    public ScheduleSendMessage getSendMessageSchedule() {
        return getSendMessageSchedule(true);
    }

    private ScheduleSendMessage getSendMessageSchedule(boolean retry) {
        ScheduleSendMessage scheduleSendMessage = null;

        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getSendMessageSchedule(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                if (result.isNull("data"))
                    return  null;

                JSONObject data = result.getJSONObject("data");
                
                scheduleSendMessage = new ScheduleSendMessage();
                scheduleSendMessage.setId(data.getInt("id"));
                scheduleSendMessage.setUserId(data.getInt("user_id"));
                scheduleSendMessage.setInstagramUserId(data.getString("instagram_user_id"));
                scheduleSendMessage.setMessage(data.getString("message"));
                scheduleSendMessage.setTimeToAction(stringToTimestamp(data.getString("send_at")));
                scheduleSendMessage.setInstagramName(data.getString("instagram_name"));
                scheduleSendMessage.setStatus(data.getInt("status"));
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getSendMessageSchedule(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return scheduleSendMessage;
    }


    public void updateSendMessageScheduleStatus(int sendMessageScheduleId, int status) {
        updateSendMessageScheduleStatus(sendMessageScheduleId, status, true);
    }

    private void updateSendMessageScheduleStatus(int sendMessageScheduleId, int status, boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.updateSendMessageScheduleStatus(token, sendMessageScheduleId, status);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    updateSendMessageScheduleStatus(sendMessageScheduleId, status, false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }
    }

    public User getUser(int id) {
        User user = null;
        Connection connection = null;

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT *" +
//                    " FROM users" +
//                    " WHERE id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setInt(1, id);
//            ResultSet rs = preparedStatement.executeQuery();
//            if (rs.next()) {
//                user = new User();
//                user.setId(rs.getInt("id"));
//                user.setEmail(rs.getString("email"));
//                user.setRobotWorking(rs.getBoolean("is_robot_working"));
//            }
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return user;
    }

    public User getUserByToken () {
        return getUserByToken(true);
    }

    private User getUserByToken(boolean retry) {
        User user = null;
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.getUser(token);

            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject result = new JSONObject(EntityUtils.toString(response.getEntity()));

                user = new User();

                user.setId(result.getJSONObject("data").getJSONObject("user").getInt("id"));
                user.setEmail(result.getJSONObject("data").getJSONObject("user").getString("email"));
                user.setRobotWorking(result.getJSONObject("data").getJSONObject("user").getInt("is_robot_working") == 1);
            }

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    return getUserByToken(false);
            }
        } catch (IOException | URISyntaxException |JSONException e) {
            logger.error("ERROR: ", e);
        }

        return user;
    }

    public List<User> getUnactiveRobotUsersBelongProxy(Proxy proxy) {
        Connection connection = null;
        ArrayList<User> users = new ArrayList<>();

        logger.debug("Start get list user!");

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT * FROM users " +
//                    "WHERE is_active_robot = ? and is_instagram_connected = ? and active = ? and proxy_id = ?";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            preparedStatement.setBoolean(1, false);
//            preparedStatement.setBoolean(2, true);
//            preparedStatement.setBoolean(3, true);
//            preparedStatement.setInt(4, proxy.getId());
//            ResultSet rs = preparedStatement.executeQuery();
//            while (rs.next()) {
//                logger.debug("get user " + rs.getString("email"));
//
//                User user = new User();
//                user.setId(rs.getInt("id"));
//                user.setEmail(rs.getString("email"));
//                user.setRobotWorking(rs.getBoolean("is_robot_working"));
//                users.add(user);
//            }
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return users;
    }

    public List<Proxy> getActiveProxies() {
        Connection connection = null;
        List<Proxy> proxies = new ArrayList<>();

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT *" +
//                    " FROM proxies" +
//                    " WHERE is_good > 0 order by id asc";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            ResultSet rs = preparedStatement.executeQuery();
//            Proxy proxy;
//            while (rs.next()) {
//                proxy = new Proxy();
//                proxy.setId(rs.getInt("id"));
//                proxy.setProxyAddress(rs.getString("proxy_address"));
//                proxy.setProxyPort(rs.getInt("proxy_port"));
//                proxy.setProxyUsername(rs.getString("proxy_username"));
//                proxy.setProxyPassword(rs.getString("proxy_password"));
//
//                proxies.add(proxy);
//            }
//            connection.commit();
//        } catch (SQLException ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                logger.error("ERROR: ", ex);
//            }
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return proxies;
    }

    public List<ScheduleSendMessage> getListScheduleSendMessage() {
        Connection connection = null;
        ScheduleSendMessage scheduleSendMessage = null;

        List<ScheduleSendMessage> listSchedule = new ArrayList<>();

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT asm.id, asm.user_id, asm.instagram_user_id, message, time_delay, send_at, instagram_name, status" +
//                    " FROM auto_send_message AS asm" +
//                    " LEFT JOIN users AS u ON u.id = asm.user_id" +
//                    " LEFT JOIN robot_settings r ON r.user_id = asm.user_id" +
//                    " WHERE asm.status = 0 AND send_at <= now() AND u.active = 1 " +
//                    " AND u.is_instagram_connected = 1 AND r.disable_send_message = 0" +
//                    " ORDER BY send_at ASC";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            ResultSet rs = preparedStatement.executeQuery();
//            while (rs.next()) {
//                scheduleSendMessage = new ScheduleSendMessage();
//                scheduleSendMessage.setId(rs.getInt("id"));
//                scheduleSendMessage.setUserId(rs.getInt("user_id"));
//                scheduleSendMessage.setInstagramUserId(rs.getString("instagram_user_id"));
//                scheduleSendMessage.setMessage(rs.getString("message"));
//                scheduleSendMessage.setDelay(rs.getInt("time_delay"));
//                scheduleSendMessage.setTimeToAction(rs.getTimestamp("send_at"));
//                scheduleSendMessage.setInstagramName(rs.getString("instagram_name"));
//                scheduleSendMessage.setStatus(rs.getInt("status"));
//
//                listSchedule.add(scheduleSendMessage);
//            }
//        } catch (Exception ex) {
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return listSchedule;
    }

    public List<ScheduleComment> getListScheduleComment() {
        Connection connection = null;
        List<ScheduleComment> listScheduleComment = new ArrayList<>();

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT sc.id, sc.user_id, post_id, content_comment, delay, time_post, post_code, status" +
//                    " FROM schedule_comments AS sc" +
//                    " LEFT JOIN users AS u ON u.id = sc.user_id" +
//                    " LEFT JOIN robot_settings r ON r.user_id = u.id" +
//                    " WHERE sc.status = 0 AND time_post <= now() AND u.active = 1 AND u.is_instagram_connected = 1" +
//                    " ORDER BY time_post ASC";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            ResultSet rs = preparedStatement.executeQuery();
//            ScheduleComment scheduleComment;
//            while (rs.next()) {
//                scheduleComment = new ScheduleComment();
//                scheduleComment.setId(rs.getInt("id"));
//                scheduleComment.setUserId(rs.getInt("user_id"));
//                scheduleComment.setMediaId(rs.getString("post_id"));
//                scheduleComment.setCommentContent(rs.getString("content_comment"));
//                scheduleComment.setDelay(rs.getInt("delay"));
//                scheduleComment.setTimeToAction(rs.getTimestamp("time_post"));
//                scheduleComment.setMediaCode(rs.getString("post_code"));
//                scheduleComment.setStatus(rs.getInt("status"));
//
//                listScheduleComment.add(scheduleComment);
//            }
//            connection.commit();
//        } catch (Exception ex) {
//            // handle any errors
//            try {
//                if (connection != null) {
//                    connection.rollback();
//                }
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return listScheduleComment;
    }

    public List<SchedulePost> getListSchedulePost() {
        Connection connection = null;
        List<SchedulePost> listSchedulePost = new ArrayList<>();

//        try {
//            BasicDataSource dataSource = connectionPool.getDataSource();
//            connection = dataSource.getConnection();
//
//            String SQL = "SELECT sp.id, sp.user_id, img_src_root, caption, location, time_to_post, delete_schedule, " +
//                    " repeat_time, post_loop, day_end_schedule, status, link_after_post, lastest_post, next_time_upload" +
//                    " FROM schedule_post sp" +
//                    " LEFT JOIN users AS u ON u.id = sp.user_id" +
//                    " LEFT JOIN robot_settings r ON r.user_id = u.id" +
//                    " WHERE status IN (0,1) AND (day_end_schedule > now() OR delete_schedule = 1) AND time_to_post <= now()" +
//                    " AND (next_time_upload is null OR next_time_upload < now())" +
//                    " AND u.active = 1 AND u.is_instagram_connected = 1" +
//                    " ORDER BY time_to_post ASC";
//            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
//            ResultSet rs = preparedStatement.executeQuery();
//            SchedulePost schedulePost = null;
//            while (rs.next()) {
//                schedulePost = new SchedulePost();
//                schedulePost.setId(rs.getInt("id"));
//                schedulePost.setUserId(rs.getInt("user_id"));
//                schedulePost.setImagePath(rs.getString("img_src_root"));
//                schedulePost.setCaption(rs.getString("caption"));
//                schedulePost.setLocation(rs.getString("location"));
//                schedulePost.setTimeToAction(rs.getTimestamp("time_to_post"));
//                schedulePost.setDeleteSchedule(rs.getBoolean("delete_schedule"));
//                schedulePost.setRepeatTime(rs.getInt("repeat_time"));
//                schedulePost.setPostLoop(rs.getBoolean("post_loop"));
//                schedulePost.setDayEndSchedule(rs.getTimestamp("day_end_schedule"));
//                schedulePost.setStatus(rs.getInt("status"));
//                schedulePost.setLinkAfterPost(rs.getString("link_after_post"));
//                schedulePost.setLastestPost(rs.getTimestamp("lastest_post"));
//                schedulePost.setNextTimeUpload(rs.getTimestamp("next_time_upload"));
//
//                listSchedulePost.add(schedulePost);
//            }
//        } catch (Exception ex) {
//            logger.error("ERROR: ", ex);
//        } finally {
//            try {
//                if (connection != null) connection.close();
//            } catch (SQLException e) {
//                logger.error("ERROR: ", e);
//            }
//        }

        return listSchedulePost;
    }


    public void cancelScheduleSendMessage() {
        cancelScheduleSendMessage(true);
    }

    private void cancelScheduleSendMessage(boolean retry) {
        String token = localDatabase.getPreference().getAccessToken();

        try {
            HttpResponse response = wssjService.cancelScheduleSendMessage(token);

            if (response.getStatusLine().getStatusCode() == 401 && retry) {
                String newToken = refreshToken();
                if (!TextUtils.isEmpty(newToken))
                    cancelScheduleSendMessage(false);
            }
        } catch (IOException | URISyntaxException e) {
            logger.error("ERROR: ", e);
        }
    }
}
