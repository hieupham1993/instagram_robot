package jp.co.rilarc.instagram_robot.helpers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import jp.co.rilarc.instagram_robot.handler.InstagramUser;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.BadHastag;
import jp.co.rilarc.instagram_robot.model.FollowHashtag;
import jp.co.rilarc.instagram_robot.model.FollowLocation;
import jp.co.rilarc.instagram_robot.model.FollowingUser;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.LikeHashtag;
import jp.co.rilarc.instagram_robot.model.LikeLocation;
import jp.co.rilarc.instagram_robot.model.LocalUser;
import jp.co.rilarc.instagram_robot.model.PreviouslyUnfollowUser;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.RelatedAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;
import jp.co.rilarc.instagram_robot.model.SchedulePost;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.model.User;

public interface IAPIHelper {

	/**
	 * Ham login vao he thong
	 * @param email
	 * @param password
	 * @return User instance - success, null: login failed
	 */
	User loginToSystem(String email, String password);

	void resetAllUser();
	InstagramAccount getInstagramAccountByToken();
	RobotSetting getRobotSettingByToken();
	ArrayList<User> getListUnactiveRobotUsers();
	void updateRobotStatus(boolean robotStatus);
	void updateRobotWorkingStatus(int userId, boolean robotStatus);
	void updateInstagramConnectStatus(boolean connectStatus, String notes);
	void disableUser(String notes);
	ArrayList<LikeHashtag> getLikeHashtagsByToken();
	JSONArray getSpamHashtagsByToken();
	BadHastag getBadHashtag(String hashtag);
	void insertBadHashtag(String hashtag);
	void deleteBadHashtag(int badHashtagId);
	void markBadHashtag(String hashtag);
	Statistic getOrCreateStatistic(String instagramUsername, String date);
	void increaseNumLikesStatistic(int id, int numLikes);
	void updateLastLikeMethod(int id, String lastMethod);
	ArrayList<LikeLocation> getLikeLocationsByToken();
	ArrayList<FollowHashtag> getFollowHashtagsByToken();
	void increaseNumFollowsStatistic(int id, int numFollows);
	JSONArray getPreviouslyUnfollowUsers();
	void updateLastFollowMethod(int id, String lastMethod);
	ArrayList<FollowLocation> getFollowLocationsByToken();
	void saveFollowingUser(String followingUserId, String followingUsername, String followingAvatarUrl);
	ArrayList<FollowingUser> getListFollowingUser(Integer daysToCheckUnfollow);
	void removeFollowingUser(int id);
	void updateCheckedFollowingUser(int id);
	void increaseNumUnfollowsStatistic(int id);
	PreviouslyUnfollowUser getPreviouslyUnfollowUser();
	void updateUnfollowedUsers(String unfollowedUsers);
	void insertActivity(int type, String instagramNameApp, String instagramUsername, String instagramAvatar, String hashtag, String location, String mediaPost, String imagePost);
	LocalUser getUserStatus(int userId);
	void updateUnfollowBoostStatus(boolean status);
	void reduceBoostAmount();
	AdvanceSetting getAdvanceSetting();
	InstagramUser getInstagramUser(String userId);
	RelatedAccount getRelatedAccount();
	void updateRelatedAccountStatus(int id, boolean status);
	void increaseLikeHashtagCount(int id, int numberMediasLiked);
	void increaseLikeLocationCount(int id, int numLiked);
	void increaseFollowHashtagCount(int id, int numFollows);
	void increaseFollowLocationCount(int id, int numFollows);
	void disableAction(String action, String notes);
	boolean getDisableSendMessageFlg();
	Proxy getProxy(int id);
	Proxy getProxyByUserId(int userId);
	SchedulePost getPostSchedule();
	void updatePostScheduleStatus (int postSchedule, int status);
	void deletePostSchedule(int scheduleId);
	void updatePostSchedule(SchedulePost schedulePost);
	ScheduleComment getCommentSchedule();
	void updateCommentScheduleStatus(int commentScheduleId, int status);
	ScheduleSendMessage getSendMessageSchedule();
	void updateSendMessageScheduleStatus(int sendMessageScheduleId, int status);
	User getUser(int id);
	User getUserByToken();
	List<User> getUnactiveRobotUsersBelongProxy(Proxy proxy);
	List<Proxy> getActiveProxies();
	List<ScheduleSendMessage> getListScheduleSendMessage();
	List<ScheduleComment> getListScheduleComment();
	List<SchedulePost> getListSchedulePost();
	void cancelScheduleSendMessage();
}
