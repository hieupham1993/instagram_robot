package jp.co.rilarc.instagram_robot.helpers;

import jp.co.rilarc.instagram_robot.utils.Configuration;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 * Created by nguyenthanhson on 10/23/17.
 */
public class ConnectionPool {
    private final String jdbcUrl;
    private final String userName;
    private final String password;

    public ConnectionPool(Configuration config) {
//		ds.setUrl("jdbc:mysql://localhost/test");
        jdbcUrl = "jdbc:mysql://" + config.getHost() + "/" + config.getDatabase();
        userName = config.getUsername();
        password= config.getPassword();
    }

    private static BasicDataSource dataSource;

    public BasicDataSource getDataSource() {
        if (dataSource == null) {
            BasicDataSource ds = new BasicDataSource();
            ds.setUrl(jdbcUrl);
            ds.setUsername(userName);
            ds.setPassword(password);

            ds.setConnectionProperties("useUnicode=true;characterEncoding=UTF-8;verifyServerCertificate=false;useSSL=false");
            ds.setMinIdle(500);
            ds.setMaxIdle(1000);
            ds.setMaxOpenPreparedStatements(100);
            ds.setDefaultAutoCommit(false);

            dataSource = ds;
        }
        return dataSource;
    }
}
