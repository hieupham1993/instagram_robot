package jp.co.rilarc.instagram_robot.exception;

public class UnfollowBlockedException extends ActionBlockException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UnfollowBlockedException(String msg) {
		super(msg);
	}
}
