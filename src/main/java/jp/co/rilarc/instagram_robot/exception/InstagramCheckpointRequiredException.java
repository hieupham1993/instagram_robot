package jp.co.rilarc.instagram_robot.exception;

public class InstagramCheckpointRequiredException extends InstagramCannotAccessException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InstagramCheckpointRequiredException(String msg) {
		super(msg);
	}
}
