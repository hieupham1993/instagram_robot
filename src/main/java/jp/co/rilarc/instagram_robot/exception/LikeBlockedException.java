package jp.co.rilarc.instagram_robot.exception;

public class LikeBlockedException extends ActionBlockException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LikeBlockedException(String msg) {
		super(msg);
	}
}
