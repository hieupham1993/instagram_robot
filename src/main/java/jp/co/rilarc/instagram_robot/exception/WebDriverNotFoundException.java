package jp.co.rilarc.instagram_robot.exception;

/**
 * Created by HieuPT on 4/16/2018.
 */
public class WebDriverNotFoundException extends Exception {

    public WebDriverNotFoundException(String message, Throwable e) {
        super(message, e);
    }
}
