package jp.co.rilarc.instagram_robot.exception;

public class FollowBlockedException extends ActionBlockException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FollowBlockedException(String msg) {
		super(msg);
	}
}
