package jp.co.rilarc.instagram_robot.exception;

public class InstagramCannotAccessException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public InstagramCannotAccessException(String msg) {
		super(msg);
	}
}
