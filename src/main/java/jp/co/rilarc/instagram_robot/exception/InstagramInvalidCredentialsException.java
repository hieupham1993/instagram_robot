package jp.co.rilarc.instagram_robot.exception;


public class InstagramInvalidCredentialsException extends InstagramCannotAccessException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public InstagramInvalidCredentialsException(String msg) {
		super(msg);
	}
}
