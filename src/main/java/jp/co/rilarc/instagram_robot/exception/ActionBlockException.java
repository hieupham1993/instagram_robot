package jp.co.rilarc.instagram_robot.exception;

public class ActionBlockException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ActionBlockException(String msg) {
		super(msg);
	}
}
