package jp.co.rilarc.instagram_robot.exception;

/**
 * Created by HieuPT on 1/29/2018.
 */
public class InstagramSentryBlockException extends InstagramCannotAccessException {

    public InstagramSentryBlockException(String msg) {
        super(msg);
    }
}
