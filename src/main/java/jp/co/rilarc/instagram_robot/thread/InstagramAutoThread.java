package jp.co.rilarc.instagram_robot.thread;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.exception.InstagramInvalidCredentialsException;
import jp.co.rilarc.instagram_robot.exception.InstagramSentryBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramUserInvalidException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.model.Action;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.InstagramAccount;
import jp.co.rilarc.instagram_robot.model.LocalUser;
import jp.co.rilarc.instagram_robot.model.Proxy;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.User;
import jp.co.rilarc.instagram_robot.thread.ActionRotation.ActionInterval;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import jp.co.rilarc.instagram_robot.utils.Utils;

public class InstagramAutoThread extends Thread {
    final private static Logger logger = LogManager.getLogger(InstagramAutoThread.class);
    
	private static final int MILISECONDS_PER_MINUTE = 60 * 1000;

	private User mUser;
    private InstagramAccount mInstagramAccount;
    private Instagram mInstagram;
    private RobotSetting mRobotSetting;
    private Configuration mConfig;
    private Proxy mProxy;
    final private IAPIHelper dbHelper;
    
    private java.sql.Date lastUpdateRobotSetting = null;
    
    private final ActionRotation actionRotation;
    private final List<ActionInterval> listActionInterval;
    
    private boolean isNewday = false;
    
    Random rand = new Random();

    public InstagramAutoThread(User mUser, IAPIHelper IAPIHelper) {
        this.mUser = mUser;

        mConfig = new Configuration();
        dbHelper = IAPIHelper;

        mInstagramAccount = dbHelper.getInstagramAccountByToken();
        
        mRobotSetting = dbHelper.getRobotSettingByToken();
        
        actionRotation = new ActionRotation(mRobotSetting, mConfig);
        listActionInterval = actionRotation.getListActionInterval();
    }

    @Override
    public void run() {
        if (mInstagramAccount == null) {
            logger.info("user " + mUser.getEmail() + " have no mInstagram account");
            return;
        }

        dbHelper.updateRobotStatus(true);
        mInstagram = new Instagram(mInstagramAccount);

        logger.info("start process of account: " + mInstagramAccount.getUsername());

        Date checkedDate = Utils.getCurrentZeroTimeDate();
        while (true) {
            try {
                mConfig = new Configuration();

                // get robot setting
                mRobotSetting = dbHelper.getRobotSettingByToken();

                // check robot stop thi thoat ra khoi vong lap va dung thread doi voi user nay
                if (mRobotSetting == null) {
                    logger.info("Turn off robot for user " + mUser.getId());
                    break;
                }
                // check sleep mode
                if (mRobotSetting.isSleepTime()) {
                    waitFor(5); // sleep 5 minutes
                    continue;
                }

                if (Utils.getCurrentZeroTimeDate().after(checkedDate)) {
                    checkedDate = Utils.getCurrentZeroTimeDate();
                    isNewday = true;
                }
                
                if(isNewday) {
                	isNewday = false;
                	actionRotation.reset(mRobotSetting, mConfig);  // Setting lai thu tu chay cac action khi sang ngay moi
                    logger.info("account " + mInstagramAccount.getUsername() + " reset list action: " + listActionInterval);
                }

                java.sql.Date updateRobotSettingTime = mRobotSetting.getmUpdatedAt();
                
                // Check xem neu robotSetting bi update thi reset lai toan bo interval
            	if(isRobotSettingUpdated(lastUpdateRobotSetting, updateRobotSettingTime) 
            			|| (updateRobotSettingTime == null && listActionInterval.isEmpty())) {
            		
            		actionRotation.reset(mRobotSetting, mConfig);
                    lastUpdateRobotSetting = updateRobotSettingTime;
            	}
            	
            	// Neu user inactive thi thoat ra khoi vong lap va dung thread doi voi user nay
            	LocalUser user = dbHelper.getUserStatus(mUser.getId());
            	if (!(user.isActive() && user.isInstagramConnected())) {
                    logger.info("Exited due to user " + mUser.getEmail() + " has expired! or cannot connect to instagram");
                    break;
                }

                // get proxy
                mProxy = dbHelper.getProxyByUserId(mUser.getId());
                mInstagram.setProxy(mProxy);

                if(!prepareForAction()) {
                	break;
                }

                // Lay ra action tiep theo
                if(!listActionInterval.isEmpty()) {
                	ActionInterval nextAction = listActionInterval.get(0);
                	// Cho cho den luc co the chay duoc
                	waitFor(nextAction.interval);
                	// Chay thread 
                	launchAction(nextAction.action);
                }
            } catch (Exception e) {
                logger.error("account " + mInstagramAccount.getUsername() + " error: " + e);
                break;
            }
        }

        logger.info("stop process of account: " + mInstagramAccount.getUsername());

        dbHelper.updateRobotStatus(false);
        dbHelper.updateRobotWorkingStatus(mUser.getId(), false);
    }
    
    /**
     * Check when robot setting was updated
     * @param lastUpdateRobotSetting Gio update cuoi cung cua robot setting
     * @param updateRobotSetting	Gio update cua robot setting lay ra tu database
     * @return true neu 2 gia tri nay khac nhau
     */
    protected boolean isRobotSettingUpdated(java.sql.Date lastUpdateRobotSetting, java.sql.Date updateRobotSetting) {
    	if(lastUpdateRobotSetting == null && updateRobotSetting == null) {
    		return false;
    	}
    	
    	if(lastUpdateRobotSetting == null && updateRobotSetting != null) {
    		return true;
    	}
    	
    	if(lastUpdateRobotSetting.before(updateRobotSetting)) {
    		return true;
    	}
    	
    	return false;
    }
    
    private void waitFor(int intervalTime) {
        try {
            logger.debug("wait " + intervalTime + " minutes");
            Thread.sleep(intervalTime * MILISECONDS_PER_MINUTE);        // 5m
        } catch (InterruptedException e) {
            logger.error("ERROR: ", e);
        }
	}

	protected void launchAction(Action action) {
		actionRotation.resetActionInterval();
    	
		switch(action) {
		case LIKE:
			startLikeThread();
			actionRotation.removeAction(Action.LIKE);
			break;
		case FOLLOW:
			startFollowThread();
			actionRotation.removeAction(Action.FOLLOW);
			break;
		case UNFOLLOW:
			startUnfollowThread();
			actionRotation.removeAction(Action.UNFOLLOW);
			break;
        case UPLOAD_PHOTO:
            startUploadPhotoThread();
            break;
        case SEND_COMMENT:
            startSendCommentThread();
            break;
        case SEND_MESSAGE:
            startSendMessageThread();
            break;
        default:
            break;
		}
    }
    
    protected void startLikeThread() {

    	AdvanceSetting advanceSetting = dbHelper.getAdvanceSetting();

        // create statistic
//        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//        Statistic statistic = dbHelper.getOrCreateStatistic(mUser.getId(), mInstagram.getInstagramAccount().getUsername(), date);


        if ((mRobotSetting.isLikesHashtagAutoLike() || mRobotSetting.isLikesLocationAutoLike()
        		|| mRobotSetting.isLikesFollowingAutoLikeFeed()) && !mRobotSetting.isDisableLike()) { // check enable auto like
            logger.info("account " + mInstagramAccount.getUsername() + ": run like thread");

            LikeThread likeThread = new LikeThread(mInstagram, mRobotSetting, advanceSetting, dbHelper, actionRotation.getLikeRunDuration());
            likeThread.start();
        }
    }
    
    protected void startFollowThread() {
    	AdvanceSetting advanceSetting = dbHelper.getAdvanceSetting();

        // create statistic
//        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//        Statistic statistic = dbHelper.getOrCreateStatistic(mUser.getId(), mInstagram.getInstagramAccount().getUsername(), date);

        if ((mRobotSetting.isFollowsHashtagAutoFollow() || mRobotSetting.isFollowsLocationAutoFollow() 
        		|| mRobotSetting.isFollowsFollowersAutoFollowBack() 
        		|| (advanceSetting != null && advanceSetting.isActive()))
        		&& !mRobotSetting.isUnfollowsBoostUnfollow()
        		&& !mRobotSetting.isDisableFollow()) { // check enable auto like
            logger.debug("account " + mInstagramAccount.getUsername() + ": run follow thread");

            FollowThread followThread = new FollowThread(mInstagram, mRobotSetting, advanceSetting, dbHelper, actionRotation.getFollowRunDuration());
            followThread.start();
        }
    }
    
    protected void startUnfollowThread() {
    	AdvanceSetting advanceSetting = dbHelper.getAdvanceSetting();

        // create statistic
//        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
//        Statistic statistic = dbHelper.getOrCreateStatistic(mUser.getId(), mInstagram.getInstagramAccount().getUsername(), date);

        if (mRobotSetting.isUnfollowsNotFollowingBackAutoUnfollow() || mRobotSetting.isUnfollowsBoostUnfollow()) {
            logger.debug("account " + mInstagramAccount.getUsername() + ": run unfollow thread");

            UnfollowThread unfollowThread = new UnfollowThread(mInstagram, mRobotSetting, advanceSetting, dbHelper, actionRotation.getUnfollowRunDuration());
            unfollowThread.start();
        }
    }

    protected void startUploadPhotoThread() {
        logger.debug("account " + mInstagramAccount.getUsername() + ": run upload photo thread");

        PostThread postThread = new PostThread(mInstagram, mRobotSetting, dbHelper);
        postThread.start();
    }
    
    protected void startSendCommentThread() {
        logger.debug("account " + mInstagramAccount.getUsername() + ": run send commentsetting thread");

        CommentThread commentThread = new CommentThread(mInstagram, mRobotSetting, dbHelper);
        commentThread.start();
    }

    protected void startSendMessageThread() {
        logger.debug("account " + mInstagramAccount.getUsername() + ": run send message thread");

        SendMessageThread sendMessageThread = new SendMessageThread(mInstagram, mRobotSetting, dbHelper);
        sendMessageThread.start();
    }

    protected boolean prepareForAction() {
    	// check mUser add or change mInstagram account
        InstagramAccount checkInstagramAccount = dbHelper.getInstagramAccountByToken();
        // mUser remove mInstagram account
        if (checkInstagramAccount == null) {
            logger.info("user_id " + mUser.getId() + " remove mInstagram account");
            return false;
        }

        // mUser change mInstagram account
        if (checkInstagramAccount.getAccountId() == null ||
                !Objects.equals(checkInstagramAccount.getAccountId(), mInstagramAccount.getAccountId())) {
            mInstagramAccount = checkInstagramAccount;
            mInstagram = new Instagram(mInstagramAccount);
            mInstagram.setProxy(mProxy);

            logger.info("account " + mInstagramAccount.getUsername() + ": change instagram user");

            if (!this.loginInstagram(true)) {
                logger.info("account: " + mInstagramAccount.getUsername() + " login failed");
                dbHelper.updateInstagramConnectStatus(false, null);
                return false;
            }
        }

        // login mInstagram
        if (mInstagramAccount.isRefreshLogin()) {
            logger.info("account: " + mInstagramAccount.getUsername() + " relogin");

            if (!this.loginInstagram(false)) {
                dbHelper.updateInstagramConnectStatus(false, null);
                logger.info("account: " + mInstagramAccount.getUsername() + " login failed");
                return false;
            }
        }

        if (!mUser.isRobotWorking()) {
            mUser.setRobotWorking(true);
            dbHelper.updateRobotWorkingStatus(mUser.getId(), true);
        }
        
        return true;
    }

    private boolean loginInstagram(boolean forceLogin) {
        boolean loginResult = false;
        try {
            loginResult = mInstagram.login(forceLogin, mConfig.isEnablePhantomjs());
        } catch (IOException | URISyntaxException e) {
        		logger.error("error", e);
        } catch(InstagramInvalidCredentialsException | InstagramUserInvalidException e) {
        		logger.error("error", e);
        		dbHelper.disableUser(e.getMessage());
        } catch(InstagramCheckpointRequiredException e) {
	        	logger.error("error", e);
	        	dbHelper.updateInstagramConnectStatus(false,
        			e.getMessage());
        } catch (InstagramSentryBlockException e) {
            logger.error("error", e);
        }

        return loginResult;
    }
}
