package jp.co.rilarc.instagram_robot.thread;

import jp.co.rilarc.instagram_robot.action.BaseAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.like.Like;
import jp.co.rilarc.instagram_robot.like.LikeByFollowing;
import jp.co.rilarc.instagram_robot.like.LikeByHashtag;
import jp.co.rilarc.instagram_robot.like.LikeByLocation;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class LikeThread extends Thread {

    private Instagram mInstagram;

    private RobotSetting mRobotSetting;

    final private int mUserId;

    final private static Logger logger = LogManager.getLogger(LikeThread.class);

    final private IAPIHelper dbHelper;

    final private Configuration config;

    private AdvanceSetting mAdvanceSetting;

    final private int duration;

    public LikeThread(Instagram instagram, RobotSetting robotSetting, AdvanceSetting advanceSetting, IAPIHelper IAPIHelper, int duration) {
        mInstagram = instagram;
        mRobotSetting = robotSetting;
        mUserId = mInstagram.getInstagramAccount().getUserId();
        mAdvanceSetting = advanceSetting;

        config = new Configuration();
        dbHelper = IAPIHelper;
        this.duration = duration;
        System.out.println("duration " + duration);
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        try {
            runThread(startTime);
        } catch (Exception e) {
            logger.error("Unknown Error: ", e);
        }
    }

    public void runThread(long startTime) {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Statistic statistic = dbHelper.getOrCreateStatistic(mInstagram.getInstagramAccount().getUsername(), date);

        int finishToRunThread = 0;
        // Neu chay het thoi gian cho phep hoac da chay het so luong thread roi thi dung
        while (System.currentTimeMillis() - startTime < duration * 60 * 1000 || finishToRunThread < 3) {
            //check like per day
            int maxLikes = config.getLikeSpeed(mRobotSetting.getLikesAutoLikeSpeed());
            if (statistic.getNumLikes() >= maxLikes) {
                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": reach to max like (" + maxLikes + ") of this day ");
                break;
            }

            int remainingLike = maxLikes - statistic.getNumLikes();
            // like 10-16 medias in a round
            int numberMediaToLike = Signatures.randomInt((remainingLike < 10 ? remainingLike : 10), (remainingLike < 16 ? remainingLike : 16));

            logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": remaining like " + remainingLike);

            //select like method
            String lastLikeMethod = statistic.getLastLikeMethod();
            String likeMethod = this.getLikeMethod(lastLikeMethod);
            Like like;

            if (Objects.equals(likeMethod, Statistic.LIKE_METHOD_HASHTAG)) {
                logger.debug("Run thread like by hashtag");
                like = new LikeByHashtag(mInstagram, mRobotSetting, dbHelper, config, mAdvanceSetting, statistic, numberMediaToLike);
            } else if (Objects.equals(likeMethod, Statistic.LIKE_METHOD_LOCATION)) {
                logger.debug("Run thread like by location");
                like = new LikeByLocation(mInstagram, mRobotSetting, dbHelper, config, mAdvanceSetting, statistic, numberMediaToLike);
            } else {
                logger.debug("Run thread like by following");
                like = new LikeByFollowing(mInstagram, mRobotSetting, dbHelper, config, mAdvanceSetting, statistic, numberMediaToLike);
            }

            logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": like by " + likeMethod);

            int numMediasLiked = 0;
            try {
                numMediasLiked = like.autoLike();

                dbHelper.increaseNumLikesStatistic(statistic.getId(), numMediasLiked);

                // update last method
                dbHelper.updateLastLikeMethod(statistic.getId(), likeMethod);

//	            statistic.setNumLikes(statistic.getNumLikes() + numMediasLiked);
                statistic.setLastLikeMethod(lastLikeMethod);

                finishToRunThread++;
            } catch (ActionBlockException e) {
                dbHelper.disableAction(BaseAction.ACTION_LIKE, e.getMessage());
                logger.info(buildLog().append(" Stop this action by spam"));
                break;
            } catch (InstagramCheckpointRequiredException e) {
                dbHelper.updateInstagramConnectStatus(false, e.getMessage());
                logger.info(buildLog().append(" Stop this action by conectivity to instagram"));
                break;
            }

            try {
                Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String getLikeMethod(String lastLikeMethod) {
        String likeMethod;

        if (lastLikeMethod == null)
            lastLikeMethod = "";

        switch (lastLikeMethod) {
            case Statistic.LIKE_METHOD_HASHTAG:
                if (mRobotSetting.isLikesLocationAutoLike()) {
                    likeMethod = Statistic.LIKE_METHOD_LOCATION;
                } else if (mRobotSetting.isLikesFollowingAutoLikeFeed()) {
                    likeMethod = Statistic.LIKE_METHOD_FOLLOWING;
                } else {
                    likeMethod = Statistic.LIKE_METHOD_HASHTAG;
                }
                break;
            case Statistic.LIKE_METHOD_LOCATION:
                if (mRobotSetting.isLikesFollowingAutoLikeFeed()) {
                    likeMethod = Statistic.LIKE_METHOD_FOLLOWING;
                } else if (mRobotSetting.isLikesHashtagAutoLike()) {
                    likeMethod = Statistic.LIKE_METHOD_HASHTAG;
                } else {
                    likeMethod = Statistic.LIKE_METHOD_LOCATION;
                }
                break;
            case Statistic.LIKE_METHOD_FOLLOWING:
            default:
                if (mRobotSetting.isLikesHashtagAutoLike()) {
                    likeMethod = Statistic.LIKE_METHOD_HASHTAG;
                } else if (mRobotSetting.isLikesLocationAutoLike()) {
                    likeMethod = Statistic.LIKE_METHOD_LOCATION;
                } else {
                    likeMethod = Statistic.LIKE_METHOD_FOLLOWING;
                }
                break;
        }

        return likeMethod;
    }

    public StringBuilder buildLog() {
        return new StringBuilder("[" + mUserId + "][" + mInstagram.getInstagramAccount().getUsername() + "][like]");
    }
}
