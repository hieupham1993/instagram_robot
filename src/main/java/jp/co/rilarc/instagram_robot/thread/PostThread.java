package jp.co.rilarc.instagram_robot.thread;

import java.sql.Timestamp;
import java.util.Objects;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.handler.ConfigureImageHandler;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.InstagramRequester;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.SchedulePost;
import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by nguyenthanhson on 10/18/17.
 */
public class PostThread extends Thread {
    private Instagram mInstagram;
    final private int mUserId;
    final private static Logger logger = LogManager.getLogger(PostThread.class);
    final private IAPIHelper dbHelper;

    public PostThread(Instagram instagram, RobotSetting robotSetting, IAPIHelper IAPIHelper) {
        mInstagram = instagram;
        mUserId = mInstagram.getInstagramAccount().getUserId();

        dbHelper = IAPIHelper;
    }

    @Override
    public void run() {
        // get post schedule
        SchedulePost schedulePost = dbHelper.getPostSchedule();

        if (schedulePost == null) {
            logger.debug("account "+ mInstagram.getInstagramAccount().getUsername() + ": doesn't have any post schedule");
            return;
        }

        logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": run post schedule id " + schedulePost.getId());


        JSONObject externalMetadata = new JSONObject();
        externalMetadata.put("caption", schedulePost.getCaption());
        if (schedulePost.getLocation() != null)
            externalMetadata.put("location", new JSONObject(schedulePost.getLocation()));

        boolean uploadResult = false;
        JSONObject uploadPhotoResponse = null;
        try {
            uploadPhotoResponse = new InstagramRequester(mInstagram).uploadPhoto(schedulePost.getImagePath(), externalMetadata);
            if (uploadPhotoResponse != null)
                uploadResult = true;
        } catch (InstagramCheckpointRequiredException e) {
            e.printStackTrace();
        }

        ConfigureImageHandler configureImageHandler = new ConfigureImageHandler(uploadPhotoResponse);

        if (uploadResult && Objects.equals(configureImageHandler.getStatus(), "ok")) { // upload successful

            if (schedulePost.isDeleteSchedule()) {
                dbHelper.deletePostSchedule(schedulePost.getId());
                return;
            }

            // update last time upload photo
            schedulePost.setLastestPost(new Timestamp(System.currentTimeMillis()));
            schedulePost.setLinkAfterPost("https://www.instagram.com/p/"+configureImageHandler.getMediaCode());

            if (schedulePost.isPostLoop()) {
                // update next time upload photo
                long nextTimeUpload = schedulePost.getTimeToAction().getTime() + schedulePost.getRepeatTime() * Const.MILISECOND_PER_HOUR;
                schedulePost.setNextTimeUpload(new Timestamp(nextTimeUpload));

                // if this is the last time upload photo
                if (schedulePost.getNextTimeUpload().after(schedulePost.getDayEndSchedule())) {
                    schedulePost.setStatus(SchedulePost.STATUS_FINISH);
                } else {
                    schedulePost.setStatus(SchedulePost.STATUS_RUNNING);
                    schedulePost.setTimeToAction(new Timestamp(nextTimeUpload));
                }
            } else {
                schedulePost.setStatus(SchedulePost.STATUS_FINISH);
            }

            dbHelper.updatePostSchedule(schedulePost);

            logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": upload photo successful");
        } else { // upload failed
            // canceling schedule
            dbHelper.updatePostScheduleStatus(schedulePost.getId(), SchedulePost.STATUS_CANCEL);
            logger.warn("account "+ mInstagram.getInstagramAccount().getUsername() + ": upload photo failed");
        }
    }
    
//    public void runThread() {
//    	// get post schedule
//        SchedulePost schedulePost = dbHelper.getPostSchedule(mUserId);
//
//        if (schedulePost == null) {
//            logger.debug("account "+ mInstagram.getInstagramAccount().getUsername() + ": doesn't have any post schedule");
//            return;
//        }
//
//        logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": run post schedule id " + schedulePost.getId());
//
//
//        JSONObject externalMetadata = new JSONObject();
//        externalMetadata.put("caption", schedulePost.getCaption());
//        if (schedulePost.getLocation() != null)
//            externalMetadata.put("location", new JSONObject(schedulePost.getLocation()));
//
//        boolean uploadResult = false;
//        JSONObject uploadPhotoResponse = null;
//        try {
//            uploadPhotoResponse = new InstagramRequester(mInstagram).uploadPhoto(schedulePost.getImagePath(), externalMetadata);
//            if (uploadPhotoResponse != null)
//                uploadResult = true;
//        } catch (InstagramCheckpointRequiredException e) {
//            e.printStackTrace();
//        }
//
//        ConfigureImageHandler configureImageHandler = new ConfigureImageHandler(uploadPhotoResponse);
//
//        if (uploadResult && Objects.equals(configureImageHandler.getStatus(), "ok")) { // upload successful
//
//            if (schedulePost.isDeleteSchedule()) {
//                dbHelper.deletePostSchedule(schedulePost.getId());
//                return;
//            }
//
//            // update last time upload photo
//            schedulePost.setLastestPost(new Timestamp(System.currentTimeMillis()));
//            schedulePost.setLinkAfterPost("https://www.instagram.com/p/"+configureImageHandler.getMediaCode());
//
//            if (schedulePost.isPostLoop()) {
//                // update next time upload photo
//                long nextTimeUpload = schedulePost.getTimeToPost().getTime() + schedulePost.getRepeatTime() * Const.MILISECOND_PER_HOUR;
//                schedulePost.setNextTimeUpload(new Timestamp(nextTimeUpload));
//
//                // if this is the last time upload photo
//                if (schedulePost.getNextTimeUpload().after(schedulePost.getDayEndSchedule())) {
//                    schedulePost.setStatus(SchedulePost.STATUS_FINISH);
//                } else {
//                    schedulePost.setStatus(SchedulePost.STATUS_RUNNING);
//                    schedulePost.setTimeToPost(new Timestamp(nextTimeUpload));
//                }
//            } else {
//                schedulePost.setStatus(SchedulePost.STATUS_FINISH);
//            }
//
//            dbHelper.updatePostSchedule(schedulePost);
//
//            logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": upload photo successful");
//        } else { // upload failed
//            // canceling schedule
//            dbHelper.updatePostScheduleStatus(schedulePost.getId(), SchedulePost.STATUS_CANCEL);
//            logger.warn("account "+ mInstagram.getInstagramAccount().getUsername() + ": upload photo failed");
//        }
//    }

}
