package jp.co.rilarc.instagram_robot.thread;

import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.unfollow.Unfollow;
import jp.co.rilarc.instagram_robot.unfollow.UnfollowBoost;
import jp.co.rilarc.instagram_robot.unfollow.UnfollowNotFollowingBack;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UnfollowThread extends Thread {
    private Instagram mInstagram;
    final private RobotSetting mRobotSetting;
    final private int mUserId;
    final private static Logger logger = LogManager.getLogger(UnfollowThread.class);
    final private IAPIHelper dbHelper;
    final private Configuration config;
    private AdvanceSetting mAdvanceSetting;
    final private int duration;

    public UnfollowThread(Instagram instagram, RobotSetting robotSetting, AdvanceSetting advanceSetting, IAPIHelper IAPIHelper, int duration) {
        mInstagram = instagram;
        mRobotSetting = robotSetting;
        mUserId = mInstagram.getInstagramAccount().getUserId();
        mAdvanceSetting = advanceSetting;
        this.duration = duration;

        config = new Configuration();
        dbHelper = IAPIHelper;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        try {
        	runThread(startTime);
        }catch(Exception e) {
        	logger.error("Unknown Error: ", e);
        }
    }
    
    public void runThread(long startTime) {
    	String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Statistic statistic = dbHelper.getOrCreateStatistic(mInstagram.getInstagramAccount().getUsername(), date);


        while (System.currentTimeMillis() - startTime < duration * 60 * 1000) {
            //check like per day
            int maxUnfollows = config.getUnfollowSpeed(mRobotSetting.getUnfollowsAutoUnfollowSpeed());
            if (statistic.getNumUnfollows() >= maxUnfollows) {
                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": reach to max unfollow (" + maxUnfollows + ") of this day");
                return;
            }

            int remainingUnFollow = maxUnfollows - statistic.getNumUnfollows();
            // like 10-16 medias in a round
            int numberUsersToUnFollow = Signatures.randomInt((remainingUnFollow < 10 ? remainingUnFollow : 10), (remainingUnFollow < 16 ? remainingUnFollow : 16));

            Unfollow unfollow;

            if (mRobotSetting.isUnfollowsBoostUnfollow()) {
                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": unfollow by boost");

                unfollow = new UnfollowBoost(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToUnFollow);
            } else {
                logger.debug("account " + mInstagram.getInstagramAccount().getUsername() + ": unfollow by check day");

                unfollow = new UnfollowNotFollowingBack(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToUnFollow);
            }

            int unfollowNum = 0;
			try {
                unfollowNum = unfollow.autoUnfollow();
			} catch (ActionBlockException e1) {
				logger.error("account " + mInstagram.getInstagramAccount().getUsername() + ": is block");
			} catch (InstagramCannotAccessException e1) {
				logger.error("account " + mInstagram.getInstagramAccount().getUsername() + " stop this action by conectivity to instagram");
			}

            if (unfollowNum > 0) {
                //update unfollow statistic
                dbHelper.increaseNumUnfollowsStatistic(statistic.getId());
            }

            try {
                Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
