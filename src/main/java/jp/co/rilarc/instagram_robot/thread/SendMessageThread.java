package jp.co.rilarc.instagram_robot.thread;

import jp.co.rilarc.instagram_robot.action.BaseAction;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.InstagramRequester;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;
import jp.co.rilarc.instagram_robot.model.ScheduleSendMessage;

public class SendMessageThread extends Thread {
    private Instagram mInstagram;
    final private int mUserId;
    final private static Logger logger = LogManager.getLogger(SendMessageThread.class);
    final private IAPIHelper dbHelper;

    public SendMessageThread(Instagram instagram, RobotSetting robotSetting, IAPIHelper IAPIHelper) {
        mInstagram = instagram;
        mUserId = mInstagram.getInstagramAccount().getUserId();

        dbHelper = IAPIHelper;
    }

    @Override
    public void run() {
    	try {
        	runThread();
        }catch(Exception e) {
        	logger.error("Unknown Error: ", e);
        }
    }
    
    public void runThread() {
    	// get post schedule
        ScheduleSendMessage scheduleSendMessage = dbHelper.getSendMessageSchedule();

        if (scheduleSendMessage == null) {
            logger.debug("account "+ mInstagram.getInstagramAccount().getUsername() + ": doesn't have any send message schedule");
            return;
        }
        
        // Get Disable Send message status
        boolean isSendMessageDisabled = dbHelper.getDisableSendMessageFlg();
        if(isSendMessageDisabled) {
        	dbHelper.updateSendMessageScheduleStatus(scheduleSendMessage.getId(), ScheduleComment.STATUS_CANCEL);
            logger.warn("send message to user " + scheduleSendMessage.getInstagramName() + " failed because spammed. Account ["+ mInstagram.getInstagramAccount().getUsername() + "]" );
            return;
        }

        logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": run send message schedule id " + scheduleSendMessage.getId());

        JSONObject sendMessageResponse = null;
        try {
            sendMessageResponse = new InstagramRequester(mInstagram).sendMessage(scheduleSendMessage.getInstagramUserId(), scheduleSendMessage.getMessage());
        } catch (ActionBlockException e) {
            dbHelper.disableAction(BaseAction.ACTION_SEND_MESSAGE, e.getMessage());
        	logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": disable send message)");
        }

        if (sendMessageResponse != null) {
            dbHelper.updateSendMessageScheduleStatus(scheduleSendMessage.getId(), ScheduleComment.STATUS_FINISH);
            logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": send message to user " + scheduleSendMessage.getInstagramName() + " successful");
        }
        else {
            dbHelper.updateSendMessageScheduleStatus(scheduleSendMessage.getId(), ScheduleComment.STATUS_CANCEL);
            logger.warn("account "+ mInstagram.getInstagramAccount().getUsername() + ": send message to user " + scheduleSendMessage.getInstagramName() + " failed");
        }
    }
}
