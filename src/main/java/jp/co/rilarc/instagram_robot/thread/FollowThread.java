package jp.co.rilarc.instagram_robot.thread;

import jp.co.rilarc.instagram_robot.action.BaseAction;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.exception.ActionBlockException;
import jp.co.rilarc.instagram_robot.exception.InstagramCannotAccessException;
import jp.co.rilarc.instagram_robot.follow.Follow;
import jp.co.rilarc.instagram_robot.follow.FollowByFollower;
import jp.co.rilarc.instagram_robot.follow.FollowByHashtag;
import jp.co.rilarc.instagram_robot.follow.FollowByLocation;
import jp.co.rilarc.instagram_robot.follow.FollowByRelatedAccount;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Signatures;
import jp.co.rilarc.instagram_robot.model.AdvanceSetting;
import jp.co.rilarc.instagram_robot.model.RelatedAccount;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.Statistic;
import jp.co.rilarc.instagram_robot.utils.Configuration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class FollowThread extends Thread {
    private Instagram mInstagram;
    final private RobotSetting mRobotSetting;
    final private int mUserId;
    final private static Logger logger = LogManager.getLogger(FollowThread.class);
    final private IAPIHelper dbHelper;
    final private Configuration config;
    private AdvanceSetting mAdvanceSetting;
    final private int duration;

    public FollowThread(Instagram instagram, RobotSetting robotSetting, AdvanceSetting advanceSetting, IAPIHelper IAPIHelper, int duration) {
        mInstagram = instagram;
        mRobotSetting = robotSetting;
        mUserId = mInstagram.getInstagramAccount().getUserId();
        mAdvanceSetting = advanceSetting;
        this.duration = duration;

        config = new Configuration();
        dbHelper = IAPIHelper;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        try {
        	runThread(startTime);
        }catch(Exception e) {
        	logger.error("Unknown Error: ", e);
        }
    }
    
    public void runThread(long startTime) {
    	String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        
        Statistic statistic = dbHelper.getOrCreateStatistic(mInstagram.getInstagramAccount().getUsername(), date);

        while (System.currentTimeMillis() - startTime < duration * 60 * 1000) {
            //check like per day
            int maxFollows = config.getFollowSpeed(mRobotSetting.getFollowsAutoFollowSpeed());
            if (statistic.getNumFollowings() >= maxFollows) {
                logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": reach to max follow (" + maxFollows + ") of this day");
                break;
            }

            int remainingFollow = maxFollows - statistic.getNumFollowings();
            // like 10-16 medias in a round
            int numberUsersToFollow = Signatures.randomInt((remainingFollow < 10 ? remainingFollow : 10), (remainingFollow < 16 ? remainingFollow : 16));

            logger.info("account " + mInstagram.getInstagramAccount().getUsername() + ": remaining follow " + remainingFollow);


            Follow follow;
            String followMethod;
            // check follow by related account
            RelatedAccount relatedAccount = dbHelper.getRelatedAccount();
            if (mAdvanceSetting != null && mAdvanceSetting.isActive() && relatedAccount != null) {
                follow = new FollowByRelatedAccount(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, relatedAccount, statistic, numberUsersToFollow);
                followMethod = "related_account";
            } else {
                //select follow method
                String lastFollowMethod = statistic.getLastFollowMethod();
                followMethod = this.getFollowMethod(lastFollowMethod);

                if (Objects.equals(followMethod, Statistic.FOLLOW_METHOD_HASHTAG))
                    follow = new FollowByHashtag(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToFollow);
                else if (Objects.equals(followMethod, Statistic.FOLLOW_METHOD_LOCATION))
                    follow = new FollowByLocation(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToFollow);
                else
                    follow = new FollowByFollower(mInstagram, mRobotSetting, mUserId, dbHelper, mAdvanceSetting, statistic, numberUsersToFollow);
            }

            logger.debug("account " + mInstagram.getInstagramAccount().getUsername() + ": follow by " + followMethod);

            int numFollowings;
			try {
                numFollowings = follow.autoFollow();
				
				dbHelper.increaseNumFollowsStatistic(statistic.getId(), numFollowings);
//	            statistic.setNumFollowings(statistic.getNumFollowings() + numMediasLiked);

	            // update last method
	            dbHelper.updateLastFollowMethod(statistic.getId(), followMethod);
	            statistic.setLastFollowMethod(followMethod);
			} catch (ActionBlockException e) {
				dbHelper.disableAction(BaseAction.ACTION_LIKE, e.getMessage());
				logger.info(buildLog().append(" Stop this action by spam"));
				break;
			} catch (InstagramCannotAccessException e) {
				dbHelper.updateInstagramConnectStatus(false, e.getMessage());
				logger.info(buildLog().append(" Stop this action by conectivity to instagram"));
				break;
			}

            try {
                Thread.sleep(Signatures.randomInt(3, 5) * 60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String getFollowMethod(String lastFollowMethod) {
        String followMethod;

        if (lastFollowMethod == null)
            lastFollowMethod = "";

        switch (lastFollowMethod) {
            case Statistic.FOLLOW_METHOD_HASHTAG:
                if (mRobotSetting.isFollowsLocationAutoFollow()) {
                    followMethod = Statistic.FOLLOW_METHOD_LOCATION;
                } else if (mRobotSetting.isFollowsFollowersAutoFollowBack()) {
                    followMethod = Statistic.FOLLOW_METHOD_FOLLOWER;
                } else {
                    followMethod = Statistic.FOLLOW_METHOD_HASHTAG;
                }
                break;
            case Statistic.FOLLOW_METHOD_LOCATION:
                if (mRobotSetting.isFollowsFollowersAutoFollowBack()) {
                    followMethod = Statistic.FOLLOW_METHOD_FOLLOWER;
                } else if (mRobotSetting.isFollowsHashtagAutoFollow()) {
                    followMethod = Statistic.FOLLOW_METHOD_HASHTAG;
                } else {
                    followMethod = Statistic.FOLLOW_METHOD_LOCATION;
                }
                break;
            case Statistic.FOLLOW_METHOD_FOLLOWER:
            default:
                if (mRobotSetting.isFollowsHashtagAutoFollow()) {
                    followMethod = Statistic.FOLLOW_METHOD_HASHTAG;
                } else if (mRobotSetting.isFollowsLocationAutoFollow()) {
                    followMethod = Statistic.FOLLOW_METHOD_LOCATION;
                } else {
                    followMethod = Statistic.FOLLOW_METHOD_FOLLOWER;
                }
                break;
        }

        return followMethod;
    }
    
    public StringBuilder buildLog() {
		return new StringBuilder("[" + mUserId + "][" + mInstagram.getInstagramAccount().getUsername() + "][follow]");
	}
}
