package jp.co.rilarc.instagram_robot.thread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import jp.co.rilarc.instagram_robot.model.Action;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.utils.Configuration;

public class ActionRotation {
	
	final private static Logger logger = LogManager.getLogger(ActionRotation.class);
	
	private static final int ONE_MINUTE = 1;
	private static final int ONE_HOUR   = 60 * ONE_MINUTE;
	private static final int ONE_DAY    = 24 * ONE_HOUR;
	private static final int NO_RUN     = 0;
	
	private static final int SPEED_ALPHA = 20;
	
	Random rand = new Random();

    final private List<ActionInterval> listActionInterval;
    
    private int likeRunDuration;
    private int followRunDuration;
    private int unfollowRunDuration;
	
	public ActionRotation(RobotSetting robotSetting, Configuration config) {
		listActionInterval = new ArrayList<>();
		reset(robotSetting, config);
	}
	
	public void reset(RobotSetting robotSetting, Configuration mConfig) {
		likeRunDuration = NO_RUN;
		followRunDuration = NO_RUN;
		unfollowRunDuration = NO_RUN;
		
		int likeActionNumber = 0;
		int followActionNumber = 0;
		int unfollowActionNumber = 0;
		
		listActionInterval.clear();
		listActionInterval.add(new ActionInterval(Action.UPLOAD_PHOTO, ONE_MINUTE));
        listActionInterval.add(new ActionInterval(Action.SEND_COMMENT, ONE_MINUTE));
        listActionInterval.add(new ActionInterval(Action.SEND_MESSAGE, ONE_MINUTE));
		
		if(!robotSetting.isDisableLike()) {
			if(robotSetting.isLikesHashtagAutoLike()) {
				likeActionNumber++;
			}
			
			if(robotSetting.isLikesLocationAutoLike()) {
				likeActionNumber++;
			}
			
			if(robotSetting.isLikesFollowingAutoLikeFeed()) {
				likeActionNumber++;
			}
		}
		
		if(!robotSetting.isDisableFollow() && !robotSetting.isUnfollowsBoostUnfollow()) {
			if(robotSetting.isFollowsHashtagAutoFollow()) {
				followActionNumber++;
			}
			
			if(robotSetting.isFollowsLocationAutoFollow()) {
				followActionNumber++;
			}
			
			if(robotSetting.isFollowsFollowersAutoFollowBack()) {
				followActionNumber++;
			}
		}
		
		if(robotSetting.isUnfollowsNotFollowingBackAutoUnfollow()) {
			unfollowActionNumber++;
		}
		
		// 
		int actionNumber = likeActionNumber + followActionNumber + unfollowActionNumber;
		
		// Neu ko co action nao duoc setting thi ko set nua
		if(actionNumber == 0) {
			return;
		}
		
		int realActionDuration = ONE_DAY;
		if(robotSetting.isGlobalSleepMode()) {
			int sleepStart = robotSetting.getGlobalSleepStart();
			int sleepEnd   = robotSetting.getGlobalSleepEnd();
			realActionDuration -= (sleepEnd - sleepStart) * ONE_HOUR;
		}
		
		int durationForOneAction = realActionDuration / actionNumber;
		
		// Tinh toan xem 1 action chay mat bao nhieu thoi gian
		if(likeActionNumber > 0)     likeRunDuration     = durationForOneAction * likeActionNumber;
		if(followActionNumber > 0)   followRunDuration   = durationForOneAction * followActionNumber;
		if(unfollowActionNumber > 0) unfollowRunDuration = durationForOneAction * unfollowActionNumber;
		
        // Random action
        Map<Action, Integer> actionsMap = new HashMap<>();
        if(likeRunDuration     != NO_RUN) actionsMap.put(Action.LIKE, likeRunDuration);
        if(followRunDuration   != NO_RUN) actionsMap.put(Action.FOLLOW, followRunDuration);
        if(unfollowRunDuration != NO_RUN) actionsMap.put(Action.UNFOLLOW, unfollowRunDuration);
        
        if(!actionsMap.isEmpty()) {
        	int interval = ONE_MINUTE;
        	List<Action> actions = new ArrayList<>(actionsMap.keySet());
        	Collections.shuffle(actions);
        	for(Action action : actions) {
        		listActionInterval.add(new ActionInterval(action, interval)); // Khoi dong action dau tien sau 1 phut
        		interval += actionsMap.get(action);
        	}
        }

        Collections.sort(listActionInterval, compareInterval);
        
        logger.info("Action interval = " + listActionInterval);
        logger.info("likeRunDuration = " + likeRunDuration);
        logger.info("followRunDuration = " + followRunDuration);
        logger.info("unfollowRunDuration = " + unfollowRunDuration);
	}
	
	public void removeAction(Action action) {
		int idx = -1;
		for(ActionInterval actionInterval : listActionInterval) {
			idx++;
			if(actionInterval.action == action) {
				break;
			}
		}
		if(idx >= 0) {
			listActionInterval.remove(idx);
		}
	}

	/**
	 * List action 
	 * @return
	 */
    public List<ActionInterval> getListActionInterval() {
		return listActionInterval;
	}

    /**
     * Time (in minute) for run like thread
     * @return	time (in minute)
     */
	public int getLikeRunDuration() {
		return likeRunDuration;
	}

	/**
	 * Time (in minute) for run follow thread
	 * @return	time (in minute)
	 */
	public int getFollowRunDuration() {
		return followRunDuration;
	}

	/**
	 * Time (in minute) for run unfollow thread
	 * @return	time (in minute)
	 */
	public int getUnfollowRunDuration() {
		return unfollowRunDuration;
	}

	/**
     * Điều chỉnh cho tốc độ thay đổi trong khoảng originSpeed - 100 ~ originSpeed
     * @param originSpeed
     * @param random
     * @return
     */
    protected static int randomSpeed(int originSpeed, Random random) {
    	return random.nextInt(SPEED_ALPHA) + (originSpeed - SPEED_ALPHA);
    }
    
    class ActionInterval {
    	Action action;
    	int interval;
    	private int initInterval;
    	
    	ActionInterval(Action action, int interval) {
    		this.action = action;
    		this.interval = interval;
    		this.initInterval = interval;
    	}
    	
    	void resetInterval() {
    		interval = rand.nextInt(initInterval) + 1;	// random from 1 to initInterval 
    	}

		@Override
		public String toString() {
			return "ActionInterval [action=" + action + ", interval=" + interval + ", initInterval=" + initInterval
					+ "]";
		}
    }
    
	/**
	 * VD:
	 * Ban dau: likeInterval: 5, followInterval: 9, unfollowInterval: 10
	 * Reset lan 1: followInterval: 4, likeInterval: 5, unfollowInterval: 5 
	 * Reset lan 2: likeInterval: 1, unfollowInterval: 1, followInterval: 9
	 * Reset lan 3: unfollowInterval: 0, likeInterval: 5, followInterval: 8
	 * Reset lan 4: likeInterval: 5, followInterval: 8, unfollowInterval: 10 
	 */
	protected void resetActionInterval() {
		if(listActionInterval.isEmpty()) {
			return;
		}
		
		ActionInterval firstActionInterval = listActionInterval.get(0);
		for(int i = 1; i < listActionInterval.size(); i++) {
    		ActionInterval actionInterval = listActionInterval.get(i);
    		actionInterval.interval -= firstActionInterval.interval;
    		if(actionInterval.interval < 0) {
    			actionInterval.interval = 0;
    		}
    	}
		firstActionInterval.resetInterval();
		
		Collections.sort(listActionInterval, compareInterval);
	}
	
    static Comparator<ActionInterval> compareInterval = new Comparator<ActionInterval>() {

		@Override
		public int compare(ActionInterval o1, ActionInterval o2) {
			return o1.interval - o2.interval;
		}
	};
}
