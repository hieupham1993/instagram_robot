package jp.co.rilarc.instagram_robot.thread;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import jp.co.rilarc.instagram_robot.exception.InstagramCheckpointRequiredException;
import jp.co.rilarc.instagram_robot.helpers.IAPIHelper;
import jp.co.rilarc.instagram_robot.instagram.Instagram;
import jp.co.rilarc.instagram_robot.instagram.InstagramRequester;
import jp.co.rilarc.instagram_robot.model.RobotSetting;
import jp.co.rilarc.instagram_robot.model.ScheduleComment;

/**
 * Created by nguyenthanhson on 10/28/17.
 */
public class CommentThread extends Thread {
    private Instagram mInstagram;
    final private int mUserId;
    final private static Logger logger = LogManager.getLogger(CommentThread.class);
    final private IAPIHelper dbHelper;

    public CommentThread(Instagram instagram, RobotSetting robotSetting, IAPIHelper IAPIHelper) {
        mInstagram = instagram;
        mUserId = mInstagram.getInstagramAccount().getUserId();

        dbHelper = IAPIHelper;
    }

    @Override
    public void run() {
    	try {
        	runThread();
        }catch(Exception e) {
        	logger.error("Unknown Error: ", e);
        }
    }
    
    public void runThread() {
    	// get post schedule
        ScheduleComment scheduleComment = dbHelper.getCommentSchedule();

        if (scheduleComment == null) {
            logger.debug("account "+ mInstagram.getInstagramAccount().getUsername() + ": doesn't have any commentsetting schedule");
            return;
        }

        logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": run commentsetting schedule id " + scheduleComment.getId());

        JSONObject uploadPhotoResponse = null;
        try {
            uploadPhotoResponse = new InstagramRequester(mInstagram).sendComment(scheduleComment.getMediaId(), scheduleComment.getCommentContent());
        } catch (InstagramCheckpointRequiredException e) {
            e.printStackTrace();
        }

        if (uploadPhotoResponse != null) {
            dbHelper.updateCommentScheduleStatus(scheduleComment.getId(), ScheduleComment.STATUS_FINISH);
            logger.info("account "+ mInstagram.getInstagramAccount().getUsername() + ": send commentsetting successful");
        }
        else {
            dbHelper.updateCommentScheduleStatus(scheduleComment.getId(), ScheduleComment.STATUS_CANCEL);
            logger.warn("account "+ mInstagram.getInstagramAccount().getUsername() + ": send commentsetting failed");
        }
    }
}
