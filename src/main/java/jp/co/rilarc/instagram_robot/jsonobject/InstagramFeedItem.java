package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramFeedItem {

    @SerializedName("media_or_ad")
    private InstagramMediaOrAd mediaOrAd;

    public InstagramMediaOrAd getMediaOrAd() {
        return mediaOrAd;
    }
}
