package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/24/2018.
 */
public class InstagramUserData {

    @SerializedName("pk")
    private long pk;

    @SerializedName("username")
    private String username;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("is_private")
    private boolean isPrivate;

    @SerializedName("profile_pic_url")
    private String profilePicUrl;

    @SerializedName("profile_pic_id")
    private String profilePickId;

    @SerializedName("is_verified")
    private boolean isVerified;

    @SerializedName("has_anonymous_profile_picture")
    private boolean hasAnonymousProfilePicture;

    @SerializedName("reel_auto_archive")
    private String reelAutoArchive;

    @SerializedName("is_favorite")
    private boolean isFavorite;

    public long getPk() {
        return pk;
    }

    public String getUsername() {
        return username;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public String getProfilePickId() {
        return profilePickId;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public boolean isHasAnonymousProfilePicture() {
        return hasAnonymousProfilePicture;
    }

    public String getReelAutoArchive() {
        return reelAutoArchive;
    }

    public boolean isFavorite() {
        return isFavorite;
    }
}
