package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramImageVersions2 {

    @SerializedName("candidates")
    private List<InstagramCandidate> candidates;

    public List<InstagramCandidate> getCandidates() {
        return candidates;
    }
}
