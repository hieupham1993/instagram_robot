package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

import org.apache.http.util.TextUtils;

import java.util.List;

/**
 * Created by HieuPT on 1/24/2018.
 */
public class InstagramFollowListResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("next_max_id")
    private String nextMaxId;

    @SerializedName("page_size")
    private int pageSize;

    @SerializedName("big_list")
    private boolean bigList;

    @SerializedName("users")
    private List<InstagramUserData> users;

    public String getStatus() {
        return status;
    }

    public String getNextMaxId() {
        return nextMaxId;
    }

    public int getPageSize() {
        return pageSize;
    }

    public boolean isBigList() {
        return bigList;
    }

    public List<InstagramUserData> getUsers() {
        return users;
    }

    public boolean hasNextPage() {
        return !TextUtils.isEmpty(nextMaxId);
    }

    public boolean isOk() {
        return "ok".equals(status);
    }
}
