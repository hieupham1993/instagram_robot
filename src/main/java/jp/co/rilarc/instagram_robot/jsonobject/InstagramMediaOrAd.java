package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import jp.co.rilarc.instagram_robot.utils.Const;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramMediaOrAd {

    public static final int MEDIA_TYPE_SINGLE_PIC = 1;

    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final int MEDIA_TYPE_CAROUSEL = 8;

    @SerializedName("taken_at")
    private long takenAt;

    @SerializedName("pk")
    private long pk;

    @SerializedName("id")
    private String id;

    @SerializedName("media_type")
    private int mediaType;

    @SerializedName("code")
    private String code;

    @SerializedName("like_count")
    private int likeCount;

    @SerializedName("comment_count")
    private int commentCount;

    @SerializedName("user")
    private InstagramUserData user;

    @SerializedName("caption")
    private InstagramCaption caption;

    @SerializedName("image_versions2")
    private InstagramImageVersions2 imageVersions2;

    @SerializedName("carousel_media")
    private List<InstagramCarouselMedia> carouselMediaList;

    public long getTakenAt() {
        return takenAt;
    }

    public long getPk() {
        return pk;
    }

    public String getId() {
        return id;
    }

    public int getMediaType() {
        return mediaType;
    }

    public String getCode() {
        return code;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public InstagramUserData getUser() {
        return user;
    }

    public InstagramCaption getCaption() {
        return caption;
    }

    public InstagramImageVersions2 getImageVersions2() {
        return imageVersions2;
    }

    public List<InstagramCarouselMedia> getCarouselMediaList() {
        return carouselMediaList;
    }

    public String getThumbnailUrl() {
        InstagramImageVersions2 images = null;
        switch (mediaType) {
            case MEDIA_TYPE_CAROUSEL:
                if (carouselMediaList != null && !carouselMediaList.isEmpty()) {
                    InstagramCarouselMedia carouselMedia = carouselMediaList.get(0);
                    if (carouselMedia != null) {
                        images = carouselMedia.getImageVersions2();
                    }
                }
                break;
            default:
                images = imageVersions2;
                break;
        }
        if (images != null) {
            List<InstagramCandidate> candidates = images.getCandidates();
            if (candidates != null && !candidates.isEmpty()) {
                InstagramCandidate candidate = candidates.get(0);
                if (candidate != null) {
                    return candidate.getUrl();
                }
            }
        }
        return Const.EMPTY_STRING;
    }

    public String getCaptionText() {
        if (caption != null) {
            return caption.getText();
        }
        return Const.EMPTY_STRING;
    }

    public String getUsername() {
        if (user != null) {
            return user.getUsername();
        }
        return Const.EMPTY_STRING;
    }

    public String getUserProfilePicUrl() {
        if (user != null) {
            return user.getProfilePicUrl();
        }
        return Const.EMPTY_STRING;
    }
}
