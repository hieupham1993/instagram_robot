package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramCaption {

    @SerializedName("text")
    private String text;

    @SerializedName("pk")
    private long pk;

    @SerializedName("user_id")
    private long userId;

    @SerializedName("user")
    private InstagramUserData user;

    public String getText() {
        return text;
    }

    public long getPk() {
        return pk;
    }

    public long getUserId() {
        return userId;
    }

    public InstagramUserData getUser() {
        return user;
    }
}
