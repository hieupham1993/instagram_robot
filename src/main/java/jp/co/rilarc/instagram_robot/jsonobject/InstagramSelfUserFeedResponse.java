package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

import org.apache.http.util.TextUtils;

import java.util.List;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramSelfUserFeedResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("next_max_id")
    private String nextMaxId;

    @SerializedName("items")
    private List<InstagramMediaOrAd> items;

    public String getStatus() {
        return status;
    }

    public String getNextMaxId() {
        return nextMaxId;
    }

    public List<InstagramMediaOrAd> getItems() {
        return items;
    }

    public boolean hasNextPage() {
        return !TextUtils.isEmpty(nextMaxId);
    }

    public boolean isOk() {
        return "ok".equals(status);
    }
}
