package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramCarouselMedia {

    @SerializedName("image_versions2")
    private InstagramImageVersions2 imageVersions2;

    public InstagramImageVersions2 getImageVersions2() {
        return imageVersions2;
    }
}
