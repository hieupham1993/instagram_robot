package jp.co.rilarc.instagram_robot.jsonobject;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/26/2018.
 */
public class InstagramCandidate {

    @SerializedName("width")
    private int width;

    @SerializedName("height")
    private int height;

    @SerializedName("url")
    private String url;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getUrl() {
        return url;
    }
}
