package jp.co.rilarc.instagram_robot.api.requests;

import com.google.gson.annotations.SerializedName;

import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HieuPT on 1/24/2018.
 */
public class WssjAddMessageScheduleRequest {

    @SerializedName("instagram_user_id")
    private List<String> instagramUserIds;

    @SerializedName("description")
    private String description;

    @SerializedName("send_at")
    private String sendAt;

    @SerializedName("delay")
    private int delay;

    public WssjAddMessageScheduleRequest() {
        instagramUserIds = new ArrayList<>();
    }

    public void addInstagramUserId(String userId, String username) {
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(username)) {
            instagramUserIds.add(userId + "{-}" + username);
        }
    }

    public List<String> getInstagramUserIds() {
        return instagramUserIds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}
