package jp.co.rilarc.instagram_robot.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class WssjGetListScheduleMessageResponse extends WssjBaseResponse<WssjGetListScheduleMessageResponse.Data> {

    public static class Data {

        @SerializedName("message")
        private List<Message> messages;

        public List<Message> getMessages() {
            return messages;
        }
    }

    public static class Message {

        @SerializedName("id")
        private int id;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("message")
        private String message;

        @SerializedName("instagram_user_id")
        private String instagramUserId;

        @SerializedName("time_delay")
        private int timeDelay;

        @SerializedName("status")
        private int status;

        @SerializedName("instagram_name")
        private String instagramName;

        @SerializedName("send_at")
        private String sendAt;

        public int getId() {
            return id;
        }

        public int getUserId() {
            return userId;
        }

        public String getMessage() {
            return message;
        }

        public String getInstagramUserId() {
            return instagramUserId;
        }

        public int getTimeDelay() {
            return timeDelay;
        }

        public int getStatus() {
            return status;
        }

        public String getInstagramName() {
            return instagramName;
        }

        public String getSendAt() {
            return sendAt;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
