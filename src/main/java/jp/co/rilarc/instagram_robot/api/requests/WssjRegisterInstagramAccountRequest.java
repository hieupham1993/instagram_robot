package jp.co.rilarc.instagram_robot.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/25/2018.
 */
public class WssjRegisterInstagramAccountRequest {

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("instagram_user_id")
    private long instagramUserId;

    public WssjRegisterInstagramAccountRequest(String username, String password, long instagramUserId) {
        this.username = username;
        this.password = password;
        this.instagramUserId = instagramUserId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public long getInstagramUserId() {
        return instagramUserId;
    }
}
