package jp.co.rilarc.instagram_robot.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/25/2018.
 */
public class WssjCancelMessageRequest {

    @SerializedName("message_id")
    private int messageId;

    public WssjCancelMessageRequest(int messageId) {
        this.messageId = messageId;
    }

    public int getMessageId() {
        return messageId;
    }
}
