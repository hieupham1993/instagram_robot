package jp.co.rilarc.instagram_robot.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class WssjLoginResponse extends WssjBaseResponse<WssjLoginResponse.Data> {

    public static class Data {

        @SerializedName("email")
        private String email;

        @SerializedName("username")
        private String instagramName;

        @SerializedName("instagram_avatar")
        private String instagramAvatar;

        @SerializedName("instagram_password")
        private String instagramPassword;

        @SerializedName("access_token")
        private String accessToken;

        @SerializedName("token_type")
        private String tokenType;

        @SerializedName("id")
        private int id;

        @SerializedName("user_id")
        private long instagramUserId;

        @SerializedName("expires_in")
        private int expireIn;

        public String getEmail() {
            return email;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getTokenType() {
            return tokenType;
        }

        public int getExpireIn() {
            return expireIn;
        }

        public String getInstagramName() {
            return instagramName;
        }

        public String getInstagramAvatar() {
            return instagramAvatar;
        }

        public String getInstagramPassword() {
            return instagramPassword;
        }

        public int getId() {
            return id;
        }

        public long getInstagramUserId() {
            return instagramUserId;
        }
    }
}
