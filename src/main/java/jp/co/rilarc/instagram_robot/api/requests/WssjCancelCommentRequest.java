package jp.co.rilarc.instagram_robot.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 1/25/2018.
 */
public class WssjCancelCommentRequest {

    @SerializedName("comment_id")
    private int commentId;

    public WssjCancelCommentRequest(int messageId) {
        this.commentId = messageId;
    }

    public int getCommentId() {
        return commentId;
    }
}
