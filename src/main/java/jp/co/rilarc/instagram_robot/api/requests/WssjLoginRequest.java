package jp.co.rilarc.instagram_robot.api.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HieuPT on 11/15/2017.
 */
public class WssjLoginRequest {

    @SerializedName("email")
    private final String email;

    @SerializedName("password")
    private final String password;

    public WssjLoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
