package jp.co.rilarc.instagram_robot.api;

import jp.co.rilarc.instagram_robot.api.requests.WssjAddCommentScheduleRequest;
import jp.co.rilarc.instagram_robot.api.requests.WssjAddMessageScheduleRequest;
import jp.co.rilarc.instagram_robot.api.requests.WssjCancelCommentRequest;
import jp.co.rilarc.instagram_robot.api.requests.WssjCancelMessageRequest;
import jp.co.rilarc.instagram_robot.api.requests.WssjLoginRequest;
import jp.co.rilarc.instagram_robot.api.requests.WssjRegisterInstagramAccountRequest;
import jp.co.rilarc.instagram_robot.api.responses.WssjBaseResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleCommentResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjGetListScheduleMessageResponse;
import jp.co.rilarc.instagram_robot.api.responses.WssjLoginResponse;
import jp.co.rilarc.instagram_robot.utils.Const;
import jp.co.rilarc.instagram_robot.utils.RetrofitClient;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by HieuPT on 11/15/2017.
 */
public interface IWssjServices {

    String END_POINT = Const.BASE_WSSJ_URL + "/api/v1/";

    IWssjServices SERVICES = RetrofitClient.getInstance().getService(IWssjServices.END_POINT, IWssjServices.class);

    @POST("login")
    Call<WssjLoginResponse> login(@Body WssjLoginRequest loginInfo);

    @GET("message/get-all")
    Call<WssjGetListScheduleMessageResponse> getListScheduleSendMessage(@Header("Authorization") String token);

    @POST("message/setup-schedule")
    Call<WssjBaseResponse<Object>> addMessageSchedule(@Header("Authorization") String token, @Body WssjAddMessageScheduleRequest requestBody);

    @POST("message/cancel")
    Call<WssjBaseResponse<Object>> cancelMessage(@Header("Authorization") String token, @Body WssjCancelMessageRequest requestBody);

    @POST("message/resume")
    Call<WssjBaseResponse<Object>> reactiveMessage(@Header("Authorization") String token, @Body WssjCancelMessageRequest requestBody);

    @POST("message/delete")
    Call<WssjBaseResponse<Object>> deleteMessage(@Header("Authorization") String token, @Body WssjCancelMessageRequest requestBody);

    @POST("message/delete-all")
    Call<WssjBaseResponse<Object>> deleteAllMessage(@Header("Authorization") String token);

    @GET("comment/get-all")
    Call<WssjGetListScheduleCommentResponse> getListScheduleComment(@Header("Authorization") String token);

    @POST("comment/update")
    Call<WssjBaseResponse<Object>> addCommentSchedule(@Header("Authorization") String token, @Body WssjAddCommentScheduleRequest requestBody);

    @POST("comment/cancel")
    Call<WssjBaseResponse<Object>> cancelComment(@Header("Authorization") String token, @Body WssjCancelCommentRequest requestBody);

    @POST("comment/resume")
    Call<WssjBaseResponse<Object>> reactiveComment(@Header("Authorization") String token, @Body WssjCancelCommentRequest requestBody);

    @POST("comment/delete")
    Call<WssjBaseResponse<Object>> deleteComment(@Header("Authorization") String token, @Body WssjCancelCommentRequest requestBody);

    @POST("comment/delete-all")
    Call<WssjBaseResponse<Object>> deleteAllComment(@Header("Authorization") String token);

    @POST("instagram/account")
    Call<WssjBaseResponse<Object>> registerInstagramAccount(@Header("Authorization") String token, @Body WssjRegisterInstagramAccountRequest requestBody);
}
