package jp.co.rilarc.instagram_robot.api.requests;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HieuPT on 1/24/2018.
 */
public class WssjAddCommentScheduleRequest {

    @SerializedName("posts")
    private List<PostContent> postContents;

    @SerializedName("content_comment")
    private String comment;

    @SerializedName("time_post")
    private String sendAt;

    @SerializedName("delay")
    private int delay;

    public WssjAddCommentScheduleRequest() {
        postContents = new ArrayList<>();
    }

    public void addInstagramUserId(PostContent content) {
        if (content != null) {
            postContents.add(content);
        }
    }

    public List<PostContent> getPostContents() {
        return postContents;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSendAt() {
        return sendAt;
    }

    public void setSendAt(String sendAt) {
        this.sendAt = sendAt;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public static class PostContent {

        @SerializedName("post_id")
        private long postId;

        @SerializedName("post_code")
        private String postCode;

        @SerializedName("post_pic_url")
        private String postPicUrl;

        @SerializedName("post_caption")
        private String postCaption;

        @SerializedName("instagram_username")
        private String username;

        @SerializedName("instagram_profile_pic_url")
        private String userProfilePic;

        public long getPostId() {
            return postId;
        }

        public String getPostPicUrl() {
            return postPicUrl;
        }

        public String getPostCaption() {
            return postCaption;
        }

        public String getUsername() {
            return username;
        }

        public String getUserProfilePic() {
            return userProfilePic;
        }

        public void setPostId(long postId) {
            this.postId = postId;
        }

        public void setPostPicUrl(String postPicUrl) {
            this.postPicUrl = postPicUrl;
        }

        public void setPostCaption(String postCaption) {
            this.postCaption = postCaption;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public void setUserProfilePic(String userProfilePic) {
            this.userProfilePic = userProfilePic;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }
    }
}
