package jp.co.rilarc.instagram_robot.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HieuPT on 11/13/2017.
 */
public class WssjGetListScheduleCommentResponse extends WssjBaseResponse<WssjGetListScheduleCommentResponse.Data> {

    public static class Data {

        @SerializedName("Comments")
        private List<Comment> comments;

        public List<Comment> getComments() {
            return comments;
        }
    }

    public static class Comment {

        @SerializedName("id")
        private int id;

        @SerializedName("user_id")
        private int userId;

        @SerializedName("post_id")
        private long postId;

        @SerializedName("instagram_username")
        private String instagramUsername;

        @SerializedName("instagram_profile_pic_url")
        private String instagramProfilePic;

        @SerializedName("post_caption")
        private String postCaption;

        @SerializedName("image_url")
        private String instagramPostPic;

        @SerializedName("content_comment")
        private String comment;

        @SerializedName("delay")
        private int timeDelay;

        @SerializedName("status")
        private int status;

        @SerializedName("time_post")
        private String sendAt;

        @SerializedName("post_code")
        private String postCode;

        public int getId() {
            return id;
        }

        public int getUserId() {
            return userId;
        }

        public String getInstagramUsername() {
            return instagramUsername;
        }

        public int getTimeDelay() {
            return timeDelay;
        }

        public int getStatus() {
            return status;
        }

        public String getSendAt() {
            return sendAt;
        }

        public String getInstagramProfilePic() {
            return instagramProfilePic;
        }

        public String getInstagramPostPic() {
            return instagramPostPic;
        }

        public String getComment() {
            return comment;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getPostId() {
            return postId;
        }

        public String getPostCaption() {
            return postCaption;
        }

        public String getPostCode() {
            return postCode;
        }
    }
}
