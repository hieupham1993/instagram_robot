## Instagram Robot

# Install java jre 8 to ubuntu 16.04 (For launch)
First, update the package index.
```shell
sudo apt-get update
```

Next, install Java. Specifically, this command will install the Java Runtime Environment (JRE).
```shell
sudo apt-get install default-jre
```

# Run from shell
```shell
java -classpath instagram_robot-1.0.jar jp.co.rilarc.instagram_robot.Robot instagram_user_id
```

# Run from PHP 
```php
$instagram_user_id
$command = 'java -classpath instagram_robot-1.0.jar jp.co.rilarc.instagram_robot.Robot ' . $instagram_user_id;
exec($command);
```

# Build (For Dev)
Install the JDK with the following command:
```shell
sudo apt-get install default-jdk
```

Move to project and run below command for building jar file
```shell
gradle makeJar
```
jar file will gen and copy to build/libs