package com.hieupt.controlsfx;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import com.hieupt.controlsfx.converter.IImageConverter;
import com.hieupt.controlsfx.event.IClickListener;

public class ImageViewTableCell<S, T> extends TableCell<S, T> {

    private final Map<String, Image> imageCaches;

    private final ImageView imageView;

    private IImageConverter<T> converter;

    private IClickListener<T> listener;

    public ImageViewTableCell(IImageConverter<T> converter) {
        this.imageCaches = new HashMap<>();
        this.converter = converter;
        this.imageView = new ImageView();
        imageView.setPreserveRatio(true);
    }

    public void setOnCellClickListener(IClickListener<T> listener) {
        this.listener = listener;
    }

    public void setImageSize(double width, double height) {
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        Node node = null;
        if (!empty && item != null) {
            node = imageView;
            String hash = converter.hash(item);
            Image image = imageCaches.get(hash);
            if (image == null) {
                image = converter.toImage(item);
                imageCaches.put(hash, image);
            }
            imageView.setImage(image);
            if (listener != null) {
                setOnMouseClicked(event -> listener.onClick(item));
            }
        }
        setGraphic(node);
    }
}
