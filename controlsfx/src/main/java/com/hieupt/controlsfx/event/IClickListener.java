package com.hieupt.controlsfx.event;

/**
 * Created by HieuPT on 1/23/2018.
 */
public interface IClickListener<T> {

    void onClick(T item);
}
