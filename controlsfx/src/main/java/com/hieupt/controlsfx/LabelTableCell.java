package com.hieupt.controlsfx;

import java.util.Objects;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.util.StringConverter;

/**
 * Created by HieuPT on 1/23/2018.
 */
public class LabelTableCell<S, T> extends TableCell<S, T> {

    private final Label label;

    private final StringConverter<T> converter;

    public LabelTableCell(StringConverter<T> converter) {
        Objects.requireNonNull(converter, "Converter must not be null");
        label = new Label();
        this.converter = converter;
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        Node node = null;
        if (!empty && item != null) {
            node = label;
            label.setText(converter.toString(item));
        }
        setGraphic(node);
    }

    public void setWrapContent(boolean value) {
        label.setWrapText(value);
    }
}
