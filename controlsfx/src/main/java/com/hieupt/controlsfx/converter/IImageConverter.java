package com.hieupt.controlsfx.converter;

import javafx.scene.image.Image;

/**
 * Created by HieuPT on 1/23/2018.
 */
public interface IImageConverter<T> {

    T fromImage(Image image);

    Image toImage(T object);

    default String hash(T object) {
        return object.toString();
    }
}
