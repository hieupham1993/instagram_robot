package com.hieupt.controlsfx;

import com.hieupt.controlsfx.utils.Constants;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

/**
 * Created by HieuPT on 1/19/2018.
 */
public class EmojiEditor extends AnchorPane implements Initializable {

    @FXML
    private WebView editor_webview;

    private String value;

    public EmojiEditor() {
        FXMLLoader loader = new FXMLLoader(EmojiEditor.class.getResource("/fxml/EmojiEditor.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        WebEngine webEngine = editor_webview.getEngine();
        webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            if (Worker.State.SUCCEEDED.equals(newValue)) {
                JSObject window = (JSObject) webEngine.executeScript("window");
                window.setMember("jsInterface", EmojiEditor.this);
            }
        });
        webEngine.load(getClass().getResource("/emojionearea/editor.html").toExternalForm());
    }

    public void onTextChanged(String text) {
        value = text;
    }

    public String getText() {
        if (value == null) {
            return Constants.EMPTY_STRING;
        }
        return value;
    }
}
