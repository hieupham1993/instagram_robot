package com.hieupt.controlsfx;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import com.hieupt.controlsfx.converter.IImageConverter;
import com.hieupt.controlsfx.event.IClickListener;

public class ImageButtonTableCell<S, T> extends TableCell<S, T> {

    private final Map<String, Image> imageCaches;

    private final Button button;

    private final ImageView imageView;

    private IImageConverter<T> converter;

    private IClickListener<T> listener;

    public ImageButtonTableCell(IImageConverter<T> converter) {
        this.imageCaches = new HashMap<>();
        this.converter = converter;
        this.button = new Button();
        this.imageView = new ImageView();
        setup();
    }

    private void setup() {
        button.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        button.setBorder(Border.EMPTY);
        button.setBackground(Background.EMPTY);
        button.setCursor(Cursor.HAND);
        imageView.setPreserveRatio(true);
    }

    public void setOnButtonClickListener(IClickListener<T> listener) {
        this.listener = listener;
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        Node node = null;
        if (!empty && item != null) {
            String hash = converter.hash(item);
            Image image = imageCaches.get(hash);
            if (image == null) {
                image = converter.toImage(item);
                imageCaches.put(hash, image);
            }
            imageView.setImage(image);
            button.setGraphic(imageView);
            if (listener != null) {
                button.setOnAction(event -> listener.onClick(item));
            }
            node = button;
        }
        setGraphic(node);
    }
}
