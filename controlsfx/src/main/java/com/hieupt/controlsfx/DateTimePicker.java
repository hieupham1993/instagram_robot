package com.hieupt.controlsfx;

import com.hieupt.controlsfx.utils.Constants;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTimePicker;

import org.joda.time.DateTime;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.DateCell;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

/**
 * Created by HieuPT on 1/20/2018.
 */
public class DateTimePicker extends GridPane implements Initializable {

    @FXML
    private JFXDatePicker date_picker;

    @FXML
    private JFXTimePicker time_picker;

    private SimpleDateFormat dateFormat = new SimpleDateFormat();

    private long timeInMillis = -1;

    public DateTimePicker() {
        FXMLLoader loader = new FXMLLoader(EmojiEditor.class.getResource("/fxml/DateTimePicker.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        date_picker.setConverter(new StringConverter<LocalDate>() {

            private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return Constants.EMPTY_STRING;
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        date_picker.setDayCellFactory(param -> new FutureDateCell());
        time_picker.setIs24HourView(true);
        time_picker.setConverter(new StringConverter<LocalTime>() {

            private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("HH:mm");

            @Override
            public String toString(LocalTime time) {
                if (time != null) {
                    return dateFormatter.format(time);
                } else {
                    return Constants.EMPTY_STRING;
                }
            }

            @Override
            public LocalTime fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalTime.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        date_picker.setOnAction(event -> reCalculateTime());
        time_picker.setOnAction(event -> reCalculateTime());
        date_picker.setValue(LocalDate.now());
        time_picker.setValue(LocalTime.now());
        reCalculateTime();
    }

    private void reCalculateTime() {
        LocalDate date = date_picker.getValue();
        LocalTime time = time_picker.getValue();
        if (date != null && time != null) {
            DateTime dateTime = DateTime.now();
            dateTime = dateTime.withYear(date.getYear());
            dateTime = dateTime.withMonthOfYear(date.getMonthValue());
            dateTime = dateTime.withDayOfMonth(date.getDayOfMonth());
            dateTime = dateTime.withHourOfDay(time.getHour());
            dateTime = dateTime.withMinuteOfHour(time.getMinute());
            dateTime = dateTime.withSecondOfMinute(0);
            dateTime = dateTime.withMillisOfSecond(0);
            timeInMillis = dateTime.getMillis();
        } else {
            timeInMillis = -1;
        }
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    public String getTime(String pattern) {
        if (timeInMillis >= 0) {
            dateFormat.applyPattern(pattern);
            return dateFormat.format(new Date(timeInMillis));
        }
        return Constants.EMPTY_STRING;
    }

    private static class FutureDateCell extends DateCell {

        @Override
        public void updateItem(LocalDate item, boolean empty) {
            super.updateItem(item, empty);
            setDisable(item.isBefore(LocalDate.now()));
        }
    }
}
