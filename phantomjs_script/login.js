var sys = require("system"),
    page = require('webpage').create();
page.viewportSize = { width:1024 , height: 600 };
//page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36';
page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Safari/604.1.38';

var username = sys.args[1];
var password = sys.args[2];

function isLoggedIn() {
	var myPageIcon = page.evaluate(function() {
	    return document.getElementsByClassName('coreSpriteDesktopNavProfile');
	});
	if (myPageIcon.length == 1) {
		return true;
	}
	
	return false;
}

//phantom.clearCookies();

var timeToLoad = 10000;
var timeLogin = timeToLoad + 1000;
var timeToCheck = timeLogin + 10000;


// Step1: Load page
setTimeout(function() {
	page.open('https:/instagram.com/accounts/login/');
}, 0);


// Step2: Check if already login (in case cookie is valid)
setTimeout(function() {
//	page.render('check_login.png');
	var loggedin = isLoggedIn();
	if (loggedin) {
		console.log('true');
		phantom.exit(1);
	}
}, timeToLoad);

// Step3: if not login then login
setTimeout(function() {
	//page.render('before_login.png');
	var ig = page.evaluate(function() {
		function getCoords(box) {
			return  {
				x: box.left,
			  y: box.top 
			};
		}	

		function getPosition(type, name) {
			// find fields to fill
			var input = document.getElementsByTagName(type);
			for(var i = 0; i < input.length; i++) {
				if(name && input[i].name == name)     return getCoords(input[i].getBoundingClientRect());
				else if(!name && input[i].className)  return getCoords(input[i].getBoundingClientRect()); // this is for login button
			}
		}
		return {
			user: getPosition('input', 'username'),
			pass: getPosition('input', 'password'),
			login: getPosition('button')
		};		
	});

	// fill in data and press login
	page.sendEvent('click',ig.user.x, ig.user.y);
	page.sendEvent('keypress', username);

	page.sendEvent('click',ig.pass.x, ig.pass.y);
	page.sendEvent('keypress', password);
	page.sendEvent('click',ig.login.x, ig.login.y);
}, timeLogin);

// Step4: Check once more time
setTimeout(function() {
	
	var loggedin = isLoggedIn();
	if (loggedin) {
		console.log('true');
	}else {
		console.log('false');
		//page.render('captures/' + username + '_login_failed.png');
	}
	phantom.exit(1);
}, timeToCheck);