/**
 * Truoc khi like thi chay lenh duoi day
 * phantomjs --cookies-file=cookie.txt login_like.js 
 * de login vao instagram
 * Sau do chi thi chay
 * phantomjs --cookies-file=cookie.txt login_likes.js 
 */
var sys = require("system"),
    page = require('webpage').create();
page.viewportSize = { width:1024 , height: 600 };
//page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36';
page.settings.userAgent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';

if (sys.args.length < 0) {
	console.log('false');
	phantom.exit(1);
}

var postId = sys.args[1];

function formatDate(date) {
    var year = date.getFullYear(),
        month = date.getMonth() + 1, // months are zero indexed
        day = date.getDate(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        second = date.getSeconds(),
        hourFormatted = hour % 12 || 12, // hour returned in 24 hour format
        minuteFormatted = minute < 10 ? "0" + minute : minute,
        morning = hour < 12 ? "am" : "pm";

    return year + "" + month + "" + day + "_" + hour + "" + minute + "" + second;
}

var timeToLike = 8000,
	timeToReload = timeToLike + 5000,
	timeToCheck = timeToReload + 5000,
	timeToCapture = timeToCheck + 35000;

var result = false;

setTimeout(function() {
	page.open('https://www.instagram.com/p/' + postId);
}, 0);
		
setTimeout(function() {
	// page.render('captures/before_like.png');
	var likeButton = page.evaluate(function() {
	    return document.getElementsByClassName('coreSpriteHeartOpen');
	});
	if (likeButton.length == 1) { // found like button
	    // click like
	    page.evaluate(function() {
            document.getElementsByClassName('coreSpriteHeartOpen')[0].click();
        });
	} else {
	    console.log('false');
	    // page.render('captures/' + formatDate(new Date()) + '_like_failed1.png');
        phantom.exit(1);
	}
}, timeToLike);

// check like result
setTimeout(function() {
	// page.render('captures/' + formatDate(new Date()) + '_before_reload.png');
	page.reload();
}, timeToReload);

setTimeout(function() {
    var dislikeButton = page.evaluate(function() {
        return document.getElementsByClassName('coreSpriteHeartFull');
    });

    if (dislikeButton.length == 1) { // found dislike button
        console.log('true');
    } else {
        console.log('false');
        // page.render('captures/' + formatDate(new Date()) + '_like_failed2.png');
    }
    phantom.exit(1);
}, timeToCheck);
