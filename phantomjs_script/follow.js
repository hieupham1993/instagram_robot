/**
 * Truoc khi like thi chay lenh duoi day
 * phantomjs --cookies-file=cookie.txt login_like.js
 * de login vao instagram
 * Sau do chi thi chay
 * phantomjs --cookies-file=cookie.txt login_likes.js
 */
var sys = require("system"),
    page = require('webpage').create();
page.viewportSize = { width:1024 , height: 600 };
//page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.82 Safari/537.36';
//page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Safari/604.1.38';

if (sys.args.length < 0) {
	console.log('false');
	phantom.exit(1);
}

var username = sys.args[1];

var timeToLike = 8000,
	timeToReload = timeToLike + 12000,
	timeToCheck = timeToReload + 5000,
	timeToCapture = timeToCheck + 30000;

var result = false;

setTimeout(function() {
	page.open('https://www.instagram.com/' + username + '/');
}, 0);

setTimeout(function() {
//	page.render('before_follow.png');
	var likeButton = page.evaluate(function() {
	    return document.getElementsByClassName('_qv64e _gexxb _r9b8f _njrw0');
	});
	if (likeButton.length == 1) { // found like button
	    // click like
	    page.evaluate(function() {
            document.getElementsByClassName('_qv64e _gexxb _r9b8f _njrw0')[0].click();
        });
	} else {
	    console.log('false');
        phantom.exit(1);
	}
}, timeToLike);

// check like result
setTimeout(function() {
	page.reload();
}, timeToReload);

setTimeout(function() {
//	page.render('after_follow.png');

    var dislikeButton = page.evaluate(function() {
        return document.getElementsByClassName('_qv64e _t78yp _r9b8f _njrw0');
    });

    if (dislikeButton.length == 1) { // found dislike button
        console.log('true');
    } else {
        console.log('false');
    }
    phantom.exit(1);
}, timeToCheck);

//check like result
//setTimeout(function() {
//    page.render('last_check3.png');
//    phantom.exit(1);
//}, timeToCapture);


//setTimeout(function() {
//	page.render('like_ok.png');
////	phantom.exit(1);
//}, 10000);
//
//setTimeout(function() {
//	page.render('last_check1.png');
////	phantom.exit(1);
//}, 10000);
//
//setTimeout(function() {
//	page.render('last_check2.png');
//	console.log('true');
//	phantom.exit(1);
//}, 20000);